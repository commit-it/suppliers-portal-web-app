<?php

use Cake\Core\Configure;

/*
 * Default Date format
 */
Configure::write('date-format', array(
    'default' => 'd-M-Y H:i',
    'd-M-Y' => 'd-M-Y',
    'd-M-y' => 'd-M-y',
    'd/m/Y-H-i-s-A' => 'd/m/Y H:i:s A',
    'Y-m-d-H-i-s' => 'Y-m-d H:i:s',
    'db-date' => 'Y-m-d'
));

/*
 * Connection type
 */
Configure::write('connection-type', 'sftp');

/*
 * Download file size
 */
Configure::write('download-file-size', 8388608);

/**
 * Login Url
 */
Configure::write('login-url', 'http://app.suppliersportal.com.au/');

/**
 * Decimal Number places
 */
Configure::write('number-format-settings', array(
    'places' => 2
));

/*
 * set sftp connections details
 */
Configure::write('sftp.port', 22);
//if (gethostname() == 'app') {
//    Configure::write('sftp.DNS', 'ftp.axistest.ventyx.com');
//} else {
//    Configure::write('sftp.DNS', 'ftp.axistest.ventyx.com');
//}
Configure::write('sftp.DNS', 'ftp.axiscloudplatform.com');
Configure::write('sftp.username', '101000322038_001');
Configure::write('sftp.password', '8aQEswEp');
//Configure::write('sftp.password', '');
//SFTP Username - 101000322038_001    SFTP Password  - 8aQEswEp
/*
 * set Purchase Order Path (po means purchase order)
 */
Configure::write('po.remotePath', 'out');
Configure::write('po.localPath', ROOT . DS . 'webroot' . DS . 'upload' . DS . 'PurchaseOrder' . DS . 'In');
Configure::write('po.localPath-out', ROOT . DS . 'webroot' . DS . 'upload' . DS . 'PurchaseOrder' . DS . 'Out');
Configure::write('po.file-type-multShipOrd', 'MultiShipmentOrder');
Configure::write('po.file-type-ack', 'ApplicationReceiptAcknowledgement');
Configure::write('po.localPath-ack', ROOT . DS . 'webroot' . DS . 'upload' . DS . 'ApplicationReceiptAcknowledgement' . DS . 'In');
Configure::write('po.localPath-ack-out', ROOT . DS . 'webroot' . DS . 'upload' . DS . 'ApplicationReceiptAcknowledgement' . DS . 'Out');
Configure::write('po.pdf-localPath', ROOT . DS . 'webroot' . DS . 'upload' . DS . 'PurchaseOrder' . DS . 'PDF');

/*
 * Set Delivery Docket File path
 */
Configure::write('dd.localPath', ROOT . DS . 'webroot' . DS . 'upload' . DS . 'DeliveryDocket');

/*
 * Set Booking Sheet File path
 */
Configure::write('booking-sheet.localPath', ROOT . DS . 'webroot' . DS . 'upload' . DS . 'BookingSheet');
Configure::write('booking-sheet.email', ROOT . DS . 'webroot' . DS . 'upload' . DS . 'BookingSheet' . DS . 'Email');

//set Purchase Order languageISOCode
Configure::write('po.languageISOCode', 'EN');

/*
 * set Invoice Path
 */
Configure::write('invoice.remotePath', 'in');
Configure::write('invoice.localPath', ROOT . DS . 'webroot' . DS . 'upload' . DS . 'Invoice' . DS . 'In');
Configure::write('invoice.localPath-out', ROOT . DS . 'webroot' . DS . 'upload' . DS . 'Invoice' . DS . 'Out');
Configure::write('invoice.file-prefix', 'In_');
Configure::write('invoice.address-delimiter', ' ');
Configure::write('invoice.address-orders', 3);
Configure::write('invoice.state-order-from-end', 2);
Configure::write('invoice.postalCode-order-from-end', 1);
// This fixed per by bunnings
Configure::write('invoice.gst-per', 10);
Configure::write('invoice.update_to_quick_book', array(
    'Yes' => 'yes',
    'No' => 'no'
));

/*
 * set Fulfilment Advice Path
 */
Configure::write('fulfilment_advice.remotePath', 'in');
Configure::write('fulfilment_advice.localPath', ROOT . DS . 'webroot' . DS . 'upload' . DS . 'Fulfilment_Advice' . DS . 'In');
Configure::write('fulfilment_advice.localPath-out', ROOT . DS . 'webroot' . DS . 'upload' . DS . 'Fulfilment_Advice' . DS . 'Out');
Configure::write('fulfilment_advice.file-prefix', 'FA_');
Configure::write('fulfilment_advice.address-delimiter', ' ');
Configure::write('fulfilment_advice.address-orders', 3);
Configure::write('fulfilment_advice.state-order-from-end', 2);
Configure::write('fulfilment_advice.postalCode-order-from-end', 1);

/*
 * set bunning user types
 */
Configure::write('bunning_user', array(
    'Buyer' => 'Buyer',
    'Payer' => 'Payer'
));

/*
 * set myob purchase order status
 */
Configure::write('myob_po_status', array(
    'ready_for_myob' => '1',
    'ready_for_portal' => '2'
));

/*
 * set Scan Book details
 */
Configure::write('scan-book.file-path', ROOT . DS . 'webroot' . DS . 'upload' . DS . 'ScanBook');
Configure::write('scan-book.view-path', DS . 'webroot' . DS . 'upload' . DS . 'ScanBook' . DS);
Configure::write('scan-book.file-prefix', 'scan_book_');
Configure::write('scan-book.email-subject', 'Scan Book');
Configure::write('scan-book.email-profile', 'supplier-portal');
Configure::write('scan-book.email-template', 'scanbook');
Configure::write('scan-book.email-layout', 'scanbook');

/*
 * set Booking Sheet Email details
 */
Configure::write('booking-sheet.email-subject', 'Booking Sheet');
Configure::write('booking-sheet.email-profile', 'supplier-portal');
Configure::write('booking-sheet.email-template', 'bookingsheet');
Configure::write('booking-sheet.email-layout', 'bookingsheet');

/*
 * set Forget Password Email details
 */
Configure::write('forget-password.email-subject', 'New Password');
Configure::write('forget-password.email-profile', 'supplier-portal');
Configure::write('forget-password.email-template', 'forgetpassword');
Configure::write('forget-password.email-layout', 'forgetpassword');


/*
 * set Purchase Order Notification Email details
 */
Configure::write('purchase-order.email-subject', 'New Purchase Orders');
Configure::write('purchase-order.email-profile', 'supplier-portal');
Configure::write('purchase-order.email-template', 'purchaseorder');
Configure::write('purchase-order.email-layout', 'purchaseorder');

/*
 * set invoice generated or not value
 */
Configure::write('invoice_generated', array(
    'yes' => 'yes',
    'no' => 'no'
));

/*
 *  purchase order fulfilment advice sent to bunnings or not
 */
Configure::write('send_fulfilment_advice_to_bunnings', array(
    'yes' => 'yes',
    'no' => 'no'
));

/*
 *  purchase order sent to myob or not value
 */
Configure::write('sent_to_myob', array(
    'yes' => 'yes',
    'no' => 'no'
));

/*
 *  purchase order sent to quick book or not value
 */
Configure::write('sent_to_quick_book', array(
    'yes' => 'yes',
    'no' => 'no'
));

/*
 * Master Items
 */
Configure::write('master-items', array(
    'localPath' => ROOT . DS . 'webroot' . DS . 'upload' . DS . 'MasterItems' . DS . 'In',
    'localPath-out' => ROOT . DS . 'webroot' . DS . 'upload' . DS . 'MasterItems' . DS . 'Out',
    'localPath-export-to-excel' => ROOT . DS . 'webroot' . DS . 'upload' . DS . 'MasterItems' . DS . 'ExportToExcel',
    'localPath-export-to-excel-for-open' => DS . 'upload' . DS . 'MasterItems' . DS . 'ExportToExcel',
    'file-prefix' => 'master_items_',
    'columns' => array(
        'Fine Line' => 'scan_barcode',
        'Bar Code' => 'bar_code',
        'Pot Size' => 'pot_size',
        'Name' => 'name',
        'Description' => 'description',
        'Genus' => 'genus',
        'Species' => 'species',
        'Cultival' => 'cultival',
        'Cost Price' => 'retail_price'
    ),
    'row_start_cnt_for_import' => 1, // first 0 for rows is empty for description & 1 row is for column name
    'column_name_row_number' => 1 // 1 for column name & 0 for first empty row
));

/*
 * Bunning Stores
 */
Configure::write('bunning-stores', array(
    'localPath' => ROOT . DS . 'webroot' . DS . 'upload' . DS . 'BunningStores' . DS . 'In',
    'localPath-out' => ROOT . DS . 'webroot' . DS . 'upload' . DS . 'BunningStores' . DS . 'Out',
    'file-prefix' => 'bunning_stores_',
    'columns' => array(
        'Name' => 'name',
        'Email' => 'email',
        'Gln' => 'gln',
        'Address' => 'address',
        'City' => 'city',
        'Country Iso Code' => 'country_iso_code',
        'Language Iso Code' => 'language_iso_code',
        'Postal Code' => 'postal_code',
        'State' => 'state',
        'Company Registration Number' => 'company_registration_number'
    ),
    'row_start_cnt_for_import' => 1,
    'column_name_row_number' => 1
));

/*
 *  Array of file_type for fileoverview
 */
Configure::write('file_type', array(
    'Purchase Order' => 'Purchase Order',
    'Application Receipt Acknowledgement' => 'Invoice Acknowledgement'
));

/*
 * Array of Invoice Acknowledgement file Statuses
 */
Configure::write('invoice_ack_invoice_status', array(
    'RECEIVED' => 'RECEIVED',
    'ERROR' => 'ERROR'
));

/*
 * Array of Fulfilment Advice Acknowledgement file Statuses
 */
Configure::write('fulfilment_advice_ack_status', array(
    'RECEIVED' => 'RECEIVED',
    'ERROR' => 'ERROR'
));

/*
 *  Array of Application_Receipt folder
 */
Configure::write('Application_Receipt', array(
    'In_Path' => ROOT . DS . 'webroot' . DS . 'upload' . DS . 'Application_Receipt' . DS . 'In',
    'Out_Path' => ROOT . DS . 'webroot' . DS . 'upload' . DS . 'Application_Receipt' . DS . 'Out',
    'Remote_Path' => 'in',
    'File_Prefix' => 'Ack_',
    'document_acknowledged_type' => array(
        'Purchase_Order' => 34,
        'Invoice' => 61,
        'Fulfilment_Advice' => 15
    )
));

/*
 * Invoice delivery_docket_generated or not value
 */
Configure::write('delivery_docket_generated', array(
    'yes' => 'yes',
    'no' => 'no'
));

/**
 *  Set path fot courier_logo_image_path
 */
Configure::write('courier_logo_image_path', ROOT . DS . 'webroot' . DS . 'upload' . DS . 'courier_logo');

/**
 *  Set path fot uploaded_courier_logo_image_path for view
 */
Configure::write('uploaded_courier_logo_image_path', DS . 'upload' . DS . 'courier_logo' . DS);

/*
 * Set Courier logo size
 */
Configure::write('booking_sheet_generated', array(
    'yes' => 'yes',
    'no' => 'no'
));

/*
 * Invoice booking_sheet_generated or not value
 */
Configure::write('courier', array(
    'logo_height' => '90',
    'logo_width' => '400',
));

/**
 *  Set host name
 */
$_SERVER['REQUEST_SCHEME'] = isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : '';
$_SERVER['HTTP_HOST'] = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
Configure::write('host_name', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST']);

/*
 * set country_iso_code
 */
Configure::write('country_iso_code', array(
    'AU' => 'Australia',
    'NZ' => 'New Zealand'
));

/*
 * Set Language ISO Code
 */
Configure::write('language_iso_code', 'EN');

/*
 * set state
 */
Configure::write('state', array(
    'ACT' => 'ACT',
    'NSW' => 'NSW',
    'NT' => 'NT',
    'QLD' => 'QLD',
    'SA' => 'SA',
    'TAS' => 'TAS',
    'VIC' => 'VIC',
    'WA' => 'WA',
));

// invoice type
Configure::write('invoice_type', array(
    'AGREED_TERMS' => 'AGREED_TERMS',
    'CORRECTED_INVOICE' => 'CORRECTED_INVOICE',
    'CREDIT_NOTE' => 'CREDIT_NOTE',
    'DEBIT_NOTE' => 'DEBIT_NOTE',
    'INVOICE' => 'INVOICE',
    'PRO_FORMA_INVOICE' => 'PRO_FORMA_INVOICE',
    'SELF_BILLED_INVOICE' => 'SELF_BILLED_INVOICE',
    'TAX_INVOICE' => 'TAX_INVOICE'
));

// set for resend_invoice
Configure::write('resend_invoice', array(
    'no' => 0,
    'yes' => 1
));

// set for update_for_myob
Configure::write('update_for_myob', array(
    'no' => 0,
    'yes' => 1
));

// set for payment_status
Configure::write('payment_status', array(
    'main' => array(
        'trial' => 'trial',
        'paid' => 'paid',
        'cancel' => 'cancel'
    ),
    'display' => array(
        'trial' => 'Trial',
        'paid' => 'Paid',
        'cancel' => 'Cancel'
    )
));

// set for access_status
Configure::write('access_status', array(
    'main' => array(
        'active' => 'active',
        'inactive' => 'inactive'
    ),
    'display' => array(
        'active' => 'Active',
        'inactive' => 'Inactive'
    )
));

// set months
Configure::write('month', array(
    '01' => 'January',
    '02' => 'February',
    '03' => 'March',
    '04' => 'April',
    '05' => 'May',
    '06' => 'June',
    '07' => 'July',
    '08' => 'August',
    '09' => 'September',
    '10' => 'October',
    '11' => 'November',
    '12' => 'December'
));

Configure::write('Stripe.mode', 'Test');
Configure::write('Stripe.Test_secret_key', 'sk_test_bty2HdsMjTIdGKVJtXgskvUZ');
Configure::write('Stripe.Test_publishable_key', 'pk_test_xvd3tdq5ogK5IaNAMoQSGB87');
Configure::write('Stripe.Live_secret_key', 'sk_live_nQ5P21hgMQ6vlYhbbuyKjtni');
Configure::write('Stripe.Live_publishable_key', 'pk_live_wZgaLizWwLYyayqjba7nzaGa');
Configure::write('Stripe.currency', 'usd');
Configure::write('Stripe.packages', array(
    'large' => array(
        'plan_id' => 'large_plan_sp',
        'amount' => 800,
        'name' => 'LARGE',
        'description' => '($8.00)'
    ),
    'medium' => array(
        'plan_id' => 'medium_plan_sp',
        'amount' => 200,
        'name' => 'MEDIUM',
        'description' => '($2.00)'
    ),
    'small' => array(
        'plan_id' => 'small_plan_sp',
        'amount' => 100,
        'name' => 'SMALL',
        'description' => '($1.00)'
    )
));

Configure::write('Stripe.packages_names', array(
    'large_plan_sp' => 'LARGE',
    'medium_plan_sp' => 'MEDIUM',
    'small_plan_sp' => 'SMALL'
        )
);
Configure::write('payment.status.active', 'active');

/**
 * Stripe responses
 */
Configure::write('stripe_response', array(
    'payment_succeeded' => 'invoice.payment_succeeded',
    'payment_failed' => 'invoice.payment_failed',
    'subscription_deleted' => 'customer.subscription.deleted',
));

/**
 *  is_myob status in process_logs table
 */
Configure::write('process_logs', array(
    'is_myob_active' => 1,
    'is_myob_inactive' => 0
));

/**
 * set default time to auto refresh page
 */
Configure::write('auto_refresh_page_time', 180000);

/**
 * set invoice_generate_by array
 */
Configure::write('invoice_generate_by', array(
    'MYOB' => 'MYOB',
    'Quick-Books' => 'Quick-Books'
));

/**
 *  Set path fot seller_business_logo_image_path
 */
Configure::write('seller_business_logo_image_path', ROOT . DS . 'webroot' . DS . 'upload' . DS . 'seller_business_logo');

/**
 *  Set path fot uploaded_seller_business_logo_image_path for view
 */
Configure::write('uploaded_seller_business_logo_image_path', DS . 'upload' . DS . 'seller_business_logo' . DS);

/**
 * seller_business logo settings
 */
Configure::write('seller_business', array(
    'logo_height' => '90',
    'logo_width' => '400',
));

/*
 * set is_printed_delivery_docket yes or no value
 */
Configure::write('is_printed_delivery_docket', array(
    'yes' => 1,
    'no' => 0
));

/**
 * Email Thanks
 */
Configure::write('email.thanks', 'Suppliers Portal');

/**
 * Quick-Book default payment term
 */
Configure::write('quick_book_default_payment_term', 'Net 60');

/*
 * set Delivery Docket Number update Notification Email details
 */
Configure::write('delivery-docket-number-update.email-subject', 'Delivery Docket Number Updated');
Configure::write('delivery-docket-number-update.email-profile', 'supplier-portal');
Configure::write('delivery-docket-number-update.email-template', 'deliverydocketnumberupdate');
Configure::write('delivery-docket-number-update.email-layout', 'deliverydocketnumberupdate');

/**
 * Set PO Archived details
 */
Configure::write('po_archived', array(
    'yes' => 'yes',
    'no' => 'no'
));

/**
 * Set invoice send to Bunnings Notification  Reminder Email
 */
Configure::write('invoice-send-bunning-reminder.email-subject', 'Send Pending Invoices To Bunning');
Configure::write('invoice-send-bunning-reminder.email-profile', 'supplier-portal');
Configure::write('invoice-send-bunning-reminder.email-template', 'sendinvoicetobunningreminder');
Configure::write('invoice-send-bunning-reminder.email-layout', 'sendinvoicetobunningreminder');

/**
 * Define hours
 */
Configure::write('hours', array(
    1 => 1,
    2 => 2,
    3 => 3,
    4 => 4,
    5=> 5,
    6 => 6,
    7 => 7,
    8 => 8,
    9 => 9,
    10 => 10,
    11 => 11,
    12 => 12,
    13 => 13,
    14 => 14,
    15 => 15,
    16 => 16,
    17 => 17,
    18 => 18,
    19 => 19,
    20 => 20,
    21 => 21,
    22 => 22,
    23 => 23,
));
