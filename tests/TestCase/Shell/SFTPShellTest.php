<?php
namespace App\Test\TestCase\Shell;

use App\Shell\SFTPShell;
use Cake\TestSuite\TestCase;

/**
 * App\Shell\SFTPShell Test Case
 */
class SFTPShellTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->io = $this->getMock('Cake\Console\ConsoleIo');
        $this->SFTP = new SFTPShell($this->io);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SFTP);

        parent::tearDown();
    }

    /**
     * Test main method
     *
     * @return void
     */
    public function testMain()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
