<?php
namespace App\Test\TestCase\Shell;

use App\Shell\SupplierPortalShell;
use Cake\TestSuite\TestCase;

/**
 * App\Shell\SupplierPortalShell Test Case
 */
class SupplierPortalShellTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->io = $this->getMock('Cake\Console\ConsoleIo');
        $this->SupplierPortal = new SupplierPortalShell($this->io);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SupplierPortal);

        parent::tearDown();
    }

    /**
     * Test main method
     *
     * @return void
     */
    public function testMain()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
