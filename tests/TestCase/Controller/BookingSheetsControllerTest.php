<?php
namespace App\Test\TestCase\Controller;

use App\Controller\BookingSheetsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\BookingSheetsController Test Case
 */
class BookingSheetsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'BookingSheets' => 'app.booking_sheets',
        'Users' => 'app.users',
        'UserRoles' => 'app.user_roles',
        'BunningStores' => 'app.bunning_stores',
        'ScanBooks' => 'app.scan_books',
        'Invoices' => 'app.invoices',
        'PurchaseOrders' => 'app.purchase_orders',
        'Currencies' => 'app.currencies',
        'PurchaseOrderStatuses' => 'app.purchase_order_statuses',
        'InvoiceItems' => 'app.invoice_items',
        'PurchaseOrderItems' => 'app.purchase_order_items',
        'DeliveryDockets' => 'app.delivery_dockets',
        'ProcessLogs' => 'app.process_logs',
        'MasterItems' => 'app.master_items',
        'BookingSheetItems' => 'app.booking_sheet_items'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
