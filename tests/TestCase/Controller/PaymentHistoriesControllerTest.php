<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PaymentHistoriesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\PaymentHistoriesController Test Case
 */
class PaymentHistoriesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'PaymentHistories' => 'app.payment_histories',
        'Payments' => 'app.payments',
        'Users' => 'app.users',
        'UserRoles' => 'app.user_roles',
        'BunningStores' => 'app.bunning_stores',
        'ScanBooks' => 'app.scan_books',
        'Invoices' => 'app.invoices',
        'PurchaseOrders' => 'app.purchase_orders',
        'Currencies' => 'app.currencies',
        'PurchaseOrderStatuses' => 'app.purchase_order_statuses',
        'InvoiceItems' => 'app.invoice_items',
        'PurchaseOrderItems' => 'app.purchase_order_items',
        'DeliveryDockets' => 'app.delivery_dockets',
        'BookingSheetItems' => 'app.booking_sheet_items',
        'BookingSheets' => 'app.booking_sheets',
        'FileOverviewes' => 'app.file_overviewes',
        'FileTypes' => 'app.file_types',
        'FileStatuses' => 'app.file_statuses',
        'ProcessLogs' => 'app.process_logs',
        'MasterItems' => 'app.master_items',
        'Statistics' => 'app.statistics',
        'Customers' => 'app.customers',
        'Plans' => 'app.plans'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
