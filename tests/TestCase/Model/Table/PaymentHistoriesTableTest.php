<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PaymentHistoriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PaymentHistoriesTable Test Case
 */
class PaymentHistoriesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'PaymentHistories' => 'app.payment_histories',
        'Payments' => 'app.payments',
        'Users' => 'app.users',
        'UserRoles' => 'app.user_roles',
        'BunningStores' => 'app.bunning_stores',
        'ScanBooks' => 'app.scan_books',
        'Invoices' => 'app.invoices',
        'PurchaseOrders' => 'app.purchase_orders',
        'Currencies' => 'app.currencies',
        'PurchaseOrderStatuses' => 'app.purchase_order_statuses',
        'InvoiceItems' => 'app.invoice_items',
        'PurchaseOrderItems' => 'app.purchase_order_items',
        'DeliveryDockets' => 'app.delivery_dockets',
        'BookingSheetItems' => 'app.booking_sheet_items',
        'BookingSheets' => 'app.booking_sheets',
        'FileOverviewes' => 'app.file_overviewes',
        'FileTypes' => 'app.file_types',
        'FileStatuses' => 'app.file_statuses',
        'ProcessLogs' => 'app.process_logs',
        'MasterItems' => 'app.master_items',
        'Statistics' => 'app.statistics',
        'Customers' => 'app.customers',
        'Plans' => 'app.plans'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PaymentHistories') ? [] : ['className' => 'App\Model\Table\PaymentHistoriesTable'];
        $this->PaymentHistories = TableRegistry::get('PaymentHistories', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PaymentHistories);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
