<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MasterItemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MasterItemsTable Test Case
 */
class MasterItemsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'MasterItems' => 'app.master_items',
        'Users' => 'app.users',
        'UserRoles' => 'app.user_roles',
        'BunningStores' => 'app.bunning_stores',
        'ScanBooks' => 'app.scan_books',
        'Invoices' => 'app.invoices',
        'PurchaseOrders' => 'app.purchase_orders',
        'Currencies' => 'app.currencies',
        'PurchaseOrderStatuses' => 'app.purchase_order_statuses',
        'InvoiceItems' => 'app.invoice_items',
        'PurchaseOrderItems' => 'app.purchase_order_items',
        'ProcessLogs' => 'app.process_logs'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MasterItems') ? [] : ['className' => 'App\Model\Table\MasterItemsTable'];
        $this->MasterItems = TableRegistry::get('MasterItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MasterItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
