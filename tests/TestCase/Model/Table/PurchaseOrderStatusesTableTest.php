<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PurchaseOrderStatusesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PurchaseOrderStatusesTable Test Case
 */
class PurchaseOrderStatusesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'PurchaseOrderStatuses' => 'app.purchase_order_statuses',
        'PurchaseOrders' => 'app.purchase_orders',
        'Currencies' => 'app.currencies',
        'Invoices' => 'app.invoices',
        'PurchaseOrderItems' => 'app.purchase_order_items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PurchaseOrderStatuses') ? [] : ['className' => 'App\Model\Table\PurchaseOrderStatusesTable'];
        $this->PurchaseOrderStatuses = TableRegistry::get('PurchaseOrderStatuses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PurchaseOrderStatuses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
