<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FileStatusesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FileStatusesTable Test Case
 */
class FileStatusesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'FileStatuses' => 'app.file_statuses',
        'FileOverviewes' => 'app.file_overviewes',
        'FileTypes' => 'app.file_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FileStatuses') ? [] : ['className' => 'App\Model\Table\FileStatusesTable'];
        $this->FileStatuses = TableRegistry::get('FileStatuses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FileStatuses);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
