<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BookingSheetItemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BookingSheetItemsTable Test Case
 */
class BookingSheetItemsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'BookingSheetItems' => 'app.booking_sheet_items',
        'BookingSheets' => 'app.booking_sheets',
        'Invoices' => 'app.invoices',
        'Users' => 'app.users',
        'UserRoles' => 'app.user_roles',
        'BunningStores' => 'app.bunning_stores',
        'ScanBooks' => 'app.scan_books',
        'ProcessLogs' => 'app.process_logs',
        'PurchaseOrders' => 'app.purchase_orders',
        'Currencies' => 'app.currencies',
        'PurchaseOrderStatuses' => 'app.purchase_order_statuses',
        'InvoiceItems' => 'app.invoice_items',
        'PurchaseOrderItems' => 'app.purchase_order_items',
        'DeliveryDockets' => 'app.delivery_dockets',
        'MasterItems' => 'app.master_items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BookingSheetItems') ? [] : ['className' => 'App\Model\Table\BookingSheetItemsTable'];
        $this->BookingSheetItems = TableRegistry::get('BookingSheetItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BookingSheetItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
