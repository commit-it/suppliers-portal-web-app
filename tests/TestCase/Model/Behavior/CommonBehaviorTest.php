<?php
namespace App\Test\TestCase\Model\Behavior;

use App\Model\Behavior\CommonBehavior;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Behavior\CommonBehavior Test Case
 */
class CommonBehaviorTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Common = new CommonBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Common);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
