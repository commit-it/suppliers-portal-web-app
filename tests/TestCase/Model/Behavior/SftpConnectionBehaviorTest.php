<?php
namespace App\Test\TestCase\Model\Behavior;

use App\Model\Behavior\SftpConnectionBehavior;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Behavior\SftpConnectionBehavior Test Case
 */
class SftpConnectionBehaviorTest extends TestCase
{

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->SftpConnection = new SftpConnectionBehavior();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SftpConnection);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
