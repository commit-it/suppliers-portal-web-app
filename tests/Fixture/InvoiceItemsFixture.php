<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * InvoiceItemsFixture
 *
 */
class InvoiceItemsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'invoice_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'purchase_order_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'order_line_number' => ['type' => 'biginteger', 'length' => 10, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => 'Item line number', 'precision' => null, 'autoIncrement' => null],
        'gtin' => ['type' => 'string', 'length' => 14, 'null' => false, 'default' => null, 'comment' => 'Global Trade Item Number', 'precision' => null, 'fixed' => null],
        'supplier_product_id' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'comment' => 'External product ID as assigned by the Supplier', 'precision' => null, 'fixed' => null],
        'bunnings_product_id' => ['type' => 'biginteger', 'length' => 7, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => 'Internal product ID as assigned by Bunnings', 'precision' => null, 'autoIncrement' => null],
        'requested_quantity' => ['type' => 'decimal', 'length' => 7, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'unit_of_measure' => ['type' => 'string', 'fixed' => true, 'length' => 3, 'null' => false, 'default' => null, 'comment' => 'Unit of measure which product is distributed as', 'precision' => null],
        'pack_quantity' => ['type' => 'biginteger', 'length' => 10, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'quotation_number' => ['type' => 'string', 'length' => 70, 'null' => true, 'default' => null, 'comment' => 'Reference Quotation Number', 'precision' => null, 'fixed' => null],
        'description' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'comment' => 'Product description', 'precision' => null, 'fixed' => null],
        'net_price' => ['type' => 'decimal', 'length' => 6, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => 'Unit price excluding GST'],
        'net_amount' => ['type' => 'decimal', 'length' => 6, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => 'Line value excluding GST'],
        'reference_purchase_order_line_number' => ['type' => 'biginteger', 'length' => 3, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => 'Reference purchase order line number', 'precision' => null, 'autoIncrement' => null],
        'gst_amount' => ['type' => 'decimal', 'length' => 6, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => 'GST amount of the item'],
        'gst_percentage' => ['type' => 'decimal', 'length' => 6, 'precision' => 2, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => 'GST percentage applied to item'],
        'comments' => ['type' => 'string', 'length' => 1000, 'null' => true, 'default' => null, 'comment' => 'Line item comments', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
'engine' => 'InnoDB', 'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'invoice_id' => 1,
            'purchase_order_id' => 1,
            'order_line_number' => '',
            'gtin' => 'Lorem ipsum ',
            'supplier_product_id' => 'Lorem ipsum dolor sit amet',
            'bunnings_product_id' => '',
            'requested_quantity' => '',
            'unit_of_measure' => 'L',
            'pack_quantity' => '',
            'quotation_number' => 'Lorem ipsum dolor sit amet',
            'description' => 'Lorem ipsum dolor sit amet',
            'net_price' => '',
            'net_amount' => '',
            'reference_purchase_order_line_number' => '',
            'gst_amount' => '',
            'gst_percentage' => '',
            'comments' => 'Lorem ipsum dolor sit amet',
            'created' => '2015-07-03 06:20:45',
            'modified' => '2015-07-03 06:20:45'
        ],
    ];
}
