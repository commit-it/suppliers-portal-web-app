<?php

require_once('QuickBookApp.php');

class PHPSample_Items extends PHPSample_QuickBookApp {

    // Execute any other additional setup for your component.
    public function __construct($appSettings) {
        
        $quickBookSettings = PHPSample_Customer::getInstance($appSettings);

        // Prep Data Services
        $this->dataService = isset($quickBookSettings['dataService']) ? $quickBookSettings['dataService'] : null;
    }
    
    /**
     * createItem - This method using for create item
     * @param type $invoiceItemData
     * @return string
     */
    public function createItem($invoiceItemData) {

        $result = array();

        // Add a Items
        $itemObj = new IPPItem();
        
        // find IncomeAccountRef Id
        try {

            if ($invoiceItemData['IncomeAccountRef'] != NULL) {

                foreach ($invoiceItemData as $invoiceItemKey => $invoiceItemVal) {

                    if (is_array($invoiceItemVal)) {

                        $invoiceItemValObj = (object) $invoiceItemVal;
                        $itemObj->$invoiceItemKey = $invoiceItemValObj;
                    } else {

                        $itemObj->$invoiceItemKey = $invoiceItemVal;
                    }
                }
                
                try {
                    
                    $resultingItemObj = $this->dataService->Add($itemObj);
                    if ($resultingItemObj) {

                        $result['status'] = 'success';
                        $result['msg'] = 'Item Created Successfully';
                        $result['response'] = $resultingItemObj;
                    } else {

                        $result['status'] = 'error';
                        $result['msg'] = 'Problem while adding Item.';
                    }
                } catch (Exception $exc) {

                    $result['status'] = 'error';
                    $errorMsg = isset($_SESSION['quick_book_error_msg']) ? '"' . $_SESSION['quick_book_error_msg'] . '"' :  '"' . $exc->getMessage() . '"';
                    $result['msg'] = 'Error occurred during Create Item: ' . $errorMsg;
                }
            } else {

                $result['status'] = 'error';
                $result['msg'] = 'Problem while adding Item, the IncomeAccountRef is not available';
            }
        } catch (Exception $exc) {

            $result['status'] = 'error';
            $errorMsg = isset($_SESSION['quick_book_error_msg']) ? '"' . $_SESSION['quick_book_error_msg'] . '"' :  '"' . $exc->getMessage() . '"';
            $result['msg'] = 'Error occurred during find Sales Account type: ' . $errorMsg;
        }

        return $result;
    }

    /**
     * findItemDetailsByFieldName - This method find the Item details by specific field
     * @param type $fieldName
     * @param type $fieldValue
     * @return string
     */
    public function findItemDetailsByFieldName($fieldName, $fieldValue) {

        $result = array();

        // Run a query
        try {

            $itemData = $this->dataService->Query("select * from Item Where $fieldName = '$fieldValue'");
            if ($itemData) {
                $result['status'] = 'success';
                $result['msg'] = 'Item available to Quick Book';
                $result['response'] = $itemData;
            } else {
                $result['status'] = 'error';
                $result['msg'] = 'Item not vailable to Quick Book';
            }
        } catch (Exception $exc) {

            $result['status'] = 'error';
            $errorMsg = isset($_SESSION['quick_book_error_msg']) ? '"' . $_SESSION['quick_book_error_msg'] . '"' :  '"' . $exc->getMessage() . '"';
            $result['msg'] = 'Error occurred during find Item: ' . $errorMsg;
        }

        return $result;
    }

}
