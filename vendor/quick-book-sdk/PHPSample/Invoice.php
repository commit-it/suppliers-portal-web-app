<?php

require_once('QuickBookApp.php');

class PHPSample_Invoice extends PHPSample_QuickBookApp {

    // Execute any other additional setup for your component.
    public function __construct($appSettings) {
        
        $quickBookSettings = PHPSample_Customer::getInstance($appSettings);
        
        // Prep Data Services
        $this->dataService = isset($quickBookSettings['dataService']) ? $quickBookSettings['dataService'] : null;
    }
    
    /**
     * createInvoice - This method create Invoice
     * @param type $invoiceData
     * @param type $data
     * @return string
     */
    public function createInvoice($invoiceData, $data = array()) {
        
        $result = array();
        
        $taxCodeResponse = $this->dataService->Query("select * from TaxCode where Name = 'GST'");
        $TaxCodeRef = isset($taxCodeResponse[0]->Id) ? $taxCodeResponse[0]->Id : NULL;
        
        // Add an employee
        $invoiceObj = new IPPInvoice();
        $invoiceObj->Deposit = 0;
        $invoiceObj->domain = 'QBO';
        $invoiceObj->sparse = false;
        $invoiceObj->SyncToken = 0;
        $invoiceObj->DocNumber = 'WEB_' . time() . '_' . mt_rand(0, 10000);
        
        $invoiceObj->SalesTermRef = isset($data['SalesTermRef']) ? $data['SalesTermRef'] : '';
        
        /* Added new custom field 'purchase ordernumber' in invoice */
        $IPPCustomField = new IPPCustomField();
        $IPPCustomField->DefinitionId = "1";
        $IPPCustomField->Name = "Purchase Order";
        $IPPCustomField->Type = "StringType";
        $IPPCustomField->StringValue = isset($data['PurchaseOrderId']) ? $data['PurchaseOrderId'] : '';
        $invoiceObj->CustomField = $IPPCustomField;
        
        $lineAmount = 0;
        foreach ($invoiceData['linesData'] as $linesData) {

            $lineAmount = $lineAmount + ($linesData['UnitPrice'] * $linesData['Qty']);
            
            $Line = new IPPLine();
            
            $Line->DetailType = 'SalesItemLineDetail';
            $Line->Description = $linesData['Description'];

            $SalesItemLineDetail = new IPPSalesItemLineDetail();
            
            $SalesItemLineDetail->ItemRef = $linesData['ItemRef'];
            $SalesItemLineDetail->Qty = $linesData['Qty'];
            $SalesItemLineDetail->UnitPrice = $linesData['UnitPrice'];
            
            if ($TaxCodeRef) {
                $invoiceData['Invoice']['GlobalTaxCalculation'] = 'TaxExcluded';
                $SalesItemLineDetail->TaxCodeRef = $TaxCodeRef;
            } else {
                $invoiceData['Invoice']['GlobalTaxCalculation'] = 'NotApplicable';
            }
            
            $Line->SalesItemLineDetail = $SalesItemLineDetail;
            
            $Line->Amount = ($linesData['UnitPrice'] * $linesData['Qty']);           
            $invoiceObj->Line[] = $Line;
        }
        
        $Line = new IPPLine();
        $SubTotalLineDetail = new IPPSubTotalLineDetail();
        $SubTotalLineDetail->ServiceDate = date('Y-m-d');
        $Line->DetailType = 'SubTotalLineDetail';
        $Line->Amount = $lineAmount;
        $Line->SubTotalLineDetail = $SubTotalLineDetail;
        $invoiceObj->Line[] = $Line;
        
        // DiscountLineDetail
        $discountPer = isset($data['Rebate']) ? $data['Rebate'] : 0;
        $Line = new IPPLine();
        $DiscountLineDetail = new IPPDiscountLineDetail();
        $DiscountLineDetail->PercentBased = true;
        $DiscountLineDetail->DiscountPercent = $discountPer;
        $Line->DetailType = 'DiscountLineDetail';
        $Line->Amount = ($lineAmount * $discountPer) / 100;
        $Line->DiscountLineDetail = $DiscountLineDetail;
        $invoiceObj->Line[] = $Line;
        
        $invoiceData['Invoice']['TotalAmt'] = $lineAmount;
        foreach ($invoiceData['Invoice'] as $invoiceKey => $invoiceVal) {

            if (is_array($invoiceVal)) {

                $invoiceValObj = (object) $invoiceVal;
                $invoiceObj->$invoiceKey = $invoiceValObj;
            } else {

                $invoiceObj->$invoiceKey = $invoiceVal;
            }
        }
        
        if (isset($invoiceData['ShipAddr']) && !empty($invoiceData['ShipAddr'])) {
            
            // Ship Addr
            $shipAddrObj = new IPPPhysicalAddress();
            
            foreach ($invoiceData['ShipAddr'] as $shipAddrKey => $shipAddrVal) {

                if (is_array($shipAddrVal)) {

                    $shipAddrValObj = (object) $shipAddrVal;
                    $shipAddrObj->$shipAddrKey = $shipAddrValObj;
                } else {

                    $shipAddrObj->$shipAddrKey = $shipAddrVal;
                }
            }
            
            $invoiceObj->ShipAddr = $shipAddrObj;
        }

        try {

            $resultinginvoiceObj = $this->dataService->Add($invoiceObj);
            if ($resultinginvoiceObj) {

                $result['status'] = 'success';
                $invoiceNumber = isset($resultinginvoiceObj->DocNumber) ? $resultinginvoiceObj->DocNumber : NULL;
                if ($invoiceNumber != NULL) {
                    
                    $result['msg'] = 'Invoice Created Successfully & the created Invoice Number is ' . $invoiceNumber;
                    $result['response'] = $resultinginvoiceObj;
                } else {

                    $result['status'] = 'error';
                    $result['msg'] = 'Problem while creating Invoice.';
                }
            } else {

                $result['status'] = 'error';
                $result['msg'] = 'Problem while creating Invoice.';
            }
            
        } catch (Exception $exc) {

            $result['status'] = 'error';
            $errorMsg = isset($_SESSION['quick_book_error_msg']) ? '"' . $_SESSION['quick_book_error_msg'] . '"' : '"' . $exc->getMessage() . '"';
            $result['msg'] = 'Error occurred during create Invoice: ' . $errorMsg;
        }

        $result['invoiceCreateDataObjOfSP'] = json_encode($invoiceObj);
        return $result;
    }

    /**
     * updateQuickBookInvoice - This method update Invoice
     * @param type $invoiceData
     * @param type $newItemsData
     * @param type $deleteItemIds
     * @param type $data
     * @return string
     */
    public function updateQuickBookInvoice($invoiceData, $newItemsData, $deleteItemIds, $data = array()) {
        
        $taxCodeResponse = $this->dataService->Query("select * from TaxCode where Name = 'GST'");
        $TaxCodeRef = isset($taxCodeResponse[0]->Id) ? $taxCodeResponse[0]->Id : NULL;
        
        $result = array();
        
        // Get the existing invoice by Id or DocNumber
        $invoiceId = $invoiceData['Id'];
        $docNumber = $invoiceData['DocNumber'];
        try {

            $quickBookInvoiceData = $this->dataService->Query("select * from Invoice Where Id = '$invoiceId'");
            if (empty($quickBookInvoiceData)) {

                $quickBookInvoiceData = $this->dataService->Query("select * from Invoice Where DocNumber = '$docNumber'");
            }
            
            $Invoice = isset($quickBookInvoiceData[0]) ? $quickBookInvoiceData[0] : array();

            if (!empty($Invoice)) {
                
                $lineCount = count($Invoice->Line);
                
                /*
                 * Remove last element from Line Arr
                 * Because that last Element is DiscountLineDetail, so we need to regenerate that
                 */
                $lastElementKey = $lineCount - 1;
                if (isset($Invoice->Line[$lastElementKey]->DetailType) && ($Invoice->Line[$lastElementKey]->DetailType == 'DiscountLineDetail')) {
                    
                    array_pop($Invoice->Line);
                }
                
                /*$newItemsData
                 * Remove last second element if Discount Available otherwise last element from Line Arr
                 * Because that last Element is IPPSubTotalLineDetail, so we need to regenerate that
                 */
                array_pop($Invoice->Line);

                $lineAmount = 0;
                foreach ($invoiceData['linesData'] as $storedLineData) {

                    foreach ($Invoice->Line as $lineKey => $quickBookLineData) {

                        $storeInvoiceItemId = isset($storedLineData['Id']) ? $storedLineData['Id'] : NULL;
                        $quickBookInvoiceItemId = isset($quickBookLineData->Id) ? $quickBookLineData->Id : NULL;
                        if ($storeInvoiceItemId == $quickBookInvoiceItemId) {

                            $Invoice->Line[$lineKey]->SalesItemLineDetail->ItemRef = $storedLineData['ItemRef'];
                            $Invoice->Line[$lineKey]->SalesItemLineDetail->Qty = $storedLineData['Qty'];
                            $Invoice->Line[$lineKey]->SalesItemLineDetail->UnitPrice = $storedLineData['UnitPrice'];
                            
                            if ($TaxCodeRef) {
                                $Invoice->Line[$lineKey]->SalesItemLineDetail->TaxCodeRef = $TaxCodeRef;
                            }
                            
                            $Invoice->Line[$lineKey]->Description = $storedLineData['Description'];
                            $Invoice->Line[$lineKey]->Amount = ($storedLineData['UnitPrice'] * $storedLineData['Qty']);

                            $lineAmount = $lineAmount + ($storedLineData['UnitPrice'] * $storedLineData['Qty']);
                        }
                    }
                }

                // Add New Lines in Invoice
                if (!empty($newItemsData)) {
                    
                    foreach ($newItemsData['linesData'] as $linesData) {

                        $lineAmount = $lineAmount + ($linesData['UnitPrice'] * $linesData['Qty']);

                        $Line = new IPPLine();
                        $Line->DetailType = 'SalesItemLineDetail';
                        $Line->Description = $linesData['Description'];
                        $Line->Amount = ($linesData['UnitPrice'] * $linesData['Qty']);

                        $SalesItemLineDetail = new IPPSalesItemLineDetail();
                        $SalesItemLineDetail->ItemRef = $linesData['ItemRef'];
                        $SalesItemLineDetail->Qty = $linesData['Qty'];
                        $SalesItemLineDetail->UnitPrice = $linesData['UnitPrice'];
                        if ($TaxCodeRef) {
                            $SalesItemLineDetail->TaxCodeRef = $TaxCodeRef;
                        }
                        $Line->SalesItemLineDetail = $SalesItemLineDetail;

                        $Invoice->Line[] = $Line;
                    }
                }
                
                // delete Lines
                if (!empty($deleteItemIds)) {
                    
                    foreach ($Invoice->Line as $lineKey => $quickBookLineData) {

                        $quickBookInvoiceItemId = isset($quickBookLineData->Id) ? $quickBookLineData->Id : NULL;
                        if (in_array($quickBookInvoiceItemId, $deleteItemIds)) {
                            
                            unset($Invoice->Line[$lineKey]);
                        }
                    }
                }
                
                // generate Subtotal Lines Arr
                $Line = new IPPLine();
                $SubTotalLineDetail = new IPPSubTotalLineDetail();
                $Line->DetailType = 'SubTotalLineDetail';
                $Line->Amount = $lineAmount;
                $Line->SubTotalLineDetail = $SubTotalLineDetail;
                $Invoice->Line[] = $Line;
                
                // DiscountLineDetail
                $discountPer = isset($data['Rebate']) ? $data['Rebate'] : 0;
                $Line = new IPPLine();
                $DiscountLineDetail = new IPPDiscountLineDetail();
                $DiscountLineDetail->PercentBased = true;
                $DiscountLineDetail->DiscountPercent = $discountPer;
                $Line->DetailType = 'DiscountLineDetail';
                $Line->Amount = ($lineAmount * $discountPer) / 100;
                $Line->DiscountLineDetail = $DiscountLineDetail;
                $Invoice->Line[] = $Line;
        
                if ($TaxCodeRef) {
                    
                    $Invoice->GlobalTaxCalculation = 'TaxExcluded';
                } else {
                    
                    $Invoice->GlobalTaxCalculation = 'NotApplicable';
                }
                
                $Invoice->TxnDate = date('Y-m-d');  // Update the invoice date to today's date 
                
                // Auto calculate gst amount during edit case
                $Invoice->TxnTaxDetail = new IPPTxnTaxDetail();
                
                $updateInvoiceResponse = '';
                try {
                    
                    $updateInvoiceResponse = $this->dataService->update($Invoice);
                    if ($updateInvoiceResponse) {

                        $result['status'] = 'success';
                        $result['response'] = $updateInvoiceResponse;
                        $result['msg'] = 'Invoice Updated Successfully to Quick-Books';
                    } else {

                        $result['status'] = 'error';
                        $errorMsg = isset($_SESSION['quick_book_error_msg']) ? '"' . $_SESSION['quick_book_error_msg'] . '"' : '"' . $exc->getMessage() . '"';
                        $result['msg'] = 'Error occurred during find Invoice for update: ' . $errorMsg;
                    }
                } catch (Exception $ex) {
                    $result['status'] = 'error';
                    $result['msg'] = 'Internet connection issue.';
                }               
                
            } else {

                $result['status'] = 'error';
                $result['msg'] = 'Invoice is not available to Quick-Books for update';
            }
        } catch (Exception $exc) {

            $result['status'] = 'error';
            $errorMsg = isset($_SESSION['quick_book_error_msg']) ? '"' . $_SESSION['quick_book_error_msg'] . '"' : '"' . $exc->getMessage() . '"';
            $result['msg'] = 'Error occurred during find Invoice for update: ' . $errorMsg;
        }

        return $result;
    }
    
    /**
     * updateFailedQuickBookInvoice - This method update failed Invoice
     * @param type $invoiceData
     * @param type $data
     * @return string
     */
    public function updateFailedQuickBookInvoice($invoiceData, $data) {
        
        $taxCodeResponse = $this->dataService->Query("select * from TaxCode where Name = 'GST'");
        $TaxCodeRef = isset($taxCodeResponse[0]->Id) ? $taxCodeResponse[0]->Id : NULL;
        
        $result = array();
        
        // Get the existing invoice by Id or DocNumber
        $invoiceId = $invoiceData['Id'];
        $docNumber = $invoiceData['DocNumber'];
        try {

            $quickBookInvoiceData = $this->dataService->Query("select * from Invoice Where Id = '$invoiceId'");
            if (empty($quickBookInvoiceData)) {

                $quickBookInvoiceData = $this->dataService->Query("select * from Invoice Where DocNumber = '$docNumber'");
            }
            
            $Invoice = isset($quickBookInvoiceData[0]) ? $quickBookInvoiceData[0] : array();
            if (!empty($Invoice)) {
                
                /*
                 * Remove element from Line Arr & Regenerate
                 */
                foreach ($Invoice->Line as $lineKey => $quickBookLineData) {

                    unset($Invoice->Line[$lineKey]);
                }
                
                $lineAmount = 0;
                foreach ($invoiceData['linesData'] as $linesData) {

                    $lineAmount = $lineAmount + ($linesData['UnitPrice'] * $linesData['Qty']);

                    $Line = new IPPLine();
                    $Line->DetailType = 'SalesItemLineDetail';
                    $Line->Description = $linesData['Description'];
                    $Line->Amount = ($linesData['UnitPrice'] * $linesData['Qty']);

                    $SalesItemLineDetail = new IPPSalesItemLineDetail();
                    $SalesItemLineDetail->ItemRef = $linesData['ItemRef'];
                    $SalesItemLineDetail->Qty = $linesData['Qty'];
                    $SalesItemLineDetail->UnitPrice = $linesData['UnitPrice'];
                    if ($TaxCodeRef) {
                        $SalesItemLineDetail->TaxCodeRef = $TaxCodeRef;
                    }
                    $Line->SalesItemLineDetail = $SalesItemLineDetail;

                    $Invoice->Line[] = $Line;
                }
                
                // generate Subtotal Lines Arr
                $Line = new IPPLine();
                $SubTotalLineDetail = new IPPSubTotalLineDetail();
                $Line->DetailType = 'SubTotalLineDetail';
                $Line->Amount = $lineAmount;
                $Line->SubTotalLineDetail = $SubTotalLineDetail;
                $Invoice->Line[] = $Line;
                
                // DiscountLineDetail
                $discountPer = isset($data['Rebate']) ? $data['Rebate'] : 0;
                $Line = new IPPLine();
                $DiscountLineDetail = new IPPDiscountLineDetail();
                $DiscountLineDetail->PercentBased = true;
                $DiscountLineDetail->DiscountPercent = $discountPer;
                $Line->DetailType = 'DiscountLineDetail';
                $Line->Amount = ($lineAmount * $discountPer) / 100;
                $Line->DiscountLineDetail = $DiscountLineDetail;
                $Invoice->Line[] = $Line;
                
                if ($TaxCodeRef) {
                    
                    $Invoice->GlobalTaxCalculation = 'TaxExcluded';
                } else {
                    
                    $Invoice->GlobalTaxCalculation = 'NotApplicable';
                }
                
                $Invoice->TxnDate = date('Y-m-d');  // Update the invoice date to today's date 
                
                // Auto calculate gst amount during edit case
                $Invoice->TxnTaxDetail = new IPPTxnTaxDetail();
                
                $updateInvoiceResponse = $this->dataService->update($Invoice);
                if ($updateInvoiceResponse) {

                    $result['status'] = 'success';
                    $result['response'] = $updateInvoiceResponse;
                    $result['msg'] = 'Invoice Updated Successfully to Quick-Books';
                } else {

                    $result['status'] = 'error';
                    $errorMsg = isset($_SESSION['quick_book_error_msg']) ? '"' . $_SESSION['quick_book_error_msg'] . '"' : '"' . $exc->getMessage() . '"';
                    $result['msg'] = 'Error occurred during find Invoice for update: ' . $errorMsg;
                }
            } else {

                $result['status'] = 'error';
                $result['msg'] = 'Invoice is not available to Quick-Books for update';
            }
        } catch (Exception $exc) {

            $result['status'] = 'error';
            $errorMsg = isset($_SESSION['quick_book_error_msg']) ? '"' . $_SESSION['quick_book_error_msg'] . '"' : '"' . $exc->getMessage() . '"';
            $result['msg'] = 'Error occurred during find Invoice for update: ' . $errorMsg;
        }

        return $result;
    }

}
