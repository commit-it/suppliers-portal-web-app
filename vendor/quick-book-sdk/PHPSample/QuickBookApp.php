<?php

require_once(ROOT . DS . 'vendor' . DS . 'quick-book-sdk' . DS . 'v3-php-sdk-2.2.0-RC' . DS . 'config.php');
require_once(PATH_SDK_ROOT . 'Core/ServiceContext.php');
require_once(PATH_SDK_ROOT . 'DataService/DataService.php');
require_once(PATH_SDK_ROOT . 'PlatformService/PlatformService.php');
require_once(PATH_SDK_ROOT . 'Utility/Configuration/ConfigurationManager.php');
require_once(PATH_SDK_ROOT . 'Core/OperationControlList.php');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class PHPSample_QuickBookApp {

    protected static $instance = null;

    protected function __construct() {
        //Thou shalt not construct that which is unconstructable!
    }

    /**
     * getInstance - This method get Instance
     * @param type $appSettings
     * @return type
     */
    public static function getInstance($appSettings) {

        if (!isset(static::$instance['dataService'])) {

            self::setDataService($appSettings);
        }

        return static::$instance;
    }

    /**
     * setDataService - This method set Instance for DataService
     * @param type $appSettings
     * @throws Exception
     */
    public static function setDataService($appSettings) {

        $serviceType = IntuitServicesType::QBO;

        // check Internet Connection Avaialble or not
        if (!$sock = @fsockopen('www.google.com', 80, $num, $error, 5))
        {
            // this means you are not connected
            throw new Exception("We have detected a slow internet connection, please try again.");
        }
        
        // Get App Config
        $realmId = $appSettings['RealmID'];
        if (!$realmId) {

            throw new Exception("Please add realm to Quick-Books Details before running this sample.");
        }

        // Prep Service Context
        $requestValidator = new OAuthRequestValidator($appSettings['AccessToken'], $appSettings['AccessTokenSecret'], $appSettings['ConsumerKey'], $appSettings['ConsumerSecret']);

        $serviceContext = new ServiceContext($realmId, $serviceType, $requestValidator);
        if (!$serviceContext) {

            throw new Exception("Problem while initializing ServiceContext to Quick-Books.");
        }

        // Prep Data Services
        $dataService = new DataService($serviceContext);
        if (!$dataService) {

            throw new Exception("Problem while initializing DataService to Quick-Books.");
        } else {

            static::$instance['dataService'] = $dataService;
        }
    }

}
