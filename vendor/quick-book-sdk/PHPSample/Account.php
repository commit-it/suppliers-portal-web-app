<?php

require_once('QuickBookApp.php');

class PHPSample_Account extends PHPSample_QuickBookApp {

// Execute any other additional setup for your component.
    public function __construct($appSettings) {

        $quickBookSettings = PHPSample_Customer::getInstance($appSettings);

// Prep Data Services
        $this->dataService = isset($quickBookSettings['dataService']) ? $quickBookSettings['dataService'] : null;
    }

    /**
     * findDataByFieldAndValue - This method find Account data by field & value
     * @param type $field
     * @param type $value
     * @return type
     */
    public function findDataByFieldAndValue($field, $value) {

        $accountSpecificData = $this->dataService->Query("select * from Account where $field = '$value'");
        return $accountSpecificData;
    }

    /**
     * createInvoice - This method create Account
     * @param type $accountData
     * @return string
     */
    public function createAccount($accountData) {

        $result = array();
        
        // create account
        $accountObj = new IPPAccount();
        
        foreach ($accountData as $accountKey => $accountVal) {

            if (is_array($accountVal)) {

                $accountValObj = (object) $accountVal;
                $accountObj->$accountKey = $accountValObj;
            } else {

                $accountObj->$accountKey = $accountVal;
            }
        }

        try {

            $resultingAccountObj = $this->dataService->Add($accountObj);
            if ($resultingAccountObj) {

                $result['status'] = 'success';
                $result['response'] = $resultingAccountObj;
            } else {

                $result['status'] = 'error';
                $result['msg'] = 'Problem while adding Account.';
            }
        } catch (Exception $exc) {

            $result['status'] = 'error';
            $errorMsg = isset($_SESSION['quick_book_error_msg']) ? '"' . $_SESSION['quick_book_error_msg'] . '"' : '"' . $exc->getMessage() . '"';
            $result['msg'] = 'Error occurred during find Account type: ' . $errorMsg;
        }
        
        return $result;
    }

}
