<?php

require_once('QuickBookApp.php');

class PHPSample_Customer extends PHPSample_QuickBookApp {
    
    // Execute any other additional setup for your component.
    public function __construct($appSettings) {
        
        $quickBookSettings = PHPSample_Customer::getInstance($appSettings);

        // Prep Data Services
        $this->dataService = isset($quickBookSettings['dataService']) ? $quickBookSettings['dataService'] : null;
    }

    /**
     * createCustomer - This method create Customer
     * @param type $customerData
     * @return string
     */
    public function createCustomer($customerData) {

        $result = array();

        // Add a customer
        $customerObj = new IPPCustomer();
        
        if (isset($customerData['Customer']) && !empty($customerData['Customer'])) {
            
            foreach ($customerData['Customer'] as $customerKey => $customerVal) {

                if (is_array($customerVal)) {

                    $customerValObj = (object) $customerVal;
                    $customerObj->$customerKey = $customerValObj;
                } else {

                    $customerObj->$customerKey = $customerVal;
                }
            }
        }
        
        if (isset($customerData['BillAddr']) && !empty($customerData['BillAddr'])) {
            
            // Bill Addr
            $billAddrObj = new IPPPhysicalAddress();
            
            foreach ($customerData['BillAddr'] as $billAddrKey => $billAddrVal) {

                if (is_array($billAddrVal)) {

                    $billAddrValObj = (object) $billAddrVal;
                    $billAddrObj->$billAddrKey = $billAddrValObj;
                } else {

                    $billAddrObj->$billAddrKey = $billAddrVal;
                }
            }
            
            $customerObj->BillAddr = $billAddrObj;
        }

        if (isset($customerData['NameBase']) && !empty($customerData['NameBase'])) {
            
            $emailAddressObj = new IPPEmailAddress();
            $emailAddressObj->Address = isset($customerData['NameBase']['PrimaryEmailAddr']) ? $customerData['NameBase']['PrimaryEmailAddr'] : NULL;
            $customerObj->PrimaryEmailAddr = $emailAddressObj;

            $telephoneNumberObj = new IPPTelephoneNumber();
            $telephoneNumberObj->FreeFormNumber = isset($customerData['NameBase']['PrimaryPhone']) ? $customerData['NameBase']['PrimaryPhone'] : NULL;
            $customerObj->PrimaryPhone = $telephoneNumberObj;
            
            $faxObj = new IPPTelephoneNumber();
            $faxObj->FreeFormNumber = isset($customerData['NameBase']['Fax']) ? $customerData['NameBase']['Fax'] : NULL;
            $customerObj->Fax = $faxObj;
        }

        
        try {

            $resultingCustomerObj = $this->dataService->Add($customerObj);
            if ($resultingCustomerObj) {

                $result['status'] = 'success';
                $result['msg'] = 'Customer Created Successfully';
                $result['response'] = $resultingCustomerObj;
            } else {

                $result['status'] = 'error';
                $result['msg'] = 'Problem while adding Customer.';
            }
        } catch (Exception $exc) {
        
            $result['status'] = 'error';
            $errorMsg = isset($_SESSION['quick_book_error_msg']) ? '"' . $_SESSION['quick_book_error_msg'] . '"' :  '"' . $exc->getMessage() . '"';
            $result['msg'] = 'Error occurred during create Customer: ' . $errorMsg;
        }
        
        return $result;
    }

    /**
     * findCustomerDetailsByFieldName - This method find the customer details by specific field
     * @param type $fieldName
     * @param type $fieldValue
     * @return string
     */
    public function findCustomerDetailsByFieldName($fieldName, $fieldValue) {

        $result = array();

        // Run a query
        try {

            $customerData = $this->dataService->Query("select * from Customer Where $fieldName = '$fieldValue'");
            if ($customerData) {
                
                $result['status'] = 'success';
                $result['msg'] = 'Customer available to Quick Book';
                $result['response'] = $customerData;
            } else {
                
                $result['status'] = 'error';
                $result['msg'] = 'Customer is not available to Quick Book';
            }
        } catch (Exception $exc) {

            $result['status'] = 'error';
            $errorMsg = isset($_SESSION['quick_book_error_msg']) ? '"' . $_SESSION['quick_book_error_msg'] . '"' :  '"' . $exc->getMessage() . '"';
            $result['msg'] = 'Error occurred during find Customer details: ' . $errorMsg;
        }

        return $result;
    }

}
