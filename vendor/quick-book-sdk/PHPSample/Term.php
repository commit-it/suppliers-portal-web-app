<?php

require_once('QuickBookApp.php');

class PHPSample_Term extends PHPSample_QuickBookApp {
    
    // Net 60 is default term
    const Default_Term = 88;

    // Execute any other additional setup for your component.
    public function __construct($appSettings) {
        
        $quickBookSettings = PHPSample_Customer::getInstance($appSettings);

        // Prep Data Services
        $this->dataService = isset($quickBookSettings['dataService']) ? $quickBookSettings['dataService'] : null;
    }
    
    /**
     * findPaymentTermLists - This menthod find Payment Term List
     * @return $paymentTermList
     */
    public function findPaymentTermLists() {

        $result = array();
        // Run a query
        try {

            $paymentTermList = $this->dataService->Query("select * from Term");
            if ($paymentTermList) {
                $result['status'] = 'success';
                $result['msg'] = 'Terms available to Quick Book';
                $result['response'] = $paymentTermList;
            } else {
                $result['status'] = 'error';
                $result['msg'] = 'Terms not vailable to Quick Book';
            }
        } catch (Exception $exc) {

            $result['status'] = 'error';
            $errorMsg = isset($_SESSION['quick_book_error_msg']) ? '"' . $_SESSION['quick_book_error_msg'] . '"' :  '"' . $exc->getMessage() . '"';
            $result['msg'] = 'Error occurred during find Item: ' . $errorMsg;
        }
        
        return $result;
    }
    
    /**
     * findTermDetailsByFieldName - This method find the Item details by specific field
     * @param type $fieldName
     * @param type $fieldValue
     * @return string
     */
    public function findTermDetailsByFieldName($fieldName, $fieldValue) {

        $result = array();

        // Run a query
        try {

            $itemData = $this->dataService->Query("select * from Term Where $fieldName = '$fieldValue'");
            if ($itemData) {
                $result['status'] = 'success';
                $result['msg'] = 'Term available to Quick Book';
                $result['response'] = $itemData;
            } else {
                $result['status'] = 'error';
                $result['msg'] = 'Term not vailable to Quick Book';
            }
        } catch (Exception $exc) {

            $result['status'] = 'error';
            $errorMsg = isset($_SESSION['quick_book_error_msg']) ? '"' . $_SESSION['quick_book_error_msg'] . '"' :  '"' . $exc->getMessage() . '"';
            $result['msg'] = 'Error occurred during find Item: ' . $errorMsg;
        }

        return $result;
    }

}
