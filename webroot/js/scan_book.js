
$('#send_scan_book').click(function() {

    var error = 0;

    if ($('#scan_file').val() == '') {
        error++;
    }

    var bunning_stores_ids = [];
    $('input[name="bunning_store_id[]"]:checked').each(function() {
        bunning_stores_ids.push(this.value);
    });

    if (bunning_stores_ids.length < 1) {
        error++;
    }
    
    if (error < 1) {
        $('#error-message > span').text('');
        $('#error-message').hide();

        // submit form
        $('#scan-book-form').submit();
    } else {
        $('#error-message > span').text('You can\'t send Scan Book to Bunning store, without select scan book & bunning store');
        $('#error-message').show();
    }

});

/**
 * selectStateWiseBunningStores - Method
 * @param {type} num
 * @returns {undefined}
 */
function selectStateWiseBunningStores(num) {
    
    if ($('#bunning-store-state-' + num).is(':checked')) {
        
        $('.state_wise_bunning_stores_'+num).prop("checked", true);
        
    } else {
        
        $('.state_wise_bunning_stores_'+num).prop("checked", false);
        
    }
    
}