/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function changePassword() {
    
    var emptyError = 0;
    var lengthError = 0;
    var allowLength = 8;
    if (!checkBlankVal('old-password')) {
        emptyError++;
    }
    
    if (!checkBlankVal('new-password')) {
        emptyError++;
    } else if (!checklength('new-password',allowLength)) {
        lengthError++;
    }
    
    if (!checkBlankVal('confirm-password')) {
        emptyError++;
    }
    
    if (emptyError > 0) {
        
        $('#change_password_error_msg').html('All fields are required, please provide that');
        $('#change_password_error_msg').show();
        
    } else if (lengthError > 0) {
        
        $('#change_password_error_msg').html('Password length not match, please enter minimum 8 character');
        $('#change_password_error_msg').show();
        
    }
    else if ($('#new-password').val() != $('#confirm-password').val()) {
        
        $('#change_password_error_msg').html('New Password not match with Confirm Password');
        $('#change_password_error_msg').show();
        
    } else {
        
        $('#change_password_error_msg').html('');
        $('#change_password_error_msg').hide();
        // submit form
        $('#change_password_form').attr('onsubmit','showLoading()');
        $('#change_password_form').submit();
    }
    
}

