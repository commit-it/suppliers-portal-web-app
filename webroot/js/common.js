

$(document).ready(function () {

    // Display loader when click on action
    $('.display-loader-onclick').click(function() {

        showLoading();
    });

    // date picker
    if ($('#trial_end_date').length > 0) {
        
        var oldTrialEndDate = $.trim($('#trial_end_date').val());
        $('#trial_end_date').datepicker({ startDate: oldTrialEndDate });
        
    } else if ($('.set-date-picker-for-tomorrow').length > 0) {
        
        var serverTomorrowDate = $.trim($('#server_tomorrow_date').val());
        $('.date-picker').datepicker({autoclose: true }).on('changeDate', function(){
         setDeliveryDate();
        }); 
        
    } else if (($('.simple-date-picker').length) > 0) {
        
        $('.simple-date-picker').datepicker();
    } else {
        
        if (($('.date-picker').length) > 0) {
            
            var serverTodayDate = $.trim($('#server_today_date').val());
            $('.date-picker').datepicker({ startDate: serverTodayDate });
        }
    }
    
    // date & time picker
    if (($('.date-time-picker').length) > 0) {
        
        $('.date-time-picker').datetimepicker();
    }
});

/**
 * checkBlankVal
 * @param {type} id
 * @returns {Boolean}
 */
function checkBlankVal(id) {
    var fieldVal = ($('#' + id).val()).trim();
    if (fieldVal != '') {
        return true;
    } else {
        return false;
    }
}

/**
 * checklength
 * @param {type} id
 * @param {type} allowLength
 * @returns {Boolean}
 */
function checklength(id, allowLength) {
    var fieldLength = (($('#' + id).val()).trim()).length;
    if (fieldLength >= allowLength) {
        return true;
    } else {
        return false;
    }
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function checkfileIsExcel(id) {
    var validExts = new Array(".xlsx", ".xls");
    var fileExt = $('#' + id).val();
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
        $('#error-message > span').text("Invalid file selected, valid files are of " + validExts.toString() + " types.");
        $('#error-message').addClass('error');
        $('#error-message').show();
        return false;
    }
    else {
        $('#error-message > span').html('');
        $('#error-message').removeClass('error');
        $('#error-message').hide();
        return true;
    }
}

/**
 * deleteItem() method
 * @param {int} id is auto key of records
 * @param {string} name is the name of deleted record
 * @returns {undefined}
 */
function deleteItem(id, name) {
    $('#delete_item_id').val(id);
    $('#delete_item_name').text(name);
}


