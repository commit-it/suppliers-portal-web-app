/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**************************************** Start script for generated_invoices action of invoice  ****************************************/

$('#generate_delivery_docket_checkbox').click(function(event) {
    if (this.checked) {
        $('input[name="generate_delivery_docket_checkbox[]"]').prop("checked", true);
    } else {
        $('input[name="generate_delivery_docket_checkbox[]"]').prop("checked", false);
    }
});

$('#generate_delivery_docket').click(function(event) {

    var generate_delivery_docket_checkbox_ids = [];
    $('input[name="generate_delivery_docket_checkbox[]"]:checked').each(function() {
        generate_delivery_docket_checkbox_ids.push(this.value);
    });

    if (generate_delivery_docket_checkbox_ids.length > 0) {

        $('#error-message span').text('');
        $('#error-message').hide();

        // trigger open delivery details popup button
        $('#open_delivery_docket_details_window').trigger('click');

    } else {

        $('#error-message span').text('You can\'t generate Delivery Docket without select Invoice, Please select at least one Invoice');
        $('#error-message').show();

    }

});

/**************************************** End script for generated_invoices action of invoice  ****************************************/


/**************************************** Start script for index action of deliverdockets  ****************************************/

$('#select_deselect_delivery_docket').click(function(event) {
    if (this.checked) {
        $('input[name="delivery_dockets[]"]').prop("checked", true);
    } else {
        $('input[name="delivery_dockets[]"]').prop("checked", false);
    }
});

$('#print_delivery_dockets').click(function(event) {

    var print_delivery_docket_checkbox_ids = [];
    $('input[name="delivery_dockets[]"]:checked').each(function() {
        print_delivery_docket_checkbox_ids.push(this.value);
    });

    if (print_delivery_docket_checkbox_ids.length > 0) {
        $('#error-message span').text('');
        $('#error-message').hide();
        $('#print_delivery_dockets_form').submit();
    } else {
        $('#error-message span').text('You can\'t print Delivery Docket without select Delivery Docket');
        $('#error-message').show();
    }

});

$('#update_delivery_docket').click(function() {
    $('#update_record_form').submit();
});

function modifyDeliveryDocketData(id, deliveryDocketDate) {
    $('#deliverydockets-id').val(id);
    $('#deliverydockets-delivery-date').val(deliveryDocketDate);
}

/**************************************** End script for index action of deliverdockets  ****************************************/


/**************************************** Start script for booking_sheet action of deliverdockets  ****************************************/

$('#select_deselect_booking_sheet').click(function(event) {
    if (this.checked) {
        $('input[name="booking_sheet_id[]"]').prop("checked", true);
    } else {
        $('input[name="booking_sheet_id[]"]').prop("checked", false);
    }
});

$('#generate_booking_sheet').click(function(event) {

    var generate_booking_sheet_checkbox_ids = [];
    $('input[name="booking_sheet_id[]"]:checked').each(function() {
        generate_booking_sheet_checkbox_ids.push(this.value);
    });

    if (generate_booking_sheet_checkbox_ids.length > 0) {

        $('#error-message span').text('');
        $('#error-message').hide();

        // submit form
        $('#booking_sheet_form').submit();

    } else {

        $('#error-message span').text('You can\'t generate Booking Sheet without select Invoice, Please select at least one Invoice');
        $('#error-message').show();

    }

});

/**************************************** End script for booking_sheet action of deliverdockets  ****************************************/


/**************************************** Start script for index action of bookingsheets  ****************************************/

$('#select_deselect_booking_sheets').click(function(event) {
    if (this.checked) {
        $('input[name="booking_sheets[]"]').prop("checked", true);
    } else {
        $('input[name="booking_sheets[]"]').prop("checked", false);
    }
});

$('#print_booking_sheets').click(function(event) {

    var generate_booking_sheet_checkbox_ids = [];
    $('input[name="booking_sheets[]"]:checked').each(function() {
        generate_booking_sheet_checkbox_ids.push(this.value);
    });

    if (generate_booking_sheet_checkbox_ids.length > 0) {

        $('#error-message span').text('');
        $('#error-message').hide();

        // submit form
        $('#print_booking_sheet_form').submit();

    } else {

        $('#error-message span').text('You can\'t print Booking Sheet without select Booking Sheet, Please select at least at list one Booking Sheet');
        $('#error-message').show();

    }

});

$('#email_booking_sheets').click(function(event) {

    var generate_booking_sheet_checkbox_ids = [];
    $('input[name="booking_sheets[]"]:checked').each(function() {
        generate_booking_sheet_checkbox_ids.push(this.value);
    });

    if (generate_booking_sheet_checkbox_ids.length > 0) {

        $('#error-message span').text('');
        $('#error-message').hide();

        $('#booking_sheet_ids_for_email').val(generate_booking_sheet_checkbox_ids);
        $('#booking-sheets-emails-div').html('');
        $('#open_email_booking_sheet_info_box').trigger('click');

    } else {

        $('#error-message span').text('You can\'t send Booking Sheet without select Booking Sheet, Please select at least one Booking Sheet');
        $('#error-message').show();

    }

});

/*
 * addMoreEmails - add more emails to send bookng sheet
 * @returns {undefined}
 */
function addMoreEmails() {

    var divLength = $('#booking-sheets-emails-div > div').length;
    var numRow = parseInt(divLength) + 1;
    var text = '';
    text = text + '<div class="row margin-top-10" id="email_details_div_' + numRow + '">' +
            '<label class="col-md-3 control-label"><br /></label>' +
            '<div class="col-md-3">' +
            '<div class="input text required"><input type="text" id="bookingsheets-name" placeholder="Name" required="required" class="form-control" autofocus="On" name="BookingSheetsEmail[' + numRow + '][name]"></div>' +
            '</div>' +
            '<div class="col-md-4">' +
            '<div class="input email required"><input type="email" id="bookingsheets-email" placeholder="Email" required="required" class="form-control" autofocus="On" name="BookingSheetsEmail[' + numRow + '][email]"></div>' +
            '</div>' +
            '<div class="col-md-1  col-md-pop-1">' +
            '<a title="Remove" onclick="removeEmails(' + numRow + ')" href="javascript:;" class="btn btn-icon-only red">' +
            '<i class="glyphicon glyphicon-remove"></i>' +
            '</a>' +
            '</div>' +
            '</div>';

    $('#booking-sheets-emails-div').append(text);

}

/*
 * removeEmails - remove email which was added for send booking sheet
 * @param {type} num
 * @returns {undefined}
 */
function removeEmails(numRow) {

    $('#email_details_div_' + numRow).remove();

}

/**
 * Cancel Booking Sheet
 */
$('#cancel_booking_sheet').click(function(event) {
    var booking_sheet_checkbox_ids = [];
    $('input[name="booking_sheets[]"]:checked').each(function() {
        booking_sheet_checkbox_ids.push(this.value);
    });

    if (booking_sheet_checkbox_ids.length > 0) {
        $('#error-message span').text('');
        $('#error-message').hide();
        $('#booking_sheet_hidden_ids').val(booking_sheet_checkbox_ids);
        $('#cancel_booking_sheet_form').submit();
    } else {
        $('#error-message span').text('You can\'t Cancel Booking Sheet without select Booking Sheet, Please select at least one Booking Sheet');
        $('#error-message').show();
    }

});

/**************************************** End script for index action of bookingsheets  ****************************************/