$('#select_deselect_pourchase_ordes').click(function(event) {
    if (this.checked) {
        $('input[name="purchase_order_id[]"]').prop("checked", true);
    } else {
        $('input[name="purchase_order_id[]"]').prop("checked", false);
    }
});

/**
 * Send selected purchase orders to myob
 */
$('#send_to_myob').click(function(event) {

    var po_ids = [];
    $('input[name="purchase_order_id[]"]:checked').each(function() {
        po_ids.push(this.value);
    });

    if (po_ids.length > 0) {

        $('#error-message span').text('');
        $('#error-message').hide();

        $('#open_send_to_myob_popup').trigger('click');

    } else {
        $('#error-message span').text('Please select at least one Purchase Order to send myob.');
        $('#error-message').show();
    }
});

/**
 * Send PO's finaly to Myob
 */
$('#send_to_myob_final').click(function(event) {

    var po_ids = [];
    $('input[name="purchase_order_id[]"]:checked').each(function() {
        po_ids.push(this.value);
    });

    $.ajax({
        url: '/purchase_orders/send_to_myob',
        type: 'post',
        dataType: 'json',
        data: '&po_ids=' + po_ids,
        beforeSend: function() {
            showLoading();
        },
        complete: function() {
            hideLoading();
        },
        success: function(resultArr) {
            location.reload('refresh');
        }
    });

});

/**
 * print selected purchase orders
 */
$('#print_purchase_orders').click(function(event) {

    var po_ids = [];
    $('input[name="purchase_order_id[]"]:checked').each(function() {
        po_ids.push(this.value);
    });

    if (po_ids.length > 0) {

        $('#error-message span').text('');
        $('#error-message').hide();

        // submit form
        $('#print_purchase_orders_form').submit();

    } else {

        $('#error-message span').text('You can\'t print Purchase Order without select Purchase Order, Please select at least one Purchase Order');
        $('#error-message').show();

    }

});

/**
 * Send selected purchase orders to Quick Book
 */
// Archive selected Purchase Orders
$('#archive_purchase_order_final').click(function(event) {
    var po_ids = [];
    $('input[name="purchase_order_id[]"]:checked').each(function() {
        po_ids.push(this.value);
    });

    if (po_ids.length > 0) {

        $('#error-message span').text('');
        $('#error-message').hide();
        
        $('<form action="/purchase_orders/archive" method="POST" onsubmit="showLoading()"/>')
                .append($('<input type="hidden" name="po_ids" value="' + po_ids + '">'))
                .appendTo($(document.body)) //it has to be added somewhere into the <body>
                .submit();

    } else {
        $('.archive-purchase-order-btn-cancel').trigger('click');
        $('#error-message span').text('Please select at least one Purchase Order to Cancel');
        $('#error-message').show();
    }
});

// check Purchase Orders is sected to open Archive Action Confirmation
$('#archive_purchase_orders').click(function(event) {

    var po_ids = [];
    $('input[name="purchase_order_id[]"]:checked').each(function() {
        po_ids.push(this.value);
    });

    if (po_ids.length > 0) {

        $('#error-message span').text('');
        $('#error-message').hide();
        
        $('#archive_purchase_order_popup').trigger('click');

    } else {
        $('#error-message span').text('Please select at least one Purchase Order to Cancel');
        $('#error-message').show();
    }
});

// Archive Popup close button action
$('.archive-purchase-order-btn-cancel').click(function(event) {
    $('input[name="purchase_order_id[]"]:checked').each(function() {
        $(this).attr('checked', false);
    });
    $('#select_deselect_pourchase_ordes').attr('checked', false);
});

/**
 * Send selected purchase orders to Quick Book
 */
$('#send_to_quick_book').click(function(event) {

    var po_ids = [];
    $('input[name="purchase_order_id[]"]:checked').each(function() {
        po_ids.push(this.value);
    });

    if (po_ids.length > 0) {

        $('#error-message span').text('');
        $('#error-message').hide();

        $('<form action="/quick_books/index" method="POST" onsubmit="showLoading()"/>')
                .append($('<input type="hidden" name="po_ids" value="' + po_ids + '">'))
                .appendTo($(document.body)) //it has to be added somewhere into the <body>
                .submit();

    } else {
        $('#error-message span').text('Please select at least one Purchase Order to send Quick Book');
        $('#error-message').show();
    }
});


/**
 * print selected purchase orders
 */
$('#send_fulfilment_advice').click(function(event) {

    var po_ids = [];
    $('input[name="purchase_order_id[]"]:checked').each(function() {
        po_ids.push(this.value);
    });

    if (po_ids.length > 0) {

        $('#error-message span').text('');
        $('#error-message').hide();

        // submit form
        $('#fulfilment_advice_purchase_orders_form').submit();

    } else {

        $('#error-message span').text('You can\'t send Fulfilment Advice without select Purchase Order, Please select at least one Purchase Order');
        $('#error-message').show();

    }

});

/**
 * setActualDeliveryDate - This method set Actual Delivery Date
 * @param {type} id
 * @param {type} actaulDeliveryDate
 * @param {type} purchaseOrderNumber
 * @returns {undefined}
 */
function setActualDeliveryDate(id, actaulDeliveryDate, purchaseOrderNumber) {
    $('#response-msg').addClass('hide');
    $('#purchaseorder-actual-delivery-date').val('');
    $('#purchaseorder-id').val(id);
    $('#purchase_order_number_text').text('Set the Actual Delivery Date of ' + purchaseOrderNumber + ' that Purchase Order');
}

/**
 * Set Actual Delivery Date
 */
$('#set_actual_delivery_date').click(function(event) {

    var purchaseOrderIdCheckboxIds = [];
    $('input[name="purchase_order_id[]"]:checked').each(function() {
        purchaseOrderIdCheckboxIds.push(this.value);
    });
    $('#response-msg').addClass('hide');
    $('#purchaseorder-actual-delivery-date').val('');
    if (purchaseOrderIdCheckboxIds.length > 0) {

        $('#error-message span').text('');
        $('#error-message').hide();

        //Set internal information
        $('#purchaseorder-id').val(purchaseOrderIdCheckboxIds);
        if (purchaseOrderIdCheckboxIds.length > 1) {
            $('#purchase_order_number_text').text('Set the actual delivery date of selected purchase orders');
        } else {
            $('#purchase_order_number_text').text('Set the actual delivery date of selected purchase order');
        }

        // trigger open delivery date popup button
        $('#set_actual_delivery_date_window').trigger('click');

    } else {

        $('#error-message span').text('You can\'t set actual delivery date without select purchase order, Please select at least one purchase order');
        $('#error-message').show();

    }

});

/**
 * Set Delivery Date
 * @returns {undefined}
 */
function setDeliveryDate() {
    $('#response-msg').addClass('hide');
//    $('#purchaseorder-actual-delivery-date').val('');
    $('#update_actaul_delivery_date').removeAttr('disabled');
    $.ajax({
        url: '/purchase_orders/check_delivery_date',
        type: 'post',
        data: 'po_ids=' + $('#purchaseorder-id').val()+
              '&purchaseorderActualDeliveryDate='+ $('#purchaseorder-actual-delivery-date').val(),
        beforeSend: function() {
            showLoading();
        },
        complete: function() {
            hideLoading();
        },
        success: function(response) {
            if(response){
                $('#response-msg').html('');
                $('#response-msg').addClass('hide');
                $('#update_actaul_delivery_date').removeAttr('disabled');
                
            }else{
                $('#update_actaul_delivery_date').attr('disabled','disabled');
                $('#response-msg').html('One or multiple of the selected POs has higher Fulfilment Advice Date than selected Deliver Date, which is not allowed by Bunnings. Please ensure you select the Delivery date at least 1 day later than the latest Fulfilment Advice Receipt date for the selected POs');
                $('#response-msg').removeClass('hide');
                
            }
        }
    });
}