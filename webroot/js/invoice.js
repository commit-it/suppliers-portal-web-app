/********************************************* Start common functions *********************************************/

/**
 *  Check variable isset or not 
 *  @param {type} a: name of the field which you want to check either exist or not
 */
isset = function(a) {
    if ((typeof (a) === 'undefined') || (a === null))
        return false;
    else
        return true;
};

/**
 * Get Sub String
 * @param {type} str
 * @returns {SubString with start 0 and end with 5th character} 
 */
function getSubString(str) {
    var res = '';
    if (str.length > 5)
    {
        res = str.substring(0, 5) + '...';
    }
    else
    {
        res = str;
    }
    return res;
}
/**************************************** End common functions *************************************************************/


/**************************************** Start script for edit  action of invoice  ****************************************/

/**
 *  Inline edit functionality for table row
 *  @param {type} thisVal: object of selected td
 *  @param {type} arrayOfTableFields
 *  @param {type} invoiceItemId
 */
function inlineEditOrCancel(thisVal, arrayOfTableFields, invoiceItemId) {
    /* Set variable for selected tr object  */
    var row = $(thisVal).parents('tr');

    /* Set variable for selected actipn */
    var typeOfAction = $(thisVal).text();

    /* Set variable for type of action for custom attribute */
    var typeOfActionCustomAttr = $(thisVal).attr('custattr');

    /* 
     *  If typeOfActionCustomAttr is edit then we have to display editatble fields.
     *  If typeOfActionCustomAttr is cancel then we have to hide editatble fields.
     */

    /* If typeOfActionCustomAttr is edit then show editatble fields for passed arrayOfTableFields fileds */
    $.each(arrayOfTableFields, function(index, value) {
        /* Set coulmn number and required fields */
        var columnNumber = isset(value.col_num) ? value.col_num : '';
        var isRequired = isset(value.required) ? value.required : '';
        var field = isset(value.field) ? value.field : '';
        var inputId = field + "_new_" + invoiceItemId;
        if (isset(typeOfActionCustomAttr) && typeOfActionCustomAttr == 'edit') {
            var columnVal = $(row.children().get(columnNumber)).children('.edit-span').text();

            /* Get the value of passed field and put it into editable field */
            $(row.children().get(columnNumber)).children('.edit-span').html('<input type="text" name=' + field + '_new[] value="' + columnVal + '" id = "' + inputId + '" required="' + isRequired + '"  onblur="calculateNetAmount(' + invoiceItemId + ');" class="form-control" />');

            /* Change the selected action to Cancel */
            $(thisVal).switchClass('glyphicon-edit','glyphicon-ok-circle');
            $(thisVal).attr("title", "Save");
            
            /* Change the selected custattr attribute value to cancel */
            $(thisVal).attr('custattr', 'save');

            /* Change delete action to cancel */
            $('#delete_cancel_invoice_items_' + invoiceItemId).switchClass('glyphicon-trash','glyphicon-remove-circle');
            $('#delete_cancel_invoice_items_' + invoiceItemId).attr('custdeletecancelattr', 'cancel');
            $('#delete_cancel_invoice_items_' + invoiceItemId).attr("title", "Cancel");
        }

        /* If typeOfActionCustomAttr is cancel then hide editatble fields for passed arrayOfTableFields fileds */
        if (isset(typeOfActionCustomAttr) && typeOfActionCustomAttr == 'save') {
            if ($("#requested_quantity_new_" + invoiceItemId).val() == '') {
                $('#requested_quantity_new_' + invoiceItemId).css('border-color', 'red');
                return false;
            }

            $('#requested_quantity_new_' + invoiceItemId).css('border-color', '');

            /* Get the existing value and put it into changed table cell */
            $(row.children().get(columnNumber)).children('.edit-span').html($("#requested_quantity_new_" + invoiceItemId).val());
            $('#hidden_requested_quantity_' + invoiceItemId).val($("#requested_quantity_new_" + invoiceItemId).val());
            $('#hidden_requested_quantity_' + invoiceItemId).val($("#requested_quantity_" + invoiceItemId).val());

            /* Change the selected action to Edit */
            $(thisVal).switchClass('glyphicon-ok-circle','glyphicon-edit');

            /* Change the selected custattr attribute value to edit */
            $(thisVal).attr('custattr', 'edit');
            
            $(thisVal).attr('title', 'Edit');
            
            /* Replace hidden values */
            $('#hidden_net_price_' + invoiceItemId).val($("#net_price_" + invoiceItemId).val());
            $('#hidden_net_amount_' + invoiceItemId).val($("#net_amount_" + invoiceItemId).val());
            $('#hidden_gst_amount_' + invoiceItemId).val($("#gst_amount_" + invoiceItemId).val());
            $('#hidden_gst_percentage_' + invoiceItemId).val($("#gst_percentage_" + invoiceItemId).val());

            /* Change cancel action to delete */
            $('#delete_cancel_invoice_items_' + invoiceItemId).switchClass('glyphicon-remove-circle', 'glyphicon-trash');
            $('#delete_cancel_invoice_items_' + invoiceItemId).attr('custdeletecancelattr', 'delete');
            $('#delete_cancel_invoice_items_' + invoiceItemId).attr('title', 'Delete');
        }
    });

}

/**
 * Replace new inoice with old invoice
 * @param {type} invoiceItemId
 * @returns {undefined}
 */
function cancelInvoiceItem(invoiceItemId, thisVal, flag_for_new_invoice_item) {
    var hiddenRequestedQuantity = isset($("#hidden_requested_quantity_" + invoiceItemId).val()) && $("#hidden_requested_quantity_" + invoiceItemId).val() > 0 ? $("#hidden_requested_quantity_" + invoiceItemId).val() : 0;
    var hiddenNetAmount = isset($("#hidden_net_amount_" + invoiceItemId).val()) && $("#hidden_net_amount_" + invoiceItemId).val() > 0 ? $("#hidden_net_amount_" + invoiceItemId).val() : 0;
    var hiddenGstAmount = isset($("#hidden_gst_amount_" + invoiceItemId).val()) && $("#hidden_gst_amount_" + invoiceItemId).val() > 0 ? $("#hidden_gst_amount_" + invoiceItemId).val() : 0;
    var subTotalInputHidden = isset($("#sub-total-input-hidden").val()) && $("#sub-total-input-hidden").val() > 0 ? $("#sub-total-input-hidden").val() : 0;

    /* Get the existing value and put it into changed table cell */
    $('#requested_quantity_new_span_' + invoiceItemId).html(hiddenRequestedQuantity);

    /* Change requested quantity as it is */
    $('#requested_quantity_' + invoiceItemId).val(hiddenRequestedQuantity);

    /* Change net amout as it is*/
    $("#net_amount_span_" + invoiceItemId).html(hiddenNetAmount);
    $("#net_amount_" + invoiceItemId).val(hiddenNetAmount);

    /* Change gst amout as it is*/
    $("#gst_amount_span_" + invoiceItemId).html(hiddenGstAmount);
    $("#gst_amount_" + invoiceItemId).val(hiddenGstAmount);

    /* Change sub total as it is*/
    $("#sub-total").html(subTotalInputHidden);
    $("#sub-total-input").val(subTotalInputHidden);

    /* Change the selected action to Edit */
    $('#delete_cancel_invoice_items_' + invoiceItemId).switchClass('glyphicon-remove-circle', 'glyphicon-trash');
    $('#edit_save_cancel_invoice_items_' + invoiceItemId).switchClass('glyphicon-ok-circle','glyphicon-edit');
     
    $('#edit_save_cancel_invoice_items_' + invoiceItemId).attr('custattr', 'edit');
    $('#edit_save_cancel_invoice_items_' + invoiceItemId).attr('title', 'Edit');

    $('#delete_cancel_invoice_items_' + invoiceItemId).attr('custdeletecancelattr', 'delete');
    $('#delete_cancel_invoice_items_' + invoiceItemId).attr('title', 'Delete');

}

/**
 *  Calculate Net Amount
 *  @param {type} invoiceItemId
 *  @param {type} flag for check calculateNetAmount for new invoice amount or existing
 */
function calculateNetAmount(invoiceItemId, flag) {
    /* Define variables */
    var requestedQuantity = isset($('#requested_quantity_new_' + invoiceItemId).val()) && $('#requested_quantity_new_' + invoiceItemId).val() > 0 ? parseFloat($('#requested_quantity_new_' + invoiceItemId).val()) : 0;

    var netPrice = isset($('#net_price_' + invoiceItemId).val()) && $('#net_price_' + invoiceItemId).val() > 0 ? parseFloat($('#net_price_' + invoiceItemId).val()) : 0;

    var gstPercentage = isset($('#gst_percentage_' + invoiceItemId).val()) && $('#gst_percentage_' + invoiceItemId).val() > 0 ? parseFloat($('#gst_percentage_' + invoiceItemId).val()) : 0;

    /* requestedQuantity must be grater than 0 */
    if (requestedQuantity < 1) {
        $('#requested_quantity_new_' + invoiceItemId).val('');
        return false;
    }

    /* Reset inputs */
    resetInput(invoiceItemId);

    /* Calculate net amount */
    var netAmount = (requestedQuantity * netPrice).toFixed(2);

    /* Replace existing net amount */
    $('#net_amount_span_' + invoiceItemId).html(netAmount);
    $('#net_amount_' + invoiceItemId).val(netAmount);

    /* Calculate gst amount */
    var gstAmount = ((netAmount * gstPercentage) / 100).toFixed(2);

    /* Replace existing gst amount */
    $('#gst_amount_span_' + invoiceItemId).text(gstAmount);
    $('#gst_amount_' + invoiceItemId).val(gstAmount);

    /* Replace the invoice */
    $('#requested_quantity_' + invoiceItemId).val(requestedQuantity);

    /* If calculate amount for new invoice item then add requested quantity in hidden input field */
    if (flag == 'new_invoice_item') {
        $('#hidden_requested_quantity_' + invoiceItemId).val(requestedQuantity);
        $('#hidden_net_amount_' + invoiceItemId).val(netAmount);
        $('#hidden_gst_amount_' + invoiceItemId).val(gstAmount);
    }

    /* Calculate sub total */
    calculateSubTotal();

}

/**
 *  Calculate sub total
 */
function calculateSubTotal() {
    var subTotal = 0;

    /* Calculate each sub total of invoice item */
    $('#invoice-items-table tr .net-amount-input').each(function() {
        var inputId = $(this).attr('id');
        var splitInput = inputId.split('_');

        /**
         *  This condition is checked because we are getting net amount based on class which is not secured because 
         *  in case '.net-amount-input' class is used for another input then we got wrong amount that's why we are 
         *  compaired original net amount input id with this input id
         */
        if ((isset(splitInput[0]) && splitInput[0] == "net") && (isset(splitInput[1]) && splitInput[1] == "amount")) {
            subTotal += isset($(this).val()) && $(this).val() > 0 ? parseFloat($(this).val()) : 0;
        }
    });

    /* Calculate final sub total */
    var subTotalFinal = (subTotal).toFixed(2);

    /* Replace old subtotal with new calculated */
    $('#sub-total-input').val(subTotalFinal);
    $('#sub-total').text(subTotalFinal);
    
    // calculate sub total after discount
    var discount_percentage = isset($('#discount_percentage').val()) && $('#discount_percentage').val() > 0 ? parseFloat($('#discount_percentage').val()) : 0;
    var tax = isset($('#tax').val()) && $('#tax').val() > 0 ? parseFloat($('#tax').val()) : 0;
    if (discount_percentage > 0) {
        
        var discountAmount = (subTotalFinal * discount_percentage) / 100;
        var subtotalAfterDiscount = subTotalFinal - discountAmount;
        var subtotalAfterDiscountFinal = (subtotalAfterDiscount).toFixed(2);
        $('#sub_total_after_discount').text(subtotalAfterDiscountFinal);
        
        // calculate total Include GST
        var gstAmount = (subtotalAfterDiscountFinal * tax) / 100;
        var totalIncludeGst = parseFloat(subtotalAfterDiscountFinal) + parseFloat(gstAmount);
        var totalIncludeGstFinal = (totalIncludeGst).toFixed(2);
        $('#total_include_gst').text(totalIncludeGstFinal);
    } else {
        
        $('#sub_total_after_discount').text(subTotalFinal);
        
        // calculate total Include GST
        var gstAmount = (subTotalFinal * tax) / 100;
        var totalIncludeGst = parseFloat(subTotalFinal) + parseFloat(gstAmount);
        var totalIncludeGstFinal = (totalIncludeGst).toFixed(2);
        $('#total_include_gst').text(totalIncludeGstFinal);
    }
}

/**
 *  Calculate Gst Amount For Invoice Items
 */
function calculateGstAmountForInvoiceItems() {
    /* Define variables */
    var netAmount = 0;
    var tax = isset($('#tax').val()) && $('#tax').val() > 0 ? parseFloat($('#tax').val()) : 0;

    /* Calculate each sub total of invoice item */
    $('#invoice-items-table tr .net-amount-input').each(function() {
        var inputId = $(this).attr('id');
        var splitInput = inputId.split('_');

        /**
         *  This condition is checked because we are getting net amount based on class which is not secured because 
         *  in case '.net-amount-input' class is used for another input then we got wrong amount that's why we are 
         *  compaired original net amount input id with this input id
         */
        if ((isset(splitInput[0]) && splitInput[0] == "net") && (isset(splitInput[1]) && splitInput[1] == "amount")) {
            var invoiceItemId = isset(splitInput[2]) && splitInput[2] > 0 ? splitInput[2] : '';
            netAmount = isset($(this).val()) && $(this).val() > 0 ? parseFloat($(this).val()) : 0;

            /* Calculate gst amount */
            var gstAmount = ((netAmount * tax) / 100).toFixed(2);

            /* Replace existing gst amount and gst percentage */
            if (invoiceItemId != '') {
                $('#gst_amount_span_' + invoiceItemId).text(gstAmount);
                $('#gst_amount_' + invoiceItemId).val(gstAmount);
                $("#hidden_gst_amount_" + invoiceItemId).val(gstAmount);

                $('#gst_percentage_span_' + invoiceItemId).text(tax);
                $('#gst_percentage_' + invoiceItemId).val(tax);
                $("#hidden_gst_percentage_" + invoiceItemId).val(tax);
            }
        }
    });

    /* Calculate sub total */
    calculateSubTotal();
}

/**
 *  Delete InvoiceItems
 *  @param {type} invoiceItemId
 *  @param {type} thisVal
 *  @param {type} flag_for_new_invoice_item, thisVal
 *  @output
 *  delete selected row
 *  add deleted invoice item id in hidden input field for deleting this invoice item in database
 */
function deleteInvoiceItems(invoiceItemId, thisVal, flag_for_new_invoice_item) {
    var result = window.confirm('Are you sure?');
    if (result == true) {
        var deletedInvoiceItemsArr = isset($('#deleted_invoice_items').val()) && $('#deleted_invoice_items').val() != '' ? [$('#deleted_invoice_items').val()] : [];
        var issetFlagforNewInvoiceItem = isset(flag_for_new_invoice_item) && flag_for_new_invoice_item != '' ? flag_for_new_invoice_item : '';

        var invoiceItemsGtin = $('#invoice_items_gtin_arr').val();
        var invoiceItemsGtinArray = isset(invoiceItemsGtin) && invoiceItemsGtin != '' ? invoiceItemsGtin.split(',') : [];

        var gtin = $('#gtin_hidden_input_' + invoiceItemId).val();

        /* If selected row is new added invoice item then doesn't need to add this id in hidden input field*/
        if (issetFlagforNewInvoiceItem != 'invoice_item_new') {
            deletedInvoiceItemsArr.push(invoiceItemId);
            $('#deleted_invoice_items').val(deletedInvoiceItemsArr);
        }

        /* Remove gtin number from invoiceItemsGtinArray because this array is used for checking duplicate plant */
        if ($.inArray(gtin, invoiceItemsGtinArray) > -1) {
            invoiceItemsGtinArray.splice($.inArray(gtin, invoiceItemsGtinArray), 1);
            $('#invoice_items_gtin_arr').val(invoiceItemsGtinArray);
        }


        /* If invoice item new then decrease the new_invoice_items_count */
        if (issetFlagforNewInvoiceItem == 'invoice_item_new') {
            var decreaseInvoiceItemsCount = isset($('#new_invoice_items_count').val()) && $('#new_invoice_items_count').val() > 0 ? $('#new_invoice_items_count').val() : 0;
            decreaseInvoiceItemsCount--;
            $('#new_invoice_items_count').val(decreaseInvoiceItemsCount);
        } else {
            /* Check Gtin Is Exist In Master Items */
            checkGtinIsExistInMasterItems(invoiceItemId);
        }
        
        /* Remove selected tr */
        $(thisVal).closest('tr').remove();
    }
}

/**
 * Remove newly added invoice item row
 * @param {type} rowCount
 * @param {type} thisVal
 * @param {type} flag
 * @param {type} flagForDeleteActionInvoiceItem
 * @returns {undefined}
 */
function removeNewInvoiceItem(rowCount, thisVal, flag, flagForDeleteActionInvoiceItem) {
    /* Remove selected tr */
    $(thisVal).closest('tr').remove();

    /* Calculate sub total */
    calculateSubTotal();

    /* If invoice item new then decrease the new_invoice_items_count */
    var decreaseInvoiceItemsCount = isset($('#new_invoice_items_count').val()) && $('#new_invoice_items_count').val() > 0 ? $('#new_invoice_items_count').val() : 0;
    decreaseInvoiceItemsCount--;
    $('#new_invoice_items_count').val(decreaseInvoiceItemsCount);
}

/**
 * This function is used for delete or cancel action
 * @param {type} invoiceItemId
 * @param {type} thisVal
 * @param {type} flag_for_new_invoice_item
 * @param {type} flag_for_delete_or_cancel_action
 * @returns {undefined}
 */
function deleteOrCancelInvoiceItems(invoiceItemId, thisVal, flag_for_new_invoice_item) {
    var flag_for_delete_or_cancel_action = $('#delete_cancel_invoice_items_' + invoiceItemId).attr('custdeletecancelattr');
    var issetFlagforNewInvoiceItem = isset(flag_for_new_invoice_item) && flag_for_new_invoice_item != '' ? flag_for_new_invoice_item : '';

    /* If this function call for delete then call delete function */
    if (flag_for_delete_or_cancel_action == 'delete') {
        deleteInvoiceItems(invoiceItemId, thisVal, flag_for_new_invoice_item);

    }

    /* If this function call for cancel then call cancel function */
    if (flag_for_delete_or_cancel_action == 'cancel') {
        cancelInvoiceItem(invoiceItemId, thisVal, flag_for_new_invoice_item);
    }

    /* After remove this invoice items recalculate sub total */
    calculateSubTotal();
}
/**
 *  Add invoice items row
 *  @param {int} invoiceId
 *  Temparory hardcoded value: unit_of_measure = EA, pack_quantity = 1
 */
function addInvoiceItemRow(invoiceId) {
    var html = '';
    var masterItems = JSON.parse($('#master_items_hidden').val());
    var invoiceItemsIds = $('#invoice_items_ids_arr').val();
    var invoiceItemTax = isset($('#tax').val()) && $('#tax').val() > 0 ? $('#tax').val() : 0;
    var flag = "'invoice_item_new'";
    var explodeInvoiceIds = invoiceItemsIds.split(',');
    var maxId = Math.max.apply(Math, explodeInvoiceIds);
    var rowCount = parseInt(maxId) + 1;
    var purchaseOrderId = $('#purchase_order_id_hidden').val();
    var flagForNewInvoiceItem = "'new_invoice_item'";
    var flagForDeleteActionInvoiceItem = "'delete'";

    /**
     *  Check first master items are remaing or not
     */
    var checkMasterItemsExist = checkMasterItemsNotDuplicate();
    if (checkMasterItemsExist > 0) {
        return false;
    }

    /* Add this rowcount in invoice_items_ids_arr field array */
    explodeInvoiceIds.push(rowCount);
    $('#invoice_items_ids_arr').val(explodeInvoiceIds);
    
    /* Get Invoice Items Description Dropdown */
    var descriptionDropDown = getInvoiceItemsDescriptionDropdown(rowCount);
    html += '<tr>';
    html += '<td>';
    html += '<span class="edit-span" id="description_dropdown_span_' + rowCount + '">';
    html += descriptionDropDown;
    html += '</span>';
    html += '<input type="hidden" name="invoice_items_new[' + rowCount + '][description]" id="hidden_description_' + rowCount + '"  />';
    html += '<input type="hidden" name="invoice_items_new[' + rowCount + '][description]" id="description_' + rowCount + '"  />';
    html += '<input type="hidden" name="invoice_items_new[' + rowCount + '][invoice_id]" value="' + invoiceId + '" />';
    html += '<input type="hidden" name="invoice_items_new[' + rowCount + '][purchase_order_id]" value="' + purchaseOrderId + '"/>';
    html += '<input type="hidden" name="invoice_items_new[' + rowCount + '][order_line_number]" />';
    html += '<input type="hidden" name="invoice_items_new[' + rowCount + '][reference_purchase_order_line_number]" />';
    html += '<input type="hidden" name="invoice_items_new[' + rowCount + '][gtin]" id="gtin_hidden_input_' + rowCount + '" />';
    html += '<input type="hidden" name="invoice_items_new[' + rowCount + '][unit_of_measure]" value="EA"/>';
    html += '<input type="hidden" name="invoice_items_new[' + rowCount + '][pack_quantity]" value="1"/>';
    html += '</td>';

    html += '<td>';
    html += '<span class="edit-span" id="requested_quantity_new_span_' + rowCount + '">';
    html += '<input type="text" id="requested_quantity_new_' + rowCount + '" onblur="calculateNetAmount(' + rowCount + ', ' + flagForNewInvoiceItem + ');" required="true" class = "hide-cls form-control" />';
    html += '</span>';
    html += '<input type="hidden" id="hidden_requested_quantity_' + rowCount + '" />';
    html += '<input type="hidden" id="requested_quantity_' + rowCount + '" name= invoice_items_new[' + rowCount + '][requested_quantity] />';
    html += '</td>';

    html += '<td>';
    html += '<span class="edit-span" id="net_price_span_' + rowCount + '" ></span>';
    html += '<input type="hidden" id="net_price_' + rowCount + '"  name="invoice_items_new[' + rowCount + '][net_price]" />';
    html += '<input id="hidden_net_price_' + rowCount + '" class="net-price-input" type="hidden" />';
    html += '</td>';

    html += '<td>';
    html += '<span class="edit-span" id="net_amount_span_' + rowCount + '"></span>';
    html += '<input id="net_amount_' + rowCount + '" type="hidden" class="net-amount-input" name="invoice_items_new[' + rowCount + '][net_amount]" />';
    html += '<input id="hidden_net_amount_' + rowCount + '" class="net-amount-input" type="hidden" />';
    html += '</td>';

    html += '<td>';
    html += '<span class="edit-span" id="gst_amount_span_' + rowCount + '"></span>';
    html += '<input type="hidden" class="net-amount-input" name="invoice_items_new[' + rowCount + '][gst_amount]" id="gst_amount_' + rowCount + '" />';
    html += '<input type="hidden" class="net-amount-input" id="hidden_gst_amount_' + rowCount + '" />';
    html += '</td>';

    html += '<td>';
    html += '<span class="edit-span" id="gst_percentage_span_' + rowCount + '">' + invoiceItemTax + '</span>';
    html += '<input type="hidden" name="invoice_items_new[' + rowCount + '][gst_percentage]" id="gst_percentage_' + rowCount + '" value="' + invoiceItemTax + '" />';
    html += '<input type="hidden" id="hidden_gst_percentage_' + rowCount + '" value="' + invoiceItemTax + '" />';
    html += '</td>';

    html += '<td class="actions">';
    html += '<a onclick="addInvoiceItems(' + rowCount + ',this);" href="javascript:void(0);" class="glyphicon glyphicon-ok-circle link-deco" title="Save"></a>';
    html += '<a onclick="removeNewInvoiceItem(' + rowCount + ',this, ' + flag + ',' + flagForDeleteActionInvoiceItem + ' );" id="delete_cancel_invoice_items_' + rowCount + '"  href="javascript:void(0);" class="padding-lft-cls glyphicon glyphicon-remove-circle link-deco" title="Cancel"></a></td>';
    html += "</tr>";

    /* Add this html to top of the tbody first row*/
    if($('#invoice-items-table > tbody > tr:first').length > 0 ){
        $('#invoice-items-table > tbody > tr:first').before(html);
    }else{
        $('#invoice-items-table > tbody').before(html);
    }

    /* If records not found tr is availabel then delete this row after adding new invoice items */
    if ($('#not_items_tr').length > 0) {
        $('table#invoice-items-table tr#not_items_tr').remove();
    }
}

/**
 * Get Invoice Items Description 
 * @param {number} rowCount
 * @returns {html} 
 */
function getInvoiceItemsDescriptionDropdown(rowCount) {
    var masterItems = JSON.parse($('#master_items_hidden').val());
    var html = '';
    var description = '';
    var gtin = '';
    var netPrice = '';

    /* Creat dropdown for description */
    html += '<select name=description_dropdown[] onchange="changeNetPriceOfNewInvoiceItems(' + rowCount + ');" id = description_dropdown_' + rowCount + ' required="required" class="form-control">';
    html += '<option value="">Select</option>';

    if (isset(masterItems)) {
        $.each(masterItems, function(key, value) {
            if ($.trim(value.name) != '') {
                description = isset(value.name) ? value.name : '';
                gtin = isset(value.bar_code) ? value.bar_code : '';
                netPrice = isset(value.retail_price) && value.retail_price > 0 ? value.retail_price : 0;
                html += '<option value="' + gtin + ',' + netPrice + '">' + description + '</option>';
            }
        });
    }

    html += '</select>';

    return html;
}

/**
 * Change Net Price Of New Invoice Item of selected discription
 * @param {number} rowCount
 */
function changeNetPriceOfNewInvoiceItems(rowCount) {
    var selectedDescritionData = $('#description_dropdown_' + rowCount + ' option:selected').val();
    var selectedDescritionText = $('#description_dropdown_' + rowCount + ' option:selected').text();
    var invoiceItemsGtin = $('#invoice_items_gtin_arr').val();
    var invoiceItemsGtinArray = isset(invoiceItemsGtin) && invoiceItemsGtin != '' ? invoiceItemsGtin.split(',') : [];
    var splitDescriptionData = isset(selectedDescritionData) && selectedDescritionData != '' ? selectedDescritionData.split(',') : [];
    var gtin = isset(splitDescriptionData[0]) ? splitDescriptionData[0] : '';
    var netPrice = isset(splitDescriptionData[1]) && splitDescriptionData[1] > 0 ? splitDescriptionData[1] : 0;

    /* Reset inputs */
    resetInput(rowCount);


    /* Check the selected gtin number is alerady exist in table or not */
    var checkDuplicatePlant = isExistPlant(rowCount, gtin, invoiceItemsGtinArray);
    if (checkDuplicatePlant == true) {
        /* if selectedDescritionData is blank then stop here */
        if (selectedDescritionData == "") {
            /* Show requested quantity hidden input */
            blankAllValues(rowCount);
        } else {
            /* Show requested quantity hidden input */
            $('#requested_quantity_new_' + rowCount).removeClass('hide-cls');
        }

        /* Replace hidden gtin inout val. */
        $('#gtin_hidden_input_' + rowCount).val(gtin);

        /* Replace net price span text. */
        $('#net_price_span_' + rowCount).text(netPrice);

        /* Replace net price hidden input value. */
        $('#net_price_' + rowCount).val(netPrice);
        $('#hidden_net_price_' + rowCount).val(netPrice);

        /* Replace net description hidden input value. */
        $('#hidden_description_' + rowCount).val(selectedDescritionText);
        $('#description_' + rowCount).val(selectedDescritionText);

        /* Calculate net amount */
        calculateNetAmount(rowCount, 'new_invoice_item');
    }

}

/**
 * Add invoice items 
 * @param {type} rowCount
 * @param {type} thisVal
 * @returns {undefined} 
 */
function addInvoiceItems(rowCount, thisVal) {
    var description = $('#description_dropdown_' + rowCount + ' option:selected').html();
    var descriptionVal = $('#description_dropdown_' + rowCount + ' option:selected').val();
    var invoiceItemsGtin = $('#invoice_items_gtin_arr').val();
    var requestedQuantity = $('#requested_quantity_new_' + rowCount).val();
    var tableFieldsTds = $('#table_fields_td_num_hidden').val();
    var splitDescriptionData = isset(descriptionVal) && descriptionVal != '' ? descriptionVal.split(',') : [];
    var gtin = isset(splitDescriptionData[0]) ? splitDescriptionData[0] : '';
    var invoiceItemsGtinArray = isset(invoiceItemsGtin) && invoiceItemsGtin != '' ? invoiceItemsGtin.split(',') : [];
    var elements = [];
    var flag = "'invoice_item_new'";

    /*Reset inputs */
    resetInput(rowCount);

    if (descriptionVal != "" && requestedQuantity > 0) {
        /* Check the selected gtin number is alerady exist in table or not */
        var checkDuplicatePlant = isExistPlant(rowCount, gtin, invoiceItemsGtinArray);


        if (checkDuplicatePlant == true) {
            /* hide description drop down */
            $('#description_dropdown_' + rowCount).hide();
            /* Replace span value with description */
            $('#description_dropdown_span_' + rowCount).html(getSubString(description));

            /* hide requested quantity input */
            $('#requested_quantity_new_' + rowCount).hide();

            /* Replace span value with requested quantity */
            $('#requested_quantity_new_span_' + rowCount).html(requestedQuantity);

            /* Change the selected action to Cancel */
            elements.push($('<a>', {'id': 'edit_save_cancel_invoice_items_' + rowCount, onclick: 'inlineEditOrCancel(this,' + tableFieldsTds + ',' + rowCount + ')', class: "edit glyphicon glyphicon-edit link-deco", custattr: 'edit', title: 'Edit'}));
            $(thisVal).replaceWith(elements);

            /* Replace cancel with delete html */
            elements.push($('<a>', {'id': 'delete_cancel_invoice_items_' + rowCount, onclick: 'deleteOrCancelInvoiceItems(' + rowCount + ',this, ' + flag + ')', class: "edit padding-lft-cls glyphicon glyphicon-trash link-deco", custdeletecancelattr: 'delete', title: 'Delete'}));
            $('#delete_cancel_invoice_items_' + rowCount).replaceWith(elements);

            /* Add this gtin to invoice_items_gtin_arr */
            invoiceItemsGtinArray.push(gtin);
            $('#invoice_items_gtin_arr').val(invoiceItemsGtinArray);
        }

    } else {
        if (descriptionVal == '') {
            $('#description_dropdown_' + rowCount).css('border-color', 'red');
        }
        if (requestedQuantity == '') {
            $('#requested_quantity_new_' + rowCount).css('border-color', 'red');
        }
    }
}

/**
 * Check f already exist plant
 * @param {type} rowCount
 * @param {type} gtin
 * @param {type} invoiceItemsGtinArray
 * @returns {undefined}
 */
function isExistPlant(rowCount, gtin, invoiceItemsGtinArray) {
    /* Reset values before test is exist gtin number */
    $('#duplicate_description_span_error').addClass('hide-cls');
    if (jQuery.inArray(gtin, invoiceItemsGtinArray) > -1) {
        $('#duplicate_description_span_error').removeClass('hide-cls');
        /**
         * If duplicatedescription then blank all fiellds and recalculate sub total
         */
        blankAllValues(rowCount);

        $('#description_dropdown_' + rowCount + ' option').prop("selected", false);

        /* After remove this invoice items recalculate sub total */
        calculateSubTotal();

        return false;
    } else {
        return true;
    }
}

/**
 * If gtin not selected then blank all fiellds and recalculate sub total
 * @param {type} rowCount
 * @returns {undefined}
 */
function blankAllValues(rowCount) {
    $('#gtin_hidden_input_' + rowCount).val('');

    $('#requested_quantity_new_' + rowCount).addClass('hide-cls');
    $('#requested_quantity_new_' + rowCount).val('');
    $('#hidden_requested_quantity_' + rowCount).val('');
    $('#requested_quantity_' + rowCount).val('');


    $('#net_price_span_' + rowCount).text('');
    $('#net_price_' + rowCount).val('');
    $('#hidden_net_price_' + rowCount).val('');

    $('#net_amount_span_' + rowCount).text('');
    $('#net_amount_' + rowCount).val('');
    $('#hidden_net_amount_' + rowCount).val('');

    $('#gst_amount_span_' + rowCount).text('');
    $('#gst_amount_' + rowCount).val('');
    $('#hidden_gst_amount_' + rowCount).val('');

    $('#description_' + rowCount).val('');
    $('#hidden_description_' + rowCount).val('');
}

/*
 *  Check is all master items already added in invoice items
 */
function checkMasterItemsNotDuplicate() {
    var masterItemsGtin = $('#master_items_gtin_array_hidden').val();
    var masterItemsGtinArr = masterItemsGtin != '' ? masterItemsGtin.split(',') : [];
    var invoiceItemsGtin = $('#invoice_items_gtin_arr').val();
    var invoiceItemsGtinArr = invoiceItemsGtin != '' ? invoiceItemsGtin.split(',') : [];
    var error = 0;

    /* In this process check master items gtin which index present in invoice items gtin array */
    var remainingMasterItems = masterItemsGtinArr.filter(function(obj) {
        return invoiceItemsGtinArr.indexOf(obj) == -1;
    });
    /**
     *  increment new invoice items count 
     */
    var newInvoiceItemsCount = isset($('#new_invoice_items_count').val()) && $('#new_invoice_items_count').val() > 0 ? parseInt($('#new_invoice_items_count').val()) : 0;
    var totalNewInvoiceItemsCount = newInvoiceItemsCount++;

    $('#master_items_not_available_span_error').addClass('hide-cls');
    if (remainingMasterItems == "" || masterItemsGtinArr.length == totalNewInvoiceItemsCount) {
        $('#master_items_not_available_span_error').removeClass('hide-cls');
        setTimeout(function() {
            $('#master_items_not_available_span_error').addClass('hide-cls');
        }, 3000);
        error = 1;
    } else {
        $('#new_invoice_items_count').val(newInvoiceItemsCount);
    }
    return error;
}

/**
 * Reset input
 * @param {type} rowCount
 * @returns {undefined}
 */
function resetInput(rowCount) {
    $('#description_dropdown_' + rowCount).css('border-color', '');
    $('#requested_quantity_new_' + rowCount).css('border-color', '');
}


/**
 * Check all invoice items are saved or not
 */
$(document).ready(function() {
    $("form#invoice-edit-form").submit(function(e) {
        $('#not_save_editable_fields_error').addClass('hide-cls');
        $('#invoice-items-table tr').each(function() {
            var actionSaveTrue = $(this).find('td:last').children('a:first').hasClass( "glyphicon-ok-circle" );
            if (actionSaveTrue) {
                $('#not_save_editable_fields_error').removeClass('hide-cls');
                e.preventDefault(); //prevent default form submit
                $('html, body').animate({scrollTop: 0}, 'slow');
                return false //break from loop
            }
        });
        $(this).attr('onsubmit','showLoading()');
    });

    /* Check Master Items Not Duplicate */
    matchedMasterItemsWithInvoiceItemsGtin();
});

/**
 * Matched Master Items With Invoice Items Gtin
 * @returns {undefined}
 */
function matchedMasterItemsWithInvoiceItemsGtin() {
    var masterItemsGtin = $('#master_items_gtin_array_hidden').val();
    var masterItemsGtinArr = isset(masterItemsGtin) && masterItemsGtin != '' ? masterItemsGtin.split(',') : [];
    var invoiceItemsGtin = $('#invoice_items_gtin_arr').val();
    var invoiceItemsGtinArr = isset(invoiceItemsGtin) && invoiceItemsGtin != '' ? invoiceItemsGtin.split(',') : [];
    var error = 0;
    var machedInvoiceItems = [];

    /* 
     * In this process check master items gtin which index present in invoice items gtin array and push this mached elemnt 
     * in machedInvoiceItems array  
     */
    masterItemsGtinArr.filter(function(obj) {
        if (invoiceItemsGtinArr.indexOf(obj) > -1) {
            machedInvoiceItems.push(obj);
        }
    });

    /* Get Mached Master Invoice Items Count */
    var machedMasterInvoiceItemsCount = machedInvoiceItems.length;

    /* Replace mached master items gtin array count */
    $('#new_invoice_items_count').val(machedMasterInvoiceItemsCount);
}

/**
 * Check Gtin Is Exist In Master Items
 * @param {type} invoiceItemId
 * @returns {undefined}
 */
function checkGtinIsExistInMasterItems(invoiceItemId) {
    /* Get invoice items gtin array */
    var masterItemsGtin = $('#master_items_gtin_array_hidden').val();
    var masterItemsGtinArr = isset(masterItemsGtin) && masterItemsGtin != '' ? masterItemsGtin.split(',') : [];
    var selectedGtin = isset($('#gtin_hidden_input_' + invoiceItemId).val()) && $('#gtin_hidden_input_' + invoiceItemId).val() != "" ? $('#gtin_hidden_input_' + invoiceItemId).val() : '';
    var indexOfMachedElement = $.inArray(selectedGtin, masterItemsGtinArr);
   
    /* 
     * If indexOfMachedElement is greater then -1 means this gtin is exist in masteritemgtin array then decrease the 
     * count of new_invoice_items_count input value and replace with decreased value after delete this gtin
     */
    var newInvoiceItemsCount = $('#new_invoice_items_count').val();
    if(indexOfMachedElement > -1){
        newInvoiceItemsCount--;
        $('#new_invoice_items_count').val(newInvoiceItemsCount);
    }
}
/**************************************** End script for edit  action of invoice  ******************************************/

/**************************************** Start script for index action of invoice  ****************************************/
 $('#select_deselect_invoices').click(function(event) {
    if (this.checked) {
        $('input[name="invoice_id[]"]').prop("checked", true);
    } else {
        $('input[name="invoice_id[]"]').prop("checked", false);
    }
});

$('#generate_invoice').click(function(event) {

    var invoice_ids = [];
    $('input[name="invoice_id[]"]:checked').each(function() {
        invoice_ids.push(this.value);
    });

    if (invoice_ids.length > 0) {

        $('#error-message span').text('');
        $('#error-message').hide();

        // submit form
        $('#invoices').submit();

    } else {

        $('#error-message span').text('You can\'t generate invoice, without select invoice');
        $('#error-message').show();

    }

});

$('#generate_delivery_docket').click(function(event) {

    var generate_delivery_docket_checkbox_ids = [];
    $('input[name="invoice_id[]"]:checked').each(function() {
        generate_delivery_docket_checkbox_ids.push(this.value);
    });

    if (generate_delivery_docket_checkbox_ids.length > 0) {

        $('#error-message span').text('');
        $('#error-message').hide();

        // trigger open delivery details popup button
        $('#generate_delivery_docket_checkbox').val(generate_delivery_docket_checkbox_ids);
        $('#open_delivery_docket_details_window').trigger('click');

    } else {

        $('#error-message span').text('You can\'t generate Delivery Docket without select Invoice, Please select at least one Invoice');
        $('#error-message').show();

    }

});

$('#generate_booking_sheet').click(function(event) {

    var generate_booking_sheet_checkbox_ids = [];
    $('input[name="invoice_id[]"]:checked').each(function() {
        generate_booking_sheet_checkbox_ids.push(this.value);
    });

    if (generate_booking_sheet_checkbox_ids.length > 0) {

        $('#error-message span').text('');
        $('#error-message').hide();

        // submit form
        $('#booking_sheet_id').val(generate_booking_sheet_checkbox_ids);
        $('#booking_sheet_form').submit();

    } else {

        $('#error-message span').text('You can\'t generate Booking Sheet without select Invoice, Please select at least one Invoice');
        $('#error-message').show();

    }

});

/**************************************** End script for index action of invoice  ****************************************/
