ALTER TABLE `purchase_orders` ADD `seller_gln` INT( 13 ) NOT NULL COMMENT 'Supplier’s GLN' AFTER `purchase_order_number`;
ALTER TABLE `purchase_orders` CHANGE `seller_gln` `seller_gln` BIGINT( 13 ) NOT NULL COMMENT 'Supplier’s GLN';
ALTER TABLE `users` ADD `role_id` INT( 11 ) NOT NULL AFTER `status` ;
