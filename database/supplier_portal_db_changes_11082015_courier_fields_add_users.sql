ALTER TABLE `users` ADD `courier_logo` VARCHAR( 250 ) NOT NULL AFTER `user_role_id` ,
ADD `courier_address` VARCHAR( 250 ) NOT NULL AFTER `courier_logo` ;

ALTER TABLE `users` ADD `courier_telephone` BIGINT( 10 ) NOT NULL AFTER `courier_address` ;

ALTER TABLE `users` ADD `courier_email` VARCHAR( 200 ) NOT NULL AFTER `courier_telephone` ;


ALTER TABLE `users` ADD `courier_name` VARCHAR( 200 ) NOT NULL AFTER `user_role_id` ;


