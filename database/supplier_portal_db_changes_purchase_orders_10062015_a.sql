ALTER TABLE `purchase_orders` CHANGE `bill_to_gln` `bill_to_gln` INT( 13 ) NOT NULL COMMENT 'Store location’s GLN',
CHANGE `bill_to_additional_id` `bill_to_additional_id` BIGINT( 5 ) NOT NULL COMMENT 'Store location’s internal ID',
CHANGE `bill_to_name` `bill_to_name` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Store location’s name',
CHANGE `bill_to_address` `bill_to_address` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Store location’s address';
ALTER TABLE `purchase_orders` ADD `buyer_gln` BIGINT( 15 ) NOT NULL COMMENT 'Ordering business unit’s GLN' AFTER `bill_to_fax_number` ,
ADD `buyer_additional_id` BIGINT( 5 ) NOT NULL COMMENT 'Ordering business unit’s internal ID' AFTER `buyer_gln` ,
ADD `buyer_name` VARCHAR( 50 ) NOT NULL COMMENT 'Ordering business unit’s name' AFTER `buyer_additional_id`;
ALTER TABLE `purchase_orders` ADD `ship_from_address` TEXT NOT NULL COMMENT 'Supplier’s DC / Agent address' AFTER `seller_name` ,
ADD `ship_from_telephone_number` VARCHAR( 12 ) NULL COMMENT 'Supplier’s DC / Agent telephone number' AFTER `ship_from_address` ,
ADD `ship_from_fax_number` VARCHAR( 12 ) NULL COMMENT 'Supplier’s DC / Agent fax number' AFTER `ship_from_telephone_number` ;
ALTER TABLE `purchase_orders` CHANGE `ship_from_address` `ship_from_address` VARCHAR( 60 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Supplier’s DC / Agent address';
ALTER TABLE `purchase_orders` DROP `total_no_of_items`;
ALTER TABLE `purchase_orders` CHANGE `deliver_date` `deliver_date` DATE NOT NULL COMMENT 'Order delivery date',
CHANGE `comments` `comments` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Order comments';
ALTER TABLE `purchase_orders` CHANGE `bill_to_gln` `bill_to_gln` BIGINT( 13 ) NOT NULL COMMENT 'Store location’s GLN';
ALTER TABLE `purchase_orders` CHANGE `buyer_gln` `buyer_gln` BIGINT( 13 ) NOT NULL COMMENT 'Ordering business unit’s GLN';
ALTER TABLE `purchase_orders` CHANGE `bill_to_telephone_number` `bill_to_telephone_number` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Store location’s telephone number',
CHANGE `bill_to_fax_number` `bill_to_fax_number` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Store location’s fax number';
ALTER TABLE `purchase_orders` CHANGE `seller_name` `seller_name` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Supplier’s name',
CHANGE `ship_from_address` `ship_from_address` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Supplier’s DC / Agent address',
CHANGE `ship_from_telephone_number` `ship_from_telephone_number` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Supplier’s DC / Agent telephone number',
CHANGE `ship_from_fax_number` `ship_from_fax_number` VARCHAR( 20 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Supplier’s DC / Agent fax number';
ALTER TABLE `purchase_orders` CHANGE `buyer_additional_id` `buyer_additional_id` VARCHAR( 20 ) NOT NULL COMMENT 'Ordering business unit’s internal ID';
