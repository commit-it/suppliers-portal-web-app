CREATE TABLE IF NOT EXISTS `payment_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_id` int(11) NOT NULL,
  `plan_id` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `payment_interval` varchar(30) NOT NULL,
  `interval_start_date` date NOT NULL,
  `interval_end_date` date NOT NULL,
  `amount` decimal(6,2) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `payment_date` datetime NOT NULL,
  `discount` int(11) DEFAULT NULL,
  `tax_percent` int(11) DEFAULT NULL,
  `status` varchar(10) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

