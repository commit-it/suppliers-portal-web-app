ALTER TABLE `invoice_items` CHANGE `requested_quantity` `requested_quantity` BIGINT( 10 ) NOT NULL ;
ALTER TABLE `purchase_order_items` CHANGE `requested_quantity` `requested_quantity` BIGINT( 10 ) NOT NULL ;
