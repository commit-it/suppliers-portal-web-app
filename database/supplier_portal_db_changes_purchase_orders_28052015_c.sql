ALTER TABLE `purchase_orders` ADD `instance_id` BIGINT( 10 ) NOT NULL COMMENT 'Transaction instance ID. A unique number' AFTER `purchase_order_number`;
ALTER TABLE `purchase_orders` ADD `creation_date_time` DATETIME NOT NULL COMMENT 'Transaction creation date and time' AFTER `instance_id`;
ALTER TABLE `purchase_orders` CHANGE `order_date` `order_date` DATE NOT NULL COMMENT 'Order date';
ALTER TABLE `purchase_orders` CHANGE `purchase_order_number` `purchase_order_number` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Order number';
ALTER TABLE `purchase_orders` ADD `seller_additional_id` VARCHAR( 10 ) NOT NULL COMMENT 'Supplier’s internal ID' AFTER `seller_gln`;
ALTER TABLE `purchase_orders` ADD `seller_name` VARCHAR( 30 ) NOT NULL COMMENT 'Supplier’s name' AFTER `seller_additional_id`;
ALTER TABLE `purchase_orders` ADD `bill_to_additional_id` BIGINT( 5 ) NOT NULL COMMENT 'Store location’s internal ID' AFTER `bill_to_gln`;
ALTER TABLE `purchase_orders` ADD `Bill To Telephone Number` VARCHAR( 12 ) NULL COMMENT 'Store location’s telephone number' AFTER `bill_to_address` ,
ADD `Bill To Fax Number` VARCHAR( 12 ) NULL COMMENT 'Store location’s fax number' AFTER `Bill To Telephone Number`;
ALTER TABLE `purchase_orders` CHANGE `Bill To Telephone Number` `bill_to_telephone_number` VARCHAR( 12 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Store location’s telephone number',
CHANGE `Bill To Fax Number` `bill_to_fax_number` VARCHAR( 12 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Store location’s fax number';
