
CREATE TABLE IF NOT EXISTS `booking_sheets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `trays` int(10) DEFAULT NULL,
  `trolleys` varchar(50) DEFAULT NULL,
  `pickup_date` datetime DEFAULT NULL,
  `drop_date` datetime DEFAULT NULL,
  `arrived_time_Vic` datetime DEFAULT NULL,
  `arrived_time_NSW` datetime DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

