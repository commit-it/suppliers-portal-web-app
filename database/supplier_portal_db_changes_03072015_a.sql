ALTER TABLE `invoices` ADD `invoice_generated` ENUM( 'no', 'yes' ) NOT NULL DEFAULT 'no' AFTER `ship_via` ;
ALTER TABLE `process_logs` CHANGE ` purchase_order_number` `purchase_order_number` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
ALTER TABLE `users` CHANGE `role_id` `user_role_id` INT( 11 ) NOT NULL ;
