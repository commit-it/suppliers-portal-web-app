ALTER TABLE `purchase_order_items` ADD `supplier_product_id` VARCHAR( 100 ) NULL COMMENT 'External product ID as assigned by the Supplier' AFTER `gtin` ,
ADD `bunnings_product_id` TINYINT( 7 ) NULL COMMENT 'Internal product ID as assigned by Bunnings' AFTER `supplier_product_id` ;
ALTER TABLE `purchase_order_items` ADD `comments` VARCHAR( 1000 ) NULL COMMENT 'Line item comments' AFTER `net_amount` ;
ALTER TABLE `purchase_order_items` CHANGE `bunnings_product_id` `bunnings_product_id` BIGINT( 7 ) NULL DEFAULT NULL COMMENT 'Internal product ID as assigned by Bunnings';
ALTER TABLE `purchase_order_items` ADD `unit_of_measure` CHAR( 3 ) NOT NULL COMMENT 'Unit of measure which product is distributed as' AFTER `requested_quantity` ;
ALTER TABLE `purchase_order_items` ADD `quotation_number` VARCHAR( 50 ) NULL COMMENT 'Reference Quotation Number' AFTER `pack_quantity` ;
