ALTER TABLE `users` ADD `quick_book_access_token` VARCHAR( 100 ) NULL AFTER `subscription_cancelled` ,
ADD `quick_book_access_token_secret` VARCHAR( 100 ) NULL AFTER `quick_book_access_token` ,
ADD `quick_book_consumer_key` VARCHAR( 100 ) NULL AFTER `quick_book_access_token_secret` ,
ADD `quick_book_consumer_secret` VARCHAR( 100 ) NULL AFTER `quick_book_consumer_key` ,
ADD `quick_book_realm_id` INT( 40 ) NULL AFTER `quick_book_consumer_secret` ;
ALTER TABLE `users` CHANGE `quick_book_realm_id` `quick_book_realm_id` INT( 40 ) NULL DEFAULT NULL COMMENT 'realm Id is the campany regirstation id';
ALTER TABLE `users` CHANGE `quick_book_realm_id` `quick_book_realm_id` BIGINT( 50 ) NULL DEFAULT NULL COMMENT 'realm Id is the campany regirstation id';
