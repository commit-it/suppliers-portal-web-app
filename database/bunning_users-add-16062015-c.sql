
CREATE TABLE IF NOT EXISTS `bunning_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('Buyer','Payer') NOT NULL,
  `name` varchar(30) NOT NULL COMMENT 'Global Location Number',
  `gln` bigint(13) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(20) NOT NULL,
  `country_iso_code` varchar(10) NOT NULL,
  `language_iso_code` varchar(10) NOT NULL,
  `postal_code` int(10) NOT NULL,
  `state` varchar(10) NOT NULL,
  `company_registration_number` varchar(30) DEFAULT NULL COMMENT 'ABN or GST Number',
  `user_id` int(11) DEFAULT NULL COMMENT 'Who was created that bunning user',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `bunning_users` ADD `email` VARCHAR( 100 ) NOT NULL AFTER `name` ;


