
CREATE TABLE IF NOT EXISTS `master_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `scan_barcode` varchar(250) NOT NULL,
  `bar_code` varchar(100) NOT NULL,
  `pot_size` varchar(30) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `genus` varchar(100) NOT NULL,
  `cultival` varchar(100) NOT NULL,
  `species` varchar(100) NOT NULL,
  `standard_cost` decimal(6,2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


