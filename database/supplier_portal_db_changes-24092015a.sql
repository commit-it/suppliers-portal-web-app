ALTER TABLE `users` ADD `trial_end_date` DATE NOT NULL AFTER `courier_email` ,
ADD `access_status` ENUM( 'active', 'inactive' ) NOT NULL DEFAULT 'active' AFTER `trial_end_date` ,
ADD `payment_status` ENUM( 'trial', 'paid', 'cancel' ) NOT NULL DEFAULT 'trial' AFTER `access_status` ;
