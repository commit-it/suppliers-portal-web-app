ALTER TABLE `invoices` ADD `booking_sheet_generated` ENUM( 'yes', 'no' ) NULL AFTER `delivery_docket_no` ;
ALTER TABLE `invoices` CHANGE `booking_sheet_generated` `booking_sheet_generated` ENUM( 'yes', 'no' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'no';
ALTER TABLE `invoices` CHANGE `booking_sheet_generated` `booking_sheet_generated` ENUM( 'yes', 'no' ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'no';
