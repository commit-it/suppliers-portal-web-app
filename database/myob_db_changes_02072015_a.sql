ALTER TABLE `purchase_orders` CHANGE `status_id` `status` ENUM( '1', '2' ) NOT NULL COMMENT '1 = Ready for MYOB, 2 = Ready for Portal';
ALTER TABLE `invoices` CHANGE `purchase_order_number` `purchase_order_number` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'This purchase_order_number is given by bunning';
DROP TABLE `statuses`;
