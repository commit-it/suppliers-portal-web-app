-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 14, 2016 at 05:34 PM
-- Server version: 5.5.50-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `myob_supplier_portal`
--

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_code` varchar(5) NOT NULL,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `currency_code`, `name`) VALUES
(1, 'AUD', 'Australia'),
(2, 'NZD', 'New Zealand');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int(11) NOT NULL,
  `invoice_number` varchar(15) NOT NULL COMMENT 'Unique invoice number Max 15 characters in length',
  `invoice_type` varchar(20) NOT NULL COMMENT 'Type of invoice, e.g TAX_INVOICE',
  `purchase_order_number` varchar(30) NOT NULL COMMENT 'This purchase_order_number is given by bunning',
  `sub_total` decimal(6,2) NOT NULL COMMENT 'Additions of purchase order items price',
  `tax` decimal(6,2) DEFAULT NULL,
  `sales_person` varchar(50) DEFAULT NULL,
  `ship_via` varchar(50) DEFAULT NULL,
  `update_for_myob` enum('0','1') NOT NULL DEFAULT '0',
  `nursery_gln` bigint(13) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_items`
--

CREATE TABLE IF NOT EXISTS `invoice_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `order_line_number` bigint(10) NOT NULL COMMENT 'Item line number',
  `gtin` varchar(14) NOT NULL COMMENT 'Global Trade Item Number',
  `supplier_product_id` varchar(100) DEFAULT NULL COMMENT 'External product ID as assigned by the Supplier',
  `bunnings_product_id` bigint(7) DEFAULT NULL COMMENT 'Internal product ID as assigned by Bunnings',
  `requested_quantity` bigint(10) NOT NULL,
  `unit_of_measure` char(3) NOT NULL COMMENT 'Unit of measure which product is distributed as',
  `pack_quantity` bigint(10) NOT NULL,
  `quotation_number` varchar(70) DEFAULT NULL COMMENT 'Reference Quotation Number',
  `description` varchar(100) NOT NULL COMMENT 'Product description',
  `net_price` decimal(6,2) NOT NULL COMMENT 'Unit price excluding GST',
  `net_amount` decimal(6,2) NOT NULL COMMENT 'Line value excluding GST',
  `reference_purchase_order_line_number` bigint(3) DEFAULT NULL COMMENT 'Reference purchase order line number',
  `gst_amount` decimal(6,2) DEFAULT NULL COMMENT 'GST amount of the item',
  `gst_percentage` decimal(6,2) DEFAULT NULL COMMENT 'GST percentage applied to item',
  `comments` varchar(1000) DEFAULT NULL COMMENT 'Line item comments',
  `name` varchar(100) DEFAULT NULL,
  `genus` varchar(100) DEFAULT NULL,
  `species` varchar(100) DEFAULT NULL,
  `cultival` varchar(100) DEFAULT NULL,
  `pot_size` varchar(30) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `process_logs`
--

CREATE TABLE IF NOT EXISTS `process_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nursery_GLN` bigint(13) DEFAULT NULL,
  `purchase_order_number` varchar(30) DEFAULT NULL,
  `action_description` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_orders`
--

CREATE TABLE IF NOT EXISTS `purchase_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_order_number` varchar(30) NOT NULL COMMENT 'Order number',
  `receiver_gln` bigint(13) NOT NULL COMMENT 'This gln is receiver & also primary account',
  `instance_id` bigint(10) NOT NULL COMMENT 'Transaction instance ID. A unique number',
  `creation_date_time` datetime NOT NULL COMMENT 'Transaction creation date and time',
  `language_iso_code` varchar(5) DEFAULT NULL COMMENT 'language ISO Code',
  `seller_gln` bigint(13) NOT NULL COMMENT 'Supplier’s GLN',
  `seller_additional_id` varchar(10) NOT NULL COMMENT 'Supplier’s internal ID',
  `seller_name` varchar(50) NOT NULL COMMENT 'Supplier’s name',
  `ship_from_additional_id` varchar(50) NOT NULL,
  `ship_from_address` varchar(100) NOT NULL COMMENT 'Supplier’s DC / Agent address',
  `ship_from_telephone_number` varchar(20) DEFAULT NULL COMMENT 'Supplier’s DC / Agent telephone number',
  `ship_from_fax_number` varchar(20) DEFAULT NULL COMMENT 'Supplier’s DC / Agent fax number',
  `bill_to_gln` bigint(13) NOT NULL COMMENT 'Store location’s GLN',
  `bill_to_additional_id` bigint(5) NOT NULL COMMENT 'Store location’s internal ID',
  `bill_to_name` varchar(50) NOT NULL COMMENT 'Store location’s name',
  `bill_to_address` varchar(100) NOT NULL COMMENT 'Store location’s address',
  `bill_to_telephone_number` varchar(20) DEFAULT NULL COMMENT 'Store location’s telephone number',
  `bill_to_fax_number` varchar(20) DEFAULT NULL COMMENT 'Store location’s fax number',
  `buyer_gln` bigint(13) NOT NULL COMMENT 'Ordering business unit’s GLN',
  `buyer_additional_id` varchar(20) NOT NULL COMMENT 'Ordering business unit’s internal ID',
  `buyer_name` varchar(50) NOT NULL COMMENT 'Ordering business unit’s name',
  `order_date` date NOT NULL COMMENT 'Order date',
  `deliver_date` date NOT NULL COMMENT 'Order delivery date',
  `total_order_amount` decimal(6,2) NOT NULL COMMENT 'Total amount excluding GST',
  `currency_id` int(11) NOT NULL,
  `comments` text COMMENT 'Order comments',
  `purchase_order_status_id` int(11) NOT NULL,
  `status` enum('1','2') NOT NULL COMMENT '1 = Ready for MYOB, 2 = Ready for Portal',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_items`
--

CREATE TABLE IF NOT EXISTS `purchase_order_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_order_id` int(11) NOT NULL,
  `order_line_number` bigint(10) NOT NULL COMMENT 'Item line number',
  `gtin` varchar(14) NOT NULL COMMENT 'Global Trade Item Number',
  `supplier_product_id` varchar(100) DEFAULT NULL COMMENT 'External product ID as assigned by the Supplier',
  `bunnings_product_id` bigint(7) DEFAULT NULL COMMENT 'Internal product ID as assigned by Bunnings',
  `requested_quantity` bigint(10) NOT NULL,
  `unit_of_measure` char(3) NOT NULL COMMENT 'Unit of measure which product is distributed as',
  `pack_quantity` bigint(10) NOT NULL,
  `quotation_number` varchar(70) DEFAULT NULL COMMENT 'Reference Quotation Number',
  `description` varchar(100) NOT NULL COMMENT 'Product description',
  `net_price` decimal(6,2) NOT NULL COMMENT 'Unit price excluding GST',
  `net_amount` decimal(6,2) NOT NULL COMMENT 'Line value excluding GST',
  `reference_purchase_order_line_number` bigint(3) DEFAULT NULL COMMENT 'Reference purchase order line number',
  `gst_amount` decimal(6,2) DEFAULT NULL COMMENT 'GST amount of the item',
  `gst_percentage` decimal(6,2) DEFAULT NULL COMMENT 'GST percentage applied to item',
  `comments` varchar(1000) DEFAULT NULL COMMENT 'Line item comments',
  `name` varchar(100) DEFAULT NULL,
  `genus` varchar(100) DEFAULT NULL,
  `species` varchar(100) DEFAULT NULL,
  `cultival` varchar(100) DEFAULT NULL,
  `pot_size` varchar(30) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_statuses`
--

CREATE TABLE IF NOT EXISTS `purchase_order_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `purchase_order_statuses`
--

INSERT INTO `purchase_order_statuses` (`id`, `status`) VALUES
(1, 'ORIGINAL'),
(2, 'Active');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
