ALTER TABLE `scan_books` ADD INDEX ( `user_id` ) ;
ALTER TABLE `purchase_orders` ADD INDEX ( `user_id` ) ;
ALTER TABLE `booking_sheets` ADD INDEX ( `user_id` ) ;
ALTER TABLE `bunning_stores` ADD INDEX ( `user_id` ) ;
ALTER TABLE `delivery_dockets` ADD INDEX ( `user_id` ) ;
ALTER TABLE `invoices` ADD INDEX ( `user_id` ) ;
ALTER TABLE `master_items` ADD INDEX ( `user_id` ) ;

