ALTER TABLE `purchase_orders` ADD `archived` ENUM( 'yes', 'no' ) NOT NULL DEFAULT 'no' AFTER `fulfilment_advice_received_date`;
