ALTER TABLE `invoices` DROP `user_id`;
ALTER TABLE `purchase_orders` ADD `seller_gln` INT( 13 ) NOT NULL COMMENT 'Supplier’s GLN' AFTER `purchase_order_number` ;
ALTER TABLE `purchase_orders` CHANGE `seller_gln` `seller_gln` BIGINT( 13 ) NOT NULL COMMENT 'Supplier’s GLN';
