ALTER TABLE `invoices` ADD `invoice_status` ENUM( 'RECEIVED', 'ERROR' ) NULL AFTER `invoice_generated` ,
ADD `delivery_docket_generated` ENUM( 'no', 'yes' ) NULL DEFAULT 'no' AFTER `invoice_status` ,
ADD `delivery_docket_no` VARCHAR( 25 ) NULL AFTER `delivery_docket_generated` ,
ADD `invoice_date` DATETIME NULL AFTER `delivery_docket_no` ;
