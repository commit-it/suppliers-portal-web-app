ALTER TABLE `users` CHANGE `country_iso_code` `country_iso_code` VARCHAR( 10 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
CHANGE `language_iso_code` `language_iso_code` VARCHAR( 10 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
CHANGE `postal_code` `postal_code` INT( 10 ) NOT NULL ,
CHANGE `state` `state` VARCHAR( 10 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
CHANGE `company_registration_number` `company_registration_number` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'ABN or GST No.',
CHANGE `seller_gln` `seller_gln` BIGINT( 13 ) NOT NULL COMMENT 'Global Location Number',
CHANGE `seller_business_name` `seller_business_name` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ,
CHANGE `seller_business_address` `seller_business_address` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;

