ALTER TABLE `invoices` ADD `discount_percentage` INT NULL AFTER `tax` ,
ADD `discount_amount` INT NULL AFTER `discount_percentage` ;
ALTER TABLE `invoices` CHANGE `discount_amount` `discount_amount` DECIMAL( 6, 2 ) NULL DEFAULT NULL ;
