ALTER TABLE `purchase_order_items` CHANGE `gst_percentage` `gst_percentage` DECIMAL( 6, 2 ) NULL DEFAULT NULL COMMENT 'GST percentage applied to item';
ALTER TABLE `invoices` ADD `reference_delivery_note_number` VARCHAR( 25 ) NULL COMMENT 'Reference delivery note number, Max 20 characters in length' AFTER `purchase_order_number` ;
ALTER TABLE `invoices` CHANGE `reference_delivery_note_number` `reference_delivery_note_number` VARCHAR( 25 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Reference delivery note number, Max 20 characters in length';

ALTER TABLE `users` ADD `country_iso_code` VARCHAR( 10 ) NULL AFTER `country` ,
ADD `language_iso_code` VARCHAR( 10 ) NULL AFTER `country_iso_code` ,
ADD `postal_code` INT( 10 ) NULL AFTER `language_iso_code` ,
ADD `state` VARCHAR( 10 ) NULL AFTER `postal_code` ,
ADD `company_registration_number` VARCHAR( 30 ) NULL COMMENT 'ABN or GST No.' AFTER `state` ;
