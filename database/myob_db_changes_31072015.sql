ALTER TABLE `invoice_items` ADD `name` VARCHAR( 100 ) NOT NULL AFTER `comments` ,
ADD `genus` VARCHAR( 100 ) NOT NULL AFTER `name` ,
ADD `species` VARCHAR( 100 ) NOT NULL AFTER `genus` ,
ADD `cultival` VARCHAR( 100 ) NOT NULL AFTER `species` ,
ADD `pot_size` VARCHAR( 30 ) NOT NULL AFTER `cultival` ;

ALTER TABLE `purchase_order_items` ADD `name` VARCHAR( 100 ) NOT NULL AFTER `comments` ,
ADD `genus` VARCHAR( 100 ) NOT NULL AFTER `name` ,
ADD `species` VARCHAR( 100 ) NOT NULL AFTER `genus` ,
ADD `cultival` VARCHAR( 100 ) NOT NULL AFTER `species` ,
ADD `pot_size` VARCHAR( 30 ) NOT NULL AFTER `cultival` ;

ALTER TABLE `invoice_items` CHANGE `name` `name` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
CHANGE `genus` `genus` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
CHANGE `species` `species` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
CHANGE `cultival` `cultival` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
CHANGE `pot_size` `pot_size` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;


ALTER TABLE `purchase_order_items` CHANGE `name` `name` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
CHANGE `genus` `genus` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
CHANGE `species` `species` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
CHANGE `cultival` `cultival` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
CHANGE `pot_size` `pot_size` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;