ALTER TABLE `process_logs` ADD `is_myob` ENUM( '0', '1' ) NOT NULL DEFAULT '0' COMMENT 'For checking record is comming from myob or not' AFTER `action_description`;
