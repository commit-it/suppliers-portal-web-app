ALTER TABLE `purchase_orders` ADD `language_iso_code` VARCHAR( 5 ) NULL COMMENT 'language ISO Code' AFTER `creation_date_time`;
ALTER TABLE `file_types` CHANGE `type` `type` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;
