
CREATE TABLE IF NOT EXISTS `process_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `	purchase_order_number` varchar(30) DEFAULT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `process_logs` ADD `user_id` INT( 11 ) NOT NULL AFTER `id` ;
ALTER TABLE `process_logs` ADD `nursery_GLN` BIGINT( 13 ) NOT NULL AFTER `id` ;
ALTER TABLE `process_logs` CHANGE `description` `action_description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;
