ALTER TABLE `purchase_order_items` ADD `reference_purchase_order_line_number` BIGINT( 3 ) NULL COMMENT 'Reference purchase order line number' AFTER `net_amount` ,
ADD `gst_amount` DECIMAL( 6, 2 ) NULL COMMENT 'GST amount of the item' AFTER `reference_purchase_order_line_number` ,
ADD `gst_percentage` DECIMAL( 2, 2 ) NULL COMMENT 'GST percentage applied to item' AFTER `gst_amount`;

