
CREATE TABLE IF NOT EXISTS `scan_books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `scan_book_file` varchar(100) NOT NULL,
  `bunnig_email_id` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `scan_books` CHANGE `bunnig_email_id` `bunnig_user_id` INT( 11 ) NOT NULL ;
ALTER TABLE `scan_books` CHANGE `bunnig_user_id` `bunning_user_id` INT( 11 ) NOT NULL ;
