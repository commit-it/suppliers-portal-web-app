ALTER TABLE `users` ADD `quick_book_payment_term` INT( 10 ) NULL COMMENT 'This field store Quick Book Payment Term Id' AFTER `quick_book_realm_id` ,
ADD `quick_book_rebate` INT( 10 ) NULL COMMENT 'This field store Quick Book Rebate value in Percent' AFTER `quick_book_payment_term` ;
ALTER TABLE `purchase_orders` CHANGE `invoice_number` `invoice_number` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL ;
ALTER TABLE `invoices` CHANGE `invoice_number` `invoice_number` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Unique invoice number Max 15 characters in length',
CHANGE `reference_delivery_note_number` `reference_delivery_note_number` VARCHAR( 30 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Reference delivery note number, Max 20 characters in length';

