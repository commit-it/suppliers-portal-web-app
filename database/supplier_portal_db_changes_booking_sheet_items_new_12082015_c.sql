
CREATE TABLE IF NOT EXISTS `booking_sheet_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `booking_sheet_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `purchase_order_id` int(11) NOT NULL,
  `office_use_only` varchar(100) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

