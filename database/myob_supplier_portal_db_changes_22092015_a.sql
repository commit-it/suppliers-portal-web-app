ALTER TABLE `invoices` ADD `update_for_myob` ENUM( '0', '1' ) NOT NULL DEFAULT '0' AFTER `ship_via` ;
ALTER TABLE `invoices` ADD `nursery_gln` BIGINT( 13 ) NOT NULL AFTER `update_for_myob` ;
