
ALTER TABLE `users` ADD `reminder_shedule` INT( 11 ) NULL DEFAULT '1' COMMENT 'This field capturing invoice send to bunnings reminder schedule' AFTER `invoice_generate_by` ,
ADD `reminder_sent_time` DATETIME NULL COMMENT 'This field capturing invoice send to bunnings reminder time' AFTER `reminder_shedule` ;