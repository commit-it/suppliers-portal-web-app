ALTER TABLE `purchase_order_items` CHANGE `description` `description` VARCHAR( 60 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Product description',
CHANGE `net_price` `net_price` DECIMAL( 6, 2 ) NOT NULL COMMENT 'Unit price excluding GST',
CHANGE `net_amount` `net_amount` DECIMAL( 6, 2 ) NOT NULL COMMENT 'Line value excluding GST';
ALTER TABLE `purchase_order_items` DROP `item_number`;
ALTER TABLE `purchase_order_items` CHANGE `gtin` `gtin` BIGINT( 14 ) NOT NULL COMMENT 'Global Trade Item Number';
ALTER TABLE `purchase_order_items` CHANGE `order_line_number` `order_line_number` BIGINT( 10 ) NOT NULL COMMENT 'Item line number',
CHANGE `pack_quantity` `pack_quantity` BIGINT( 10 ) NOT NULL ,
CHANGE `quotation_number` `quotation_number` VARCHAR( 70 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL COMMENT 'Reference Quotation Number',
CHANGE `description` `description` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'Product description';

