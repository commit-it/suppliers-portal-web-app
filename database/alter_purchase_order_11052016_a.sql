ALTER TABLE `purchase_orders` ADD `actual_delivery_date` DATE NOT NULL AFTER `deliver_date` ;
ALTER TABLE `purchase_orders` ADD `actual_delivery_date_updated` ENUM( '1', '0' ) NOT NULL DEFAULT '0' AFTER `actual_delivery_date` ;
