ALTER TABLE `purchase_orders` ADD `invoice_number` VARCHAR( 15 ) NULL AFTER `purchase_order_number` ;
ALTER TABLE `purchase_order_items` CHANGE `gtin` `gtin` VARCHAR( 14 ) NOT NULL COMMENT 'Global Trade Item Number';
