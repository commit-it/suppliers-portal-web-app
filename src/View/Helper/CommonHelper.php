<?php

/* src/View/Helper/LinkHelper.php */

namespace App\View\Helper;

use Cake\View\Helper;

class CommonHelper extends Helper {

    /**
     * Get sub string of your whole string
     * If string lenght is greater than 5 then return sub string other wise return default string
     * @param type $string = default value is NULL
     * @param type $start = default value is 0
     * @param type $end  = default value is 5
     * 
     * @output substring
     */
    public function getSubString($string = NULL, $start = 0, $length = 5) {
        $stringLength = strlen($string);
        if ($stringLength > $length) {
            return substr($string, $start, $length) . '...';
        } else {
            return $string;
        }
    }

    /**
     * checkActiveAction() method
     * @paran $controllerArr is controller names
     * @paran $actionArr is action names
     * Check Active Action
     * return TRUE/FALSE
     */
    function checkActiveAction($controllerArr, $actionArr) {

        if (in_array($this->request->controller, $controllerArr) && in_array($this->request->action, $actionArr)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Convert Cent To Prise
     * @param type $prise
     * @param type $decimals
     * @param type $currencySign
     * @return type
     */
    function convertCentToPrise($prise, $decimals = 2, $currencySign = NULL) {
        return $currencySign . number_format((($prise) / 100), $decimals);
    }

    /**
     * multisortArray - Method
     * @param type $merge_arr
     * @param type $modelArr
     * @param type $field
     * @param type $sort
     * @return type
     */
    public function multisortArray($merge_arr, $modelArr, $field, $sort) {

        $sortArr = array();
        if (!empty($modelArr)) {
            foreach ($merge_arr as $k => $v) {
                foreach ($modelArr as $modelKey => $modelVal) {
                    if (isset($v[$modelArr[$modelKey]][$field])) {
                        $sortArr[$field][$k] = $v[$modelVal][$field];
                    }
                }
            }
        } else {
            foreach ($merge_arr as $k => $v) {
                $sortArr[$field][$k] = $v[$field];
            }
        }
        
        if ($sort == 'asc') {
            array_multisort($sortArr[$field], SORT_ASC, $merge_arr);
        } else {
            array_multisort($sortArr[$field], SORT_DESC, $merge_arr);
        }
        return $merge_arr;
    }
}
