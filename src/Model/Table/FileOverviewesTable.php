<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Log\Log;

/**
 * FileOverviewes Model
 */
class FileOverviewesTable extends Table {

    const Ack_RECEIVED_Status = 'RECEIVED';
    const Ack_ERROR_Status = 'ERROR';

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->table('file_overviewes');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('FileTypes', [
            'foreignKey' => 'file_type_id'
        ]);
        $this->belongsTo('FileStatuses', [
            'foreignKey' => 'file_status_id'
        ]);
        $this->belongsTo('Invoices', [
            'foreignKey' => 'invoice_id'
        ]);

        $this->addBehavior('SftpConnection');
        $this->addBehavior('Common');
        $this->addBehavior('Email');

        $this->ProcessLogs = TableRegistry::get('ProcessLogs');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric'])
                ->allowEmpty('id', 'create')
                ->requirePresence('file_name', 'create')
                ->notEmpty('file_name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['file_type_id'], 'FileTypes'));
        $rules->add($rules->existsIn(['file_status_id'], 'FileStatuses'));
        return $rules;
    }

    /**
     * @Returns a File List
     * 
     */
    public function getPOFileList() {

        $results = array();
        $processLogsObj = TableRegistry::get('ProcessLogs');

        /*
         * @connect sftp using sftpConnection() method
         */
        $fileList = array();
        $resSFTP = $this->sftpConnection();
        if ($resSFTP) {
            $fileList = $this->getFileList(Configure::read('po.remotePath'));
        }

        /*
         * @store available file list in table
         */
        if (!empty($fileList)) {

            $results[] = implode(", ", $fileList) . " these files are available to SFTP";

            // store file list
            $storeFilesResultArr = $this->storeFileList($fileList);
            foreach ($storeFilesResultArr as $storeFileResult) {

                $results[] = $storeFileResult;
            }
        } else {

            $results[] = "Files are not available to SFTP";
        }

        /*
         * Generate Process Log Arr & save 
         */
        $processLogResult = $this->ProcessLogs->generateProcessLogArrAndSave($results);

        return $results;
    }

    /**
     * @store available file list
     * 
     * storeFileList() method
     * Return Save file Result
     */
    public function storeFileList($fileList) {

        /*
         * Find FileType id by Type
         */
        $fileTypeId = $this->checkIfFieldIsExist($this->FileTypes->findByType(FileTypesTable::Purchase_Order)->first(), 'id');

        /*
         * Find Status id by name
         */
        $fileStatusId = $this->checkIfFieldIsExist($this->FileStatuses->findByStatus(FileStatusesTable::Available)->first(), 'id');

        $saveData = array();
        foreach ($fileList as $file) {

            /*
             * check if file is already exist of not for save
             */
            $existCount = $this->findByFileName($file)->count();

            /*
             * Insert Record if file not exist
             */
            if ($existCount < 1) {
                $fileOverviewes = $this->newEntity();
                $fileOverviewes->file_type_id = $fileTypeId;
                $fileOverviewes->file_name = $file;
                $fileOverviewes->file_status_id = $fileStatusId;

                if ($this->save($fileOverviewes)) {
                    $saveData[] = "Record is inserted for " . $file . " file";
                } else {
                    $saveData[] = "Record is not inserted for " . $file . " file";
                }
            } else {
                $saveData[] = $file . " this file already exists";
            }
        }

        return $saveData;
    }

    /**
     * @download available file list
     * 
     * downloadPOFileList() method
     * Return Save file Result
     */
    public function downloadPOFileList() {

        $results = array();

        /*
         * @connect sftp using sftpConnection() method
         */
        $resSFTP = $this->sftpConnection();
        if ($resSFTP) {

            /*
             * Find Status id by name
             */
            $availableFileStatusId = $this->checkIfFieldIsExist($this->FileStatuses->findByStatus(FileStatusesTable::Available)->first(), 'id');
            $availablefileList = $this->find('all', [
                        'fields' => ['file_name'],
                        'conditions' => ['file_status_id' => $availableFileStatusId]
                    ])->toArray();

            $downloadFileList = array();
            foreach ($availablefileList as $availablefile) {
                $downloadFileList[] = $availablefile['file_name'];
            }

            if (!empty($downloadFileList)) {

                /*
                 * Download files
                 */
                $fileNameArr = $this->downloadFiles(Configure::read('po.remotePath'), Configure::read('po.localPath'), $downloadFileList);

                if (!empty($fileNameArr)) {

                    $results[] = implode(", ", $fileNameArr) . " these files are Downloaded successfully";

                    /*
                     * Find Status id by name
                     */
                    $fileStatusId = $this->checkIfFieldIsExist($this->FileStatuses->findByStatus(FileStatusesTable::Downloaded)->first(), 'id');

                    foreach ($fileNameArr as $fileName) {
                        $query = $this->updateStatus($fileName, $fileStatusId);
                        if ($query) {
                            $results[] = $fileName . " file status is updated successfully by " . FileStatusesTable::Downloaded;
                            /*
                             * Delete file from sftp after download
                             */
                            $deleteFileStatus = $this->deleteFile(Configure::read('po.remotePath'), $fileName);
                            if ($deleteFileStatus) {
                                $results[] = $fileName . " file is deleted successfully from SFTP";
                            } else {
                                $results[] = $fileName . " file is not deleted successfully from SFTP";
                            }
                        } else {
                            $results[] = $fileName . " file status is not updated successfully by " . FileStatusesTable::Downloaded;
                        }
                    }
                } else {
                    $results[] = "Files are not available for download.";
                }
            } else {
                $results[] = "Files are not available for download.";
            }
        } else {
            $results[] = "Some error is occurred in sftp connection";
        }

        /*
         * Generate Process Log Arr & save 
         */
        $processLogResult = $this->ProcessLogs->generateProcessLogArrAndSave($results);

        return $results;
    }

    /*
     * @update file status
     * updateStatus() method
     * return true or false
     */

    public function updateStatus($fileName, $fileStatusId) {
        $query = $this->query();

        $result = $query->update()
                ->set(['file_status_id' => $fileStatusId])
                ->where(['file_name' => $fileName])
                ->execute();

        return $result;
    }

    /*
     * @update file type
     * updateFileTypeId() method
     * return true or false
     */

    public function updateFileTypeId($fileName, $fileTypeId) {
        $query = $this->query();

        $result = $query->update()
                ->set(['file_type_id' => $fileTypeId])
                ->where(['file_name' => $fileName])
                ->execute();

        return $result;
    }

    /**
     * updateFieldsByField - Method
     * @param type $fieldName
     * @param type $fieldValue
     * @param type $updateFieldsArr
     * @return type
     */
    public function updateFieldsByField($fieldName, $fieldValue, $updateFieldsArr) {
        $query = $this->query();

        $result = $query->update()
                ->set($updateFieldsArr)
                ->where([$fieldName => $fieldValue])
                ->execute();

        return $result;
    }

    /*
     * @import xml purchase order files
     */

    public function importXmlPurchaseOrders() {
        $results = array();

        // get status of files
        $xmlFileList = $this->findByFileStatusId($this->FileStatuses->findByStatus(FileStatusesTable::Downloaded)->first()->id);
        $purchaseOrderArr = array();

        $currencyObj = TableRegistry::get('Currencies');
        $orderStatusObj = TableRegistry::get('PurchaseOrderStatuses');
        $purchaseOrderObj = TableRegistry::get('PurchaseOrders');
        $userObj = TableRegistry::get('Users');
        $invoiceObj = TableRegistry::get('Invoices');
        $statisticsObj = TableRegistry::get('Statistics');

        $resultCnt = 0;
        $purchaseOrderImported = 0;
        $processLogArr = array();
        $userIdArr = array();

        foreach ($xmlFileList as $xmlFile) {

            $resultCnt++;

            if ($this->checkFileIsExist(Configure::read('po.localPath') . DS . $xmlFile->file_name)) {
                $xmlArray = array();
                try {
                    $xmlArray = \Cake\Utility\Xml::toArray(\Cake\Utility\Xml::build(Configure::read('po.localPath') . DS . $xmlFile->file_name));
                } catch (\Exception $e) {
                    $this->updateStatus($xmlFile->file_name, 4);
                    Log::error($e->getMessage());
                    continue;
                }

                if (!empty($xmlArray)) {

                    foreach ($xmlArray as $xmlResult) {
                        // check file type is MultiShipmentOrder for import or not
                        if ($xmlResult['sh:StandardBusinessDocumentHeader']['sh:DocumentIdentification']['sh:Type'] == Configure::read('po.file-type-multShipOrd')) {

                            // check if seller (i.e user is exist or not in system)
                            $userId = $this->checkIfFieldIsExist($userObj->findBySellerGln($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderPartyInformation']['seller']['gln'])->first(), 'id');

                            // if user_id is not exist in database, not allow to import po file
                            if ($userId != null) {

                                // this method generate Application Receipt Of Purchase Order & send to sftp
                                $applicationReceiptResult = $purchaseOrderObj->generateApplicationReceiptOfPurcaseOrderXmlFileAndSendToSftp($xmlResult, Configure::read('invoice_ack_invoice_status.RECEIVED'));

                                $purchaseOrderArr['user_id'] = $userId;
                                $purchaseOrderArr['purchase_order_number'] = $this->checkIsset($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderIdentification']['uniqueCreatorIdentification']);
                                $purchaseOrderArr['instance_id'] = $xmlResult['sh:StandardBusinessDocumentHeader']['sh:DocumentIdentification']['sh:InstanceIdentifier'];
                                $purchaseOrderArr['creation_date_time'] = date('Y-m-d H:i:s', strtotime($xmlResult['sh:StandardBusinessDocumentHeader']['sh:DocumentIdentification']['sh:CreationDateAndTime']));
                                $purchaseOrderArr['seller_gln'] = $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderPartyInformation']['seller']['gln'];
                                $purchaseOrderArr['receiver_gln'] = $xmlResult['sh:StandardBusinessDocumentHeader']['sh:Receiver']['sh:Identifier']['@'];

                                foreach ($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderPartyInformation']['seller']['additionalPartyIdentification'] as $sellerAdditionalPartyIdentification) {
                                    if ($sellerAdditionalPartyIdentification['additionalPartyIdentificationType'] == PurchaseOrdersTable::Seller_Additional_Id) {
                                        $purchaseOrderArr['seller_additional_id'] = $sellerAdditionalPartyIdentification['additionalPartyIdentificationValue'];
                                    }
                                    if ($sellerAdditionalPartyIdentification['additionalPartyIdentificationType'] == PurchaseOrdersTable::Seller_Name) {
                                        $purchaseOrderArr['seller_name'] = $sellerAdditionalPartyIdentification['additionalPartyIdentificationValue'];
                                    }
                                }

                                $purchaseOrderArr['ship_from_gln'] = $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderLogisticalInformation']['shipToLogistics']['shipFrom']['gln'];
                                foreach ($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderLogisticalInformation']['shipToLogistics']['shipFrom']['additionalPartyIdentification'] as $shipToAdditionalPartyIdentification) {
                                    if ($shipToAdditionalPartyIdentification['additionalPartyIdentificationType'] == PurchaseOrdersTable::Ship_From_Name) {
                                        $purchaseOrderArr['ship_from_name'] = $shipToAdditionalPartyIdentification['additionalPartyIdentificationValue'];
                                    }
                                    if ($shipToAdditionalPartyIdentification['additionalPartyIdentificationType'] == PurchaseOrdersTable::Ship_From_Additional_Id) {
                                        $purchaseOrderArr['ship_from_additional_id'] = $shipToAdditionalPartyIdentification['additionalPartyIdentificationValue'];
                                    }
                                    if ($shipToAdditionalPartyIdentification['additionalPartyIdentificationType'] == PurchaseOrdersTable::Ship_From_Address) {
                                        $purchaseOrderArr['ship_from_address'] = $shipToAdditionalPartyIdentification['additionalPartyIdentificationValue'];
                                    }
                                    if ($shipToAdditionalPartyIdentification['additionalPartyIdentificationType'] == PurchaseOrdersTable::Ship_From_Telephone_Number) {
                                        $purchaseOrderArr['ship_from_telephone_number'] = $shipToAdditionalPartyIdentification['additionalPartyIdentificationValue'];
                                    }
                                    if ($shipToAdditionalPartyIdentification['additionalPartyIdentificationType'] == PurchaseOrdersTable::Ship_From_Fax_Number) {
                                        $purchaseOrderArr['ship_from_fax_number'] = $shipToAdditionalPartyIdentification['additionalPartyIdentificationValue'];
                                    }
                                }

                                $purchaseOrderArr['bill_to_gln'] = $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderPartyInformation']['billTo']['gln'];

                                foreach ($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderPartyInformation']['billTo']['additionalPartyIdentification'] as $billToAdditionalPartyIdentification) {
                                    if ($billToAdditionalPartyIdentification['additionalPartyIdentificationType'] == PurchaseOrdersTable::Bill_To_Additional_Id) {
                                        $purchaseOrderArr['bill_to_additional_id'] = $billToAdditionalPartyIdentification['additionalPartyIdentificationValue'];
                                    }
                                    if ($billToAdditionalPartyIdentification['additionalPartyIdentificationType'] == PurchaseOrdersTable::Bill_To_Name_Type) {
                                        $purchaseOrderArr['bill_to_name'] = $billToAdditionalPartyIdentification['additionalPartyIdentificationValue'];
                                    }
                                    if ($billToAdditionalPartyIdentification['additionalPartyIdentificationType'] == PurchaseOrdersTable::Bill_To_Address_Type) {
                                        $purchaseOrderArr['bill_to_address'] = $billToAdditionalPartyIdentification['additionalPartyIdentificationValue'];
                                    }
                                    if ($billToAdditionalPartyIdentification['additionalPartyIdentificationType'] == PurchaseOrdersTable::Bill_To_Telephone_Number) {
                                        $purchaseOrderArr['bill_to_telephone_number'] = $billToAdditionalPartyIdentification['additionalPartyIdentificationValue'];
                                    }
                                    if ($billToAdditionalPartyIdentification['additionalPartyIdentificationType'] == PurchaseOrdersTable::Bill_To_Fax_Number) {
                                        $purchaseOrderArr['bill_to_fax_number'] = $billToAdditionalPartyIdentification['additionalPartyIdentificationValue'];
                                    }
                                }

                                $purchaseOrderArr['buyer_gln'] = $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderPartyInformation']['buyer']['gln'];
                                foreach ($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderPartyInformation']['buyer']['additionalPartyIdentification'] as $buyerAdditionalPartyIdentification) {
                                    if ($buyerAdditionalPartyIdentification['additionalPartyIdentificationType'] == PurchaseOrdersTable::Buyer_Additional_Id) {
                                        $purchaseOrderArr['buyer_additional_id'] = $buyerAdditionalPartyIdentification['additionalPartyIdentificationValue'];
                                    }
                                    if ($buyerAdditionalPartyIdentification['additionalPartyIdentificationType'] == PurchaseOrdersTable::Buyer_Name) {
                                        $purchaseOrderArr['buyer_name'] = $buyerAdditionalPartyIdentification['additionalPartyIdentificationValue'];
                                    }
                                }

                                $purchaseOrderArr['order_date'] = date('Y-m-d', strtotime($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['@creationDateTime']));
                                $purchaseOrderArr['deliver_date'] = $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderLogisticalInformation']['orderLogisticalDateGroup']['requestedDeliveryDate']['date'];
                                $purchaseOrderArr['total_order_amount'] = $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['totalMonetaryAmountOfOrder']['monetaryAmount'];
                                $purchaseOrderArr['currency_id'] = $this->checkIfFieldIsExist($currencyObj->findByCurrencyCode($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['totalMonetaryAmountOfOrder']['currencyCode']['currencyISOCode'])->first(), 'id');

                                if (isset($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['additionalOrderInstruction']['language']['languageISOCode']) && $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['additionalOrderInstruction']['language']['languageISOCode'] == Configure::read('po.languageISOCode')) {
                                    $purchaseOrderArr['comments'] = $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['additionalOrderInstruction']['longText'];
                                    $purchaseOrderArr['language_iso_code'] = $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['additionalOrderInstruction']['language']['languageISOCode'];
                                }

                                // check status is available if not then add
                                $purchaseOrderStatusId = $this->checkIfFieldIsExist($orderStatusObj->findByStatus($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['@documentStatus'])->first(), 'id');
                                if ($purchaseOrderStatusId != null) {
                                    $purchaseOrderArr['purchase_order_status_id'] = $purchaseOrderStatusId;
                                } else {
                                    // Add new status for purchase order status
                                    $purchaseOrderStatusId = '';
                                    $orderStatus = $orderStatusObj->newEntity();
                                    $orderStatus->status = $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['@documentStatus'];
                                    if ($orderStatusObj->save($orderStatus)) {
                                        // The $article entity contain the id now
                                        $purchaseOrderStatusId = $orderStatus->id;
                                    }
                                    $purchaseOrderArr['purchase_order_status_id'] = $purchaseOrderStatusId;
                                }
                            } else {

                                $results[] = 'Seller is not exist in users table for import ' . $xmlFile->file_name . ' file';

                                // generate process log arr
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Seller is not exist in users table for import ' . $xmlFile->file_name . ' file');
                            }
                        } elseif ($xmlResult['sh:StandardBusinessDocumentHeader']['sh:DocumentIdentification']['sh:Type'] == Configure::read('po.file-type-ack')) {

                            $allowMoveUpdate = 0;

                            $documentAcknowledgedType = isset($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['sBDHApplicationReceiptAcknowledgement']['commandApplicationReceiptAcknowledgement']['documentApplicationReceiptAcknowledgement']['originalDocumentType']) ? $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['sBDHApplicationReceiptAcknowledgement']['commandApplicationReceiptAcknowledgement']['documentApplicationReceiptAcknowledgement']['originalDocumentType'] : '';

                            // check if Application Ack is for invoice
                            if ($documentAcknowledgedType == Configure::read('Application_Receipt.document_acknowledged_type.Invoice')) {

                                $instanceId = isset($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['applicationReceiptAcknowledgementIdentification']['uniqueCreatorIdentification']) ? $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['applicationReceiptAcknowledgementIdentification']['uniqueCreatorIdentification'] : '';
                                $invoiceNumber = isset($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['sBDHApplicationReceiptAcknowledgement']['commandApplicationReceiptAcknowledgement']['documentApplicationReceiptAcknowledgement']['originalEntityIdentification']['uniqueCreatorIdentification']) ? $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['sBDHApplicationReceiptAcknowledgement']['commandApplicationReceiptAcknowledgement']['documentApplicationReceiptAcknowledgement']['originalEntityIdentification']['uniqueCreatorIdentification'] : '';
                                $invoiceDate = isset($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['@creationDateTime']) ? date('Y-m-d H:i:s', strtotime($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['@creationDateTime'])) : '';

                                // find purchase_order_id by instanceId
                                $purchaseOrderId = $this->checkIfFieldIsExist($purchaseOrderObj->findByInstanceId($instanceId)->applyOptions(['remove_user_condition' => true])->first(), 'id');

                                // find invode_id by purchase_order_id or invoice_number
                                $invoiceId = $this->checkIfFieldIsExist($invoiceObj->findByInvoiceNumber($invoiceNumber)->applyOptions(['remove_user_condition' => true])->first(), 'id');
                                if (!$invoiceNumber) {
                                    $invoiceId = $this->checkIfFieldIsExist($invoiceObj->findByPurchaseOrderId($purchaseOrderId)->applyOptions(['remove_user_condition' => true])->first(), 'id');
                                }

                                $invoiceStatus = isset($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['sBDHApplicationReceiptAcknowledgement']['statusType']) ? $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['sBDHApplicationReceiptAcknowledgement']['statusType'] : '';

                                if ($invoiceId) {
                                    //Get user id by invoice id
                                    $userId = $this->checkIfFieldIsExist($invoiceObj->findById($invoiceId)->applyOptions(['remove_user_condition' => true])->first(), 'user_id');
                                    Log::debug('importXmlPurchaseOrders - Userid => ' . $userId);

                                    //Get delivery_docket_no of that user
                                    $userData = $userObj->findById($userId)->first();
                                    $deliveryDocketNo = $userData->delivery_docket_no;
                                    Log::debug('importXmlPurchaseOrders - existing delivery_docket_no => ' . $deliveryDocketNo);

                                    //Increment by one in delivery docket number
                                    $updatedDeliveryDocketNo = ++$deliveryDocketNo;
                                    Log::debug('importXmlPurchaseOrders - updated updatedDeliveryDocketNo => ' . $updatedDeliveryDocketNo);
                                    Log::debug('importXmlPurchaseOrders - invoiceId => ' . $invoiceId);

                                    // update invoice data from Invoice Acknowledgement file
                                    $invoiceArticle = $invoiceObj->get($invoiceId, ['remove_user_condition' => true]);
                                    $invoiceArticle->invoice_status = $invoiceStatus;

                                    //Changing the Delivery Docket Number to DD000000 
//                                    $invoiceArticle->delivery_docket_no = $invoiceNumber;
                                    $invoiceArticle->delivery_docket_no = $updatedDeliveryDocketNo;
                                    $invoiceArticle->invoice_date = $invoiceDate;
                                    $invoiceArticle->resend_invoice = Configure::read('resend_invoice.no');

                                    $results[] = $xmlFile->file_name . '(Invoice) file status is ' . $invoiceStatus;
                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . ' file status is ' . $invoiceStatus);

                                    if ($invoiceObj->save($invoiceArticle)) {
                                        Log::debug(' Invoice updated ');
                                        //Update incremented delivery_docket_no in users table
                                        $userUpdate = $userObj->get($userId, ['remove_user_condition' => true]);
                                        $userUpdate->delivery_docket_no = $updatedDeliveryDocketNo;
                                        $userresult = $userObj->save($userUpdate);
                                        Log::debug('importXmlPurchaseOrders - $userresult => ' . print_r($userresult, TRUE));

                                        // Send Email to user whenever update delivery docket number from Invoice Ack, so he can re-print delivery docket
                                        $updatedInvoiceNumber = $invoiceNumber;
                                        if ($updatedInvoiceNumber != $invoiceNumber) {

                                            $userData = $userObj->findById($userId)->first();
                                            $userName = isset($userData->full_name) ? $userData->full_name : '';
                                            $userEmail = isset($userData->email) ? $userData->email : '';
                                            $to = [$userEmail => $userName];
                                            $emailData = array();
                                            $emailData['oldDeliveryDocketNumber'] = $invoiceNumber;
                                            $emailData['newDeliveryDocketNumber'] = $invoiceNumber;
                                            $emailData['invoiceNumber'] = $invoiceNumber;
                                            $poNotificationSendResult = $this->deliveryDocketNumberUpdateNotificationEmail($to, $emailData);
                                        }

                                        $results[] = $xmlFile->file_name . "(Invoice) file data updated successfully in invoices table";

                                        // generate process log arr
                                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . "(Invoice) file data updated successfully in invoices table");

                                        // if invoice status is RECEIVED then entry in statistics table
                                        if ($invoiceStatus == Configure::read('invoice_ack_invoice_status.RECEIVED')) {

                                            $userId = $this->checkIfFieldIsExist($invoiceObj->findById($invoiceId)->applyOptions(['remove_user_condition' => true])->first(), 'user_id');
                                            $month = date("m", strtotime($invoiceDate));
                                            $year = date("Y", strtotime($invoiceDate));

                                            // check is already in voice generated for same user, month & year
                                            $statisticArr = $statisticsObj->find('all', [
                                                        'fields' => ['id', 'count'],
                                                        'conditions' => ['year' => $year, 'month' => $month, 'user_id' => $userId],
                                                    ])->toArray();

                                            if (!empty($statisticArr)) {

                                                $statisticId = isset($statisticArr[0]['id']) ? $statisticArr[0]['id'] : '';
                                                $statisticsArticle = $statisticsObj->get($statisticId);
                                                $statisticsArticle->count = isset($statisticArr[0]['count']) ? ($statisticArr[0]['count'] + 1) : 0;
                                                if ($statisticsObj->save($statisticsArticle)) {

                                                    $results[] = 'data updated successfully for ' . $xmlFile->file_name . '(Invoice) this invoices file in statistics table';
                                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('data updated successfully for ' . $xmlFile->file_name . '(Invoice) this invoices file in statistics table', $userId);
                                                } else {

                                                    $results[] = 'data are not updated successfully for ' . $xmlFile->file_name . '(Invoice) this invoices file in statistics table';
                                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('data updated successfully for ' . $xmlFile->file_name . '(Invoice) this invoices file in statistics table', $userId);
                                                }
                                            } else {

                                                $statisticNewArr = array();
                                                $statisticNewArr['month'] = $month;
                                                $statisticNewArr['year'] = $year;
                                                $statisticNewArr['count'] = 1;
                                                $statisticNewArr['user_id'] = $userId;
                                                $statisticsEntity = $statisticsObj->newEntity($statisticNewArr);
                                                if ($statisticsObj->save($statisticsEntity)) {

                                                    $results[] = 'data saved successfully for ' . $xmlFile->file_name . '(Invoice) this invoices file in statistics table';
                                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('data updated successfully for ' . $xmlFile->file_name . '(Invoice) this invoices file in statistics table', $userId);
                                                } else {

                                                    $results[] = 'data are not saved successfully for ' . $xmlFile->file_name . '(Invoice) this invoices file in statistics table';
                                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('data updated successfully for ' . $xmlFile->file_name . '(Invoice) this invoices file in statistics table', $userId);
                                                }
                                            }
                                        }

                                        $allowMoveUpdate++;
                                    } else {

                                        $results[] = $xmlFile->file_name . "(Invoice) file data does not updated successfully in invoices table";

                                        // generate process log arr
                                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . "(Invoice) file data does not updated successfully in invoices table");
                                    }
                                } else {

                                    $results[] = $xmlFile->file_name . "(Invoice) file not match with any invoice to proceed";

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . "(Invoice) file not match with any purchase order & invoice to proceed");
                                }
                            } else if ($documentAcknowledgedType == Configure::read('Application_Receipt.document_acknowledged_type.Fulfilment_Advice')) {

                                // check if Application Ack is for Fulfilment Advice
                                $fulfilmentAdviceNumber = isset($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['sBDHApplicationReceiptAcknowledgement']['commandApplicationReceiptAcknowledgement']['documentApplicationReceiptAcknowledgement']['originalEntityIdentification']['uniqueCreatorIdentification']) ? $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['sBDHApplicationReceiptAcknowledgement']['commandApplicationReceiptAcknowledgement']['documentApplicationReceiptAcknowledgement']['originalEntityIdentification']['uniqueCreatorIdentification'] : '';

                                // find purchase_order_id by fulfilment_despatch_advice_identification_number
                                $purchaseOrderId = $this->checkIfFieldIsExist($purchaseOrderObj->findByFulfilmentDespatchAdviceIdentificationNumber($fulfilmentAdviceNumber)->applyOptions(['remove_user_condition' => true])->first(), 'id');

                                $fulfilmentAdviceStatus = isset($xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['sBDHApplicationReceiptAcknowledgement']['statusType']) ? $xmlResult['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['sBDHApplicationReceiptAcknowledgement']['statusType'] : '';

                                if ($purchaseOrderId) {

                                    // update invoice data from Invoice Acknowledgement file
                                    $purchaseOrderArticle = $purchaseOrderObj->get($purchaseOrderId, ['remove_user_condition' => true]);
                                    $purchaseOrderArticle->fulfilment_advice_status = $fulfilmentAdviceStatus;

                                    /**
                                     * If $fulfilmentAdviceStatus is RECEIVED then only update fulfilment_advice_received_date as current date
                                     * This is used for set delivery date for purchase order
                                     * */
                                    if ($fulfilmentAdviceStatus == Configure::read('fulfilment_advice_ack_status.RECEIVED')) {
                                        $purchaseOrderArticle->fulfilment_advice_received_date = date('Y-m-d');
                                    }

                                    $results[] = $xmlFile->file_name . '(Fulfilment Advice) file status is ' . $fulfilmentAdviceStatus;

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . '(Fulfilment Advice) file status is ' . $fulfilmentAdviceStatus);

                                    if ($purchaseOrderObj->save($purchaseOrderArticle)) {

                                        $results[] = $xmlFile->file_name . "(Fulfilment Advice) file data updated successfully in purchase_orders table";

                                        // generate process log arr
                                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . "(Fulfilment Advice) file data updated successfully in purchase_orders table");

                                        $allowMoveUpdate++;
                                    } else {

                                        $results[] = $xmlFile->file_name . "(Fulfilment Advice) file data does not updated successfully in purchase_orders table";

                                        // generate process log arr
                                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . "(Fulfilment Advice) file data does not updated successfully in purchase_orders table");
                                    }
                                } else {

                                    $results[] = $xmlFile->file_name . "(Fulfilment Advice) file not match with any purchase order to proceed";

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . "(Fulfilment Advice) file not match with any purchase order to proceed");
                                }
                            }

                            if ($allowMoveUpdate > 0) {

                                /*
                                 * Find Status id by name
                                 */
                                $fileTypeId = $this->checkIfFieldIsExist($this->FileTypes->findByType(FileTypesTable::Ack)->first(), 'id');

                                /*
                                 * Find Status id by name
                                 */
                                $fileStatusId = $this->checkIfFieldIsExist($this->FileStatuses->findByStatus(FileStatusesTable::Completed)->first(), 'id');

                                // update fields by file_name
                                $invoiceId = isset($invoiceId) ? $invoiceId : '';
                                $updateArr = array(
                                    'file_type_id' => $fileTypeId,
                                    'file_status_id' => $fileStatusId,
                                    'invoice_id' => $invoiceId
                                );
                                $updateResult = $this->updateFieldsByField('file_name', $xmlFile->file_name, $updateArr);
                                if ($updateResult) {

                                    /*
                                     * File moved in Out folder of ApplicationReceiptAcknowledgement
                                     */
                                    $ackFileMovePath = Configure::read('po.localPath-ack-out');
                                    $folderName = 'Out';

                                    // Move file in another folder
                                    if ($this->moveFileInAnotherFolder($xmlFile->file_name, Configure::read('po.localPath'), $ackFileMovePath)) {

                                        $results[] = $xmlFile->file_name . " file moved successfully in ApplicationReceiptAcknowledgement " . $folderName . " folder";

                                        // generate process log arr
                                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . " file moved successfully in ApplicationReceiptAcknowledgement " . $folderName . " folder");
                                    } else {

                                        $results[] = $xmlFile->file_name . " file not moved successfully in ApplicationReceiptAcknowledgement " . $folderName . " folder";

                                        // generate process log arr
                                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . " file not moved successfully in ApplicationReceiptAcknowledgement " . $folderName . " folder");
                                    }
                                } else {

                                    $results[] = $xmlFile->file_name . " file record are not updated by ApplicationReceiptAcknowledgement";

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . " file record are not updated by ApplicationReceiptAcknowledgement");
                                }
                            } else {

                                $results[] = $xmlFile->file_name . " file record are not updated by ApplicationReceiptAcknowledgement";

                                // generate process log arr
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . " file record are not updated by ApplicationReceiptAcknowledgement");
                            }
                        }
                    }

                    if ((!empty($purchaseOrderArr)) && ($userId != null)) {

                        $userIdArr[] = $userId;
                        $purchaseOrderArr['purchase_order_items'] = $this->purchaseOrderItemsInserting($xmlArray);

                        $entity = $purchaseOrderObj->newEntity($purchaseOrderArr, [
                            'associated' => ['PurchaseOrderItems']
                        ]);

                        if (!$purchaseOrderObj->exists(['purchase_order_number' => $purchaseOrderArr['purchase_order_number'], 'user_id' => $userId])) {

                            $purchaseOrderImported++;
                            $entity = $purchaseOrderObj->newEntity($purchaseOrderArr);
                            $poResult = $purchaseOrderObj->save($entity);
                            if ($poResult) {

                                $results[] = "Data are imported successfully of " . $xmlFile->file_name . " file";

                                // generate process log arr
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr("Data are imported successfully of " . $xmlFile->file_name . " file", $purchaseOrderArr['user_id'], $purchaseOrderArr['seller_gln'], $purchaseOrderArr['purchase_order_number']);

                                /*
                                 * Find Status id by name
                                 */
                                $fileStatusId = $this->checkIfFieldIsExist($this->FileStatuses->findByStatus(FileStatusesTable::Completed)->first(), 'id');

                                // update file status by completed
                                $query = $this->updateStatus($xmlFile->file_name, $fileStatusId);
                                if ($query) {

                                    $results[] = $xmlFile->file_name . " file record are updated successfully by Completed";

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . " file record are updated successfully by Completed", $purchaseOrderArr['user_id'], $purchaseOrderArr['seller_gln'], $purchaseOrderArr['purchase_order_number']);

                                    // move imported file in out folder
                                    if ($this->moveFileInAnotherFolder($xmlFile->file_name, Configure::read('po.localPath'), Configure::read('po.localPath-out'))) {

                                        $results[] = $xmlFile->file_name . " file moved successfully in Out folder";

                                        // generate process log arr
                                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . " file moved successfully in Out folder", $purchaseOrderArr['user_id'], $purchaseOrderArr['seller_gln'], $purchaseOrderArr['purchase_order_number']);
                                    } else {

                                        $results[] = $xmlFile->file_name . " file not moved successfully in Out folder";

                                        // generate process log arr
                                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . " file not moved successfully in Out folder", $purchaseOrderArr['user_id'], $purchaseOrderArr['seller_gln'], $purchaseOrderArr['purchase_order_number']);
                                    }
                                } else {

                                    $results[] = $xmlFile->file_name . " file record are not updated by Completed";

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . " file record are not updated by Completed", $purchaseOrderArr['user_id'], $purchaseOrderArr['seller_gln'], $purchaseOrderArr['purchase_order_number']);
                                }
                            } else {

                                $results[] = "Data are not imported successfully of " . $xmlFile->file_name . " file due to some reason, please try again..";

                                // generate process log arr
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr("Data are not imported successfully of " . $xmlFile->file_name . " file due to some reason, please try again..", $purchaseOrderArr['user_id'], $purchaseOrderArr['seller_gln'], $purchaseOrderArr['purchase_order_number']);
                                $this->updateStatus($xmlFile->file_name, 4);
                            }
                        } else {

                            $results[] = "Data are already imported of " . $xmlFile->file_name . " file";

                            // generate process log arr
                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr("Data are already imported of " . $xmlFile->file_name . " file", $purchaseOrderArr['user_id'], $purchaseOrderArr['seller_gln'], $purchaseOrderArr['purchase_order_number']);

                            // move imported file in out folder
                            if ($this->moveFileInAnotherFolder($xmlFile->file_name, Configure::read('po.localPath'), Configure::read('po.localPath-out'))) {

                                $results[] = $xmlFile->file_name . " file moved successfully in Out folder";

                                // generate process log arr
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . " file moved successfully in Out folder", $purchaseOrderArr['user_id'], $purchaseOrderArr['seller_gln'], $purchaseOrderArr['purchase_order_number']);
                            } else {

                                $results[] = $xmlFile->file_name . " file not moved successfully in Out folder";

                                // generate process log arr
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . " file not moved successfully in Out folder", $purchaseOrderArr['user_id'], $purchaseOrderArr['seller_gln'], $purchaseOrderArr['purchase_order_number']);
                            }
                        }
                    } else {
                        if ($xmlResult['sh:StandardBusinessDocumentHeader']['sh:DocumentIdentification']['sh:Type'] == Configure::read('po.file-type-multShipOrd')) {
                            $results[] = "In " . $xmlFile->file_name . " file data are not available or not match with existing users for import";

                            // generate process log arr
                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr("In " . $xmlFile->file_name . " file data are not available or not match with existing users for import");
                        }
                    }
                    unset($purchaseOrderArr, $userId);
                } else {

                    $results[] = $xmlFile->file_name . " file is empty";

                    // generate process log arr
                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . " file is empty");
                    
                    $this->updateStatus($xmlFile->file_name, 4);
                }
            } else {
                $results[] = $xmlFile->file_name . " file is not exist for import";

                // generate process log arr
                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile->file_name . " file is not exist for import");
                
                $this->updateStatus($xmlFile->file_name, 4);
            }
        }

        if ($resultCnt < 1) {

            $results[] = "Files are not exist for Import";

            // store process log details
            $processLogResult = $this->ProcessLogs->generateProcessLogArrAndSave("Files are not exist for Import");
        }

        // If Purchase order Imported then send notification email to user
        if ($purchaseOrderImported > 0) {

            $userIdArr = array_filter($userIdArr);
            $userIdArr = array_unique($userIdArr);
            if (!empty($userIdArr)) {

                foreach ($userIdArr as $userId) {

                    // find user data
                    // send email testing
                    $userData = $userObj->findById($userId)->first();
                    $userName = isset($userData->full_name) ? $userData->full_name : '';
                    $userEmail = isset($userData->email) ? $userData->email : '';
                    $to = [$userEmail => $userName];
                    $poNotificationSendResult = $this->purchaseOrderRequestNotificationEmail($to);
                    if ($poNotificationSendResult) {

                        $results[] = "PO notification email send successfully";
                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr("PO notification email send successfully");
                    } else {

                        $results[] = "Files are not exist for Import";
                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr("PO notification email Not Send To User Name ==>" . $userName . " User Email is ==>" . $userEmail . " User ID Is ==>" . $userData->id);
                    }
                }
            }
        }

        if (!empty($processLogArr)) {

            // save process logs
            $processLogResult = $this->ProcessLogs->saveProcessLogs($processLogArr);
        }

        return $results;
    }

    public function purchaseOrderItemsInserting($xmlArr) {

        $i = 0;
        $purchaseOrderItemArr = array();
        if (isset($xmlArr['StandardBusinessDocument']['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['multiShipmentOrderLineItem'][0])) {
            foreach ($xmlArr['StandardBusinessDocument']['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['multiShipmentOrderLineItem'] as $xmlResponse) {
                $purchaseOrderItemArr[$i]['order_line_number'] = $xmlResponse['@number'];
                $purchaseOrderItemArr[$i]['gtin'] = $xmlResponse['tradeItemIdentification']['gtin'];
                $purchaseOrderItemArr[$i]['requested_quantity'] = $xmlResponse['requestedQuantity']['value'];
                $purchaseOrderItemArr[$i]['unit_of_measure'] = $xmlResponse['requestedQuantity']['unitOfMeasure']['measurementUnitCodeValue'];
                foreach ($xmlResponse['tradeItemIdentification']['additionalTradeItemIdentification'] as $tradeItemIdentification) {
                    if ($tradeItemIdentification['additionalTradeItemIdentificationType'] == PurchaseOrderItemsTable::Supplier_Product_Id_Type) {
                        $purchaseOrderItemArr[$i]['supplier_product_id'] = $tradeItemIdentification['additionalTradeItemIdentificationValue'];
                    }
                    if ($tradeItemIdentification['additionalTradeItemIdentificationType'] == PurchaseOrderItemsTable::Bunnings_Product_Id_Type) {
                        $purchaseOrderItemArr[$i]['bunnings_product_id'] = $tradeItemIdentification['additionalTradeItemIdentificationValue'];
                    }
                    if ($tradeItemIdentification['additionalTradeItemIdentificationType'] == PurchaseOrderItemsTable::Description_Type) {
                        $purchaseOrderItemArr[$i]['description'] = $tradeItemIdentification['additionalTradeItemIdentificationValue'];
                    }
                    if ($tradeItemIdentification['additionalTradeItemIdentificationType'] == PurchaseOrderItemsTable::Pack_Quantity_Type) {
                        $purchaseOrderItemArr[$i]['pack_quantity'] = $tradeItemIdentification['additionalTradeItemIdentificationValue'];
                    }
                    if ($tradeItemIdentification['additionalTradeItemIdentificationType'] == PurchaseOrderItemsTable::Quotation_Number_Type) {
                        $purchaseOrderItemArr[$i]['quotation_number'] = $tradeItemIdentification['additionalTradeItemIdentificationValue'];
                    }
                }
                $purchaseOrderItemArr[$i]['net_price'] = $xmlResponse['netPrice']['amount']['monetaryAmount'];
                $purchaseOrderItemArr[$i]['net_amount'] = $xmlResponse['netAmount']['amount']['monetaryAmount'];
                if (isset($xmlResponse['additionalOrderLineInstruction']['language']['languageISOCode']) && $xmlResponse['additionalOrderLineInstruction']['language']['languageISOCode'] == Configure::read('po.languageISOCode')) {
                    $purchaseOrderItemArr[$i]['comments'] = isset($xmlResponse['additionalOrderLineInstruction']['longText']) ? $xmlResponse['additionalOrderLineInstruction']['longText'] : '';
                }
                $i++;
            }
        } else {
            $xmlResponse = $xmlArr['StandardBusinessDocument']['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['multiShipmentOrderLineItem'];
            $purchaseOrderItemArr[$i]['order_line_number'] = $xmlResponse['@number'];
            $purchaseOrderItemArr[$i]['gtin'] = $xmlResponse['tradeItemIdentification']['gtin'];
            $purchaseOrderItemArr[$i]['requested_quantity'] = $xmlResponse['requestedQuantity']['value'];
            $purchaseOrderItemArr[$i]['unit_of_measure'] = $xmlResponse['requestedQuantity']['unitOfMeasure']['measurementUnitCodeValue'];
            foreach ($xmlResponse['tradeItemIdentification']['additionalTradeItemIdentification'] as $tradeItemIdentification) {
                if ($tradeItemIdentification['additionalTradeItemIdentificationType'] == PurchaseOrderItemsTable::Supplier_Product_Id_Type) {
                    $purchaseOrderItemArr[$i]['supplier_product_id'] = $tradeItemIdentification['additionalTradeItemIdentificationValue'];
                }
                if ($tradeItemIdentification['additionalTradeItemIdentificationType'] == PurchaseOrderItemsTable::Bunnings_Product_Id_Type) {
                    $purchaseOrderItemArr[$i]['bunnings_product_id'] = $tradeItemIdentification['additionalTradeItemIdentificationValue'];
                }
                if ($tradeItemIdentification['additionalTradeItemIdentificationType'] == PurchaseOrderItemsTable::Description_Type) {
                    $purchaseOrderItemArr[$i]['description'] = $tradeItemIdentification['additionalTradeItemIdentificationValue'];
                }
                if ($tradeItemIdentification['additionalTradeItemIdentificationType'] == PurchaseOrderItemsTable::Pack_Quantity_Type) {
                    $purchaseOrderItemArr[$i]['pack_quantity'] = $tradeItemIdentification['additionalTradeItemIdentificationValue'];
                }
                if ($tradeItemIdentification['additionalTradeItemIdentificationType'] == PurchaseOrderItemsTable::Quotation_Number_Type) {
                    $purchaseOrderItemArr[$i]['quotation_number'] = $tradeItemIdentification['additionalTradeItemIdentificationValue'];
                }
            }
            $purchaseOrderItemArr[$i]['net_price'] = $xmlResponse['netPrice']['amount']['monetaryAmount'];
            $purchaseOrderItemArr[$i]['net_amount'] = $xmlResponse['netAmount']['amount']['monetaryAmount'];
            if (isset($xmlResponse['additionalOrderLineInstruction']['language']['languageISOCode']) && $xmlResponse['additionalOrderLineInstruction']['language']['languageISOCode'] == Configure::read('po.languageISOCode')) {
                $purchaseOrderItemArr[$i]['comments'] = isset($xmlResponse['additionalOrderLineInstruction']['longText']) ? $xmlResponse['additionalOrderLineInstruction']['longText'] : '';
            }
            $i++;
        }
        return $purchaseOrderItemArr;
    }

    /*
     * Save Data into myob database
     */

    public function insertDataInMyobDB($purchaseOrder, $purchaseOrderItems) {

        $myobConnection = ConnectionManager::get('myob');

        /* Create object of MasterItems table */
        $masterItemsObj = TableRegistry::get('MasterItems');

        $purchaseOrder['status'] = Configure::read('myob_po_status.ready_for_myob');
        $date = date('Y-m-d H:i:s');
        $purchaseOrder['created'] = $date;
        $purchaseOrder['modified'] = $date;

        // insert data in purchase_orders table
        $resutlMyobPO = $myobConnection->insert('purchase_orders', $purchaseOrder);

        // find last inserted purchase order id
        $insertedPOId = $resutlMyobPO->lastInsertId('purchase_orders', 'id');

        // insert data in purchase_order_items table
        foreach ($purchaseOrderItems as $purchaseOrderItem) {
            $resultMyobPOItems = '';
            $purchaseOrderItem['purchase_order_id'] = $insertedPOId;
            $date = date('Y-m-d H:i:s');
            $purchaseOrderItem['created'] = $date;
            $purchaseOrderItem['modified'] = $date;

            /* Get Master Items */
            $purchaOrderGtin = isset($purchaseOrderItem['gtin']) ? $purchaseOrderItem['gtin'] : '';
            $masterItemsData = $masterItemsObj->find('all', ['fields' => ['name', 'genus', 'cultival', 'species', 'pot_size'], 'conditions' => ['bar_code' => $purchaOrderGtin]]);

            if (iterator_count($masterItemsData) < 1) {
                // check gtin > 13 & prepend is O then remove it
                if (strlen($purchaOrderGtin) > 13) {
                    $prefix = '0';
                    $purchaOrderGtin = $this->removePrefix($purchaOrderGtin, $prefix);
                    $masterItemsData = $masterItemsObj->find('all', ['fields' => ['name', 'genus', 'cultival', 'species', 'pot_size'], 'conditions' => ['bar_code' => $purchaOrderGtin]]);
                }
            }

            /* Set master items data in Purchase Order Item */
            if (iterator_count($masterItemsData) > 0) {
                foreach ($masterItemsData as $masterItem) {
                    $purchaseOrderItem['name'] = $masterItem->name;
                    $purchaseOrderItem['genus'] = $masterItem->genus;
                    $purchaseOrderItem['cultival'] = $masterItem->cultival;
                    $purchaseOrderItem['species'] = $masterItem->species;
                    $purchaseOrderItem['pot_size'] = $masterItem->pot_size;
                }
            }

            $resultMyobPOItems = $myobConnection->insert('purchase_order_items', $purchaseOrderItem);
        }

        unset($myobConnection);

        return $resutlMyobPO;
    }

    /**
     * generateFulfilmentAdviceAndSendToSftp - This method generate Fulfilment Advice xml file & send to SFTP
     * @return string
     */
    public function generateFulfilmentAdviceAndSendToSftp() {

        // generate Fulfilment Advice
        $purchaseOrderObj = TableRegistry::get('PurchaseOrders');

        $purchaseOrderIdsArr = $purchaseOrderObj->find('list', ['keyField' => 'id', 'valueField' => 'id', 'conditions' => [
                        'send_fulfilment_advice_to_bunnings' => Configure::read('send_fulfilment_advice_to_bunnings.no'),
                        'archived' => Configure::read('po_archived.no')
            ]])->applyOptions(['remove_user_condition' => true])->toArray();

        $fulfilmentAdviceResult = array();
        if (!empty($purchaseOrderIdsArr)) {

            $fulfilmentAdviceResult = $purchaseOrderObj->generateFulfilmentAdviceXmlFileAndSendToSftp($purchaseOrderIdsArr);
        } else {

            $fulfilmentAdviceResult[] = 'No PO\'s available to generate Fulfilment Advice';
        }

        return $fulfilmentAdviceResult;
    }

//    public function testemailsend() {
//        $userName = "Rahul Bhatewara";
//        $userEmail = "webassic1@gmail.com";
//        $to = [$userEmail => $userName];
//        $poNotificationSendResult = $this->purchaseOrderRequestNotificationEmail($to);
//        if ($poNotificationSendResult) {
//            return "PO notification email send successfully";
//        } else {
//
//            return "PO notification email Not Send To User Name ==>" . $userName . " User Email is ==>" . $userEmail;
//        }
//    }

}
