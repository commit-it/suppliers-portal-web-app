<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use DOMDocument;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\Network\Session;
use ArrayObject;

/**
 * Invoices Model
 */
class InvoicesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {

        $this->table('invoices');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('PurchaseOrders', [
            'foreignKey' => 'purchase_order_id'
        ]);
        $this->hasMany('InvoiceItems', [
            'foreignKey' => 'invoice_id'
        ]);
        $this->hasOne('DeliveryDockets', [
            'foreignKey' => 'invoice_id'
        ]);
        $this->hasOne('BookingSheetItems', [
            'foreignKey' => 'invoice_id'
        ]);
        $this->hasMany('FileOverviewes', [
            'foreignKey' => 'invoice_id'
        ]);

        $this->addBehavior('Common');
        $this->addBehavior('Email');
        $this->addBehavior('SftpConnection');
        $this->session = new Session();

        $this->ProcessLogs = TableRegistry::get('ProcessLogs');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {

        $validator
                ->add('id', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
                ->allowEmpty('id', 'create')
                ->requirePresence('user_id', 'create')
                ->notEmpty('user_id')
                ->requirePresence('purchase_order_id', 'create')
                ->notEmpty('purchase_order_id')
                ->requirePresence('invoice_number', 'create')
                ->notEmpty('invoice_number')
                ->requirePresence('purchase_order_number', 'create')
                ->notEmpty('purchase_order_number')
                ->requirePresence('reference_delivery_note_number', 'create')
                ->notEmpty('reference_delivery_note_number')
                ->add('sub_total', 'valid', ['rule' => 'decimal', 'message' => 'This value should be Decimal'])
                ->requirePresence('sub_total', 'create')
                ->notEmpty('sub_total')
                ->add('tax', 'valid', ['rule' => 'decimal', 'message' => 'This value should be Decimal'])
                ->allowEmpty('tax')
                ->allowEmpty('sales_person')
                ->allowEmpty('ship_via')
                ->allowEmpty('invoice_status')
                ->allowEmpty('delivery_docket_no')
                ->allowEmpty('invoice_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['purchase_order_id'], 'PurchaseOrders'));
        return $rules;
    }

    /**
     * Return query for auth user
     * @param \Cake\Event\Event $event
     * @param \Cake\ORM\Query $query
     * @param ArrayObject $options
     * @return boolean|\Cake\ORM\Query
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options) {
        /**
         * This condition is checked when we don't need to check current user 
         */
        if (isset($options['remove_user_condition']) && $options['remove_user_condition'] > 0) {
            return TRUE;
        }
        $query->where([$this->alias() . '.user_id' => $this->session->read('Auth.User.id')]);
        return $query;
    }

    /**
     * getGeneratedInvoicesFromMyob() method.
     *
     * @return created xml file invoice
     */
    public function getGeneratedInvoicesFromMyob() {

        $results = array();
        $processLogArr = array();
        $userSellerGln = '';
        $purchaseOrderItemsObj = TableRegistry::get('PurchaseOrderItems');

        /*
         * check dir is exist
         * if not then create
         */
        $dirResult = $this->makeDirectoryIfNotExist(Configure::read('invoice.localPath'));

        // get all Purchase Orders which are Ready for Portal from myob.
        $myobConnection = ConnectionManager::get('myob');

        $status = Configure::read('myob_po_status.ready_for_portal');
        $conditions = ['purchase_orders.status' => $status];

        /* Get current user id */
        $userId = $this->session->read('Auth.User.id');

        /* If user id found then append condition get only those record which user seller id is matched with purchase order seller id */
        if ($userId != '') {
            $conditions = array_merge($conditions, ['purchase_orders.seller_gln' => $this->session->read('Auth.User.seller_gln')]);
        }

        $purchaseOrderResult = $myobConnection
                ->newQuery()
                ->select('purchase_orders.*, pos.status, cu.currency_code')
                ->from('purchase_orders')
                ->leftJoin(['pos' => 'purchase_order_statuses'], ['pos.id = purchase_orders.purchase_order_status_id'])
                ->leftJoin(['cu' => 'currencies'], ['cu.id = purchase_orders.currency_id'])
                ->where($conditions)
                ->execute()
                ->fetchAll('assoc');

        if (!empty($purchaseOrderResult)) {

            foreach ($purchaseOrderResult as $purchaseOrder) {

                // find the purhcase order items
                $purchaseOrderItems = $myobConnection
                        ->newQuery()
                        ->select('*')
                        ->from('purchase_order_items')
                        ->where(['purchase_order_id' => $purchaseOrder['id']])
                        ->execute()
                        ->fetchAll('assoc');

                // find the invoice data
                $invoices = $myobConnection
                        ->newQuery()
                        ->select('*')
                        ->from('invoices')
                        ->where(['purchase_order_id' => $purchaseOrder['id']])
                        ->execute()
                        ->fetch('assoc');

                // find the invoice_items data
                $invoiceItems = $myobConnection
                        ->newQuery()
                        ->select('*')
                        ->from('invoice_items')
                        ->where(['purchase_order_id' => $purchaseOrder['id']])
                        ->execute()
                        ->fetchAll('assoc');

                /* Get max values of colunms */
                $columns = array('order_line_number', 'reference_purchase_order_line_number');
                $maxValuesInvoiceItems = $this->getMaxColumnValue($columns, $purchaseOrder['id'], $myobConnection);

                /* Get max order_line_number */
                $maxOrderLineNumber = isset($maxValuesInvoiceItems[0]['max_order_line_number']) && $maxValuesInvoiceItems[0]['max_order_line_number'] > 0 ? $maxValuesInvoiceItems[0]['max_order_line_number'] : 0;

                /* Get max reference_purchase_order_line_number */
                $maxReferencePurchaseOrderLineNumber = isset($maxValuesInvoiceItems[0]['max_reference_purchase_order_line_number']) && $maxValuesInvoiceItems[0]['max_reference_purchase_order_line_number'] > 0 ? $maxValuesInvoiceItems[0]['max_reference_purchase_order_line_number'] : 0;

                if (!empty($purchaseOrderItems) && !empty($invoices) && !empty($invoiceItems)) {
                    $pruchaseOrderData = $this->PurchaseOrders->findByPurchaseOrderNumberAndSellerGln($invoices['purchase_order_number'], $invoices['nursery_gln'])->applyOptions(['remove_user_condition' => true])->first();

                    if (isset($pruchaseOrderData->id)) {
                        $myobInvoiceId = $invoices['id'];
                        $InvoiceId = $this->checkIfFieldIsExist($this->findByPurchaseOrderNumber($invoices['purchase_order_number'])->applyOptions(['remove_user_condition' => true])->first(), 'id');

                        // insert record in invoices & invoice_items table of supplier_portal db
                        unset($invoices['id']);
                        unset($invoices['created']);
                        unset($invoices['modified']);
                        $invoices['user_id'] = $pruchaseOrderData->user_id;
                        $invoices['purchase_order_id'] = $pruchaseOrderData->id;
                        $invoices['reference_delivery_note_number'] = $invoices['invoice_number'];
                        $invoices['resend_invoice'] = Configure::read('resend_invoice.no');

                        // check tax already applier or not, if not then apply default
                        if ($invoices['tax'] < 1) {
                            $gstPer = Configure::read('invoice.gst-per');
                        } else {
                            $gstPer = $invoices['tax'];
                        }

                        $invoices['tax'] = $gstPer;
                        if ($InvoiceId != null) {
                            $invoices['id'] = $InvoiceId;
                        }

                        $incrementOrderLineNumber = 1;
                        $incrementReferencePurchaseOrderLineNumber = 1;

                        // Modify invoice_items data
                        for ($i = 0; $i < count($invoiceItems); $i++) {
                            unset($invoiceItems[$i]['id']);
                            unset($invoiceItems[$i]['invoice_id']);
                            unset($invoiceItems[$i]['name']);
                            unset($invoiceItems[$i]['genus']);
                            unset($invoiceItems[$i]['species']);
                            unset($invoiceItems[$i]['cultival']);
                            unset($invoiceItems[$i]['pot_size']);
                            unset($invoiceItems[$i]['created']);
                            unset($invoiceItems[$i]['modified']);
                            $invoiceItems[$i]['purchase_order_id'] = $pruchaseOrderData->id;

                            $invoiceItems[$i]['gst_percentage'] = $gstPer;
                            $invoiceItems[$i]['gst_amount'] = ($invoiceItems[$i]['net_amount'] * $gstPer) / 100;

                            /* If order_line_number is blank then assign $maxOrderLineNumber+$increment to this */
                            $invoiceItems[$i]['order_line_number'] = ($invoiceItems[$i]['order_line_number'] != '') ? $invoiceItems[$i]['order_line_number'] : 0;
                            if (isset($invoiceItems[$i]['order_line_number']) && $invoiceItems[$i]['order_line_number'] < 1) {
                                $invoiceItems[$i]['order_line_number'] = $maxOrderLineNumber + $incrementOrderLineNumber;
                                $incrementOrderLineNumber++;
                            }

                            /* If reference_purchase_order_line_number is blank then assign $maxReferencePurchaseOrderLineNumber+$increment to this */
                            $invoiceItems[$i]['reference_purchase_order_line_number'] = ($invoiceItems[$i]['reference_purchase_order_line_number'] != '') ? $invoiceItems[$i]['reference_purchase_order_line_number'] : 0;
                            if (isset($invoiceItems[$i]['reference_purchase_order_line_number']) && $invoiceItems[$i]['reference_purchase_order_line_number'] < 1) {
                                $invoiceItems[$i]['reference_purchase_order_line_number'] = $maxReferencePurchaseOrderLineNumber + $incrementReferencePurchaseOrderLineNumber;
                                $incrementReferencePurchaseOrderLineNumber++;
                            }
                        }

                        $invoices['invoice_items'] = $invoiceItems;

                        $invoiceEntity = $this->newEntity($invoices, [
                            'associated' => ['InvoiceItems']
                        ]);

                        $invoiceResult = $this->save($invoiceEntity, ['associated' => ['InvoiceItems']]);
                        if ($invoiceResult) {

                            $results[] = 'Invoice is inserted for ' . $invoices['purchase_order_number'] . ' this Purchase order in supplier_portal database';

                            // generate process log arr
                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Invoice is inserted for ' . $invoices['purchase_order_number'] . ' this Purchase order in supplier_portal database', $pruchaseOrderData->user_id, $pruchaseOrderData->seller_gln, $pruchaseOrderData->purchase_order_number);

                            // update purchase_orders tables of supplier_portal db from myob_supplier_portal
                            $updateInvoiceNumber = $this->PurchaseOrders->updateInvoiceNumberByPurchaseOrderNumber($invoices['invoice_number'], $invoices['purchase_order_number']);


                            if ($updateInvoiceNumber) {

                                $results[] = 'Invoice number updated for ' . $invoices['purchase_order_number'] . ' this Purchase order in purchase_orders table of supplier_portal database';

                                // generate process log arr
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Invoice number updated for ' . $invoices['purchase_order_number'] . ' this Purchase order in purchase_orders table of supplier_portal database', $pruchaseOrderData->user_id, $pruchaseOrderData->seller_gln, $pruchaseOrderData->purchase_order_number);

                                $poi = 0;

                                // update purchase_order_items tables of supplier_portal db from myob_supplier_portal
                                foreach ($purchaseOrderItems as $purchaseOrderItem) {

                                    $query = $purchaseOrderItemsObj->query();
                                    $query->update()
                                            ->set(['reference_purchase_order_line_number' => $purchaseOrderItem['reference_purchase_order_line_number'], 'gst_amount' => $purchaseOrderItem['gst_amount'], 'gst_percentage' => $purchaseOrderItem['gst_percentage']])
                                            ->where(['purchase_order_id' => $pruchaseOrderData->id, 'gtin' => $purchaseOrderItem['gtin']]);
                                    $updateResult = $query->execute();
                                    if ($updateResult) {
                                        $poi++;
                                    }
                                }

                                if ($poi == count($purchaseOrderItems)) {

                                    $results[] = 'All purchase order items updated successfully';

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('All purchase order items updated successfully', $pruchaseOrderData->user_id, $pruchaseOrderData->seller_gln, $pruchaseOrderData->purchase_order_number);
                                } else {

                                    $results[] = 'All purchase order items are not updated successfully';

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('All purchase order items are not updated successfully', $pruchaseOrderData->user_id, $pruchaseOrderData->seller_gln, $pruchaseOrderData->purchase_order_number);
                                }

                                // delete generated invoice from myob_supplier_portal db
                                $deleteInvoice = $myobConnection->newQuery()
                                        ->delete()
                                        ->from('invoices')
                                        ->where(['purchase_order_number' => $invoices['purchase_order_number']])
                                        ->execute();
                                if ($deleteInvoice) {

                                    $results[] = 'Invoice deleted successfully in myob database';

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Invoice deleted successfully in myob database', $pruchaseOrderData->user_id, $pruchaseOrderData->seller_gln, $pruchaseOrderData->purchase_order_number);
                                } else {

                                    $results[] = 'Invoice is not deleted successfully in myob database';

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Invoice is not deleted successfully in myob database', $pruchaseOrderData->user_id, $pruchaseOrderData->seller_gln, $pruchaseOrderData->purchase_order_number);
                                }

                                // delete generated invoice_items from myob_supplier_portal db
                                $deleteInvoiceItems = $myobConnection->newQuery()
                                        ->delete()
                                        ->from('invoice_items')
                                        ->where(['invoice_id' => $myobInvoiceId])
                                        ->execute();
                                if ($deleteInvoiceItems) {

                                    $results[] = 'Invoice Items deleted successfully in myob database';

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Invoice Items deleted successfully in myob database', $pruchaseOrderData->user_id, $pruchaseOrderData->seller_gln, $pruchaseOrderData->purchase_order_number);
                                } else {

                                    $results[] = 'Invoice Items is not deleted successfully in myob database';

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Invoice Items is not deleted successfully in myob database', $pruchaseOrderData->user_id, $pruchaseOrderData->seller_gln, $pruchaseOrderData->purchase_order_number);
                                }

                                // delete purchase_orders from myob_supplier_portal db
                                $deletePurchaseOrder = $myobConnection->newQuery()
                                        ->delete()
                                        ->from('purchase_orders')
                                        ->where(['purchase_order_number' => $invoices['purchase_order_number']])
                                        ->execute();
                                if ($deletePurchaseOrder) {

                                    $results[] = 'Purchase Order deleted successfully in myob database';

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Purchase Order deleted successfully in myob database', $pruchaseOrderData->user_id, $pruchaseOrderData->seller_gln, $pruchaseOrderData->purchase_order_number);
                                } else {

                                    $results[] = 'Purchase Order is not deleted successfully in myob database';

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Purchase Order is not deleted successfully in myob database', $pruchaseOrderData->user_id, $pruchaseOrderData->seller_gln, $pruchaseOrderData->purchase_order_number);
                                }

                                // delete purchase_order_items from myob_supplier_portal db
                                $deletePurchaseOrderItems = $myobConnection->newQuery()
                                        ->delete()
                                        ->from('purchase_order_items')
                                        ->where(['purchase_order_id' => $purchaseOrder['id']])
                                        ->execute();
                                if ($deletePurchaseOrderItems) {

                                    $results[] = 'Purchase Order Items deleted successfully in myob database';

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Purchase Order Items deleted successfully in myob database', $pruchaseOrderData->user_id, $pruchaseOrderData->seller_gln, $pruchaseOrderData->purchase_order_number);
                                } else {

                                    $results[] = 'Purchase Order Items is not deleted successfully in myob database';

                                    // generate process log arr
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Purchase Order Items is not deleted successfully in myob database', $pruchaseOrderData->user_id, $pruchaseOrderData->seller_gln, $pruchaseOrderData->purchase_order_number);
                                }
                            } else {

                                $results[] = 'Invoice number not updated for ' . $invoices['purchase_order_number'] . ' this Purchase order in purchase_orders table of supplier_portal database';

                                // generate process log arr
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Invoice number not updated for ' . $invoices['purchase_order_number'] . ' this Purchase order in purchase_orders table of supplier_portal database', $pruchaseOrderData->user_id, $pruchaseOrderData->seller_gln, $pruchaseOrderData->purchase_order_number);
                            }
                        } else {

                            $results[] = 'Invoice is not inserted for ' . $invoices['purchase_order_number'] . ' this Purchase order in supplier_portal database';

                            // generate process log arr
                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Invoice is not inserted for ' . $invoices['purchase_order_number'] . ' this Purchase order in supplier_portal database', $pruchaseOrderData->user_id, $pruchaseOrderData->seller_gln, $pruchaseOrderData->purchase_order_number);
                        }
                    } else {

                        $results[] = $invoices['purchase_order_number'] . ' this Purchase order is not exist for invoice in table';

                        // generate process log arr
                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($invoices['purchase_order_number'] . ' this Purchase order is not exist for invoice in table');
                    }
                } else {

                    $results[] = 'No data available in purchase_order_items or invoices or invoice_items tables for ' . $purchaseOrder['purchase_order_number'] . ' this purchase order number';

                    // generate process log arr
                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('No data available in purchase_order_items or invoices or invoice_items tables for ' . $purchaseOrder['purchase_order_number'] . ' this purchase order number');
                }
            }
        } else {

            $results[] = 'Invoices are not ready for any purchase orders';

            // generate process log arr
            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Invoices are not ready for any purchase orders');
        }

        if (!empty($processLogArr)) {

            // save process logs
            $processLogResult = $this->ProcessLogs->saveProcessLogs($processLogArr);
        }

        return $results;
    }

    /**
     * Get Max Column Value 
     * @param type $column
     * @param type $purchaseOrderId
     * @return type Max Column Value 
     */
    public function getMaxColumnValue($columns = NULL, $purchaseOrderId, $myobConnection) {
        /* Define variables */
        $result = '';
        $fields = array();
        if (isset($columns) && !empty($columns)) {
            /* Create array for max value fields */
            foreach ($columns as $column) {
                $fields[] = 'max(' . $column . ') as max_' . $column;
            }

            /* Get Max Column Value */
            try {
                $result = $myobConnection
                        ->execute('SELECT ' . implode(", ", $fields) . ' FROM invoice_items WHERE purchase_order_id = :purchase_order_id', ['purchase_order_id' => $purchaseOrderId])
                        ->fetchAll('assoc');
            } catch (Exception $ex) {
                $result = '';
            }
        }

        /* Return result */
        return $result;
    }

    /**
     * generateInvoiceXmlFileAndSendToSftp - Method
     * @param type $invoiceIds
     * @param type $invoiceGenerated
     * @return string
     */
    public function generateInvoiceXmlFileAndSendToSftp($invoiceIds = array(), $invoiceGenerated = null) {

        /*
         * check dir is exist
         * if not then create
         */
        $dirResult = $this->makeDirectoryIfNotExist(Configure::read('invoice.localPath'));

        $results = array();
        $processLogArr = array();

        // create object
        $bunningStoresObj = TableRegistry::get('BunningStores');

        // Below code for regenerated invoice
        $updateResendInvoice = Configure::read('resend_invoice.yes');
        if ($invoiceGenerated == null) {
            $invoiceGenerated = Configure::read('invoice_generated.no');
            $updateResendInvoice = Configure::read('resend_invoice.no');
        }

        if (!empty($invoiceIds)) {
            $invoiceArr = $this->find('all', [
                        'conditions' => array(
                            'Invoices.invoice_generated' => $invoiceGenerated,
                            'Invoices.resend_invoice' => Configure::read('resend_invoice.no'),
                            'Invoices.id IN' => $invoiceIds
                        ),
                        'contain' => ['InvoiceItems', 'PurchaseOrders', 'PurchaseOrders.Currencies', 'PurchaseOrders.PurchaseOrderStatuses']
                    ])->toArray();
        } else {
            $invoiceArr = $this->find('all', [
                        'conditions' => array(
                            'Invoices.invoice_generated' => $invoiceGenerated,
                            'Invoices.resend_invoice' => Configure::read('resend_invoice.no')
                        ),
                        'contain' => ['InvoiceItems', 'PurchaseOrders', 'PurchaseOrders.Currencies', 'PurchaseOrders.PurchaseOrderStatuses']]
                    )->applyOptions(['remove_user_condition' => true])->toArray();
        }

        if (!empty($invoiceArr)) {
            foreach ($invoiceArr as $invoice) {

                $purchaseOrder = isset($invoice['purchase_order']) ? $invoice['purchase_order'] : array();
                $invoiceItems = isset($invoice['invoice_items']) ? $invoice['invoice_items'] : array();

                if (!empty($invoice) && !empty($purchaseOrder) && !empty($invoiceItems)) {

                    // generate invoice xml file
                    $xmlFile = Configure::read('invoice.file-prefix') . strtotime('now') . '_' . $this->generateRandomCode(5);

                    // Before generate invoice check buyer and payer is available in list
                    if (!empty($invoiceIds)) {
                        $buyerId = $this->checkIfFieldIsExist($bunningStoresObj->findByGlnAndType($purchaseOrder['buyer_gln'], Configure::read('bunning_user.Buyer'))->first(), 'id');
                        $payerId = $this->checkIfFieldIsExist($bunningStoresObj->findByGlnAndType($purchaseOrder['bill_to_gln'], Configure::read('bunning_user.Payer'))->first(), 'id');
                    } else {
                        $buyerId = $this->checkIfFieldIsExist($bunningStoresObj->findByGlnAndTypeAndUserId($purchaseOrder['buyer_gln'], Configure::read('bunning_user.Buyer'), $invoice['user_id'])->applyOptions(['remove_user_condition' => true])->first(), 'id');
                        $payerId = $this->checkIfFieldIsExist($bunningStoresObj->findByGlnAndTypeAndUserId($purchaseOrder['bill_to_gln'], Configure::read('bunning_user.Payer'), $invoice['user_id'])->applyOptions(['remove_user_condition' => true])->first(), 'id');
                    }

                    if ($buyerId && $payerId) {

                        $invoiceGenerateResult = $this->generateInvoiceXmlFile($purchaseOrder, $invoiceItems, $invoice, $xmlFile);
                        $results[] = $invoiceGenerateResult;

                        // store process log details
                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($invoiceGenerateResult, $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);

                        /*
                         * @connect sftp using sftpConnection() method
                         */
                        $resSFTP = $this->sftpConnection();
                        if ($resSFTP) {

                            $uploadFileResult = $this->upload(Configure::read('invoice.localPath') . DS . $xmlFile . '.xml', Configure::read('invoice.remotePath') . DS . $xmlFile . '.xml');
                            if ($uploadFileResult) {

                                $results[] = $xmlFile . ' this generated invoice file uploaded successfully to sftp';

                                // store process log details
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . ' this generated invoice file uploaded successfully to sftp', $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);

                                // move uploaded file in out folder
                                if ($this->moveFileInAnotherFolder($xmlFile . '.xml', Configure::read('invoice.localPath'), Configure::read('invoice.localPath-out'))) {

                                    $results[] = $xmlFile . " file moved successfully in Out folder.";

                                    // store process log details
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . " file moved successfully in Out folder", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);

                                    // update invoice generated status
                                    $updateArr = array(
                                        'invoice_generated' => Configure::read('invoice_generated.yes'),
                                        'time_of_invoice_sent_to_bunnings' => date('Y-m-d H:i:s'),
                                        'resend_invoice' => $updateResendInvoice
                                    );
                                    $updateResult = $this->updateFieldsByField('id', $invoice['id'], $updateArr);
                                    if ($updateResult) {

                                        $results[] = $invoice['invoice_number'] . " invoice_generated field is updated successfully by Yes";

                                        // store process log details
                                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($invoice['invoice_number'] . " invoice_generated field is updated successfully by Yes", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                                    } else {

                                        $results[] = $invoice['invoice_number'] . " invoice_generated field is not updated successfully by Yes";

                                        // store process log details
                                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($invoice['invoice_number'] . " invoice_generated field is not updated successfully by Yes", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                                    }
                                } else {

                                    $results[] = $xmlFile . " file not moved successfully in Out folder";

                                    // store process log details
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . " file not moved successfully in Out folder", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                                }
                            } else {

                                $results[] = $xmlFile . ' this generated invoice file not uploaded successfully to sftp';

                                // store process log details
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . ' this generated invoice file not uploaded successfully to sftp', $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                            }
                        } else {

                            $results[] = "Some error is occurred in sftp connection";

                            // store process log details
                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr("Some error is occurred in sftp connection", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                        }
                    } else {

                        if (!$buyerId) {

                            $results[] = $purchaseOrder['buyer_gln'] . " this buyer is not present in Bunning Stores to generate invoice of " . $purchaseOrder['purchase_order_number'] . " Purchase Order";

                            // store process log details
                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($purchaseOrder['buyer_gln'] . " this buyer is not present in Bunning Stores to generate invoice of " . $purchaseOrder['purchase_order_number'] . " Purchase Order", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                        }
                        if (!$payerId) {

                            $results[] = $purchaseOrder['bill_to_gln'] . " this payer is not present in Bunning Stores to generate invoice of " . $purchaseOrder['purchase_order_number'] . " Purchase Order";

                            // store process log details
                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($purchaseOrder['bill_to_gln'] . " this payer is not present in Bunning Stores to generate invoice of " . $purchaseOrder['purchase_order_number'] . " Purchase Order", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                        }
                    }
                } else {

                    $results[] = "Getting empty data of purchase_orders or invoices or invoice_items";

                    // store process log details
                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr("Getting empty data of purchase_orders or invoices or invoice_items");
                }
            }
        } else {

            $results[] = "No invoice available for generate.";

            // store process log details
            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr("No invoice available for generate.");
        }

        if (!empty($processLogArr)) {

            // save process logs
            $processLogResult = $this->ProcessLogs->saveProcessLogs($processLogArr);
        }

        return $results;
    }

    /*
     * @generate Invoice Xml File
     */

    public function generateInvoiceXmlFile($purchaseOrder, $invoiceItems, $invoices, $xmlFile) {

        // generate xml for invoice
        $invoiceXml = $this->generateInvoiceXml($purchaseOrder, $invoiceItems, $invoices);
        $doc = new DOMDocument();
        $doc->formatOutput = true;
        try {

            $doc->loadXML($invoiceXml);
            $fileResult = $doc->save(Configure::read('invoice.localPath') . DS . $xmlFile . '.xml');
            if ($fileResult) {
                return "Invoice xml file generated successfully...";
            } else {
                return "Invoice xml file doesn't generated, pleae try again.";
            }
        } catch (\Cake\Utility\Exception\XmlException $e) {
            return InternalErrorException();
        }
    }

    /*
     * @generate Invoice Xml
     */

    public function generateInvoiceXml($purchaseOrder, $invoiceItems, $invoice) {

        // Instance Identifier of Fulfilment Advice Document
        $instanceIdentifier = $this->generateRandomNumber(7);

        $invoiceXml = '';
        $invoiceXml = $invoiceXml . '<?xml version="1.0" encoding="UTF-8"?>
        <sh:StandardBusinessDocument xmlns:sh="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader" xmlns:eanucc="urn:ean.ucc:2" xmlns:pay="urn:ean.ucc:pay:2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader ../Schemas/sbdh/StandardBusinessDocumentHeader.xsd urn:ean.ucc:2 ../Schemas/InvoiceProxy.xsd">
            <sh:StandardBusinessDocumentHeader>
                <sh:HeaderVersion>1.0</sh:HeaderVersion>
                <sh:Sender>
                    <sh:Identifier Authority="EAN.UCC">' . $purchaseOrder['receiver_gln'] . '</sh:Identifier>
                </sh:Sender>
                <sh:Receiver>
                    <sh:Identifier Authority="EAN.UCC">' . $purchaseOrder['buyer_gln'] . '</sh:Identifier>
                </sh:Receiver>
                <sh:DocumentIdentification>
                    <sh:Standard>EAN.UCC</sh:Standard>
                    <sh:TypeVersion>2.4</sh:TypeVersion>
                    <sh:InstanceIdentifier>' . $instanceIdentifier . '</sh:InstanceIdentifier>
                    <sh:Type>Invoice</sh:Type>
                    <sh:CreationDateAndTime>' . date("Y-m-d\TH:i:s") . '</sh:CreationDateAndTime>
                </sh:DocumentIdentification>
            </sh:StandardBusinessDocumentHeader>
            <eanucc:message>
                <entityIdentification>
                    <uniqueCreatorIdentification>MSG-' . $instanceIdentifier . '</uniqueCreatorIdentification>
                    <contentOwner>
                        <gln>' . $purchaseOrder['seller_gln'] . '</gln>
                    </contentOwner>
                </entityIdentification>
                <eanucc:transaction>
                    <entityIdentification>
                        <uniqueCreatorIdentification>TRA-' . $instanceIdentifier . '</uniqueCreatorIdentification>
                        <contentOwner>
                            <gln>' . $purchaseOrder['seller_gln'] . '</gln>
                        </contentOwner>
                    </entityIdentification>
                    <command>
                        <eanucc:documentCommand>
                            <documentCommandHeader type="ADD">
                                <entityIdentification>
                                    <uniqueCreatorIdentification>CMD-' . $instanceIdentifier . '</uniqueCreatorIdentification>
                                    <contentOwner>
                                        <gln>' . $purchaseOrder['seller_gln'] . '</gln>
                                    </contentOwner>
                                </entityIdentification>
                            </documentCommandHeader>
                            <documentCommandOperand>
                                <pay:invoice creationDateTime="' . date("Y-m-d\TH:i:s") . '" documentStatus="' . $purchaseOrder['purchase_order_status']['status'] . '">
                                    <contentVersion>
                                        <versionIdentification>2.4</versionIdentification>
                                    </contentVersion>
                                    <documentStructureVersion>
                                        <versionIdentification>2.4</versionIdentification>
                                    </documentStructureVersion>
                                    <invoiceIdentification>
                                        <uniqueCreatorIdentification>' . $invoice['invoice_number'] . '</uniqueCreatorIdentification>
                                        <contentOwner>
                                            <gln>' . $purchaseOrder['seller_gln'] . '</gln>
                                        </contentOwner>
                                    </invoiceIdentification>
                                    <invoiceCurrency>
                                        <currencyISOCode>' . $purchaseOrder['currency']['currency_code'] . '</currencyISOCode>
                                    </invoiceCurrency>
                                    <invoiceType>' . $invoice['invoice_type'] . '</invoiceType>
                                    <countryOfSupplyOfGoods>
                                        <countryISOCode>AU</countryISOCode>
                                    </countryOfSupplyOfGoods>';
//                                    <shipTo>
//                                        <shipTo>
//                                            <gln>' . $purchaseOrder['bill_to_gln'] . '</gln>
//                                            <additionalPartyIdentification>
//                                                <additionalPartyIdentificationValue>' . $purchaseOrder['bill_to_additional_id'] . '</additionalPartyIdentificationValue>
//                                                <additionalPartyIdentificationType>BUYER_ASSIGNED_IDENTIFIER_FOR_A_PARTY</additionalPartyIdentificationType>
//                                            </additionalPartyIdentification>
//                                            <additionalPartyIdentification>
//                                                <additionalPartyIdentificationValue>' . $purchaseOrder['bill_to_name'] . '</additionalPartyIdentificationValue>
//                                                <additionalPartyIdentificationType>FOR_INTERNAL_USE_1</additionalPartyIdentificationType>
//                                            </additionalPartyIdentification>
//                                            <additionalPartyIdentification>
//                                                <additionalPartyIdentificationValue>' . $purchaseOrder['bill_to_address'] . '</additionalPartyIdentificationValue>
//                                                <additionalPartyIdentificationType>FOR_INTERNAL_USE_2</additionalPartyIdentificationType>
//                                            </additionalPartyIdentification>
//                                            <additionalPartyIdentification>
//                                                <additionalPartyIdentificationValue>' . $purchaseOrder['bill_to_telephone_number'] . '</additionalPartyIdentificationValue>
//                                                <additionalPartyIdentificationType>FOR_INTERNAL_USE_3</additionalPartyIdentificationType>
//                                            </additionalPartyIdentification>
//                                            <additionalPartyIdentification>
//                                                <additionalPartyIdentificationValue>' . $purchaseOrder['bill_to_fax_number'] . '</additionalPartyIdentificationValue>
//                                                <additionalPartyIdentificationType>FOR_INTERNAL_USE_4</additionalPartyIdentificationType>
//                                            </additionalPartyIdentification>
//                                        </shipTo>
//                                        <shipFrom>
//                                            <gln>' . $purchaseOrder['seller_gln'] . '</gln>
//                                            <additionalPartyIdentification>
//                                                <additionalPartyIdentificationValue>' . $purchaseOrder['seller_additional_id'] . '</additionalPartyIdentificationValue>
//                                                <additionalPartyIdentificationType>BUYER_ASSIGNED_IDENTIFIER_FOR_A_PARTY</additionalPartyIdentificationType>
//                                            </additionalPartyIdentification>
//                                            <additionalPartyIdentification>
//                                                <additionalPartyIdentificationValue>' . $purchaseOrder['seller_name'] . '</additionalPartyIdentificationValue>
//                                                <additionalPartyIdentificationType>FOR_INTERNAL_USE_1</additionalPartyIdentificationType>
//                                            </additionalPartyIdentification>
//                                            <additionalPartyIdentification>
//                                                <additionalPartyIdentificationValue>' . $purchaseOrder['ship_from_address'] . '</additionalPartyIdentificationValue>
//                                                <additionalPartyIdentificationType>FOR_INTERNAL_USE_2</additionalPartyIdentificationType>
//                                            </additionalPartyIdentification>
//                                            <additionalPartyIdentification>
//                                                <additionalPartyIdentificationValue>' . $purchaseOrder['ship_from_telephone_number'] . '</additionalPartyIdentificationValue>
//                                                <additionalPartyIdentificationType>FOR_INTERNAL_USE_3</additionalPartyIdentificationType>
//                                            </additionalPartyIdentification>
//                                            <additionalPartyIdentification>
//                                                <additionalPartyIdentificationValue>' . $purchaseOrder['ship_from_fax_number'] . '</additionalPartyIdentificationValue>
//                                                <additionalPartyIdentificationType>FOR_INTERNAL_USE_4</additionalPartyIdentificationType>
//                                            </additionalPartyIdentification>
//                                        </shipFrom>
//                                    </shipTo>
        $invoiceXml = $invoiceXml . '<payer>
                                        <partyIdentification>
                                            <gln>' . $purchaseOrder['bill_to_gln'] . '</gln>
                                            <additionalPartyIdentification>
                                                <additionalPartyIdentificationValue>' . $purchaseOrder['bill_to_additional_id'] . '</additionalPartyIdentificationValue>
                                                <additionalPartyIdentificationType>BUYER_ASSIGNED_IDENTIFIER_FOR_A_PARTY</additionalPartyIdentificationType>
                                            </additionalPartyIdentification>
                                        </partyIdentification>';

        $payerAddress = $this->separateUserAddressForGenerateInvoice($purchaseOrder['bill_to_address'], 'BunningStores', 'findByGln', $purchaseOrder['bill_to_gln']);
        $invoiceXml = $invoiceXml . '<nameAndAddress>
                                            <city>' . $payerAddress['city'] . '</city>
                                            <countryCode>
                                                <countryISOCode>' . $payerAddress['countryISOCode'] . '</countryISOCode>
                                            </countryCode>
                                            <languageOfTheParty>
                                                <languageISOCode>' . $payerAddress['languageISOCode'] . '</languageISOCode>
                                            </languageOfTheParty>
                                            <name> ' . htmlspecialchars($purchaseOrder['bill_to_name']) . '</name>
                                            <postalCode>' . $payerAddress['postalCode'] . '</postalCode>
                                            <state>' . $payerAddress['state'] . '</state>
                                            <streetAddressOne>' . $payerAddress['streetAddressOne'] . '</streetAddressOne>
                                        </nameAndAddress>
                                    </payer>
                                    <buyer>
                                        <partyIdentification>
                                            <gln>' . $purchaseOrder['buyer_gln'] . '</gln>
                                            <additionalPartyIdentification>
                                                <additionalPartyIdentificationValue>' . $purchaseOrder['buyer_additional_id'] . '</additionalPartyIdentificationValue>
                                                <additionalPartyIdentificationType>BUYER_ASSIGNED_IDENTIFIER_FOR_A_PARTY</additionalPartyIdentificationType>
                                            </additionalPartyIdentification>
                                        </partyIdentification>';

        $buyerAdd = array();
        $buyerAddress = $this->separateUserAddressForGenerateInvoice($buyerAdd, 'BunningStores', 'findByGln', $purchaseOrder['buyer_gln']);
        $invoiceXml = $invoiceXml . '<nameAndAddress>
                                            <city>' . $buyerAddress['city'] . '</city>
                                            <countryCode>
                                                <countryISOCode>' . $buyerAddress['countryISOCode'] . '</countryISOCode>
                                            </countryCode>
                                            <languageOfTheParty>
                                                <languageISOCode>' . $buyerAddress['languageISOCode'] . '</languageISOCode>
                                            </languageOfTheParty>
                                            <name>' . htmlspecialchars($purchaseOrder['buyer_name']) . '</name>
                                            <postalCode>' . $buyerAddress['postalCode'] . '</postalCode>
                                            <state>' . $buyerAddress['state'] . '</state>
                                            <streetAddressOne>' . $buyerAddress['streetAddressOne'] . '</streetAddressOne>
                                        </nameAndAddress>
                                        <companyRegistrationNumber>' . $buyerAddress['companyRegistrationNumber'] . '</companyRegistrationNumber>
                                    </buyer>
                                    <seller>
                                        <partyIdentification>
                                            <gln>' . $purchaseOrder['seller_gln'] . '</gln>
                                            <additionalPartyIdentification>
                                                <additionalPartyIdentificationValue>' . $purchaseOrder['seller_additional_id'] . '</additionalPartyIdentificationValue>
                                                <additionalPartyIdentificationType>BUYER_ASSIGNED_IDENTIFIER_FOR_A_PARTY</additionalPartyIdentificationType>
                                            </additionalPartyIdentification>
                                        </partyIdentification>';

        $sellerAddress = $this->separateUserAddressForGenerateInvoice($purchaseOrder['ship_from_address'], 'Users', 'findBySellerGln', $purchaseOrder['seller_gln']);
        $invoiceXml = $invoiceXml . '<nameAndAddress>
                                            <city>' . $sellerAddress['city'] . '</city>
                                            <countryCode>
                                                <countryISOCode>' . $sellerAddress['countryISOCode'] . '</countryISOCode>
                                            </countryCode>
                                            <languageOfTheParty>
                                                <languageISOCode>' . $sellerAddress['languageISOCode'] . '</languageISOCode>
                                            </languageOfTheParty>
                                            <name>' . htmlspecialchars($purchaseOrder['seller_name']) . '</name>
                                            <postalCode>' . $sellerAddress['postalCode'] . '</postalCode>
                                            <state>' . $sellerAddress['state'] . '</state>
                                            <streetAddressOne>' . $sellerAddress['streetAddressOne'] . '</streetAddressOne>
                                        </nameAndAddress>
                                        <companyRegistrationNumber>' . $sellerAddress['companyRegistrationNumber'] . '</companyRegistrationNumber>
                                    </seller>';
        $totalTaxAmount = 0;
        $totalInvoiceAmount = 0;

        // check the purchase order is offrange order or not
        $offRangeOrderPos = strpos($purchaseOrder['purchase_order_number'], 'C');
        $directShipOffRangeOrderPos = strpos($purchaseOrder['purchase_order_number'], 'D');

        foreach ($invoiceItems as $invoiceItem) {

            $invoiceItem['gst_amount'] = ($invoiceItem['gst_amount'] != null) ? $invoiceItem['gst_amount'] : 0;
            $invoiceItem['gst_percentage'] = ($invoiceItem['gst_percentage'] != null) ? $invoiceItem['gst_percentage'] : 0;

            // check gtin > 14 otherwise prepend 0
            if (strlen($invoiceItem['gtin']) < 14) {
                $gtinPrefix = 0;
                $prefixCnt = 14 - strlen($invoiceItem['gtin']);
                $invoiceItem['gtin'] = $this->addPrefixToString($invoiceItem['gtin'], $gtinPrefix, $prefixCnt);
            }

            $invoiceXml = $invoiceXml . '<invoiceLineItem number="' . $invoiceItem['order_line_number'] . '">
                                            <tradeItemIdentification>
                                                <gtin>' . $invoiceItem['gtin'] . '</gtin>';
//            if ((($offRangeOrderPos !== FALSE) || ($directShipOffRangeOrderPos !== FALSE)) && ($invoiceItem['gtin'] <= 0)) {
            if ($invoiceItem['bunnings_product_id'] != '') {
                $invoiceXml = $invoiceXml . '<additionalTradeItemIdentification>
                                                    <additionalTradeItemIdentificationValue>' . $invoiceItem['bunnings_product_id'] . '</additionalTradeItemIdentificationValue>
                                                    <additionalTradeItemIdentificationType>BUYER_ASSIGNED</additionalTradeItemIdentificationType>
                                                </additionalTradeItemIdentification>';
            }
            if ($invoiceItem['supplier_product_id'] != '') {
                $invoiceXml = $invoiceXml . '<additionalTradeItemIdentification>
                                                    <additionalTradeItemIdentificationValue>' . $invoiceItem['supplier_product_id'] . '</additionalTradeItemIdentificationValue>
                                                    <additionalTradeItemIdentificationType>SUPPLIER_ASSIGNED</additionalTradeItemIdentificationType>
                                                </additionalTradeItemIdentification>';
            }
//            }
            $invoiceXml = $invoiceXml . '</tradeItemIdentification>
                                            <invoicedQuantity>
                                                <value>' . $invoiceItem['requested_quantity'] . '</value>
                                                <unitOfMeasure>
                                                    <measurementUnitCodeValue>' . $invoiceItem['unit_of_measure'] . '</measurementUnitCodeValue>
                                                </unitOfMeasure>
                                            </invoicedQuantity>
                                            <transferOfOwnershipDate>' . date("Y-m-d", strtotime($purchaseOrder['actual_delivery_date'])) . '</transferOfOwnershipDate>
                                            <amountInclusiveAllowancesCharges>' . $invoiceItem['net_amount'] . '</amountInclusiveAllowancesCharges>
                                            <itemDescription>
                                                <language>
                                                    <languageISOCode>' . $purchaseOrder['language_iso_code'] . '</languageISOCode>
                                                </language>
                                                <text>' . htmlspecialchars($invoiceItem['description']) . '</text>
                                            </itemDescription>
                                            <itemPriceBaseQuantity>
                                                <value>' . $invoiceItem['pack_quantity'] . '</value>
                                            </itemPriceBaseQuantity>
                                            <itemPriceInclusiveAllowancesCharges>' . $invoiceItem['net_price'] . '</itemPriceInclusiveAllowancesCharges>
                                            <orderIdentification>
                                                <documentLineReference number="' . $invoiceItem['reference_purchase_order_line_number'] . '"></documentLineReference>
                                            </orderIdentification>
                                            <invoiceLineTaxInformation>
                                                <taxAmount>' . $invoiceItem['gst_amount'] . '</taxAmount>
                                                <taxPercentage>' . $invoiceItem['gst_percentage'] . '</taxPercentage>
                                            </invoiceLineTaxInformation>
                                        </invoiceLineItem>';

            $totalTaxAmount = $totalTaxAmount + $invoiceItem['gst_amount'];
            $totalInvoiceAmount = $totalInvoiceAmount + $invoiceItem['net_amount'];
        }

        $invoiceXml = $invoiceXml . '<invoiceTotals>
                                        <totalInvoiceAmount>' . $totalInvoiceAmount . '</totalInvoiceAmount>
                                        <totalTaxAmount>' . $totalTaxAmount . '</totalTaxAmount>
                                        <totalInvoiceAmountPayable>' . ($totalInvoiceAmount + $totalTaxAmount) . '</totalInvoiceAmountPayable>
                                    </invoiceTotals>
                                    <deliveryNote>
                                        <uniqueCreatorIdentification>' . $purchaseOrder['fulfilment_despatch_advice_identification_number'] . '</uniqueCreatorIdentification>
                                        <contentOwner>
                                            <gln>' . $purchaseOrder['seller_gln'] . '</gln>
                                        </contentOwner>
                                    </deliveryNote>
                                    <orderIdentification>
                                        <uniqueCreatorIdentification>' . $purchaseOrder['purchase_order_number'] . '</uniqueCreatorIdentification>
                                        <contentOwner>
                                            <gln>' . $purchaseOrder['seller_gln'] . '</gln>
                                        </contentOwner>
                                    </orderIdentification>
                                </pay:invoice>
                            </documentCommandOperand>
                        </eanucc:documentCommand>
                    </command>
                </eanucc:transaction>
            </eanucc:message>
        </sh:StandardBusinessDocument>';

        return $invoiceXml;
    }

    /**
     * updateFieldsByField - Method
     * @param type $fieldName
     * @param type $fieldValue
     * @param type $updateFieldsArr
     * @return type
     */
    public function updateFieldsByField($fieldName, $fieldValue, $updateFieldsArr) {
        $query = $this->query();
        $result = $query->update()
                ->set($updateFieldsArr)
                ->where([$fieldName => $fieldValue])
                ->execute();
        return $result;
    }

    /**
     * updateFieldsByField - Method
     * @param type $conditionsArr
     * @param type $updateFieldsArr
     * @return type
     */
    public function updateFieldsByFields($conditionsArr, $updateFieldsArr) {
        $query = $this->query();
        $result = $query->update()
                ->set($updateFieldsArr)
                ->where($conditionsArr)
                ->execute();
        return $result;
    }

    /**
     * Get Availabe Invoices Count
     * @return type
     */
    public function getAvailabelInvoicesCount() {
        return $this->find('all', ['conditions' => ['invoice_generated' => Configure::read('invoice_generated.no')]])->count();
    }

    /**
     * getReceivedInvoicesCount - Method
     * @return type
     */
    public function getReceivedInvoicesCount() {
        return $this->find('all', [
                    'conditions' => [
                        'invoice_generated' => Configure::read('invoice_generated.yes'),
                        'invoice_status' => Configure::read('invoice_ack_invoice_status.RECEIVED')
                    ]
                ])->count();
    }

    public function AddOrUpdateModifiedInvoiceInMyob($invoiceId) {

        $invoiceData = $this->find('all', [
                    'conditions' => array('Invoices.id' => $invoiceId),
                    'contain' => ['InvoiceItems', 'PurchaseOrders']
                ])->toArray();

        // myob connection
        $myobConnection = ConnectionManager::get('myob');

        // check if invoice is already exist
        if (!empty($invoiceData)) {

            // find the invoice id
            $condition = ['invoice_number' => $invoiceData[0]['invoice_number'], 'purchase_order_number' => $invoiceData[0]['purchase_order_number']];
            $invoiceId = $this->findMyobTableFieldValue($myobConnection, 'invoices', 'id', $condition);

            $myobInvoiceData = array();
            $currentDate = date(Configure::read('date-format.Y-m-d-H-i-s'));
            $myobInvoiceData['invoice_number'] = $invoiceData[0]['invoice_number'];
            $myobInvoiceData['invoice_type'] = $invoiceData[0]['invoice_type'];
            $myobInvoiceData['purchase_order_number'] = $invoiceData[0]['purchase_order_number'];
            $myobInvoiceData['sub_total'] = $invoiceData[0]['sub_total'];
            $myobInvoiceData['tax'] = $invoiceData[0]['tax'];
            $myobInvoiceData['sales_person'] = $invoiceData[0]['sales_person'];
            $myobInvoiceData['ship_via'] = $invoiceData[0]['ship_via'];
            $myobInvoiceData['update_for_myob'] = Configure::read('update_for_myob.yes');
            $myobInvoiceData['nursery_gln'] = $invoiceData[0]['purchase_order']['seller_gln'];

            if ($invoiceId != null) {

                // update invoice
                $myobInvoiceData['modified'] = $currentDate;

                $myobConnection->newQuery()
                        ->update('invoices')
                        ->set($myobInvoiceData)
                        ->where(['id' => $invoiceId])
                        ->execute();

                $myobConnection->newQuery()
                        ->delete()
                        ->from('invoice_items')
                        ->where(['invoice_id' => $invoiceId])
                        ->execute();

                foreach ($invoiceData[0]['invoice_items'] as $invoiceItem) {

                    // generate invoice items arr
                    $invoiceItemArr = $this->generateInvoiceItemArr($invoiceId, $invoiceItem);

                    // insert invoice item
                    $invoiceItemArr['created'] = $currentDate;
                    $invoiceItemArr['modified'] = $currentDate;

                    $myobConnection->insert('invoice_items', $invoiceItemArr);
                }
            } else {

                // insert invoice
                $myobInvoiceData['created'] = $currentDate;
                $myobInvoiceData['modified'] = $currentDate;

                $myobConnection->insert('invoices', $myobInvoiceData);

                // find the invoice id
                $condition = ['invoice_number' => $invoiceData[0]['invoice_number'], 'purchase_order_number' => $invoiceData[0]['purchase_order_number']];
                $invoiceId = $this->findMyobTableFieldValue($myobConnection, 'invoices', 'id', $condition);

                foreach ($invoiceData[0]['invoice_items'] as $invoiceItem) {

                    // generate invoice items arr
                    $invoiceItemArr = $this->generateInvoiceItemArr($invoiceId, $invoiceItem);
                    $invoiceItemArr['created'] = $currentDate;
                    $invoiceItemArr['modified'] = $currentDate;

                    // insert invoice item
                    $myobConnection->insert('invoice_items', $invoiceItemArr);
                }
            }

            return 1;
        } else {

            return 0;
        }
    }

    /**
     * findMyobTableFieldValue - Method
     * @param type $myobConnection
     * @param type $table
     * @param type $field
     * @param type $condition
     * @return type
     */
    public function findMyobTableFieldValue($myobConnection, $table, $field, $condition) {

        $data = $myobConnection->newQuery()
                ->select($field)
                ->from($table)
                ->where($condition)
                ->execute()
                ->fetch('assoc');

        return isset($data[$field]) ? $data[$field] : null;
    }

    /**
     * generateInvoiceItemArr - Method
     * @param type $invoiceId
     * @param type $invoiceItem
     * @return type
     */
    public function generateInvoiceItemArr($invoiceId, $invoiceItem) {

        $masterItemsObj = TableRegistry::get('MasterItems');

        $currentDate = date(Configure::read('date-format.Y-m-d-H-i-s'));

        $invoiceItemArr = array();
        $invoiceItemArr['invoice_id'] = $invoiceId;
        $invoiceItemArr['order_line_number'] = $invoiceItem['order_line_number'];
        $invoiceItemArr['gtin'] = $invoiceItem['gtin'];
        $invoiceItemArr['supplier_product_id'] = $invoiceItem['supplier_product_id'];
        $invoiceItemArr['bunnings_product_id'] = $invoiceItem['bunnings_product_id'];
        $invoiceItemArr['requested_quantity'] = $invoiceItem['requested_quantity'];
        $invoiceItemArr['unit_of_measure'] = $invoiceItem['unit_of_measure'];
        $invoiceItemArr['pack_quantity'] = $invoiceItem['pack_quantity'];
        $invoiceItemArr['quotation_number'] = $invoiceItem['quotation_number'];
        $invoiceItemArr['description'] = $invoiceItem['description'];
        $invoiceItemArr['net_price'] = $invoiceItem['net_price'];
        $invoiceItemArr['net_amount'] = $invoiceItem['net_amount'];
        $invoiceItemArr['reference_purchase_order_line_number'] = $invoiceItem['reference_purchase_order_line_number'];
        $invoiceItemArr['gst_amount'] = $invoiceItem['gst_amount'];
        $invoiceItemArr['gst_percentage'] = $invoiceItem['gst_percentage'];
        $invoiceItemArr['comments'] = $invoiceItem['comments'];

        $masterItemsData = $masterItemsObj->find('all', ['fields' => ['name', 'genus', 'cultival', 'species', 'pot_size'], 'conditions' => ['bar_code' => $invoiceItem['gtin']]]);
        /* Set master items data in Purchase Order Item */
        if (iterator_count($masterItemsData) > 0) {
            foreach ($masterItemsData as $masterItem) {
                $invoiceItemArr['name'] = $masterItem->name;
                $invoiceItemArr['genus'] = $masterItem->genus;
                $invoiceItemArr['cultival'] = $masterItem->cultival;
                $invoiceItemArr['species'] = $masterItem->species;
                $invoiceItemArr['pot_size'] = $masterItem->pot_size;
            }
        }

        return $invoiceItemArr;
    }

    /**
     * createQuickBookInvoiceAndUpdateRelatedDataInDB - This method create Invoice which was Received by QuickBook & also update purchase order
     * @param type $invoiceQuickBook
     * @param type $purchaseOrderData
     * @return type
     */
    public function createQuickBookInvoiceAndUpdateRelatedDataInDB($invoiceQuickBook, $purchaseOrderData) {

        $invoiceResult = array();
        $invoiceDataToStoreInDataBase = array();
        $invoiceDataToStoreInDataBase['Purchase_Order'] = $purchaseOrderData;

        // create model object
        $invoiceItemsObj = TableRegistry::get('InvoiceItems');
        $purchaseOrdersObj = TableRegistry::get('PurchaseOrders');
        $masterItemsObj = TableRegistry::get('MasterItems');

        // insert invoice
        $quickBookInvoiceData = array();
        $currentDate = date(Configure::read('date-format.Y-m-d-H-i-s'));

        $tax = isset($invoiceQuickBook->TxnTaxDetail->TaxLine->TaxLineDetail->TaxPercent) ? $invoiceQuickBook->TxnTaxDetail->TaxLine->TaxLineDetail->TaxPercent : 0;
        $lineCnt = count($invoiceQuickBook->Line);

        $quickBookInvoiceData['user_id'] = isset($purchaseOrderData['user_id']) ? $purchaseOrderData['user_id'] : null;
        $quickBookInvoiceData['purchase_order_id'] = isset($purchaseOrderData['id']) ? $purchaseOrderData['id'] : null;
        $quickBookInvoiceData['invoice_number'] = isset($invoiceQuickBook->DocNumber) ? $invoiceQuickBook->DocNumber : null;
        $quickBookInvoiceData['quick_book_invoice_id'] = isset($invoiceQuickBook->Id) ? $invoiceQuickBook->Id : null;
        $quickBookInvoiceData['invoice_type'] = 'TAX_INVOICE';
        $quickBookInvoiceData['purchase_order_number'] = isset($purchaseOrderData['purchase_order_number']) ? $purchaseOrderData['purchase_order_number'] : null;
        $quickBookInvoiceData['reference_delivery_note_number'] = isset($invoiceQuickBook->DocNumber) ? $invoiceQuickBook->DocNumber : null;
        $lastElementKey = $lineCnt - 1;
        $lastSecondElementKey = $lineCnt - 2;
        $discountAmount = 0;
        if (isset($invoiceQuickBook->Line[$lastElementKey]->DetailType) && ($invoiceQuickBook->Line[$lastElementKey]->DetailType == 'DiscountLineDetail')) {

            $quickBookInvoiceData['sub_total'] = isset($invoiceQuickBook->Line[$lastSecondElementKey]->Amount) ? $invoiceQuickBook->Line[$lastSecondElementKey]->Amount : $invoiceQuickBook->TotalAmt;
            $discountAmount = isset($invoiceQuickBook->Line[$lastElementKey]->Amount) ? $invoiceQuickBook->Line[$lastElementKey]->Amount : 0;
        } else {

            $quickBookInvoiceData['sub_total'] = isset($invoiceQuickBook->Line[$lastElementKey]->Amount) ? $invoiceQuickBook->Line[$lastElementKey]->Amount : $invoiceQuickBook->TotalAmt;
        }
        $quickBookInvoiceData['tax'] = $tax;
        $quickBookInvoiceData['discount_percentage'] = $this->session->read('Auth.User.quick_book_rebate');
        $quickBookInvoiceData['discount_amount'] = $discountAmount;
        $invoiceDate = isset($invoiceQuickBook->MetaData->CreateTime) ? date(Configure::read('date-format.Y-m-d-H-i-s'), strtotime($invoiceQuickBook->MetaData->CreateTime)) : $currentDate;
        $quickBookInvoiceData['invoice_date'] = $invoiceDate;

        $invoiceDataToStoreInDataBase['Invoice'] = $quickBookInvoiceData;
        
        $invoiceEntities = $this->newEntity($quickBookInvoiceData);
        if (!$invoiceEntities->errors()) {
            $invoiceSaveResult = $this->save($invoiceEntities);
            if ($invoiceSaveResult) {
                $invoiceResult['success'][] = 'Invoice has been saved successfully in database';
            } else {
                $invoiceResult['error'][] = 'Some error occurred during save invoice, please try again.';
            }
        } else {
            if (!empty($invoiceEntities->errors())) {

                $errorMsgs = $this->getErrorMsgForDisplay($invoiceEntities->errors());
                $invoiceResult['error'][] = $errorMsgs;
            } else {

                $invoiceResult['error'][] = 'Some error occurred during save invoice, please try again.';
            }
        }
        $invoiceId = isset($invoiceSaveResult->id) ? $invoiceSaveResult->id : NULL;

        $i = 1;
        $invoiceItemArr = array();
        if ($invoiceId) {
            foreach ($purchaseOrderData['purchase_order_items'] as $purchaseOrderItems) {

                $invoiceItem = array();
                $invoiceItem['invoice_id'] = $invoiceId;

                // Check Quick-Books Invoice Item Id & Store
                foreach ($invoiceQuickBook->Line as $invoiceItemsData) {

                    $itemRefId = isset($invoiceItemsData->SalesItemLineDetail->ItemRef) ? $invoiceItemsData->SalesItemLineDetail->ItemRef : null;
                    $quickBookItemId = isset($purchaseOrderItems['quick_book_item_id']) ? $purchaseOrderItems['quick_book_item_id'] : null;
                    if ($itemRefId == $quickBookItemId) {

                        $invoiceItem['quick_book_invoice_item_id'] = $invoiceItemsData->Id;
                    }
                }

                // Some times description not working 
                $description = isset($purchaseOrderItems['description']) ? $purchaseOrderItems['description'] : null;
                if ($description == null) {
                    $masterItemData = $masterItemsObj->findDataByGtin($purchaseOrderItems['gtin']);
                    if (!empty($masterItemData)) {
                        $description = (empty($masterItemData->description)) ? $masterItemData->name : $masterItemData->description;
                    }
                }

                $invoiceItem['purchase_order_id'] = isset($purchaseOrderData['id']) ? $purchaseOrderData['id'] : null;
                $invoiceItem['order_line_number'] = isset($purchaseOrderItems['order_line_number']) ? $purchaseOrderItems['order_line_number'] : $i;
                $invoiceItem['gtin'] = isset($purchaseOrderItems['gtin']) ? $purchaseOrderItems['gtin'] : null;
                $invoiceItem['supplier_product_id'] = isset($purchaseOrderItems['supplier_product_id']) ? $purchaseOrderItems['supplier_product_id'] : null;
                $invoiceItem['bunnings_product_id'] = isset($purchaseOrderItems['bunnings_product_id']) ? $purchaseOrderItems['bunnings_product_id'] : null;
                $invoiceItem['requested_quantity'] = isset($purchaseOrderItems['requested_quantity']) ? $purchaseOrderItems['requested_quantity'] : null;
                $invoiceItem['unit_of_measure'] = isset($purchaseOrderItems['unit_of_measure']) ? $purchaseOrderItems['unit_of_measure'] : null;
                $invoiceItem['pack_quantity'] = isset($purchaseOrderItems['pack_quantity']) ? $purchaseOrderItems['pack_quantity'] : null;
                $invoiceItem['quotation_number'] = isset($purchaseOrderItems['quotation_number']) ? $purchaseOrderItems['quotation_number'] : null;
                $invoiceItem['description'] = $description;
                $invoiceItem['net_price'] = isset($purchaseOrderItems['net_price']) ? $purchaseOrderItems['net_price'] : null;
                $invoiceItem['net_amount'] = isset($purchaseOrderItems['net_amount']) ? $purchaseOrderItems['net_amount'] : null;
                $invoiceItem['reference_purchase_order_line_number'] = isset($purchaseOrderItems['reference_purchase_order_line_number']) ? $purchaseOrderItems['reference_purchase_order_line_number'] : $i;
                $invoiceItem['gst_amount'] = ($purchaseOrderItems['net_amount'] * $tax) / 100;
                $invoiceItem['gst_percentage'] = $tax;
                $invoiceItem['comments'] = isset($purchaseOrderItems['comments']) ? $purchaseOrderItems['comments'] : null;

                $invoiceItemArr[] = $invoiceItem;
                $i++;
            }

            $invoiceDataToStoreInDataBase['Invoice_Items'] = $invoiceItemArr;
            $entities = $invoiceItemsObj->newEntities($invoiceItemArr);
            foreach ($entities as $entity) {
                if (!$entity->errors()) {
                    $result = $invoiceItemsObj->save($entity);
                    if ($result) {
                        $invoiceResult['success'][] = $entity->gtin . ' invoice Item has been saved successfully in database';
                    } else {
                        $invoiceResult['error'][] = 'Some error occurred during save ' . $entity->gtin . ' this invoice item, please try again.';
                    }
                } else {
                    if (!empty($entity->errors())) {

                        $errorMsgs = $this->getErrorMsgForDisplay($entity->errors());
                        $invoiceResult['error'][] = 'Below errors occurred during save ' . $entity->gtin . ' this invoice item';
                        $invoiceResult['error'][] = $errorMsgs;
                    } else {

                        $invoiceResult['error'][] = 'Some error occurred during ' . $entity->gtin . ' this invoice item, please try again.';
                    }
                }
            }

            // update Quick Book Item Id in master_items table
            $invoiceNumber = isset($invoiceQuickBook->DocNumber) ? $invoiceQuickBook->DocNumber : null;
            $updateArr = array(
                'sent_to_quick_book' => Configure::read('sent_to_quick_book.yes'),
                'invoice_number' => $invoiceNumber
            );
            $conditionArr = array('id' => $purchaseOrderData['id']);
            $updatePORecord = $purchaseOrdersObj->updateFieldsByFields($conditionArr, $updateArr);
            if ($updatePORecord) {
                $invoiceResult['success'][] = 'Purchase Order sent_to_quick_book status updated successfully by Yes';
            } else {
                $invoiceResult['error'][] = 'Purchase Order sent_to_quick_book status don\'t updated successfully by Yes';
            }
        }

        // capture $invoiceDataToStoreInDataBase in process log to debug
        $invoiceDataToStoreInDataBaseObject = json_encode($invoiceDataToStoreInDataBase);
        $processLogResult = $this->ProcessLogs->generateProcessLogArrAndSave('Data Object of Supplier Portal to Store Invoice Data : ' . $invoiceDataToStoreInDataBaseObject);
        
        // update result
        return $invoiceResult;
    }

    /**
     * updateInvoiceItemsDataModifedByQuickBook - This method update Invoice Items Details which was modifed by Quick-Books
     * @param type $quickBookResponse
     * @param type $invoiceData
     * @return type
     */
    public function updateInvoiceItemsDataModifedByQuickBook($quickBookResponse, $invoiceData) {

        $invoiceItemsObj = TableRegistry::get('InvoiceItems');
        foreach ($invoiceData['invoice_items'] as $invoiceItems) {

            // Check Quick-Books Invoice Item Id & Store
            foreach ($quickBookResponse->Line as $quickBookInvoiceItems) {

                $itemRefId = isset($quickBookInvoiceItems->SalesItemLineDetail->ItemRef) ? $quickBookInvoiceItems->SalesItemLineDetail->ItemRef : null;
                $quickBookItemId = isset($invoiceItems['quick_book_item_id']) ? $invoiceItems['quick_book_item_id'] : null;
                if ($itemRefId == $quickBookItemId) {

                    $updateArr = array(
                        'quick_book_invoice_item_id' => $quickBookInvoiceItems->Id
                    );
                    $conditionArr = array('id' => $invoiceItems['id']);

                    // update Invoice Item$quickBookResponse
                    $result[] = $invoiceItemsObj->updateFieldsByFields($conditionArr, $updateArr);
                }
            }
        }

        $lineCnt = count($quickBookResponse->Line);
        $lastElementKey = $lineCnt - 1;
        $lastSecondElementKey = $lineCnt - 2;
        $discountAmount = 0;
        $quickBookInvoiceData = array();
        if (isset($quickBookResponse->Line[$lastElementKey]->DetailType) && ($quickBookResponse->Line[$lastElementKey]->DetailType == 'DiscountLineDetail')) {

            $quickBookInvoiceData['sub_total'] = isset($quickBookResponse->Line[$lastSecondElementKey]->Amount) ? $quickBookResponse->Line[$lastSecondElementKey]->Amount : $quickBookResponse->TotalAmt;
            $discountAmount = isset($quickBookResponse->Line[$lastElementKey]->Amount) ? $quickBookResponse->Line[$lastElementKey]->Amount : 0;
        } else {

            $quickBookInvoiceData['sub_total'] = isset($quickBookResponse->Line[$lastElementKey]->Amount) ? $quickBookResponse->Line[$lastElementKey]->Amount : $quickBookResponse->TotalAmt;
        }
        $quickBookInvoiceData['discount_amount'] = $discountAmount;
        $updateConditionArr['invoice_number'] = isset($quickBookResponse->DocNumber) ? $quickBookResponse->DocNumber : '';

        // update invoice data
        $this->updateMultipleFields($quickBookInvoiceData, $updateConditionArr);
        return $result;
    }

    /**
     * Update Multiple Fields
     * @param type $updatedArr
     * @param type $condtions
     * @return boolean
     */
    public function updateMultipleFields($updatedArr, $condtions) {
        if (!empty($updatedArr) && !empty($condtions)) {
            try {
                // update invoice record for delivery_docket_generated column
                return $this->updateAll($updatedArr, $condtions);
            } catch (Exception $ex) {
                return FALSE;
            }
        }
        return FALSE;
    }

    /**
     * Send Pending Invoices to Bunnings reminder
     */
    public function sendInvoiceReminder() {

        $processLogArr = array();
        $results = array();
        // generate process log array
        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Send Pending Invoices to Bunnings reminder : Started');

        $invoices = $this->find('all', [
                    'fields' => array('Invoices.id', 'Invoices.invoice_number', 'Invoices.invoice_generated', 'Invoices.delivery_docket_generated', 'Invoices.booking_sheet_generated', 'Invoices.purchase_order_number'),
                    'conditions' => array(
                        'Invoices.invoice_generated' => Configure::read('invoice_generated.no'),
                        'Invoices.delivery_docket_generated' => Configure::read('delivery_docket_generated.yes'),
                        'Invoices.booking_sheet_generated' => Configure::read('booking_sheet_generated.yes'),
                    ),
                    'contain' => [
                        'Users' => ['fields' => ['Users.id', 'Users.seller_gln', 'Users.full_name', 'Users.email', 'Users.reminder_shedule', 'Users.reminder_sent_time']],
                    ],
                ])->applyOptions(['remove_user_condition' => true])->toArray();

        if (isset($invoices) && !empty($invoices)) {

            $newarray = array();
            $finalarray = array();
            foreach ($invoices as $key => $invoice) {

                $newarray['email'] = $invoice->user->email;
                $newarray['user_id'] = $invoice->user->id;
                $newarray['full_name'] = $invoice->user->full_name;
                $newarray['invoice_number'] = $invoice->invoice_number;
                $newarray['purchase_order_number'] = $invoice->purchase_order_number;
                $newarray['purchase_order_number'] = $invoice->purchase_order_number;
                $newarray['nursery_GLN'] = $invoice->user->seller_gln;
                if (empty($invoice->user->reminder_sent_time)) {

                    $finalarray[$invoice->user->email][] = $newarray;
                }
                if (isset($invoice->user->reminder_sent_time) && !empty($invoice->user->reminder_sent_time)) {

                    $lastemailSendDateTime = $this->getFormatedDateFromObject($invoice->user->reminder_sent_time);
                    $reminder_shedule = date("Y-m-d H:i:s", strtotime('-' . $invoice->user->reminder_shedule . ' hour'));
                    if (strtotime($lastemailSendDateTime) <= strtotime($reminder_shedule)) {

                        $finalarray[$invoice->user->email][] = $newarray;
                    }
                }
            }

            if (isset($finalarray) && !empty($finalarray)) {

                $users = TableRegistry::get('Users');
                foreach ($finalarray as $email => $invoiceArr) {

                    if (isset($invoiceArr) && !empty($invoiceArr)) {

                        $invoiceList = '';
                        $clientName = $invoiceArr[0]['full_name'];
                        $userId = $invoiceArr[0]['user_id'];
                        $to = [$email => $clientName];
                        $invoiceText = (count($invoiceArr) > 1) ? 'Invoices' : 'Invoice';
                        foreach ($invoiceArr as $invoice => $invoicedata) {
                            $invoiceList.="Invoice Number - " . $invoicedata['invoice_number'] . "<br />";
                        }

                        // send mail to user related pending invoices for bunnings
                        $testemail = $this->sendInvoiceToBunningReminder($to, $clientName, $invoiceText, $invoiceList);
                        if ($testemail > 0) {

                            $query = $users->updateFieldsByCondition(['reminder_sent_time' => date('Y-m-d H:i:s')], ['id' => $userId]);

                            // generate process log array
                            foreach ($invoiceArr as $invoicelogs => $invoicedatalog) {
                                $results[] = 'Send Pending Invoices to Bunnings reminder - Email Send successfully for (' . $invoicedatalog['invoice_number'] . ') invoice of ' . $clientName . " User";
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Send Pending Invoices to Bunnings reminder : Email Send successfully for (' . $invoicedatalog['invoice_number'] . ') invoice', $invoicedatalog['user_id'], $invoicedatalog['nursery_GLN'], $invoicedatalog['purchase_order_number']);
                            }
                        } else {

                            // generate process log array
                            foreach ($invoiceArr as $invoice => $invoicedata) {
                                $results[] = 'Send Pending Invoices to Bunnings reminder - Email Sending failed for (' . $invoicedatalog['invoice_number'] . ') invoice of ' . $clientName . " User";
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Send Pending Invoices to Bunnings reminder : Email Sending failed for (' . $invoicedatalog['invoice_number'] . ') invoice', $invoicedatalog['user_id'], $invoicedatalog['nursery_GLN'], $invoicedatalog['purchase_order_number']);
                            }
                        }
                    } else {

                        // find username from email
                        $user = $users->findByEmail($email)->applyOptions(['remove_user_condition' => true])->toArray();

                        // generate process log array
                        $results[] = 'Send Pending Invoices to Bunnings reminder - No invoices are pending for bunnings to ' . $clientName . " this User & Nursery GLN is " . $user[0]['seller_gln'];
                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Send Pending Invoices to Bunnings reminder : No invoices are pending for bunnings', $user[0]['id'], $user[0]['seller_gln']);
                    }
                }
            } else {

                // generate process log array
                $results[] = 'Send Pending Invoices to Bunnings reminder - No invoices are pending for bunnings to all users';
                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Send Pending Invoices to Bunnings reminder : No invoices are pending for bunnings to all users');
            }
        } else {
            // generate process log array
            $results[] = 'Send Pending Invoices to Bunnings reminder - No invoices are pending for bunnings to all users';
            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Send Pending Invoices to Bunnings reminder : No invoices are pending for bunnings to all users');
        }

        if (!empty($processLogArr)) {
            // save process logs
            $processLogResult = $this->ProcessLogs->saveProcessLogs($processLogArr);
        }
        return $results;
    }

        /**
     * 
     * @param type $invoiceIds
     * @param type $invoiceGenerated
     * @return string
     */
    public function resendGenerateInvoiceXmlFileAndSendToSftp($invoiceIds, $invoiceGenerated = null) {

        /*
         * check dir is exist
         * if not then create
         */
        $dirResult = $this->makeDirectoryIfNotExist(Configure::read('invoice.localPath'));

        $results = array();
        $processLogArr = array();

        // create object
        $bunningStoresObj = TableRegistry::get('BunningStores');

        // Below code for regenerated invoice
        $updateResendInvoice = Configure::read('resend_invoice.yes');
        if ($invoiceGenerated == null) {
            $invoiceGenerated = Configure::read('invoice_generated.no');
            $updateResendInvoice = Configure::read('resend_invoice.no');
        }

        if (!empty($invoiceIds)) {
            $invoiceArr = $this->find('all', [
                        'conditions' => array(
                            'Invoices.invoice_generated' => $invoiceGenerated,
                            'Invoices.resend_invoice' => Configure::read('resend_invoice.no'),
                            'Invoices.id IN' => $invoiceIds
                        ),
                        'contain' => ['InvoiceItems', 'PurchaseOrders', 'PurchaseOrders.Currencies', 'PurchaseOrders.PurchaseOrderStatuses']
                    ])->toArray();
        } else {
            $invoiceArr = $this->find('all', [
                        'conditions' => array(
                            'Invoices.invoice_generated' => $invoiceGenerated,
                            'Invoices.resend_invoice' => Configure::read('resend_invoice.no')
                        ),
                        'contain' => ['InvoiceItems', 'PurchaseOrders', 'PurchaseOrders.Currencies', 'PurchaseOrders.PurchaseOrderStatuses']]
                    )->applyOptions(['remove_user_condition' => true])->toArray();
        }
    
        if (!empty($invoiceArr)) {
            foreach ($invoiceArr as $invoice) {

                $purchaseOrder = isset($invoice['purchase_order']) ? $invoice['purchase_order'] : array();
                $invoiceItems = isset($invoice['invoice_items']) ? $invoice['invoice_items'] : array();
//echo '$purchaseOrder==><pre>';
//        print_r($purchaseOrder);
//echo '$invoiceItems==><pre>';
//        print_r($invoiceItems);
//echo '$invoice==><pre>';
//        print_r($invoice);
//        exit;
           
                if (!empty($invoice) && !empty($purchaseOrder) && !empty($invoiceItems)) {

                    // generate invoice xml file
                    $xmlFile = Configure::read('invoice.file-prefix') . strtotime('now') . '_' . $this->generateRandomCode(5);
//echo "xmlFile_name==>".$xmlFile."<br>";
                    // Before generate invoice check buyer and payer is available in list
                    if (!empty($invoiceIds)) {
                        $buyerId = $this->checkIfFieldIsExist($bunningStoresObj->findByGlnAndType($purchaseOrder['buyer_gln'], Configure::read('bunning_user.Buyer'))->first(), 'id');
                        $payerId = $this->checkIfFieldIsExist($bunningStoresObj->findByGlnAndType($purchaseOrder['bill_to_gln'], Configure::read('bunning_user.Payer'))->first(), 'id');
                    } else {
                        $buyerId = $this->checkIfFieldIsExist($bunningStoresObj->findByGlnAndTypeAndUserId($purchaseOrder['buyer_gln'], Configure::read('bunning_user.Buyer'), $invoice['user_id'])->applyOptions(['remove_user_condition' => true])->first(), 'id');
                        $payerId = $this->checkIfFieldIsExist($bunningStoresObj->findByGlnAndTypeAndUserId($purchaseOrder['bill_to_gln'], Configure::read('bunning_user.Payer'), $invoice['user_id'])->applyOptions(['remove_user_condition' => true])->first(), 'id');
                    }
//echo "buyerId==>".$buyerId."<br>";
//echo "payerId==>".$payerId."<br>";
                    if ($buyerId && $payerId) {

                        $invoiceGenerateResult = $this->generateInvoiceXmlFile($purchaseOrder, $invoiceItems, $invoice, $xmlFile);
                        $results[] = $invoiceGenerateResult;
//    echo '$invoiceGenerateResult==><pre>';
//        print_r($invoiceGenerateResult);
//        exit;
                        // store process log details
                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($invoiceGenerateResult, $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);

                        /*
                         * @connect sftp using sftpConnection() method
                         */
                        $resSFTP = $this->sftpConnection();
                        if ($resSFTP) {

                            $uploadFileResult = $this->upload(Configure::read('invoice.localPath') . DS . $xmlFile . '.xml', Configure::read('invoice.remotePath') . DS . $xmlFile . '.xml');
                            if ($uploadFileResult) {

                                $results[] = $xmlFile . ' this generated invoice file uploaded successfully to sftp';

                                // store process log details
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . ' this generated invoice file uploaded successfully to sftp', $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);

                                // move uploaded file in out folder
                                if ($this->moveFileInAnotherFolder($xmlFile . '.xml', Configure::read('invoice.localPath'), Configure::read('invoice.localPath-out'))) {

                                    $results[] = $xmlFile . " file moved successfully in Out folder.";

                                    // store process log details
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . " file moved successfully in Out folder", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);

                                    // update invoice generated status
                                    $updateArr = array(
                                        'invoice_generated' => Configure::read('invoice_generated.yes'),
                                        'time_of_invoice_sent_to_bunnings' => date('Y-m-d H:i:s'),
                                        'resend_invoice' => $updateResendInvoice
                                    );
                                    $updateResult = $this->updateFieldsByField('id', $invoice['id'], $updateArr);
                                    if ($updateResult) {

                                        $results[] = $invoice['invoice_number'] . " invoice_generated field is updated successfully by Yes";

                                        // store process log details
                                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($invoice['invoice_number'] . " invoice_generated field is updated successfully by Yes", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                                    } else {

                                        $results[] = $invoice['invoice_number'] . " invoice_generated field is not updated successfully by Yes";

                                        // store process log details
                                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($invoice['invoice_number'] . " invoice_generated field is not updated successfully by Yes", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                                    }
                                } else {

                                    $results[] = $xmlFile . " file not moved successfully in Out folder";

                                    // store process log details
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . " file not moved successfully in Out folder", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                                }
                            } else {

                                $results[] = $xmlFile . ' this generated invoice file not uploaded successfully to sftp';

                                // store process log details
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . ' this generated invoice file not uploaded successfully to sftp', $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                            }
                        } else {

                            $results[] = "Some error is occurred in sftp connection";

                            // store process log details
                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr("Some error is occurred in sftp connection", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                        }
                    } else {

                        if (!$buyerId) {

                            $results[] = $purchaseOrder['buyer_gln'] . " this buyer is not present in Bunning Stores to generate invoice of " . $purchaseOrder['purchase_order_number'] . " Purchase Order";

                            // store process log details
                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($purchaseOrder['buyer_gln'] . " this buyer is not present in Bunning Stores to generate invoice of " . $purchaseOrder['purchase_order_number'] . " Purchase Order", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                        }
                        if (!$payerId) {

                            $results[] = $purchaseOrder['bill_to_gln'] . " this payer is not present in Bunning Stores to generate invoice of " . $purchaseOrder['purchase_order_number'] . " Purchase Order";

                            // store process log details
                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($purchaseOrder['bill_to_gln'] . " this payer is not present in Bunning Stores to generate invoice of " . $purchaseOrder['purchase_order_number'] . " Purchase Order", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                        }
                    }
                } else {

                    $results[] = "Getting empty data of purchase_orders or invoices or invoice_items";

                    // store process log details
                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr("Getting empty data of purchase_orders or invoices or invoice_items");
                }
            }
        } else {

            $results[] = "No invoice available for generate.";

            // store process log details
            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr("No invoice available for generate.");
        }

        if (!empty($processLogArr)) {

            // save process logs
            $processLogResult = $this->ProcessLogs->saveProcessLogs($processLogArr);
        }

        return $results;
    }
}
