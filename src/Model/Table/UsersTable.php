<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Users Model
 */
class UsersTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Common');
        $this->belongsTo('UserRoles', [
            'foreignKey' => 'user_role_id'
        ]);
        $this->hasMany('BunningStores', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Invoices', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('ProcessLogs', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('PurchaseOrders', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('ScanBooks', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('MasterItems', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('DeliveryDockets', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('BookingSheets', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Statistics', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Payments', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Couriers', [
            'foreignKey' => 'user_id'
        ]);

        $this->ProcessLogs = TableRegistry::get('ProcessLogs');
        $this->Payments = TableRegistry::get('Payments');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
                ->allowEmpty('id', 'create')
                ->requirePresence('full_name', 'create')
                ->notEmpty('full_name')
                ->add('email', 'valid', ['rule' => 'email', 'message' => 'This value should be in email format'])
                ->requirePresence('email', 'create')
                ->notEmpty('email')
                ->requirePresence('user_role_id', 'create')
                ->notEmpty('user_role_id')
                ->requirePresence('phone_number', 'create')
                ->notEmpty('phone_number')
                ->requirePresence('address', 'create')
                ->notEmpty('address')
                ->requirePresence('city', 'create')
                ->notEmpty('city')
                ->requirePresence('country', 'create')
                ->notEmpty('country')
                ->requirePresence('password', 'create')
                ->notEmpty('password')
                ->add('password', 'valid', ['rule' => ['minLength', 8]])
                ->add('seller_gln', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
                ->notEmpty('seller_gln')
                ->notEmpty('seller_business_name')
                ->notEmpty('seller_business_address')
                ->notEmpty('country_iso_code')
                ->notEmpty('language_iso_code')
                ->notEmpty('postal_code')
                ->notEmpty('state')
                ->notEmpty('company_registration_number')
                ->add('company_registration_number', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
                ->notEmpty('trial_end_date')
                ->notEmpty('invoice_generate_by');
        return $validator;
    }

    /**
     * compareOldPasswordWithDB
     * @param type $password
     * @param type $id
     * @return type
     */
    public function compareOldPasswordWithDB($password, $id) {
        $query = $this->find()->where(['id' => $id])->first();
        $data = $query->toArray();
        return (new DefaultPasswordHasher)->check($password, $data['password']);
    }

    /**
     * inactiveExpiredNursery - Method
     * @return string
     */
    public function inactiveExpiredNursery() {

        $resultArr = array();
        $processLogArr = array();

        $expiredNurseryArrOne = $this->find('all', [
                    'fields' => ['id', 'full_name'],
                    'conditions' => [
                        'Users.trial_end_date <' => date('Y-m-d'),
                        'Users.access_status' => Configure::read('access_status.main.active'),
                        'Users.payment_status' => Configure::read('payment_status.main.trial'),
                        'UserRoles.name' => UserRolesTable::User,
                    ],
                    'contain' => ['UserRoles']
                ])->toArray();

        $expiredNurseryArrTwo = $this->find('all', [
                    'fields' => ['id', 'full_name'],
                    'conditions' => [
                        'Users.current_subscription_end_date <' => date('Y-m-d'),
                        'Users.payment_status' => Configure::read('payment_status.main.cancel'),
                        'UserRoles.name' => UserRolesTable::User
                    ],
                    'contain' => ['UserRoles']
                ])->toArray();

        $expiredNurseryArr = array();
        foreach ($expiredNurseryArrOne as $expiredNurseryOne) {
            $expiredNurseryArr[$expiredNurseryOne['id']] = $expiredNurseryOne['full_name'];
        }
        foreach ($expiredNurseryArrTwo as $expiredNurseryOne) {
            $expiredNurseryArr[$expiredNurseryOne['id']] = $expiredNurseryOne['full_name'];
        }

        foreach ($expiredNurseryArr as $expiredNurseryId => $expiredNurseryName) {

            $updateArr = array(
                'access_status' => Configure::read('access_status.main.inactive')
            );
            $conditionArr = array(
                'id' => $expiredNurseryId
            );
            $updateResult = $this->updateFieldsByCondition($updateArr, $conditionArr);
            if ($updateResult) {

                $resultArr[] = $expiredNurseryName . ' this Expired Nursery updated successfully by inactive';

                // store process log details
                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($expiredNurseryName . ' this Expired Nursery updated successfully by inactive');
            } else {

                $resultArr[] = $expiredNurseryName . ' this Expired Nursery is not updated successfully by inactive';

                // store process log details
                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($expiredNurseryName . ' this Expired Nursery is not updated successfully by inactive');
            }
        }

        if (empty($expiredNurseryArr)) {

            $resultArr[] = 'Nurseries are not Expired to updated inactive';

            // store process log details
            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr('Nurseries are not Expired to updated inactive');
        }

        if (!empty($processLogArr)) {

            // save process logs
            $processLogResult = $this->ProcessLogs->saveProcessLogs($processLogArr);
        }

        return $resultArr;
    }

    /**
     * updateFieldsByCondition - Method
     * @param type $updateFieldsArr
     * @param type $conditionArr
     * @return type
     */
    public function updateFieldsByCondition($updateFieldsArr, $conditionArr) {
        $query = $this->query();
        $result = $query->update()
                ->set($updateFieldsArr)
                ->where($conditionArr)
                ->execute();
        return $result;
    }

    /**
     * checkAllowtoAccessPackageUrl - Method
     * @param type $userId
     * @return type
     */
    public function checkAllowtoAccessPackageUrl($userId) {

        $userCnt = $this->find()->where(['id' => $userId, 'access_status' => Configure::read('access_status.main.inactive')])->count();
        return $userCnt;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['email']));
        return $rules;
    }

    /**
     * Update Users Access Status
     * @param type $userInfo
     */
    public function updateUsersAccessStatus($userInfo) {
        /* Updated array */
        $updateArr['access_status'] = Configure::read('access_status.main.inactive');

        /* Condition array */
        $conditionArr['id'] = isset($userInfo->user->id) ? $userInfo->user->id : 0;

        /* Update users table */
        return $this->updateFieldsByCondition($updateArr, $conditionArr);
    }

    /**
     * Update Users Info
     * @param type $userInfo
     */
    public function updateUsersInfo($userInfo) {
        /* Updated array */
        $updateArr['payment_status'] = Configure::read('payment_status.main.cancel');
        $updateArr['subscription_cancelled'] = date('Y-m-d');

        /* Condition array */
        $conditionArr['id'] = isset($userInfo->user->id) ? $userInfo->user->id : 0;

        /* Update users table */
        return $this->updateFieldsByCondition($updateArr, $conditionArr);
    }

    /**
     * Find Record By User Id - Method
     * @param type $customerId
     * @return type
     */
    public function findRecordByUserId($userId) {
        try {
            return $this->get($userId, [
                        'contain' => ['Payments', 'Payments.PaymentHistories']
                    ])->toArray();
        } catch (Exception $exc) {
            return '';
        }
    }

    /**
     * Get Users Information
     * @return string
     */
    public function getUsersAndPaymentInfo() {
        try {
            return [
                'conditions' => [
                    'UserRoles.name' => UserRolesTable::User,
                ],
                'contain' => [
                    'Payments',
                    'Payments.PaymentHistories',
                    'UserRoles'
                ]
            ];
        } catch (Exception $exc) {
            return '';
        }
    }
    
    /**
     * Find Record By Seller Gln
     * @param type $processLogNurseryGLN
     * @return int
     */
    public function findRecordBySellerGln($processLogNurseryGLN){
        try {
            return $this->findBySellerGln($processLogNurseryGLN)->first();
        } catch (Exception $ex) {
            return 0;
        }
    }
}
