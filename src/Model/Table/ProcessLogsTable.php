<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Network\Session;
use Cake\Datasource\ConnectionManager;
use ArrayObject;
use Cake\Core\Configure;

/**
 * ProcessLogs Model
 */
class ProcessLogsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->table('process_logs');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);

        $this->addBehavior('Searchable');
        
        $this->session = new Session();
    }

    // filters
    public $filterArgs = array(
        'action_description' => array(
            'type' => 'like'
        )
    );
    
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric'])
                ->allowEmpty('id', 'create')
                ->allowEmpty('nursery_GLN')
                ->allowEmpty('purchase_order_number')
                ->requirePresence('action_description', 'create')
                ->notEmpty('action_description');

        return $validator;
    }

    /**
     * generateProcessLogArr - method
     * @param type $processLogText
     * @param type $userId
     * @param type $nurseryGLN
     * @param type $purchaseOrderNumber
     * @return type
     */
    public function generateProcessLogArr($processLogText, $userId = null, $nurseryGLN = null, $purchaseOrderNumber = null) {

        $storeProcessLogArr = array();
        $storeProcessLogArr['nursery_GLN'] = $nurseryGLN;
        $storeProcessLogArr['user_id'] = $userId;
        $storeProcessLogArr['purchase_order_number'] = $purchaseOrderNumber;
        $storeProcessLogArr['action_description'] = $processLogText;

        return $storeProcessLogArr;
    }

    /**
     * generateProcessLogArrAndSave - method
     * @param type $processLogArr
     * @param type $userId
     * @param type $nurseryGLN
     * @param type $purchaseOrderNumber
     * @return boolean
     */
    public function generateProcessLogArrAndSave($processLogArr, $userId = null, $nurseryGLN = null, $purchaseOrderNumber = null) {

        $storeProcessLogArr = array();
        if (is_array($processLogArr)) {

            foreach ($processLogArr as $processLog) {

                $storeArr = array();
                $storeArr['nursery_GLN'] = $nurseryGLN;
                $storeArr['user_id'] = $userId;
                $storeArr['purchase_order_number'] = $purchaseOrderNumber;
                $storeArr['action_description'] = $processLog;
                $storeProcessLogArr[] = $storeArr;
            }
        } else {

            $storeArr = array();
            $storeArr['nursery_GLN'] = $nurseryGLN;
            $storeArr['user_id'] = $userId;
            $storeArr['purchase_order_number'] = $purchaseOrderNumber;
            $storeArr['action_description'] = $processLogArr;
            $storeProcessLogArr[] = $storeArr;
        }

        $storeProcessLogArr = array_filter($storeProcessLogArr);
        if (!empty($storeProcessLogArr)) {

            $this->saveProcessLogs($storeProcessLogArr);
        }

        return TRUE;
    }

    /**
     * saveProcessLogs - method
     * @param type $storeProcessLogArr
     * @return type
     */
    public function saveProcessLogs($storeProcessLogArr) {

        $result = '';
        $processLogArticles = $this->newEntities($storeProcessLogArr);
        foreach ($processLogArticles as $processLogArticle) {
            if (!$processLogArticle->errors()) {
                $result = $this->save($processLogArticle);
            }
        }
        return $result;
    }

    /**
     * readProcessLogFromMyob - Method
     * This method read process log from Myob db & inster it into Supplier Portal db if not empty
     * Once inserted in Supplier Portal db then delete from Myob db
     * @return string
     */
    public function readProcessLogFromMyob() {

        $result = array();

        // get myob db connection
        $myobConnection = ConnectionManager::get('myob');

        // get all process logs for insert in supplier portal db from myob db.
        $processLogArr = $myobConnection
                ->newQuery()
                ->select('id,nursery_GLN, purchase_order_number, action_description, created')
                ->from('process_logs')
                ->execute()
                ->fetchAll('assoc');

        if (!empty($processLogArr)) {

            foreach ($processLogArr as $processLog) {

                $processLogId = $processLog['id'];
                unset($processLog['id']);
                $processLogData = array();
                
                /* When is_myob = 1 then record is comming from myob was confirmed */
                $processLog['is_myob'] = Configure::read('process_logs.is_myob_active');
                
                /* Get user_id from GLN */
                if(isset($processLog['nursery_GLN']) && $processLog['nursery_GLN'] != ''){
                    $usersRecords = $this->Users->findRecordBySellerGln($processLog['nursery_GLN']);
                    $processLog['user_id'] = isset($usersRecords->id) ? $usersRecords->id :'';
                }
                $processLogData[] = $processLog;
                
                // save process log in supplier portal db
                $processLogResult = $this->saveProcessLogs($processLogData);
               
                if ($processLogResult) {
                    $result[] = 'Process inserted successfully in Supplier Portal DB';
                     
                    // Delete inserted Process Log records in Myob db
                    $deleteProcessLog = $myobConnection->newQuery()
                            ->delete()
                            ->from('process_logs')
                            ->where(['id' => $processLogId])
                            ->execute();
                    if ($deleteProcessLog) {

                        $result[] = 'Process Log deleted successfully from Myob DB';
                    } else {

                        $result[] = 'Process Log not deleted successfully from Myob DB';
                    }
                } else {
                    $result[] = 'Process Log not inserted successfully in Supplier Portal DB';
                }
            }
        } else {

            $result[] = 'Process Log is empty';
        }

        return $result;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
    
    /**
     * Return query for auth user
     * @param \Cake\Event\Event $event
     * @param \Cake\ORM\Query $query
     * @param ArrayObject $options
     * @return boolean|\Cake\ORM\Query
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options) {
        
        /**
         * This condition is checked when we don't need to check current user 
         */
        if (isset($options['remove_user_condition']) && $options['remove_user_condition'] > 0) {
            return TRUE;
        }

        /**
         *  This condition is checked because when save data in table at that time 
         *  we need not required before find method
         */
        if ($this->session->read('Auth.User.id') != "" && $this->session->read('Auth.User.role_name') != UserRolesTable::Admin) {
            $query->where([$this->alias() . '.user_id' => $this->session->read('Auth.User.id')]);
            return $query;
        }
    }

}
