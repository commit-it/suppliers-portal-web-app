<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PurchaseOrderStatuses Model
 */
class PurchaseOrderStatusesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('purchase_order_statuses');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->hasMany('PurchaseOrders', [
            'foreignKey' => 'purchase_order_status_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }
}
