<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FileTypes Model
 */
class FileTypesTable extends Table {
    
    const Purchase_Order = 'Purchase Order';
    const Invoice = 'Invoice';
    const Ack = 'Application Receipt Acknowledgement';

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->table('file_types');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->hasMany('FileOverviewes', [
            'foreignKey' => 'file_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric'])
                ->allowEmpty('id', 'create')
                ->requirePresence('type', 'create')
                ->notEmpty('type');

        return $validator;
    }

}
