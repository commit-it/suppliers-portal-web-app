<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Network\Session;
use ArrayObject;

/**
 * Couriers Model
 */
class CouriersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('couriers');
        $this->displayField('name');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('BookingSheets', [
            'foreignKey' => 'courier_id'
        ]);
        
        $this->session = new Session();
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->requirePresence('logo', 'create')
            ->allowEmpty('logo')
            ->requirePresence('address', 'create')
            ->notEmpty('address')
            ->requirePresence('telephone', 'create')
            ->notEmpty('telephone')
            ->add('email', 'valid', ['rule' => 'email'])
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        return $validator;
    }

    /**
     * Return query for auth user
     * @param \Cake\Event\Event $event
     * @param \Cake\ORM\Query $query
     * @param ArrayObject $options
     * @return boolean|\Cake\ORM\Query
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options) {
        /**
         * This condition is checked when we don't need to check current user 
         */
        if (isset($options['remove_user_condition']) && $options['remove_user_condition'] > 0) {
            return TRUE;
        } else {
            $query->where([$this->alias() . '.user_id' => $this->session->read('Auth.User.id')]);
            return $query;
        }
    }
    
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
}
