<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * InvoiceItems Model
 */
class InvoiceItemsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->table('invoice_items');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Invoices', [
            'foreignKey' => 'invoice_id'
        ]);
        $this->belongsTo('PurchaseOrders', [
            'foreignKey' => 'purchase_order_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric'])
                ->allowEmpty('id', 'create')
                ->requirePresence('order_line_number', 'create')
                ->notEmpty('order_line_number')
                ->requirePresence('gtin', 'create')
                ->notEmpty('gtin')
                ->add('requested_quantity', 'valid', ['rule' => 'decimal'])
                ->requirePresence('requested_quantity', 'create')
                ->notEmpty('requested_quantity')
                ->requirePresence('unit_of_measure', 'create')
                ->notEmpty('unit_of_measure')
                ->requirePresence('pack_quantity', 'create')
                ->notEmpty('pack_quantity')
                ->allowEmpty('quotation_number')
                ->requirePresence('description', 'create')
                ->notEmpty('description')
                ->add('net_price', 'valid', ['rule' => 'decimal'])
                ->requirePresence('net_price', 'create')
                ->notEmpty('net_price')
                ->add('net_amount', 'valid', ['rule' => 'decimal'])
                ->requirePresence('net_amount', 'create')
                ->notEmpty('net_amount')
                ->allowEmpty('reference_purchase_order_line_number')
                ->add('gst_amount', 'valid', ['rule' => 'decimal'])
                ->allowEmpty('gst_amount')
                ->add('gst_percentage', 'valid', ['rule' => 'decimal'])
                ->allowEmpty('gst_percentage')
                ->allowEmpty('comments');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['invoice_id'], 'Invoices'));
        $rules->add($rules->existsIn(['purchase_order_id'], 'PurchaseOrders'));
        return $rules;
    }
   
    /**
     *  Add Invoice Items
     *  @param type $invoiceItemsArr
     *  @return Boolean 
     */
    public function addInvoiceItems($invoiceItemsArr) {        
        /* Set variables */
        $result = 0;
        $i = 0;
        $increment = 1;

        /* Get count of invoice items array */
        $invoiceItemsCount = count($invoiceItemsArr);


        if ($invoiceItemsCount > 0) {
            
            /* Check invoice item is already exist for this invoice or not */
            $invoiceItemsArr = $this->removeDuplicateInvoiceItems($invoiceItemsArr);
            
            /* Get max order_line_number */
            $maxOrderLineNumber = $this->getMaxValueInColumn('order_line_number');

            /* Get max reference_purchase_order_line_number */
            $maxReferencePurchaseOrderLineNumber = $this->getMaxValueInColumn('reference_purchase_order_line_number');

            /* Set entities for invoice items */
            $invoiceItemsEntities = $this->newEntities($invoiceItemsArr);

            /* Add invoice items */
            foreach ($invoiceItemsEntities as $invoiceItemsEntity) {
                $invoiceItemsEntity->order_line_number = $maxOrderLineNumber + $increment;
                $invoiceItemsEntity->reference_purchase_order_line_number = $maxReferencePurchaseOrderLineNumber + $increment;
                try {
                    $invoiceResult = $this->save($invoiceItemsEntity);
                    $i++;
                } catch (Exception $ex) {
                    
                }
                $increment++;
            }

            /* If count of result is equal to  updated count then return true otherwise return false */
            $result = $invoiceItemsCount == $i ? 1 : 0;
        }
        return $result;
    }

    /**
     * Get Formated Values For Invoice Items
     * 
     * Check if order_line_number and reference_purchase_order_line_number of invoice_items 
     * then assign max order_line_number value to order_line_number and 
     * assign max reference_purchase_order_line_number value to reference_purchase_order_line_number
     * 
     * @param type $invoiceItems
     * @return $invoiceItems
     */
    public function getFormatedValuesForInvoiceItems($invoiceItems, $purchaseOrderId) {
        
        /* Define variables */
        $incrementOrderLineNumber = 1;
        $incrementReferencePurchaseOrderLineNumber = 1;

        /* Get max order_line_number */
        $maxOrderLineNumber = $this->getMaxValueInColumn('order_line_number', $purchaseOrderId);

        /* Get max reference_purchase_order_line_number */
        $maxReferencePurchaseOrderLineNumber = $this->getMaxValueInColumn('reference_purchase_order_line_number', $purchaseOrderId);

        /* Set order_line_number and reference_purchase_order_line_number values */
        foreach ($invoiceItems as $key => $invoiceItem) {
            /* If order_line_number is blank then assign $maxOrderLineNumber+$increment to this */
            if (isset($invoiceItem['order_line_number']) && $invoiceItem['order_line_number'] < 1) {
                $invoiceItems[$key]['order_line_number'] = $maxOrderLineNumber + $incrementOrderLineNumber;
                $incrementOrderLineNumber++;
            }

            /* If reference_purchase_order_line_number is blank then assign $maxReferencePurchaseOrderLineNumber+$increment to this */
            if (isset($invoiceItem['reference_purchase_order_line_number']) && $invoiceItem['reference_purchase_order_line_number'] < 1) {
                $invoiceItems[$key]['reference_purchase_order_line_number'] = $maxReferencePurchaseOrderLineNumber + $incrementReferencePurchaseOrderLineNumber;
                $incrementReferencePurchaseOrderLineNumber++;
            }
        }
        return $invoiceItems;
    }

    /**
     *  deleteInvoiceItems method
     *  @param type $exploadedDeletedInvoiceItemsIds
     *  @return Boolean true/false 
     */
    public function deleteInvoiceItems($exploadedDeletedInvoiceItemsIds) {
        
        try {
            
            return $this->deleteAll(['id IN' => $exploadedDeletedInvoiceItemsIds]);
        } catch (Exception $ex) {
            
            return FALSE;
        }
    }

    /**
     * getMaxValueInColumn - Method
     * @param type $column
     * @param type $purchaseOrderId
     * @return type
     */
    public function getMaxValueInColumn($column = NULL, $purchaseOrderId = NULL) {
        
        /* Set variables */
        $maxColumn = 'max_' . $column;

        /* Fetch max id from database */
        $query = $this->find('all');
        $query->select([$maxColumn => $query->func()->max($column)]);
        $query->where([$this->alias() . '.purchase_order_id' => $purchaseOrderId]);
        $result = $query->toArray();

        /* Return result */
        return isset($result[0][$maxColumn]) && !empty($result[0][$maxColumn]) ? $result[0][$maxColumn] : 0;
    }
    
    /**
     * updateFieldsByField - Method
     * @param type $conditionArr
     * @param type $updateFieldsArr
     * @return type
     */
    public function updateFieldsByFields($conditionArr, $updateFieldsArr) {
        $query = $this->query();

        $result = $query->update()
                ->set($updateFieldsArr)
                ->where($conditionArr)
                ->execute();

        return $result;
    }

    /**
     * Remove Duplicate Invoice Items
     * @param type $invoiceItemsArr
     */
     public function removeDuplicateInvoiceItems($invoiceItemsArr) {
        if (isset($invoiceItemsArr) && !empty($invoiceItemsArr)) {
            foreach ($invoiceItemsArr as $key => $invoiceItem) {
                $resultCnt = $this->find('all', ['conditions' => ['invoice_id' => @$invoiceItem['invoice_id'], 'gtin' => @$invoiceItem['gtin']]])->count();
                if ($resultCnt > 0) {
                    unset($invoiceItemsArr[$key]);
                }
            }
        }
        return $invoiceItemsArr;
    }

}
