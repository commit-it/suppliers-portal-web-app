<?php
namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PaymentHistories Model
 */
class PaymentHistoriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('payment_histories');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Payments', [
            'foreignKey' => 'payment_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->add('email', 'valid', ['rule' => 'email'])
            ->requirePresence('email', 'create')
            ->notEmpty('email')
            ->requirePresence('payment_interval', 'create')
            ->notEmpty('payment_interval')
            ->add('interval_start_date', 'valid', ['rule' => 'date'])
            ->requirePresence('interval_start_date', 'create')
            ->notEmpty('interval_start_date')
            ->add('interval_end_date', 'valid', ['rule' => 'date'])
            ->requirePresence('interval_end_date', 'create')
            ->notEmpty('interval_end_date')
            ->add('amount', 'valid', ['rule' => 'decimal'])
            ->requirePresence('amount', 'create')
            ->notEmpty('amount')
            ->requirePresence('currency', 'create')
            ->notEmpty('currency')
            ->add('payment_date', 'valid', ['rule' => 'datetime'])
            ->requirePresence('payment_date', 'create')
            ->notEmpty('payment_date')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['payment_id'], 'Payments'));
        return $rules;
    }
    
    /**
     * savePaymentHistory - Method
     * @param type $response
     * @param type $userInfo
     * @return type
     */
    public function savePaymentHistory($response, $userInfo) {
        
        /* Create new entity for payment history table */
        $paymentHistoryEntity = $this->newEntity();
        
        $paymentId = isset($userInfo->id) ? $userInfo->id : 0;
        
        $paymentHistory = $this->PaymentHistories->find('all', [
            'conditions' => ['payment_id' => $paymentId],
            'order' => ['interval_end_date' => 'DESC'],
        ])->first();
        
        /* Set payment history records */
        $paymentHistoryEntity->payment_id = $paymentId;
        $paymentHistoryEntity->plan_id = isset($response->data->object->lines->data[0]->plan->id) ? $response->data->object->lines->data[0]->plan->id : '';
        $paymentHistoryEntity->email = isset($paymentHistory->email) ? $paymentHistory->email : '';
        $paymentHistoryEntity->payment_interval = isset($response->data->object->lines->data[0]->plan->interval) ? $response->data->object->lines->data[0]->plan->interval : '';
        $paymentHistoryEntity->interval_start_date = isset($response->data->object->period_start) ? date('Y-m-d',$response->data->object->period_start) : '';
        $paymentHistoryEntity->interval_end_date = isset($response->data->object->period_end) ? date('Y-m-d',$response->data->object->period_end) : '';
        $paymentHistoryEntity->amount = isset($response->data->object->lines->data[0]->plan->amount) ? $response->data->object->lines->data[0]->plan->amount : 0;
        $paymentHistoryEntity->currency = isset($response->data->object->lines->data[0]->plan->currency) ? $response->data->object->lines->data[0]->plan->currency : '';
        $paymentHistoryEntity->payment_date = isset($response->data->object->date) ? date('Y-m-d H:i:s',$response->data->object->date) : '';
        $paymentHistoryEntity->discount = isset($response->data->object->discount) ? $response->data->object->discount : '';
        $paymentHistoryEntity->tax_percent = isset($response->data->object->tax_percent) ? $response->data->object->tax_percent : '';
        $paymentHistoryEntity->status = 'active';
        
        return $this->addPaymentHistory($paymentHistoryEntity);     

    }
    
    /**
     * Aadd Payment History
     * @param type $paymentHistoryEntity
     * @return string
     */
    public function addPaymentHistory($paymentHistoryEntity){
        try {
            return $this->save($paymentHistoryEntity);
        } catch (Exception $exc) {
            return '';
        }
        
    }
}
