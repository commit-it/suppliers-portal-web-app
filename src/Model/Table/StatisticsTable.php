<?php
namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;

/**
 * Statistics Model
 */
class StatisticsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('statistics');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
            ->allowEmpty('id', 'create')
            ->add('month', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
            ->requirePresence('month', 'create')
            ->notEmpty('month')
            ->add('year', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
            ->requirePresence('year', 'create')
            ->notEmpty('year')
            ->add('count', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
            ->requirePresence('count', 'create')
            ->notEmpty('count');

        return $validator;
    }
    
    public function modifyArrForDispaly($data) {
        
        $statisticsData = array();
        
        foreach ($data as $statistics) {
            
            $monthArr = array_filter(explode(',', $statistics['month_list']));
            $countArr = array_filter(explode(',', $statistics['count_list']));
            
            $statisticsData[$statistics['user_id']]['full_name'] = isset($statistics['user']['full_name']) ? $statistics['user']['full_name'] : $statistics['user_id']; 
            for ($i = 0; $i < count($monthArr); $i++) {
                
                $statisticsData[$statistics['user_id']]['data'][$monthArr[$i]] = $countArr[$i];
            }

            foreach (Configure::read('month') as $monthKey => $monthName) {
                
                if (!in_array($monthKey, $monthArr)) {
                    $statisticsData[$statistics['user_id']]['data'][$monthKey] = 0;
                }
            }
            
            ksort($statisticsData[$statistics['user_id']]['data']);
        }
        
        return $statisticsData;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
}
