<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use DOMDocument;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
use Cake\Network\Session;
use ArrayObject;
use Cake\Core\Configure;
use Cake\I18n\Time;

/**
 * PurchaseOrders Model
 */
class PurchaseOrdersTable extends Table {
    /*
     * Define constant variables
     */

    const Seller_Additional_Id = 'BUYER_ASSIGNED_IDENTIFIER_FOR_A_PARTY';
    const Seller_Name = 'FOR_INTERNAL_USE_1';
    const Ship_From_Name = 'FOR_INTERNAL_USE_1';
    const Ship_From_Additional_Id = 'BUYER_ASSIGNED_IDENTIFIER_FOR_A_PARTY';
    const Ship_From_Address = 'FOR_INTERNAL_USE_2';
    const Ship_From_Telephone_Number = 'FOR_INTERNAL_USE_3';
    const Ship_From_Fax_Number = 'FOR_INTERNAL_USE_4';
    const Bill_To_Additional_Id = 'BUYER_ASSIGNED_IDENTIFIER_FOR_A_PARTY';
    const Bill_To_Name_Type = 'FOR_INTERNAL_USE_1';
    const Bill_To_Address_Type = 'FOR_INTERNAL_USE_2';
    const Bill_To_Telephone_Number = 'FOR_INTERNAL_USE_3';
    const Bill_To_Fax_Number = 'FOR_INTERNAL_USE_4';
    const Buyer_Additional_Id = 'BUYER_ASSIGNED_IDENTIFIER_FOR_A_PARTY';
    const Buyer_Name = 'FOR_INTERNAL_USE_1';
    // Below two variables are defined for myob database
    const Completed = 'Completed';

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->table('purchase_orders');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Currencies', [
            'foreignKey' => 'currency_id'
        ]);
        $this->belongsTo('PurchaseOrderStatuses', [
            'foreignKey' => 'purchase_order_status_id'
        ]);
        $this->hasMany('InvoiceItems', [
            'foreignKey' => 'purchase_order_id'
        ]);
        $this->hasMany('Invoices', [
            'foreignKey' => 'purchase_order_id'
        ]);
        $this->hasMany('PurchaseOrderItems', [
            'foreignKey' => 'purchase_order_id'
        ]);
        $this->hasOne('DeliveryDockets', [
            'foreignKey' => 'purchase_order_id'
        ]);
        $this->hasOne('BookingSheetItems', [
            'foreignKey' => 'purchase_order_id'
        ]);
        $this->addBehavior('Common');
        $this->session = new Session();

        $this->ProcessLogs = TableRegistry::get('ProcessLogs');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
                ->allowEmpty('id', 'create')
                ->requirePresence('user_id', 'create')
                ->notEmpty('user_id')
                ->requirePresence('purchase_order_number', 'create')
                ->notEmpty('purchase_order_number')
                ->requirePresence('instance_id', 'create')
                ->notEmpty('instance_id')
                ->requirePresence('creation_date_time', 'create')
                ->notEmpty('creation_date_time')
                ->requirePresence('seller_gln', 'create')
                ->notEmpty('seller_gln')
                ->requirePresence('seller_additional_id', 'create')
                ->notEmpty('seller_additional_id')
                ->requirePresence('seller_name', 'create')
                ->notEmpty('seller_name')
                ->requirePresence('ship_from_address', 'create')
                ->notEmpty('ship_from_address')
                ->add('bill_to_gln', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
                ->requirePresence('bill_to_gln', 'create')
                ->notEmpty('bill_to_gln')
                ->requirePresence('bill_to_additional_id', 'create')
                ->notEmpty('bill_to_additional_id')
                ->requirePresence('bill_to_name', 'create')
                ->notEmpty('bill_to_name')
                ->requirePresence('bill_to_address', 'create')
                ->notEmpty('bill_to_address')
                ->requirePresence('buyer_gln', 'create')
                ->notEmpty('buyer_gln')
                ->requirePresence('buyer_additional_id', 'create')
                ->notEmpty('buyer_additional_id')
                ->requirePresence('buyer_name', 'create')
                ->notEmpty('buyer_name')
                ->add('order_date', 'valid', ['rule' => 'date', 'message' => 'This value should be in date format'])
                ->requirePresence('order_date', 'create')
                ->notEmpty('order_date')
                ->add('deliver_date', 'valid', ['rule' => 'date', 'message' => 'This value should be in date format'])
                ->requirePresence('deliver_date', 'create')
                ->notEmpty('deliver_date')
                ->add('total_order_amount', 'valid', ['rule' => 'decimal', 'message' => 'This value should be Decimal'])
                ->requirePresence('total_order_amount', 'create')
                ->notEmpty('total_order_amount')
                ->allowEmpty('comments');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['currency_id'], 'Currencies'));
        $rules->add($rules->existsIn(['purchase_order_status_id'], 'PurchaseOrderStatuses'));
        return $rules;
    }

    /**
     * Return query for auth user
     * @param \Cake\Event\Event $event
     * @param \Cake\ORM\Query $query
     * @param ArrayObject $options
     * @return boolean|\Cake\ORM\Query
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options) {

        /**
         * This condition is checked when we don't need to check current user 
         */
        if (isset($options['remove_user_condition']) && $options['remove_user_condition'] > 0) {
            return TRUE;
        }

        /**
         *  This condition is checked because when save data in table at that time 
         *  we need not required before find method
         */
        if ($this->session->read('Auth.User.id') != "") {
            $query->where([$this->alias() . '.user_id' => $this->session->read('Auth.User.id')]);
            return $query;
        }
    }

    /**
     * updateFieldsByField - Method
     * @param type $conditionArr
     * @param type $updateFieldsArr
     * @return type
     */
    public function updateFieldsByFields($conditionArr, $updateFieldsArr) {
        $query = $this->query();

        $result = $query->update()
                ->set($updateFieldsArr)
                ->where($conditionArr)
                ->execute();

        return $result;
    }

    /*
     * @update file type
     * updateFileTypeId() method
     * return true or false
     */

    public function updateInvoiceNumberByPurchaseOrderNumber($invoiceNumber, $purchaseOrderNumber) {
        $query = $this->query();

        $result = $query->update()
                ->set(['invoice_number' => $invoiceNumber])
                ->where(['purchase_order_number' => $purchaseOrderNumber])
                ->execute();

        return $result;
    }

    /*
     * @purchase Order Send To Myob
     * purchaseOrderSendToMyob() method
     * return result
     */

    public function purchaseOrderSendToMyob($purchaseOrderIds) {

        $FileOverviewesObj = TableRegistry::get('FileOverviewes');
        $PurchaseOrderStatusesObj = TableRegistry::get('PurchaseOrderStatuses');
        $CurrenciesObj = TableRegistry::get('Currencies');
        $MasterItemsObj = TableRegistry::get('MasterItems');

        $finalResponse = array();
        $response = array();
        $errorMsgArr = array();
        foreach ($purchaseOrderIds as $purchaseOrderId) {

            $purchaseOrderArr = $this->get($purchaseOrderId, [
                        'contain' => ['PurchaseOrderItems']
                    ])->toArray();

            if ($purchaseOrderArr['actual_delivery_date_updated'] > 0) {

                $listOfExistMasterItems = array();
                $listOfNotExistMasterItems = array();
                $errorMasterItem = 0;
                if (isset($purchaseOrderArr['purchase_order_items']) && !empty($purchaseOrderArr['purchase_order_items'])) {

                    foreach ($purchaseOrderArr['purchase_order_items'] as $purchaseOrderItemData) {

                        $masterItemData = $MasterItemsObj->findDataByGtin($purchaseOrderItemData['gtin']);
                        if (!empty($masterItemData)) {

                            $listOfExistMasterItems[] = $masterItemData->bar_code;
                        } else {

                            $listOfNotExistMasterItems[] = $purchaseOrderItemData['gtin'];
                        }
                    }

                    $listOfExistMasterItems = array_filter($listOfExistMasterItems);
                    $listOfNotExistMasterItems = array_filter($listOfNotExistMasterItems);

                    if (count($purchaseOrderArr['purchase_order_items']) != count($listOfExistMasterItems)) {

                        $errorMasterItem++;
                    }
                }

                if ($errorMasterItem < 1) {

                    // check if purchase order already send to myob
                    $myobConnection = ConnectionManager::get('myob');
                    $purchaseOrderResult = $myobConnection
                            ->newQuery()
                            ->select('count(id)')
                            ->from('purchase_orders')
                            ->where(['purchase_order_number' => $purchaseOrderArr['purchase_order_number']])
                            ->execute()
                            ->fetch();

                    if ($purchaseOrderResult[0] < 1) {
                        if (!empty($purchaseOrderArr)) {

                            // check purchase_order_status_id of myob db and add if exist there
                            $purchaseOrderStatus = $this->checkIfFieldIsExist($PurchaseOrderStatusesObj->findById($purchaseOrderArr['purchase_order_status_id'])->first(), 'status');

                            $purchaseOrderStatusResult = $myobConnection
                                    ->newQuery()
                                    ->select('id')
                                    ->from('purchase_order_statuses')
                                    ->where(['status' => $purchaseOrderStatus])
                                    ->execute()
                                    ->fetch();

                            // Add new status for purchase order status
                            if (!isset($purchaseOrderStatusResult[0])) {
                                $purchaseOrderStatusArr = array();
                                $purchaseOrderStatusArr['status'] = $purchaseOrderStatus;
                                $purchaseOrderStatusSaveResult = $myobConnection->insert('purchase_order_statuses', $purchaseOrderStatusArr);

                                $purchaseOrderStatusResult = $myobConnection
                                        ->newQuery()
                                        ->select('id')
                                        ->from('purchase_order_statuses')
                                        ->where(['status' => $purchaseOrderStatus])
                                        ->execute()
                                        ->fetch();
                            }

                            // check currency_id of myob db and add if exist there
                            $currencyCode = $this->checkIfFieldIsExist($CurrenciesObj->findById($purchaseOrderArr['currency_id'])->first(), 'currency_code');

                            $currenciesResult = $myobConnection
                                    ->newQuery()
                                    ->select('id')
                                    ->from('currencies')
                                    ->where(['currency_code' => $currencyCode])
                                    ->execute()
                                    ->fetch();

                            // Add new currency for currencies order status
                            if (!isset($currenciesResult[0])) {
                                $currenciesResultArr = array();
                                $currenciesResultArr['currency_code'] = $currencyCode;
                                $currenciesResultArr = $myobConnection->insert('currencies', $currenciesResultArr);

                                $currenciesResult = $myobConnection
                                        ->newQuery()
                                        ->select('id')
                                        ->from('currencies')
                                        ->where(['currency_code' => $currencyCode])
                                        ->execute()
                                        ->fetch();
                            }

                            $purchaseOrderArr['purchase_order_status_id'] = $purchaseOrderStatusResult[0];
                            $purchaseOrderArr['currency_id'] = $currenciesResult[0];

                            // unset unwanted fields
                            unset($purchaseOrderArr['id']);
                            unset($purchaseOrderArr['user_id']);
                            unset($purchaseOrderArr['invoice_number']);
                            unset($purchaseOrderArr['sent_to_myob']);
                            unset($purchaseOrderArr['sent_to_quick_book']);
                            unset($purchaseOrderArr['send_fulfilment_advice_to_bunnings']);
                            unset($purchaseOrderArr['created']);
                            unset($purchaseOrderArr['modified']);
                            $purchaseOrderArr['creation_date_time'] = $this->getFormatedDateFromObject($purchaseOrderArr['creation_date_time']);
                            $purchaseOrderArr['order_date'] = $this->getFormatedDateFromObject($purchaseOrderArr['order_date']);
                            $purchaseOrderArr['deliver_date'] = $this->getFormatedDateFromObject($purchaseOrderArr['deliver_date']);
                            if (isset($purchaseOrderArr['purchase_order_items']) && !empty($purchaseOrderArr['purchase_order_items'])) {
                                for ($i = 0; $i < count($purchaseOrderArr['purchase_order_items']); $i++) {
                                    unset($purchaseOrderArr['purchase_order_items'][$i]['id']);
                                    unset($purchaseOrderArr['purchase_order_items'][$i]['purchase_order_id']);
                                    unset($purchaseOrderArr['purchase_order_items'][$i]['reference_purchase_order_line_number']);
                                    unset($purchaseOrderArr['purchase_order_items'][$i]['gst_amount']);
                                    unset($purchaseOrderArr['purchase_order_items'][$i]['gst_percentage']);
                                    unset($purchaseOrderArr['purchase_order_items'][$i]['created']);
                                    unset($purchaseOrderArr['purchase_order_items'][$i]['modified']);
                                }
                                $purchaseOrderItemsArr = $purchaseOrderArr['purchase_order_items'];
                                unset($purchaseOrderArr['purchase_order_items']);
                                $result = $FileOverviewesObj->insertDataInMyobDB($purchaseOrderArr, $purchaseOrderItemsArr);
                                if ($result) {
                                    $response[] = $purchaseOrderArr['purchase_order_number'] . ' purchase order send successfully to Myob.';

                                    /* Update sent_to_myob field in purchase order table for suplyer-portal */
                                    $updatePurchaseOrder = $this->updateSentToMyob(Configure::read('sent_to_myob.yes'), $purchaseOrderId);
                                    if ($updatePurchaseOrder) {
                                        $response[] = $purchaseOrderArr['purchase_order_number'] . " Purchase order is updated successfully";
                                    } else {
                                        $response[] = $purchaseOrderArr['purchase_order_number'] . " Purchase order is not updated successfully";
                                    }
                                } else {
                                    $response[] = $purchaseOrderArr['purchase_order_number'] . ' purchase order does not send successfully to Myob, please try again.';
                                }
                            } else {
                                $response[] = 'Purchase Order items are  not exist for selected purchase order, you can\'t send that to Myob';
                            }
                        } else {
                            $response[] = 'Selected purchase order not exist, you can\'t send that to Myob';
                        }
                    } else {
                        $response[] = $purchaseOrderArr['purchase_order_number'] . ' purchase order is already send to Myob, you can\'t send that';
                    }
                } else {
                    if (count($listOfNotExistMasterItems) > 1) {
                        $errorMsgArr[] = 'You can\'t send PO #' . $purchaseOrderArr['purchase_order_number'] . ' to Myob because ' . implode(', ', $listOfNotExistMasterItems) . ' items are not present in Master Items list.';
                    } else {
                        $errorMsgArr[] = 'You can\'t send PO #' . $purchaseOrderArr['purchase_order_number'] . ' to Myob because ' . implode(', ', $listOfNotExistMasterItems) . ' item is not present in Master Items list.';
                    }
                }
            } else {

                $errorMsgArr[] = 'You can\'t send that PO #' . $purchaseOrderArr['purchase_order_number'] . ' to Myob, you need to set Actual Delivery Date';
            }
        }

        unset($FileOverviewesObj);
        unset($PurchaseOrderStatusesObj);
        unset($CurrenciesObj);
        unset($MasterItemsObj);

        $finalResponse['response'] = $response;

        if (!empty($errorMsgArr)) {
            $errorMsgArr[] = 'Please update the above List and resend the above PO.';
        }
        $finalResponse['error'] = $errorMsgArr;
        return $finalResponse;
    }

    /**
     * @update sent_to_myob
     * @param type $sentToMyob
     * @param type $purchaseOrderId
     */
    public function updateSentToMyob($sentToMyob, $purchaseOrderId) {
        $query = $this->query();
        try {
            return $query->update()
                            ->set(['sent_to_myob' => $sentToMyob])
                            ->where(['id' => $purchaseOrderId])
                            ->execute();
        } catch (Exception $ex) {
            return $ex;
        }
    }

    /**
     * Get Availabe Purchase Orders Count
     * @return type
     */
    public function getAvailabelPurchaseOrdersCount() {
        return $this->find('all', ['conditions' => [
                        'sent_to_myob' => Configure::read('sent_to_myob.no'),
                        'sent_to_quick_book' => Configure::read('sent_to_quick_book.no'),
                        'archived' => Configure::read('po_archived.no')
            ]])->count();
    }

    /**
     * managePurchaseOrdersPdf method - manage pdf content
     * @param type $purchaseOrders
     * @return string
     */
    public function managePurchaseOrdersPdf($purchaseOrders) {

        $html = '';
        $html = $html . '<!DOCTYPE html>
                        <html>
                            <head>
                                <title>Purchase Order</title>
                                <link rel="stylesheet" type="text/css" href="' . WWW_ROOT . 'css' . DS . 'dompdf_style.css" media="screen" />
                            </head>
                        <body>';
        foreach ($purchaseOrders as $purchaseOrder) {
            $billToName = @$purchaseOrder['bill_to_name'];
            $comments = @$purchaseOrder['comments'];
            $html = $html . '<div class="width-100">'
            . '<h1>' . strtoupper($purchaseOrder['user']['seller_business_name']) . '</h1>'
            . '<br />'
            . '<h3>' . __('Purchase Order Number') . ' : ' . $purchaseOrder['purchase_order_number'] . '</h3>'
            . '<p class="font-size-19"><b>' . __('Store Name') . ' :</b> ' . $billToName . '</p>';
            if (isset($comments) && !empty($comments)) {
                $html = $html . '<p class="font-size-19"><b>' . __('Comments') . ' :</b> ' . $comments . '</p>';
            }
            $html = $html . '<br />'
            . '<table border="1" class="purchase-order">'
                    . '<tr>'
                    . '<th class="width-15">' . __('Quantity') . '</th>'
                    . '<th class="width-60">' . __('Description') . '</th>'
                    . '<th class="width-25">' . __('Comment') . '</th>'
                    . '</tr>';
            foreach ($purchaseOrder['purchase_order_items'] as $purchaseOrderItems) {
                $html = $html . '<tr>'
                        . '<td>' . $purchaseOrderItems['requested_quantity'] . '</td>'
                        . '<td>' . $purchaseOrderItems['description'] . '</td>'
                        . '<td>' . $purchaseOrderItems['comments'] . '</td>'
                        . '</tr>';
            }
            $html = $html . '</table>'
                    . '</div>'
                    . '<span style="page-break-after: always"></span>';
        }
        $html = $html . '</body>'
                . '</html>';

        return $html;
    }

    /**
     * generateFulfilmentAdviceXmlFileAndSendToSftp - This method genrate Fulfilment Advice Xml file & send to sftp
     * @param type $POIdsArr
     * @return string
     */
    public function generateFulfilmentAdviceXmlFileAndSendToSftp($POIdsArr) {

        $this->addBehavior('SftpConnection');

        // create object
        $bunningStoresObj = TableRegistry::get('BunningStores');

        if (is_array($POIdsArr)) {

            $purchaseOrdersArr = $this->find('all', [
                        'conditions' => array(
                            'PurchaseOrders.id IN' => $POIdsArr,
                            'PurchaseOrders.archived' => Configure::read('po_archived.no')
                        ),
                        'contain' => ['PurchaseOrderItems']
                    ])->applyOptions(['remove_user_condition' => true])->toArray();
        } else {

            $purchaseOrdersArr = $this->find('all', [
                        'conditions' => array(
                            'PurchaseOrders.id' => $POIdsArr,
                            'PurchaseOrders.archived' => Configure::read('po_archived.no')
                        ),
                        'contain' => ['PurchaseOrderItems']
                    ])->applyOptions(['remove_user_condition' => true])->toArray();
        }

        $results = array();
        $processLogArr = array();
        if (!empty($purchaseOrdersArr)) {

            foreach ($purchaseOrdersArr as $purchaseOrder) {

                // fulfilment Advice Number of Fulfilment Advice Document
                $fulfilmentAdviceNumber = $this->generateRandomNumber(10);

                // fulfilment advice xml file name
                $xmlFile = Configure::read('fulfilment_advice.file-prefix') . strtotime('now') . '_' . $this->generateRandomCode(5);

                // Before generate fulfilment advice check buyer and payer is available in list
                $buyerId = $this->checkIfFieldIsExist($bunningStoresObj->findByGlnAndType($purchaseOrder['buyer_gln'], Configure::read('bunning_user.Buyer'))->applyOptions(['remove_user_condition' => true])->first(), 'id');
                $payerId = $this->checkIfFieldIsExist($bunningStoresObj->findByGlnAndType($purchaseOrder['bill_to_gln'], Configure::read('bunning_user.Payer'))->applyOptions(['remove_user_condition' => true])->first(), 'id');

                if ($buyerId && $payerId) {

                    // generate fulfilment advice xml file
                    $fulfilmentAdviceResult = $this->generateFulfilmentAdviceXmlFile($purchaseOrder, $xmlFile, $fulfilmentAdviceNumber);
                    if (isset($fulfilmentAdviceResult['success'])) {

                        // update po send_fulfilment_advice_to_bunnings
                        $updateArr = array(
                            'fulfilment_despatch_advice_identification_number' => $fulfilmentAdviceNumber
                        );
                        $conditionArr = array(
                            'id' => $purchaseOrder['id'],
                            'user_id' => $purchaseOrder['user_id']
                        );
                        $updateResult = $this->updateFieldsByFields($conditionArr, $updateArr);
                        if ($updateResult) {

                            $results['success'][] = $purchaseOrder['purchase_order_number'] . " send_fulfilment_advice_to_bunnings field is updated successfully by Yes with " . $fulfilmentAdviceNumber . " this fulfilment Advice Number";

                            // store process log details
                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($purchaseOrder['purchase_order_number'] . " send_fulfilment_advice_to_bunnings field is updated successfully by Yes with " . $fulfilmentAdviceNumber . " this fulfilment Advice Number", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                        } else {

                            $results['error'][] = $purchaseOrder['purchase_order_number'] . " send_fulfilment_advice_to_bunnings field is not updated successfully by Yes";

                            // store process log details
                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($purchaseOrder['purchase_order_number'] . " send_fulfilment_advice_to_bunnings field is not updated successfully by Yes", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                        }

                        $results['success'][] = $fulfilmentAdviceResult['success'];
                        $fulfilmentAdviceResultMsg = $fulfilmentAdviceResult['success'];
                    } else {

                        $results['error'][] = isset($fulfilmentAdviceResult['error']) ? $fulfilmentAdviceResult['error'] : 'Getting Some error during create Fulfilment Advice';
                        $fulfilmentAdviceResultMsg = isset($fulfilmentAdviceResult['error']) ? $fulfilmentAdviceResult['error'] : 'Getting Some error during create Fulfilment Advice';
                    }

                    // store process log details
                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($fulfilmentAdviceResultMsg, $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);

                    /*
                     * @connect sftp using sftpConnection() method
                     */
                    $resSFTP = $this->sftpConnection();
                    if ($resSFTP) {

                        $uploadFileResult = $this->upload(Configure::read('fulfilment_advice.localPath') . DS . $xmlFile . '.xml', Configure::read('fulfilment_advice.remotePath') . DS . $xmlFile . '.xml');
                        if ($uploadFileResult) {

                            $results['success'][] = $xmlFile . ' this generated fulfilment advice file uploaded successfully to sftp';

                            // store process log details
                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . ' this generated fulfilment advice file uploaded successfully to sftp', $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);

                            // move uploaded file in out folder
                            if ($this->moveFileInAnotherFolder($xmlFile . '.xml', Configure::read('fulfilment_advice.localPath'), Configure::read('fulfilment_advice.localPath-out'))) {

                                $results['success'][] = $xmlFile . " file moved successfully in Out folder.";

                                // store process log details
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . " file moved successfully in Out folder", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);

                                // update po send_fulfilment_advice_to_bunnings
                                $updateArr = array(
                                    'send_fulfilment_advice_to_bunnings' => Configure::read('send_fulfilment_advice_to_bunnings.yes')
                                );
                                $conditionArr = array(
                                    'id' => $purchaseOrder['id'],
                                    'user_id' => $purchaseOrder['user_id']
                                );
                                $updateResult = $this->updateFieldsByFields($conditionArr, $updateArr);
                                if ($updateResult) {

                                    $results['success'][] = $purchaseOrder['purchase_order_number'] . " send_fulfilment_advice_to_bunnings field is updated successfully by Yes";

                                    // store process log details
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($purchaseOrder['purchase_order_number'] . " send_fulfilment_advice_to_bunnings field is updated successfully by Yes", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                                } else {

                                    $results['error'][] = $purchaseOrder['purchase_order_number'] . " send_fulfilment_advice_to_bunnings field is not updated successfully by Yes";

                                    // store process log details
                                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($purchaseOrder['purchase_order_number'] . " send_fulfilment_advice_to_bunnings field is not updated successfully by Yes", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                                }
                            } else {

                                $results['error'][] = $xmlFile . " file does not moved successfully in Out folder";

                                // store process log details
                                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . " file not moved successfully in Out folder", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                            }
                        } else {

                            $results['error'][] = $xmlFile . ' this generated fulfilment advice file not uploaded successfully to sftp';

                            // store process log details
                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . ' this generated fulfilment advice file not uploaded successfully to sftp', $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                        }
                    } else {

                        $results['error'][] = "Some error is occurred in sftp connection";

                        // store process log details
                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr("Some error is occurred in sftp connection", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                    }
                } else {

                    if (!$buyerId) {

                        $results['error'][] = $purchaseOrder['buyer_gln'] . " this buyer is not present in Bunning Stores to generate fulfilment advice of " . $purchaseOrder['purchase_order_number'] . " Purchase Order";

                        // store process log details
                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($purchaseOrder['buyer_gln'] . " this buyer is not present in Bunning Stores to generate invoice of " . $purchaseOrder['purchase_order_number'] . " Purchase Order", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                    }
                    if (!$payerId) {

                        $results['error'][] = $purchaseOrder['bill_to_gln'] . " this payer is not present in Bunning Stores to generate fulfilment advice of " . $purchaseOrder['purchase_order_number'] . " Purchase Order";

                        // store process log details
                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($purchaseOrder['bill_to_gln'] . " this payer is not present in Bunning Stores to generate fulfilment advice of " . $purchaseOrder['purchase_order_number'] . " Purchase Order", $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                    }
                }
            }
        } else {

            $results['error'][] = "No Purchase Orders available for Fulfilment Advice.";

            // store process log details
            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr("No Purchase Orders available for Fulfilment Advice");
        }

        if (!empty($processLogArr)) {

            // save process logs
            $processLogResult = $this->ProcessLogs->saveProcessLogs($processLogArr);
        }

        return $results;
    }

    /**
     * generateFulfilmentAdviceXmlFile - generate fulfilment advice xml file
     * @param type $purchaseOrder
     * @param type $xmlFile
     * @param number $fulfilmentAdviceNumber - This is document reference number
     * @return string
     */
    public function generateFulfilmentAdviceXmlFile($purchaseOrder, $xmlFile, $fulfilmentAdviceNumber) {

        /*
         * check dir is exist
         * if not then create
         */
        $dirInResult = $this->makeDirectoryIfNotExist(Configure::read('fulfilment_advice.localPath'));
        $dirOutResult = $this->makeDirectoryIfNotExist(Configure::read('fulfilment_advice.localPath-out'));

        $result = array();

        // generate xml for Fulfilment Advice
        $invoiceXml = $this->generateFulfilmentAdviceXml($purchaseOrder, $fulfilmentAdviceNumber);

        $doc = new DOMDocument();
        $doc->formatOutput = true;
        try {

            $purchaseOrderNumber = isset($purchaseOrder['purchase_order_number']) ? $purchaseOrder['purchase_order_number'] : 'Purchase Order';
            $doc->loadXML($invoiceXml);
            $fileResult = $doc->save(Configure::read('fulfilment_advice.localPath') . DS . $xmlFile . '.xml');
            if ($fileResult) {
                $result['success'] = "Fulfilment Advice xml file generated successfully for " . $purchaseOrderNumber;
            } else {
                $result['error'] = "Fulfilment Advice xml file doesn't generated for " . $purchaseOrderNumber . ", pleae try again.";
            }
        } catch (\Cake\Utility\Exception\XmlException $e) {

            $result['error'] = InternalErrorException();
        }

        return $result;
    }

    /**
     * generateFulfilmentAdviceXml - This method generate Fulfilment Advice Xml file
     * @param array $purchaseOrder
     * @param number $fulfilmentAdviceNumber - This is document reference number
     * @return string
     */
    public function generateFulfilmentAdviceXml($purchaseOrder, $fulfilmentAdviceNumber) {

        // Instance Identifier of Fulfilment Advice Document
        $instanceIdentifier = $this->generateRandomNumber(7);

        // Generate fulfilment advice
        $unitOfMeasure = isset($purchaseOrder['purchase_order_items'][0]['unit_of_measure']) ? $purchaseOrder['purchase_order_items'][0]['unit_of_measure'] : 'EA';

        // The logistic unit is 1 due to the bunning using model 3 Packaging
        $logisticUnit = 1;

        // The logistic unit type is PF due to the bunning
        $logisticUnitType = 'PF';

        $purchaseOrderItemsXml = '';
        foreach ($purchaseOrder['purchase_order_items'] as $purchaseOrderItem) {

            // check gtin > 14 otherwise prepend 0
            if (strlen($purchaseOrderItem['gtin']) < 14) {
                $gtinPrefix = 0;
                $prefixCnt = 14 - strlen($purchaseOrderItem['gtin']);
                $purchaseOrderItem['gtin'] = $this->addPrefixToString($purchaseOrderItem['gtin'], $gtinPrefix, $prefixCnt);
            }

            $purchaseOrderItemsXml = $purchaseOrderItemsXml . '
                            <despatchAdviceLineItem>
                                <lineItemNumber>' . $purchaseOrderItem['order_line_number'] . '</lineItemNumber>
                                <despatchedQuantity measurementUnitCode="' . $purchaseOrderItem['unit_of_measure'] . '">' . $purchaseOrderItem['requested_quantity'] . '</despatchedQuantity>
                                <transactionalTradeItem>
                                    <gtin>' . $purchaseOrderItem['gtin'] . '</gtin>';
            if (trim($purchaseOrderItem['bunnings_product_id']) != '') {
                $purchaseOrderItemsXml = $purchaseOrderItemsXml . '<additionalTradeItemIdentification additionalTradeItemIdentificationTypeCode="BUYER_ASSIGNED">' . $purchaseOrderItem['bunnings_product_id'] . '</additionalTradeItemIdentification>';
            }
            if (trim($purchaseOrderItem['supplier_product_id']) != '') {
                $purchaseOrderItemsXml = $purchaseOrderItemsXml . '<additionalTradeItemIdentification additionalTradeItemIdentificationTypeCode="SUPPLIER_ASSIGNED">' . $purchaseOrderItem['supplier_product_id'] . '</additionalTradeItemIdentification>';
            }
            if ($purchaseOrderItem['order_line_number'] > 0) {
                $purchaseOrderItemsXml = $purchaseOrderItemsXml . '<additionalTradeItemIdentification additionalTradeItemIdentificationTypeCode="FOR_INTERNAL_USE_2">' . $purchaseOrderItem['order_line_number'] . '</additionalTradeItemIdentification>';
            }
            $purchaseOrderItemsXml = $purchaseOrderItemsXml . '<tradeItemDescription languageCode="' . $purchaseOrder['language_iso_code'] . '">' . $purchaseOrderItem['description'] . '</tradeItemDescription>
                                </transactionalTradeItem>
                            </despatchAdviceLineItem>';
        }

        $directShipAndDirectShipOffRangeOrderPos = strpos($purchaseOrder['purchase_order_number'], 'D');

        $fulfilmentAdviceXml = '';
        $fulfilmentAdviceXml = $fulfilmentAdviceXml . '<?xml version="1.0" encoding="UTF-8"?>
                    <despatch_advice:despatchAdviceMessage xmlns:despatch_advice="urn:gs1:ecom:despatch_advice:xsd:3" xmlns:sh="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:gs1:ecom:despatch_advice:xsd:3 ../Schemas/gs1/ecom/DespatchAdvice.xsd">
                        <sh:StandardBusinessDocumentHeader>
                            <sh:HeaderVersion>1.0</sh:HeaderVersion>
                            <sh:Sender>
                                <sh:Identifier Authority="GS1">' . $purchaseOrder['receiver_gln'] . '</sh:Identifier>
                            </sh:Sender>
                            <sh:Receiver>
                                <sh:Identifier Authority="GS1">' . $purchaseOrder['buyer_gln'] . '</sh:Identifier>
                            </sh:Receiver>
                            <sh:DocumentIdentification>
                                <sh:Standard>GS1</sh:Standard>
                                <sh:TypeVersion>3.1</sh:TypeVersion>
                                <sh:InstanceIdentifier>' . $instanceIdentifier . '</sh:InstanceIdentifier>
                                <sh:Type>Despatch Advice Message</sh:Type>
                                <sh:MultipleType>false</sh:MultipleType>
                                <sh:CreationDateAndTime>' . date("Y-m-d\TH:i:s") . '</sh:CreationDateAndTime>
                            </sh:DocumentIdentification>
                        </sh:StandardBusinessDocumentHeader>
                        <despatchAdvice>
                            <creationDateTime>' . date("Y-m-d\TH:i:s") . '</creationDateTime>
                            <documentStatusCode>ORIGINAL</documentStatusCode>
                            <despatchAdviceIdentification>
                                <entityIdentification>' . $fulfilmentAdviceNumber . '</entityIdentification>
                            </despatchAdviceIdentification>
                            <receiver>
                                <gln>' . $purchaseOrder['buyer_gln'] . '</gln>
                                <additionalPartyIdentification additionalPartyIdentificationTypeCode="BUYER_ASSIGNED_IDENTIFIER_FOR_A_PARTY">' . $purchaseOrder['buyer_additional_id'] . '</additionalPartyIdentification>
                                <organisationDetails>
                                    <organisationName>' . htmlspecialchars($purchaseOrder['buyer_name']) . '</organisationName>
                                </organisationDetails>
                            </receiver>
                            <shipper>
                                <gln>' . $purchaseOrder['seller_gln'] . '</gln>
                                <additionalPartyIdentification additionalPartyIdentificationTypeCode="BUYER_ASSIGNED_IDENTIFIER_FOR_A_PARTY">' . $purchaseOrder['seller_additional_id'] . '</additionalPartyIdentification>
                                <organisationDetails>
                                    <organisationName>' . htmlspecialchars($purchaseOrder['seller_name']) . '</organisationName>
                                </organisationDetails>
                            </shipper>
                            <shipTo>';
        if (($directShipAndDirectShipOffRangeOrderPos === FALSE)) {
            $fulfilmentAdviceXml = $fulfilmentAdviceXml . '<gln>' . $purchaseOrder['bill_to_gln'] . '</gln>
                                <additionalPartyIdentification additionalPartyIdentificationTypeCode="BUYER_ASSIGNED_IDENTIFIER_FOR_A_PARTY">' . $purchaseOrder['bill_to_additional_id'] . '</additionalPartyIdentification>';
        }

        $shipToAddress = $this->separateUserAddressForGenerateInvoice($purchaseOrder['bill_to_address'], 'BunningStores', 'findByGln', $purchaseOrder['bill_to_gln']);
        $fulfilmentAdviceXml = $fulfilmentAdviceXml . '<address>
                                    <city>' . $shipToAddress['city'] . '</city>
                                    <countryCode>' . $shipToAddress['countryISOCode'] . '</countryCode>
                                    <name>' . htmlspecialchars($purchaseOrder['bill_to_name']) . '</name>
                                    <postalCode>' . $shipToAddress['postalCode'] . '</postalCode>
                                    <state>' . $shipToAddress['state'] . '</state>
                                    <streetAddressOne>' . htmlspecialchars($shipToAddress['streetAddressOne']) . '</streetAddressOne>
                                </address>';

        $fulfilmentAdviceXml = $fulfilmentAdviceXml . '</shipTo>
                            <shipFrom>
                                <gln>' . $purchaseOrder['ship_from_gln'] . '</gln>
                                <additionalPartyIdentification additionalPartyIdentificationTypeCode="BUYER_ASSIGNED_IDENTIFIER_FOR_A_PARTY">' . $purchaseOrder['ship_from_additional_id'] . '</additionalPartyIdentification>';

        $shipFromAddress = $this->separateUserAddressForGenerateInvoice($purchaseOrder['ship_from_address'], 'BunningStores', 'findByGln', $purchaseOrder['seller_gln']);
        $fulfilmentAdviceXml = $fulfilmentAdviceXml . '<address>
                                    <city>' . $shipFromAddress['city'] . '</city>
                                    <countryCode>' . $shipFromAddress['countryISOCode'] . '</countryCode>
                                    <name>' . $purchaseOrder['ship_from_name'] . '</name>
                                    <postalCode>' . $shipFromAddress['postalCode'] . '</postalCode>
                                    <state>' . $shipFromAddress['state'] . '</state>
                                    <streetAddressOne>' . $shipFromAddress['streetAddressOne'] . '</streetAddressOne>
                                </address>';
        $fulfilmentAdviceXml = $fulfilmentAdviceXml . '</shipFrom>
                            <carrier>
                                <gln>' . $purchaseOrder['seller_gln'] . '</gln>
                                <additionalPartyIdentification additionalPartyIdentificationTypeCode="BUYER_ASSIGNED_IDENTIFIER_FOR_A_PARTY">' . $purchaseOrder['seller_additional_id'] . '</additionalPartyIdentification>
                                <organisationDetails>
                                    <organisationName>' . $purchaseOrder['seller_name'] . '</organisationName>
                                </organisationDetails>
                            </carrier>
                            <despatchInformation>
                                <actualShipDateTime>' . date("Y-m-d\TH:i:s", strtotime("+7 day")) . '</actualShipDateTime>
                                <estimatedDeliveryDateTime>' . date("Y-m-d\TH:i:s", strtotime("+7 day")) . '</estimatedDeliveryDateTime>
                            </despatchInformation>
                            <despatchAdviceTransportInformation>
                            <consignmentIdentification>
                                <ginc>000000000</ginc>
                                <additionalConsignmentIdentification additionalConsignmentIdentificationTypeCode="CARRIER_ASSIGNED">' . $purchaseOrder['seller_gln'] . '</additionalConsignmentIdentification>
                            </consignmentIdentification>
                            </despatchAdviceTransportInformation>
                            <despatchAdviceTotals>
                                <packageTotal>
                                    <packageTypeCode>' . $logisticUnitType . '</packageTypeCode>
                                    <totalPackageQuantity>' . $logisticUnit . '</totalPackageQuantity>
                                </packageTotal>
                            </despatchAdviceTotals>
                            <purchaseOrder>
                                <entityIdentification>' . $purchaseOrder['purchase_order_number'] . '</entityIdentification>
                            </purchaseOrder>
                            <despatchAdviceLogisticUnit>
                            <packageTypeCode>' . $logisticUnitType . '</packageTypeCode>
                            <quantityOfChildren>' . $logisticUnit . '</quantityOfChildren>
                            <logisticUnitIdentification>
                                <sscc>000000000000000000</sscc>
                            </logisticUnitIdentification>';

        $purchaseOrder['language_iso_code'] = ($purchaseOrder['language_iso_code'] != null) ? $purchaseOrder['language_iso_code'] : 'EN';

        $fulfilmentAdviceXml = $fulfilmentAdviceXml . $purchaseOrderItemsXml;

        $fulfilmentAdviceXml = $fulfilmentAdviceXml . '</despatchAdviceLogisticUnit>
                        </despatchAdvice>
                    </despatch_advice:despatchAdviceMessage>';

        return $fulfilmentAdviceXml;
    }

    /**
     * generateApplicationReceiptOfPurcaseOrderXmlFileAndSendToSftp - This method genrate Application Receipt PO Xml file & send to sftp
     * @param type $poXmlData
     * @return string
     */
    public function generateApplicationReceiptOfPurcaseOrderXmlFileAndSendToSftp($poXmlData, $applicationReceiptStatus) {

        $this->addBehavior('SftpConnection');

        $processLogArr = array();

        $purchaseOrderNumber = isset($poXmlData['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderIdentification']['uniqueCreatorIdentification']) ? $poXmlData['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderIdentification']['uniqueCreatorIdentification'] : 'Purchase Order';
        $xmlFile = Configure::read('Application_Receipt.File_Prefix') . $purchaseOrderNumber . '_' . time();

        // generate Application Receipt xml file
        $ApplicationReceiptResult = $this->generateApplicationReceiptOfPurcaseOrderXmlFile($poXmlData, $xmlFile, $applicationReceiptStatus);
        if (isset($ApplicationReceiptResult['success'])) {

            $results['success'][] = $ApplicationReceiptResult['success'];
            $ApplicationReceiptResultMsg = $ApplicationReceiptResult['success'];
        } else {

            $results['error'][] = isset($ApplicationReceiptResult['error']) ? $ApplicationReceiptResult['error'] : 'Getting Some error during create Purchase Order Application Receipt';
            $ApplicationReceiptResultMsg = isset($ApplicationReceiptResult['error']) ? $ApplicationReceiptResult['error'] : 'Getting Some error during create Purchase Order Application Receipt';
        }

        // store process log details
        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($ApplicationReceiptResultMsg);

        /*
         * @connect sftp using sftpConnection() method
         */
        $resSFTP = $this->sftpConnection();
        if ($resSFTP) {

            $uploadFileResult = $this->upload(Configure::read('Application_Receipt.In_Path') . DS . $xmlFile . '.xml', Configure::read('Application_Receipt.Remote_Path') . DS . $xmlFile . '.xml');
            if ($uploadFileResult) {

                $results['success'][] = $xmlFile . ' this generated Application Receipt file uploaded successfully to sftp';

                // store process log details
                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . ' this generated Application Receipt file uploaded successfully to sftp');

                // move uploaded file in out folder
                if ($this->moveFileInAnotherFolder($xmlFile . '.xml', Configure::read('Application_Receipt.In_Path'), Configure::read('Application_Receipt.Out_Path'))) {

                    $results['success'][] = $xmlFile . " file moved successfully in Out folder.";

                    // store process log details
                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . " file moved successfully in Out folder");
                } else {

                    $results['error'][] = $xmlFile . " file does not moved successfully in Out folder";

                    // store process log details
                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . " file not moved successfully in Out folder");
                }
            } else {

                $results['error'][] = $xmlFile . ' this generated Application Receipt file not uploaded successfully to sftp';

                // store process log details
                $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($xmlFile . ' this generated Application Receipt file not uploaded successfully to sftp');
            }
        } else {

            $results['error'][] = "Some error is occurred in sftp connection";

            // store process log details
            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr("Some error is occurred in sftp connection");
        }

        if (!empty($processLogArr)) {

            // save process logs
            $processLogResult = $this->ProcessLogs->saveProcessLogs($processLogArr);
        }

        return $results;
    }

    /**
     * generateApplicationReceiptOfPurcaseOrderXmlFile - This method generate Application Receipt Xml file
     * @param type $poXml
     * @param type $xmlFile
     * @param type $applicationReceiptStatus
     * @return type
     */
    public function generateApplicationReceiptOfPurcaseOrderXmlFile($poXml, $xmlFile, $applicationReceiptStatus) {

        /*
         * check dir is exist
         * if not then create
         */
        $dirInResult = $this->makeDirectoryIfNotExist(Configure::read('Application_Receipt.In_Path'));
        $dirOutResult = $this->makeDirectoryIfNotExist(Configure::read('Application_Receipt.Out_Path'));

        $result = array();

        // generate xml for Fulfilment Advice
        $invoiceXml = $this->generateApplicationReceiptPOXml($poXml, $applicationReceiptStatus);

        $doc = new DOMDocument();
        $doc->formatOutput = true;
        try {

            $purchaseOrderNumber = isset($poXml['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderIdentification']['uniqueCreatorIdentification']) ? $poXml['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderIdentification']['uniqueCreatorIdentification'] : 'Purchase Order';
            $doc->loadXML($invoiceXml);
            $fileResult = $doc->save(Configure::read('Application_Receipt.In_Path') . DS . $xmlFile . '.xml');
            if ($fileResult) {
                $result['success'] = "Application_Receipt xml file generated successfully for " . $purchaseOrderNumber;
            } else {
                $result['error'] = "Application_Receipt xml file doesn't generated for " . $purchaseOrderNumber . ", pleae try again.";
            }
        } catch (\Cake\Utility\Exception\XmlException $e) {

            $result['error'] = InternalErrorException();
        }

        return $result;
    }

    /**
     * generateApplicationReceiptPOXml - This method generate Application Receipt Xml file of Purchase Order
     * @param type $poXml
     * @param type $applicationReceiptStatus
     * @return string
     */
    public function generateApplicationReceiptPOXml($poXml, $applicationReceiptStatus) {

        $documentPOAcknowledgedType = Configure::read('Application_Receipt.document_acknowledged_type.Purchase_Order');
        $instanceIdentifier = $this->generateRandomNumber(7);

        $applicationReceiptXml = '<?xml version="1.0" encoding="UTF-8"?>
                                <sh:StandardBusinessDocument xmlns:sh="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader" xmlns:eanucc="urn:ean.ucc:2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.unece.org/cefact/namespaces/StandardBusinessDocumentHeader ..\Schemas\sbdh\StandardBusinessDocumentHeader.xsd urn:ean.ucc:2 ..\Schemas\ApplicationReceiptAcknowledgementProxy.xsd">
                                <sh:StandardBusinessDocumentHeader>
                                  <sh:HeaderVersion>1.0</sh:HeaderVersion>
                                  <sh:Sender>
                                    <sh:Identifier Authority="EAN.UCC">' . $poXml['sh:StandardBusinessDocumentHeader']['sh:Receiver']['sh:Identifier']['@'] . '</sh:Identifier>
                                  </sh:Sender>
                                  <sh:Receiver>
                                    <sh:Identifier Authority="EAN.UCC">' . $poXml['sh:StandardBusinessDocumentHeader']['sh:Sender']['sh:Identifier']['@'] . '</sh:Identifier>
                                  </sh:Receiver>
                                  <sh:DocumentIdentification>
                                    <sh:Standard>EAN.UCC</sh:Standard>
                                    <sh:TypeVersion>2.0.2</sh:TypeVersion>
                                    <sh:InstanceIdentifier>' . $instanceIdentifier . '</sh:InstanceIdentifier>
                                    <sh:Type>ApplicationReceiptAcknowledgement</sh:Type>
                                    <sh:CreationDateAndTime>' . date("Y-m-d\TH:i:s") . '</sh:CreationDateAndTime>
                                  </sh:DocumentIdentification>
                                </sh:StandardBusinessDocumentHeader>
                                <eanucc:message>
                                  <entityIdentification>
                                    <uniqueCreatorIdentification>MSG-' . $instanceIdentifier . '</uniqueCreatorIdentification>
                                    <contentOwner>
                                      <gln>' . $poXml['sh:StandardBusinessDocumentHeader']['sh:Receiver']['sh:Identifier']['@'] . '</gln>
                                    </contentOwner>
                                  </entityIdentification>
                                  <eanucc:transaction>
                                    <entityIdentification>
                                      <uniqueCreatorIdentification>TRA-' . $instanceIdentifier . '</uniqueCreatorIdentification>
                                      <contentOwner>
                                        <gln>' . $poXml['sh:StandardBusinessDocumentHeader']['sh:Receiver']['sh:Identifier']['@'] . '</gln>
                                      </contentOwner>
                                    </entityIdentification>
                                    <command>
                                      <eanucc:documentCommand>
                                        <documentCommandHeader type="ADD">
                                          <entityIdentification>
                                            <uniqueCreatorIdentification>CMD-' . $instanceIdentifier . '</uniqueCreatorIdentification>
                                            <contentOwner>
                                              <gln>' . $poXml['sh:StandardBusinessDocumentHeader']['sh:Receiver']['sh:Identifier']['@'] . '</gln>
                                            </contentOwner>
                                          </entityIdentification>
                                        </documentCommandHeader>
                                        <documentCommandOperand>
                                          <eanucc:applicationReceiptAcknowledgement creationDateTime="' . date('Y-m-d\TH:i:s', strtotime($poXml['sh:StandardBusinessDocumentHeader']['sh:DocumentIdentification']['sh:CreationDateAndTime'])) . '" documentStatus="ORIGINAL">
                                            <contentVersion>
                                              <versionIdentification>2.0.2</versionIdentification>
                                            </contentVersion>
                                            <documentStructureVersion>
                                              <versionIdentification>2.0.2</versionIdentification>
                                            </documentStructureVersion>
                                            <applicationReceiptAcknowledgementIdentification>
                                              <uniqueCreatorIdentification>' . $poXml['sh:StandardBusinessDocumentHeader']['sh:DocumentIdentification']['sh:InstanceIdentifier'] . '</uniqueCreatorIdentification>
                                              <contentOwner>
                                                <gln>' . $poXml['sh:StandardBusinessDocumentHeader']['sh:Sender']['sh:Identifier']['@'] . '</gln>
                                              </contentOwner>
                                            </applicationReceiptAcknowledgementIdentification>
                                            <sBDHApplicationReceiptAcknowledgement>
                                              <statusType>RECEIVED</statusType>
                                              <commandApplicationReceiptAcknowledgement>
                                                <originalCommandType>ADD</originalCommandType>
                                                <statusType>' . $applicationReceiptStatus . '</statusType>
                                                <originalEntityIdentification>
                                                  <uniqueCreatorIdentification>CMD-' . $poXml['sh:StandardBusinessDocumentHeader']['sh:DocumentIdentification']['sh:InstanceIdentifier'] . '</uniqueCreatorIdentification>
                                                  <contentOwner>
                                                    <gln>' . $poXml['sh:StandardBusinessDocumentHeader']['sh:Sender']['sh:Identifier']['@'] . '</gln>
                                                  </contentOwner>
                                                </originalEntityIdentification>
                                                <documentApplicationReceiptAcknowledgement>
                                                  <originalDocumentCreationDateTime>' . date('Y-m-d\TH:i:s', strtotime($poXml['sh:StandardBusinessDocumentHeader']['sh:DocumentIdentification']['sh:CreationDateAndTime'])) . '</originalDocumentCreationDateTime>
                                                  <originalDocumentType>' . $documentPOAcknowledgedType . '</originalDocumentType>
                                                  <statusType>RECEIVED</statusType>
                                                  <originalEntityIdentification>
                                                    <uniqueCreatorIdentification>' . $poXml['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['order:multiShipmentOrder']['orderIdentification']['uniqueCreatorIdentification'] . '</uniqueCreatorIdentification>
                                                    <contentOwner>
                                                      <gln>' . $poXml['sh:StandardBusinessDocumentHeader']['sh:Sender']['sh:Identifier']['@'] . '</gln>
                                                    </contentOwner>
                                                  </originalEntityIdentification>
                                                </documentApplicationReceiptAcknowledgement>
                                              </commandApplicationReceiptAcknowledgement>
                                            </sBDHApplicationReceiptAcknowledgement>
                                          </eanucc:applicationReceiptAcknowledgement>
                                        </documentCommandOperand>
                                      </eanucc:documentCommand>
                                    </command>
                                  </eanucc:transaction>
                                </eanucc:message>
                              </sh:StandardBusinessDocument>';

        return $applicationReceiptXml;
    }

    /**
     * Update purchase orders
     * @param type $updatedArr
     * @param type $conditions
     * @return string
     */
    public function updateAllFieldsByFields($updatedArr, $conditions) {
        try {
            return $this->updateAll(
                            $updatedArr, $conditions
            );
        } catch (Exception $ex) {
            return '';
        }
    }

    /**
     * Get Availabe Purchase Orders Count
     * @return type
     */
    public function getPurchaseOrders($poIdArr) {

        $fulfilmentAdviceReceivedDates = [];
        try {
            $purchaseOrderData = $this->find('all', [
                        'fields' => array('fulfilment_advice_received_date'),
                        'conditions' => array(
                            'id IN' => $poIdArr,
                            'archived' => Configure::read('po_archived.no')
                        ),
                        'order' => ['fulfilment_advice_received_date' => 'DESC'],
                    ])->toArray();
            if (isset($purchaseOrderData) && !empty($purchaseOrderData)) {
                foreach ($purchaseOrderData as $purchaseOrder) {
                    $time = new Time($purchaseOrder->fulfilment_advice_received_date);
                    $date = $time->format('Y-m-d');
                    if (strtotime($date) > 0) {
                        $fulfilmentAdviceReceivedDates[] = date('Y-m-d', strtotime($purchaseOrder->fulfilment_advice_received_date));
                    } else {
                        $fulfilmentAdviceReceivedDates = '';
                        break;
                    }
                }
            }
        } catch (Exception $ex) {
            return [];
        }
        return $fulfilmentAdviceReceivedDates;
    }

}
