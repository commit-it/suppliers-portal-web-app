<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Network\Session;
use ArrayObject;
use Cake\Core\Configure;

/**
 * BookingSheets Model
 */
class BookingSheetsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->table('booking_sheets');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Couriers', [
            'foreignKey' => 'courier_id'
        ]);
        $this->hasMany('BookingSheetItems', [
            'foreignKey' => 'booking_sheet_id'
        ]);

        $this->session = new Session();

        $this->addBehavior('Common');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
                ->allowEmpty('id', 'create')
                ->add('trays', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
                ->allowEmpty('trays')
                ->add('trolleys', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
                ->allowEmpty('trolleys')
                ->add('pickup_date', 'valid', ['rule' => 'datetime', 'message' => 'This value should be in date format'])
                ->allowEmpty('pickup_date')
                ->add('drop_date', 'valid', ['rule' => 'datetime', 'message' => 'This value should be in date format'])
                ->allowEmpty('drop_date')
                ->add('arrived_time_Vic', 'valid', ['rule' => 'datetime', 'message' => 'This value should be in date format'])
                ->allowEmpty('arrived_time_Vic')
                ->add('arrived_time_NSW', 'valid', ['rule' => 'datetime', 'message' => 'This value should be in date format'])
                ->allowEmpty('arrived_time_NSW');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }

    /**
     * Return query for auth user
     * @param \Cake\Event\Event $event
     * @param \Cake\ORM\Query $query
     * @param ArrayObject $options
     * @return boolean|\Cake\ORM\Query
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options) {
        /**
         * This condition is checked when we don't need to check current user 
         */
        if (isset($options['remove_user_condition']) && $options['remove_user_condition'] > 0) {
            return TRUE;
        }
        $query->where([$this->alias() . '.user_id' => $this->session->read('Auth.User.id')]);
        return $query;
    }
    
    /**
     * manageBookingSheetPdf method - manage pdf content
     * @param type $bookingSheetArr
     * @return string
     */
    public function manageBookingSheetPdf($bookingSheetArr) {
        $html = '';
        $html = $html . '<!DOCTYPE html>
                        <html>
                            <head>
                                <title>Booking Sheet</title>
                                <link rel="stylesheet" type="text/css" href="' . WWW_ROOT . 'css' . DS . 'dompdf_style.css" media="screen" />
                            </head>
                        <body>';
        foreach ($bookingSheetArr as $bookingSheet) {
            
            $trollysDetails = array();
            foreach ($bookingSheet['booking_sheet_items'] as $bookingSheetItem) {
                
                if (isset($trollysDetails[$bookingSheetItem['purchase_order']['bill_to_gln']]) && isset($trollysDetails[$bookingSheetItem['purchase_order']['bill_to_gln']]['number'])) {
                    $trollysDetails[$bookingSheetItem['purchase_order']['bill_to_gln']]['number'] = $trollysDetails[$bookingSheetItem['purchase_order']['bill_to_gln']]['number'] + 1;
                    $trollysDetails[$bookingSheetItem['purchase_order']['bill_to_gln']]['totalTrollys'] = $trollysDetails[$bookingSheetItem['purchase_order']['bill_to_gln']]['totalTrollys'] + $bookingSheetItem['trolleys'];
                } else {
                    $trollysDetails[$bookingSheetItem['purchase_order']['bill_to_gln']]['number'] = 1;
                    $trollysDetails[$bookingSheetItem['purchase_order']['bill_to_gln']]['totalTrollys'] = $bookingSheetItem['trolleys'];
                }
            }

            $sellerBusinessLogo = (isset($bookingSheet['user']['seller_business_logo']) && $bookingSheet['user']['seller_business_logo'] != '') ? Configure::read('host_name') . DS . 'webroot' . DS . 'upload' . DS . 'seller_business_logo' . DS . $bookingSheet['user']['seller_business_logo'] : Configure::read('host_name') . DS . 'webroot' . DS . 'img' . DS . 'no_image.png';
            $courierAddress = isset($bookingSheet['courier']['address']) ? $bookingSheet['courier']['address'] : '';
            $courierTelephone = isset($bookingSheet['courier']['telephone']) ? $bookingSheet['courier']['telephone'] : '';
            $courierEmail = isset($bookingSheet['courier']['email']) ? $bookingSheet['courier']['email'] : '';

            $bookingSheet['booking_sheet_items'] = $this->multisortArray($bookingSheet['booking_sheet_items'], array('purchase_order'), 'bill_to_name', 'asc');

            $pickup = 'No';
            $drop = 'No';
            if (strtotime($bookingSheet['pickup_date']) <= strtotime(date('d-m-Y'))) {
                $pickup = 'Yes';
            }
            if (strtotime($bookingSheet['drop_date']) <= strtotime(date('d-m-Y'))) {
                $drop = 'Yes';
            }

            $trollysOrStillages = 'Trolleys';
            $firstStoreAddress = isset($bookingSheet['booking_sheet_items'][0]['purchase_order']['bill_to_address']) ? $bookingSheet['booking_sheet_items'][0]['purchase_order']['bill_to_address'] : '';
            $firstStoreGlnNumber = isset($bookingSheet['booking_sheet_items'][0]['purchase_order']['bill_to_gln']) ? $bookingSheet['booking_sheet_items'][0]['purchase_order']['bill_to_gln'] : '';
            $firstStoreModifiedAddress = $this->separateUserAddressForGenerateInvoice($firstStoreAddress, 'BunningStores', 'findByGln', $firstStoreGlnNumber);
            if (in_array(strtoupper($firstStoreModifiedAddress['state']), array('SA', 'VIC'))) {
                $trollysOrStillages = 'Stillages';
            }
            
            $supplierCode = '';
            switch ($bookingSheet['user']['state']) {
                case 'NT':
                    $supplierCode = 'JON073';
                    break;
                case 'SA':
                    $supplierCode = 'JON074';
                    break;
                case 'TAS':
                    $supplierCode = 'JON075';
                    break;
                case 'VIC':
                    $supplierCode = 'JON076';
                    break;
                case 'QLD':
                    $supplierCode = 'JON077';
                    break;
                default:
                    break;
            }

            $html = $html . '<table border="1" class="booking-sheet">'
                    . '<tr>'
                    . '<td colspan="5">'
                    . '<p><center><img src="' . $sellerBusinessLogo . '" alt="Courier Logo" width="' . Configure::read('courier.logo_width') . 'px" height="' . Configure::read('courier.logo_height') . 'px" style="margin:5px 0;" /></center></p>'
                    . '</td>'
                    . '<td colspan="6">'
                    . '<p class="align-right">'
                    . '<b>Tel:</b> ' . $courierTelephone . '<br />'
                    . '<b>Email:</b> ' . $courierEmail . '<br />'
                    . $courierAddress
                    . '</p>'
                    . '</td>'
                    . '<tr>'
                    . '<td colspan="11"><center><h2>Supplier Manifest / Booking Sheet</h2></center></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td colspan="11" class="gray-bg">'
                    . '<center><b>Please do not modify the template format, insert or delete rows for store details only. Each truck requires a separate manifest & the stock delievered needs to match the manifest</b></center>'
                    . '</td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td colspan="2"><h3>Supplier:</h3></td>'
                    . '<td colspan="5"><center>' . $bookingSheet['user']['seller_business_name'] . '</center></td>'
                    . '<td colspan="2"><h3>Supplier Code:</h3></td>'
                    . '<td colspan="2"><center>' . $supplierCode . '</center></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td colspan="2"><b>Contact Name:</b></td>'
                    . '<td colspan="5"><center>' . $bookingSheet['user']['full_name'] . '</center></td>'
                    . '<td colspan="2"><b>Batch #:(Optional)</b></td>'
                    . '<td colspan="2"></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td colspan="2"><b>Email Address:</b></td>'
                    . '<td colspan="5"><center>' . $bookingSheet['user']['email'] . '</center></td>'
                    . '<td colspan="2"><b>Contact Ph #:</b></td>'
                    . '<td colspan="2"><center>' . $bookingSheet['user']['phone_number'] . '</center></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td colspan="7" class="gray-bg">'
                    . '<center><b>Please ensure all required Plant Health Assurance Certificates (PHAC) are supplied with full botanical plant names included on IDD/ Invoice</b></center>'
                    . '</td>'
                    . '<td colspan="2"><b>Date:</b></td>'
                    . '<td colspan="2"><center>' . date(Configure::read('date-format.d-M-Y'), strtotime($bookingSheet['pickup_date'])) . '</center></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td><b>Store #</b></td>'
                    . '<td><center><b>Store Name</b></center></td>'
                    . '<td colspan="2"><center><b>Bunnings PO <br /> Number</b></center></td>'
                    . '<td colspan="3"><center><b>Supplier Invoice <br /> Number</b></center></td>'
                    . '<td><center><b>Pots / Items</b></center></td>'
                    . '<td><center><b>Trays</b></center></td>'
                    . '<td><center><b>' . $trollysOrStillages . '</b></center></td>'
                    . '<td><center><b>Seedling Racks</b></center></td>'
                    . '</tr>';
            $totalPots = 0;
            $totalStillages = 0;
            foreach ($bookingSheet['booking_sheet_items'] as $bookingSheetItems) {

                $totalItems = 0;
                foreach ($bookingSheetItems['invoice']['invoice_items'] as $invoiceItems) {
                    $totalItems = ($totalItems + $invoiceItems['requested_quantity']);
                }
                $totalPots = ($totalPots + $totalItems);
                
                $invoiceNumber = isset($bookingSheetItems['invoice']['invoice_number']) ? $bookingSheetItems['invoice']['invoice_number'] : '';
                $html = $html . '<tr>'
                        . '<td>' . $bookingSheetItems['purchase_order']['bill_to_additional_id'] . '</td>'
                        . '<td>' . $bookingSheetItems['purchase_order']['bill_to_name'] . '</td>'
                        . '<td colspan="2">' . $bookingSheetItems['purchase_order']['purchase_order_number'] . '</td>'
                        . '<td colspan="3">' . $invoiceNumber . '</td>'
                        . '<td>' . $totalItems . '</td>';
                        if (!isset($trollysDetails[$bookingSheetItems['purchase_order']['bill_to_gln']]['renderTrollys'])) {
                            $html = $html . '<td rowspan="' . $trollysDetails[$bookingSheetItems['purchase_order']['bill_to_gln']]['number'] . '"></td>';
                            $html = $html . '<td rowspan="' . $trollysDetails[$bookingSheetItems['purchase_order']['bill_to_gln']]['number'] . '">' . $trollysDetails[$bookingSheetItems['purchase_order']['bill_to_gln']]['totalTrollys'] . '</td>';
                            $html = $html . '<td rowspan="' . $trollysDetails[$bookingSheetItems['purchase_order']['bill_to_gln']]['number'] . '"></td>';
                            $trollysDetails[$bookingSheetItems['purchase_order']['bill_to_gln']]['renderTrollys'] = 1;
                            $totalStillages = ($totalStillages + $trollysDetails[$bookingSheetItems['purchase_order']['bill_to_gln']]['totalTrollys']);
                            
                        }
                $html = $html . '</tr>';
            }
            $html = $html . '<tr>'
                    . '<td colspan="4"></td>'
                    . '<td colspan="3"><center><b>Total</b></center></td>'
                    . '<td>' . $totalPots . '</td>'
                    . '<td></td>'
                    . '<td>' . $totalStillages . '</td>'
                    . '<td></td>'
                    . '</tr>'
                    . '</table>'
                    . '<span style="page-break-after: always"></span>'
                    . '<table border="1" class="booking-sheet">'
                    . '<tr>'
                    . '<td class="width-50"><b>Total Stacks</b></td>'
                    . '<td class="width-50"></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td class="width-50"><b>Total ' . $trollysOrStillages . '</b></td>'
                    . '<td class="width-50">' . $totalStillages . '</td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td class="width-50"><b>Total Seedling Racks</b></td>'
                    . '<td class="width-50"></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td class="width-50"><b>Total Pallets</b></td>'
                    . '<td class="width-50"></td>'
                    . '</tr>'
                    . '</table>'
                    . '<table class="signature-table">'
                    . '<tr>'
                    . '<td colspan="5"><h3>Stock received by:</h3></td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td class="width-15"><b>Name:</b></td>'
                    . '<td class="width-30 border-bottom-black"></td>'
                    . '<td class="width-10"><br /></td>'
                    . '<td class="width-15"><b>Signature:</b></td>'
                    . '<td class="width-30 border-bottom-black"></td>'
                    . '</tr>'
                    . '</table>'
                    . '<span style="page-break-after: always"></span>';
        }
        $html = $html . '</body>'
                . '</html>';
        return $html;
    }

    /**
     * Cancel Booking Sheets
     * @param type $bookingSheetIds
     * @return boolean
     */
    public function cancelBookingSheets($bookingSheetIds) {
        if (!empty($bookingSheetIds)) {
            try {
                return $this->deleteAll(['id IN' => $bookingSheetIds]);
            } catch (Exception $ex) {

                return FALSE;
            }
        }
        return FALSE;
    }

}
