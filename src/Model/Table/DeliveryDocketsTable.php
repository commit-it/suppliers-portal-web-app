<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Network\Session;
use ArrayObject;
use Cake\Core\Configure;

/**
 * DeliveryDockets Model
 */
class DeliveryDocketsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->table('delivery_dockets');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Invoices', [
            'foreignKey' => 'invoice_id'
        ]);
        $this->belongsTo('PurchaseOrders', [
            'foreignKey' => 'purchase_order_id'
        ]);
        $this->session = new Session();
        
        $this->addBehavior('Common');
        $this->addBehavior('Searchable');
        
        $this->Invoices = TableRegistry::get('Invoices');
    }

    // filters
    public $filterArgs = array(
        'delivery_date' => array(
            'type' => 'value'
        )
    );
    
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
                ->allowEmpty('id', 'create')
                ->add('delivery_date', 'valid', ['rule' => 'date', 'message' => 'This value should be in date format'])
                ->requirePresence('delivery_date', 'create')
                ->notEmpty('delivery_date');

        return $validator;
    }

    /**
     * Generate delivery docket array for save
     * 
     * @param $data & $userId
     * @return delivery docket array for save
     */
    public function generateDeliveryDocketArr($data, $userId) {

        $deliveryDocketFinalArr = array();
        $deliveryDocketArr = array();
        $invoiceIds = array();
        $generateDeliveryDocketCheckboxArr = isset($data['generate_delivery_docket_checkbox']) ? $data['generate_delivery_docket_checkbox'] : array();
        $generateDeliveryDocketCheckboxModifiedArr = array();
        if (is_array($generateDeliveryDocketCheckboxArr)) {
            $generateDeliveryDocketCheckboxModifiedArr = $generateDeliveryDocketCheckboxArr;
        } else {
            if (strpos($generateDeliveryDocketCheckboxArr, ',') !== false) {
                $invoiceIdArr = explode(',', $generateDeliveryDocketCheckboxArr);
                foreach ($invoiceIdArr as $invoiceId) {
                    $purchaseOrderId = $this->checkIfFieldIsExist($this->Invoices->findById($invoiceId)->first(), 'purchase_order_id');
                    $generateDeliveryDocketCheckboxModifiedArr[] = $invoiceId . ',' . $purchaseOrderId;
                }
            } else {
                $invoiceId = $generateDeliveryDocketCheckboxArr;
                $purchaseOrderId = $this->checkIfFieldIsExist($this->Invoices->findById($invoiceId)->first(), 'purchase_order_id');
                $generateDeliveryDocketCheckboxModifiedArr[] = $invoiceId . ',' . $purchaseOrderId;
            }
        }

        foreach ($generateDeliveryDocketCheckboxModifiedArr as $generateDeliveryDocketCheckbox) {

            $deliveryDocketData = array();
            $deliveryDocketData['user_id'] = $userId;
            $invoiceAndPurchaseOrderIds = explode(',', $generateDeliveryDocketCheckbox);
            $deliveryDocketData['invoice_id'] = isset($invoiceAndPurchaseOrderIds[0]) ? $invoiceAndPurchaseOrderIds[0] : '';
            $deliveryDocketData['purchase_order_id'] = isset($invoiceAndPurchaseOrderIds[1]) ? $invoiceAndPurchaseOrderIds[1] : '';
            $deliveryDocketData['delivery_date'] = isset($data['delivery_date']) ? $data['delivery_date'] : '';
            $deliveryDocketId = $this->checkIfFieldIsExist($this->findByInvoiceIdAndPurchaseOrderId($deliveryDocketData['invoice_id'],$deliveryDocketData['purchase_order_id'])->first(), 'id');
            if ($deliveryDocketId) {
                $deliveryDocketData['id'] = $deliveryDocketId;
            }
            $deliveryDocketArr[] = $deliveryDocketData;

            $invoiceIds[] = isset($invoiceAndPurchaseOrderIds[0]) ? $invoiceAndPurchaseOrderIds[0] : '';
        }

        $deliveryDocketFinalArr['DeliveryDocket'] = $deliveryDocketArr;
        $deliveryDocketFinalArr['InvoiceIds'] = $invoiceIds;
        return $deliveryDocketFinalArr;
    }

    /**
     * manageDeliveryDocketPdf method - manage pdf content
     * @param type $deliveryDocketsArr
     * @return string
     */
    public function manageDeliveryDocketPdf($deliveryDocketsArr) {

        $html = '';
        $html = $html . '<!DOCTYPE html>
                        <html>
                            <head>
                                <title>Delivery Dockets</title>
                                <link rel="stylesheet" type="text/css" href="' . WWW_ROOT . 'css' . DS . 'dompdf_style.css" media="screen" />
                            </head>
                        <body>';
        $html = $html . '<div class="delivery-docket">';
        foreach($deliveryDocketsArr as $deliveryDocket) {
            $delivery_date = isset($deliveryDocket['delivery_date']) && strtotime($deliveryDocket['delivery_date']) > 0 ? str_replace('-', '/', date('d-m-y', strtotime($deliveryDocket['delivery_date']))) : '';
            $deliveryDocketNumber = ($deliveryDocket['invoice']['delivery_docket_no']) ? $deliveryDocket['invoice']['delivery_docket_no'] : $deliveryDocket['invoice']['invoice_number'];
            $html = $html 
//                    . '<table><tr>'
//                    . '<td class="width-60 font-bold font-size-10">'
//                    .  $deliveryDocket['user']['seller_business_name'] . '<br />'
//                    .  $deliveryDocket['user']['seller_business_address'] . '<br />'
//                    . 'ABN : ' . $deliveryDocket['user']['company_registration_number']                      
//                    . '</td>'                  
//                    . '<td class="width-20 font-bold font-size-10" style="vertical-align:text-top"><b>Report Date:</b> ' . date(Configure::read('date-format.d/m/Y-H-i-s-A')) . '</td>'
//                    . '</tr></table>'
                    . '<center><h2>DELIVERY DOCKET</h2></center><br />'
//                    . '<table>'
//                    . '<tr>'
//                    . '<td class="width-15 font-bold">Order Number</td>'
//                    . '<td class="width-5 font-bold"> :</td>'
//                    . '<td class="width-25">' . $deliveryDocket['purchase_order']['purchase_order_number'] . '</td>'
//                    . '</tr>'
//                    . '<tr>'
//                    . '<td class="width-15 font-bold">Invoice Date</td>'
//                    . '<td class="width-5 font-bold"> :</td>'
//                    . '<td class="width-25">' . date(Configure::read('date-format.d-M-Y'), strtotime($deliveryDocket['invoice']['invoice_date'])) . '</td>'
//                    . '</tr>'
//                    . '<tr>'
//                    . '<td class="width-18 font-bold">Delivery Docket</td>'
//                    . '<td class="width-2 font-bold">:</td>'
//                    . '<td class="width-80">' . $deliveryDocketNumber . '</td>'
//                    . '</tr>'
//                    . '</table><br />'
//                    . '<h3>&nbsp;Prepared By :</h3>'
                      .'<table style="width: 100%;" class="seller-table">
                            <tbody>
                                <tr>
                                  <td style="width: 100%;font-size:25px; font-weight:bold !important"  colspan="2" class="seller_nm">
                                        ' . $deliveryDocket['purchase_order']['seller_name'] . '
                                        
                                  </td>
                                  
                                </tr>
                                <tr>
                                  <td style="width: 40%;">
                                     ' . $deliveryDocket['purchase_order']['ship_from_address'] . ' <br/>
                                     Phone: ' . $deliveryDocket['purchase_order']['ship_from_telephone_number'] . ' <br/>
                                     Fax: ' . $deliveryDocket['purchase_order']['ship_from_fax_number'] . ' <br/><br/><br/>
                                  </td>
                                  <td style="width: 55%;">
                                        <table class="inner-cls">
                                          <tbody>
                                            <tr><th> DELIVERY DOCKET: </th><th>' . $deliveryDocketNumber . '</th></tr>
                                            <tr><td  class="align-center">Purchase Order: </td><td class="align-center">' . $deliveryDocket['purchase_order']['purchase_order_number'] . '</td></tr>
                                            <tr><td class="align-center">Delivery Date</td><td class="align-center">'.$delivery_date . '</td></tr>
                                          </tbody>
                                        </table>
                                     </td>
                                </tr>
                             </tbody>
                         </table>'
//                    . '<table>'
//                    . '<tr>'
//                    . '<td class="width-40"><b>Supplier :</b></td>'
//                    . '<td class="width-20"><br /></td>'
//                    . '<td class="width-40"><b>Ship To :</b></td>'
//                    . '</tr>'
//                    . '<tr><td colspan="3"><br /></td></tr>'
//                    . '<tr><td>' . $deliveryDocket['purchase_order']['seller_name'] . '</td><td><br /></td><td>' . $deliveryDocket['purchase_order']['bill_to_name'] . '</td></tr>'
//                    . '<tr><td colspan="3"><br /></td></tr>'
//                    . '<tr><td>' . $deliveryDocket['purchase_order']['ship_from_address'] . '</td><td><br /></td><td>' . $deliveryDocket['purchase_order']['bill_to_address'] . '</td></tr>'
//                    . '<tr><td>Phone: ' . $deliveryDocket['purchase_order']['ship_from_telephone_number'] . '</td><td><br /></td><td style="width:35%">Phone: ' . $deliveryDocket['purchase_order']['bill_to_telephone_number'] . '</td></tr>'
//                    . '<tr><td>Fax: ' . $deliveryDocket['purchase_order']['ship_from_fax_number'] . '</td><td><br /></td><td>Fax: ' . $deliveryDocket['purchase_order']['bill_to_fax_number'] . '</td></tr>'
//                    . '</table>'
                    . '<table>'
                    . '<tr>'
                    . '<td class="width-40"><b>Ship To</b></td>'
                    . '</tr>'
                    . '<tr><td>' . $deliveryDocket['purchase_order']['bill_to_name'] . '</td></tr>'
                    . '<tr><td>' . $deliveryDocket['purchase_order']['bill_to_address'] . '</td></tr>'
                    . '<tr><td style="width:35%">Phone: ' . $deliveryDocket['purchase_order']['bill_to_telephone_number'] . '</td></tr>'
                    . '<tr><td>Fax: ' . $deliveryDocket['purchase_order']['bill_to_fax_number'] . '</td></tr>'
                    . '</table>'
                    . '<br /><br />';
//                    . '<div><b>Note to Supplier :</b> ' . $deliveryDocket['purchase_order']['comments'] . '</div><br />';
            $html = $html . '<table border="1" style="border-spacing: 0;border-collapse: collapse;">'
                    . '<tr>'
                    . '<th class="width-5">#</th>'
                    . '<th class="width-10">Item</th>'
//                    . '<th class="width-15">Barcode</th>'
                    . '<th class="width-25">Description</th>'
                    . '<th class="width-5">Qty</th>'
                    . '<th class="width-10">UOM</th>'
//                    . '<th class="width-15">Supplier Item No.</th>'
//                    . '<th class="width-15">Delivery Date</th>'
                    . '</tr>';
                    $i = 1;    
                    foreach ($deliveryDocket['invoice']['invoice_items'] as $invoiceItems) {
                        $html = $html . '<tr>'
                                . '<td class="align-center">' . $i++ . '</td>'
                                . '<td class="align-center">' . $invoiceItems['bunnings_product_id'] . '</td>'
//                                . '<td>' . $invoiceItems['gtin'] . '</td>'
                                . '<td class="align-center">' . $invoiceItems['description'] . '</td>'
                                . '<td class="align-center">' . $invoiceItems['requested_quantity'] . '</td>'
                                . '<td class="align-center">' . $invoiceItems['unit_of_measure'] . '</td>'
//                                . '<td>' . $invoiceItems['supplier_product_id'] . '</td>'
//                                . '<td>' . date(Configure::read('date-format.d-M-y'), strtotime($deliveryDocket['delivery_date'])) . '</td>'
                                . '</tr>';
                    }
            $html = $html . '</table>'
                    . '<span style="page-break-after: always"></span>';
                
        }
        $html = $html . '</div>';
        $html = $html . '</body>';
        $html = $html . '</html>';
        return $html;
        
    }
    public function manageDeliveryDocketPdf_bk($deliveryDocketsArr) {

        $html = '';
        $html = $html . '<!DOCTYPE html>
                        <html>
                            <head>
                                <title>Delivery Dockets</title>
                                <link rel="stylesheet" type="text/css" href="' . WWW_ROOT . 'css' . DS . 'dompdf_style.css" media="screen" />
                            </head>
                        <body>';
        $html = $html . '<div class="delivery-docket">';
        foreach($deliveryDocketsArr as $deliveryDocket) {
            $deliveryDocketNumber = ($deliveryDocket['invoice']['delivery_docket_no']) ? $deliveryDocket['invoice']['delivery_docket_no'] : $deliveryDocket['invoice']['invoice_number'];
            $html = $html . '<table><tr>'
                    . '<td class="width-60 font-bold font-size-10">'
                    .  $deliveryDocket['user']['seller_business_name'] . '<br />'
                    .  $deliveryDocket['user']['seller_business_address'] . '<br />'
                    . 'ABN : ' . $deliveryDocket['user']['company_registration_number']                      
                    . '</td>'                  
                    . '<td class="width-20 font-bold font-size-10" style="vertical-align:text-top"><b>Report Date:</b> ' . date(Configure::read('date-format.d/m/Y-H-i-s-A')) . '</td>'
                    . '</tr></table>'
                    . '<center><h2>Delivery Docket</h2></center><br />'
                    . '<table>'
                    . '<tr>'
                    . '<td class="width-15 font-bold">Order Number</td>'
                    . '<td class="width-5 font-bold"> :</td>'
                    . '<td class="width-25">' . $deliveryDocket['purchase_order']['purchase_order_number'] . '</td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td class="width-15 font-bold">Invoice Date</td>'
                    . '<td class="width-5 font-bold"> :</td>'
                    . '<td class="width-25">' . date(Configure::read('date-format.d-M-Y'), strtotime($deliveryDocket['invoice']['invoice_date'])) . '</td>'
                    . '</tr>'
                    . '<tr>'
                    . '<td class="width-18 font-bold">Delivery Docket</td>'
                    . '<td class="width-2 font-bold">:</td>'
                    . '<td class="width-80">' . $deliveryDocketNumber . '</td>'
                    . '</tr>'
                    . '</table><br />'
                    . '<h3>&nbsp;Prepared By :</h3>'
                    . '<table>'
                    . '<tr>'
                    . '<td class="width-40"><b>Supplier :</b></td>'
                    . '<td class="width-20"><br /></td>'
                    . '<td class="width-40"><b>Ship To :</b></td>'
                    . '</tr>'
                    . '<tr><td colspan="3"><br /></td></tr>'
                    . '<tr><td>' . $deliveryDocket['purchase_order']['seller_name'] . '</td><td><br /></td><td>' . $deliveryDocket['purchase_order']['bill_to_name'] . '</td></tr>'
                    . '<tr><td colspan="3"><br /></td></tr>'
                    . '<tr><td>' . $deliveryDocket['purchase_order']['ship_from_address'] . '</td><td><br /></td><td>' . $deliveryDocket['purchase_order']['bill_to_address'] . '</td></tr>'
                    . '<tr><td>Phone: ' . $deliveryDocket['purchase_order']['ship_from_telephone_number'] . '</td><td><br /></td><td style="width:35%">Phone: ' . $deliveryDocket['purchase_order']['bill_to_telephone_number'] . '</td></tr>'
                    . '<tr><td>Fax: ' . $deliveryDocket['purchase_order']['ship_from_fax_number'] . '</td><td><br /></td><td>Fax: ' . $deliveryDocket['purchase_order']['bill_to_fax_number'] . '</td></tr>'
                    . '</table>'
                    . '<br /><br />'
                    . '<div><b>Note to Supplier :</b> ' . $deliveryDocket['purchase_order']['comments'] . '</div><br />';
            $html = $html . '<table border="1" style="border-spacing: 0;border-collapse: collapse;">'
                    . '<tr>'
                    . '<th class="width-5">#</th>'
                    . '<th class="width-10">Item</th>'
                    . '<th class="width-15">Barcode</th>'
                    . '<th class="width-25">Description</th>'
                    . '<th class="width-5">Qty</th>'
                    . '<th class="width-10">UOM</th>'
                    . '<th class="width-15">Supplier Item No.</th>'
                    . '<th class="width-15">Delivery Date</th>'
                    . '</tr>';
                    $i = 1;    
                    foreach ($deliveryDocket['invoice']['invoice_items'] as $invoiceItems) {
                        $html = $html . '<tr>'
                                . '<td class="align-center">' . $i++ . '</td>'
                                . '<td>' . $invoiceItems['bunnings_product_id'] . '</td>'
                                . '<td>' . $invoiceItems['gtin'] . '</td>'
                                . '<td>' . $invoiceItems['description'] . '</td>'
                                . '<td>' . $invoiceItems['requested_quantity'] . '</td>'
                                . '<td>' . $invoiceItems['unit_of_measure'] . '</td>'
                                . '<td>' . $invoiceItems['supplier_product_id'] . '</td>'
                                . '<td>' . date(Configure::read('date-format.d-M-y'), strtotime($deliveryDocket['delivery_date'])) . '</td>'
                                . '</tr>';
                    }
            $html = $html . '</table>'
                    . '<span style="page-break-after: always"></span>';
                
        }
        $html = $html . '</div>';
        $html = $html . '</body>';
        $html = $html . '</html>';
        return $html;
        
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['invoice_id'], 'Invoices'));
        $rules->add($rules->existsIn(['purchase_order_id'], 'PurchaseOrders'));
        return $rules;
    }

    /**
     * Return query for auth user
     * @param \Cake\Event\Event $event
     * @param \Cake\ORM\Query $query
     * @param ArrayObject $options
     * @return boolean|\Cake\ORM\Query
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options) {
        /**
         * This condition is checked when we don't need to check current user 
         */
        if (isset($options['remove_user_condition']) && $options['remove_user_condition'] > 0) {
            return TRUE;
        }
        $query->where([$this->alias() . '.user_id' => $this->session->read('Auth.User.id')]);
        return $query;
    }

    /**
     * updateFieldsByField - Method
     * @param type $conditionArr
     * @param type $updateFieldsArr
     * @return type
     */
    public function updateFieldsByFields($updateFieldsArr,$conditionArr) {
        try {
            $result = $this->updateAll($updateFieldsArr,$conditionArr);
        } catch (Exception $ex) {
//            return '';
            
        }
        echo '$result <pre>';print_r($result);exit;
    }

}
