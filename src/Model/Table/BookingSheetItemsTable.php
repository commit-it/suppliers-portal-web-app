<?php

namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BookingSheetItems Model
 */
class BookingSheetItemsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->table('booking_sheet_items');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('BookingSheets', [
            'foreignKey' => 'booking_sheet_id'
        ]);
        $this->belongsTo('Invoices', [
            'foreignKey' => 'invoice_id'
        ]);
        $this->belongsTo('PurchaseOrders', [
            'foreignKey' => 'purchase_order_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric'])
                ->allowEmpty('id', 'create')
                ->allowEmpty('office_use_only');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['booking_sheet_id'], 'BookingSheets'));
        $rules->add($rules->existsIn(['invoice_id'], 'Invoices'));
        $rules->add($rules->existsIn(['purchase_order_id'], 'PurchaseOrders'));
        return $rules;
    }

    /**
     * Cancel Booking Sheet Items
     * @param type $bookingSheetIds
     * @return boolean
     */
    public function cancelBookingSheetItems($bookingSheetIds) {
        if (!empty($bookingSheetIds)) {
            try {
                return $this->deleteAll(['booking_sheet_id IN' => $bookingSheetIds]);
            } catch (Exception $ex) {

                return FALSE;
            }
        }
        return FALSE;
    }

    /**
     * Get Invoice Ids
     * @param type $bookingSheetIds
     * @return type
     */
    public function getInvoiceIds($bookingSheetIds) {
        $invoiceIds =[];
        try {
          $invoiceData = $this->find('all', [
                    'fields' => array('invoice_id'),
                    'conditions' => array('booking_sheet_id IN' => $bookingSheetIds)
                ])->toArray();
          if(isset($invoiceData) && !empty($invoiceData)){
              foreach ($invoiceData as $invoice){
                  $invoiceIds[] = $invoice->invoice_id;
              }
          }
        } catch (Exception $ex) {
            return [];
        }
        return $invoiceIds;
    }
}
