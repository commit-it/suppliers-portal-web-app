<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Network\Session;
use ArrayObject;

/**
 * ScanBooks Model
 */
class ScanBooksTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->table('scan_books');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);

        $this->belongsTo('BunningStores', [
            'foreignKey' => 'bunning_store_id'
        ]);

        $this->addBehavior('Common');
        $this->session = new Session();
    }   

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
                ->allowEmpty('id', 'create')
                ->requirePresence('scan_book_file', 'create')
                ->notEmpty('scan_book_file');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['bunning_store_id'], 'BunningStores'));
        return $rules;
    }
    
    /**
     * Return query for auth user
     * @param \Cake\Event\Event $event
     * @param \Cake\ORM\Query $query
     * @param ArrayObject $options
     * @return boolean|\Cake\ORM\Query
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options) {
        /**
         * This condition is checked when we don't need to check current user 
         */
        if (isset($options['remove_user_condition']) && $options['remove_user_condition'] > 0) {
            return TRUE;
        }
        $query->where([$this->alias() . '.user_id' => $this->session->read('Auth.User.id')]);
        return $query;
    }
    
    /**
     * Get Total Scan Book Count
     * @return type
     */
    public function getTotalScanBookCount() {
       return $this->find('all')->count();
    }
}
