<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use \Cake\Core\Configure;
use Cake\Event\Event;
use Cake\Network\Session;
use ArrayObject;

/**
 * MasterItems Model
 */
class MasterItemsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->table('master_items');
        $this->displayField('name');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        
        $this->addBehavior('Common');
        $this->addBehavior('Searchable');
                
        $this->session = new Session();
    }
    
    // filters
    public $filterArgs = array(
        'scan_barcode' => array(
            'type' => 'value'
        ),
        'bar_code' => array(
            'type' => 'value'
        ),
        'pot_size' => array(
            'type' => 'like'
        ),
        'name' => array(
            'type' => 'like'
        ),
        'retail_price' => array(
            'type' => 'value'
        )
    );

    
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
                ->allowEmpty('id', 'create')
                ->requirePresence('scan_barcode', 'create')
                ->notEmpty('scan_barcode')
                ->requirePresence('bar_code', 'create')
                ->notEmpty('bar_code')
                ->requirePresence('pot_size', 'create')
                ->notEmpty('pot_size')
                ->requirePresence('name', 'create')
                ->notEmpty('name')
                ->add('retail_price', 'valid', ['rule' => 'decimal', 'message' => 'This value should be Decimal'])
                ->requirePresence('retail_price', 'create')
                ->notEmpty('retail_price')
                ->notEmpty('genus')
                ->notEmpty('species');

        return $validator;
    }
    
    /**
     * Return query for auth user
     * @param \Cake\Event\Event $event
     * @param \Cake\ORM\Query $query
     * @param ArrayObject $options
     * @return boolean|\Cake\ORM\Query
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options) {
        /**
         * This condition is checked when we don't need to check current user 
         */
        if (isset($options['remove_user_condition']) && $options['remove_user_condition'] > 0) {
            return TRUE;
        } else {
            $query->where([$this->alias() . '.user_id' => $this->session->read('Auth.User.id')]);
            return $query;
        }
    }
    
    /**
     * import master items
     * @param $dataArr
     * insert records in master_items table
     * @return result 
     */
    public function modifyMasterItemsArr($dataArr, $userId) {
        
        $masterItemsData = array();
        $masterItemsColumns = isset($dataArr[Configure::read('master-items.column_name_row_number')]) ? $dataArr[Configure::read('master-items.column_name_row_number')] : array();
        
        $r = 0;
        foreach ($dataArr as $data) {
            if ($r > (Configure::read('master-items.row_start_cnt_for_import') - 1)) {
                $masterItemsData[$r]['user_id'] = $userId;
                foreach ($data as $key => $value) {
                    $masterItemsData[$r][Configure::read('master-items.columns.'.$masterItemsColumns[$key])] = $value;
                }
            }
            $r++;
        }
        
        return $masterItemsData;
        
    }
    
    /**
     *  Get all master items of current user
     *  @param int $userId
     */
    public function getCurrentUserMasterItems($userId) {
        return $this->find('all', [
            'fields' => ['bar_code', 'retail_price', 'description', 'name'],
        ])->toArray();
    }
    
    
    /**
     * findDataByGtin - Method
     * @param type $gtin
     * @return type
     */
    public function findDataByGtin($gtin = null) {
        
        /* Get Master Items */
        $masterItemsData = $this->findByBarCode($gtin)->first();
        if (empty($masterItemsData)) {
            // check gtin > 13 & prepend is O then remove it
            if (strlen($gtin) > 13) {
                $prefix = '0';
                $gtin = $this->removePrefix($gtin, $prefix);
                $masterItemsData = $this->findByBarCode($gtin)->first();
            }
        }
        return $masterItemsData;
    }
    
    /**
     * updateFieldsByField - Method
     * @param type $conditionArr
     * @param type $updateFieldsArr
     * @return type
     */
    public function updateFieldsByFields($conditionArr, $updateFieldsArr) {
        $query = $this->query();

        $result = $query->update()
                ->set($updateFieldsArr)
                ->where($conditionArr)
                ->execute();

        return $result;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
}
