<?php
namespace App\Model\Table;

use App\Model\Entity\Payment;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Payments Model
 */
class PaymentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('payments');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Common');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('PaymentHistories', [
            'foreignKey' => 'payment_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        return $validator;
    }
    
    /**
     * modifiedResponseForSave - Method
     * @param type $response
     * @param type $userId
     * @return type
     */
    public function modifiedResponseForSave($response, $userId) {
        
        $paymentData = array();
        
        // payment details
        $paymentData['user_id'] = $userId;
        $paymentData['customer_id'] = isset($response->id) ? $response->id : '';
        
        // payment histories details
        $paymentData['payment_histories'][0]['plan_id'] = isset($response->subscriptions->data[0]->plan->id) ? $response->subscriptions->data[0]->plan->id : '';
        $paymentData['payment_histories'][0]['email'] = isset($response->sources->data[0]->name) ? $response->sources->data[0]->name : '';
        $paymentData['payment_histories'][0]['payment_interval'] = isset($response->subscriptions->data[0]->plan->interval) ? $response->subscriptions->data[0]->plan->interval : '';
        $paymentData['payment_histories'][0]['interval_start_date'] = isset($response->subscriptions->data[0]->current_period_start) ? date('Y-m-d', $response->subscriptions->data[0]->current_period_start) : '';
        $paymentData['payment_histories'][0]['interval_end_date'] = isset($response->subscriptions->data[0]->current_period_end) ? date('Y-m-d', $response->subscriptions->data[0]->current_period_end) : '';
        $paymentData['payment_histories'][0]['amount'] = isset($response->subscriptions->data[0]->plan->amount) ? $response->subscriptions->data[0]->plan->amount : '';
        $paymentData['payment_histories'][0]['currency'] = isset($response->subscriptions->data[0]->plan->currency) ? $response->subscriptions->data[0]->plan->currency : '';
        $paymentData['payment_histories'][0]['payment_date'] = isset($response->subscriptions->data[0]->start) ? date('Y-m-d H:i:s', $response->subscriptions->data[0]->start) : '';
        $paymentData['payment_histories'][0]['discount'] = isset($response->subscriptions->data[0]->discount) ? $response->subscriptions->data[0]->discount : '';
        $paymentData['payment_histories'][0]['tax_percent'] = isset($response->subscriptions->data[0]->tax_percent) ? $response->subscriptions->data[0]->tax_percent : '';
        $paymentData['payment_histories'][0]['status'] = isset($response->subscriptions->data[0]->status) ? $response->subscriptions->data[0]->status : '';
        
        return $paymentData;
    }
    
    /**
     * modifiedSubscriptionCreateResponseForPaymentHistories - Method
     * @param type $response
     * @param type $userId
     * @return type
     */
    public function modifiedSubscriptionCreateResponseForPaymentHistories($response, $paymentId, $emailId) {
        
        $paymentData = array();
        
        // payment histories details
        $paymentData['payment_id'] = $paymentId;
        $paymentData['plan_id'] = isset($response->plan->id) ? $response->plan->id : '';
        $paymentData['email'] = $emailId;
        $paymentData['payment_interval'] = isset($response->plan->interval) ? $response->plan->interval : '';
        $paymentData['interval_start_date'] = isset($response->current_period_start) ? date('Y-m-d', $response->current_period_start) : '';
        $paymentData['interval_end_date'] = isset($response->current_period_end) ? date('Y-m-d', $response->current_period_end) : '';
        $paymentData['amount'] = isset($response->plan->amount) ? $response->plan->amount : '';
        $paymentData['currency'] = isset($response->plan->currency) ? $response->plan->currency : '';
        $paymentData['payment_date'] = isset($response->start) ? date('Y-m-d H:i:s', $response->start) : '';
        $paymentData['discount'] = isset($response->discount) ? $response->discount : '';
        $paymentData['tax_percent'] = isset($response->tax_percent) ? $response->tax_percent : '';
        $paymentData['status'] = isset($response->status) ? $response->status : '';
        
        return $paymentData;
    }
    
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }
    
     /**
     * Find Record By Customer Id - Method
     * @param type $customerId
     * @return type
     */
    public function findRecordByCustomerId($customerId) {
        try {
            return $this->find('all', [
                    'conditions' => ['customer_id' => $customerId],
                     'contain' => ['Users']
                ])->first();
        } catch (Exception $exc) {
            return '';
        }

    }
}
