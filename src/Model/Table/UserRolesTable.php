<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * UserRoles Model
 */
class UserRolesTable extends Table
{
    
    const Admin  = 'Admin';
    const User   =  'User';

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('user_roles');
        $this->displayField('name');
        $this->primaryKey('id');
        $this->hasMany('Users', [
            'foreignKey' =>'user_role_id'                   
        ]);
        $this->addBehavior('Common');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }
}
