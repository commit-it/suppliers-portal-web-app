<?php
namespace App\Model\Table;

use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PurchaseOrderItems Model
 */
class PurchaseOrderItemsTable extends Table
{

    /*
     * Define constant variables
     */
    const Supplier_Product_Id_Type = 'SUPPLIER_ASSIGNED';
    const Bunnings_Product_Id_Type = 'BUYER_ASSIGNED';
    const Description_Type = 'FOR_INTERNAL_USE_1';
    const Pack_Quantity_Type = 'FOR_INTERNAL_USE_2';
    const Quotation_Number_Type = 'FOR_INTERNAL_USE_3';
    
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('purchase_order_items');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('PurchaseOrders', [
            'foreignKey' => 'purchase_order_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->add('order_line_number', 'valid', ['rule' => 'numeric'])
            ->requirePresence('order_line_number', 'create')
            ->notEmpty('order_line_number')
            ->requirePresence('gtin', 'create')
            ->notEmpty('gtin')
            ->add('requested_quantity', 'valid', ['rule' => 'decimal'])
            ->requirePresence('requested_quantity', 'create')
            ->notEmpty('requested_quantity')
            ->requirePresence('unit_of_measure', 'create')
            ->notEmpty('unit_of_measure')
            ->add('pack_quantity', 'valid', ['rule' => 'numeric'])
            ->requirePresence('pack_quantity', 'create')
            ->notEmpty('pack_quantity')
            ->allowEmpty('description')
            ->add('net_amount', 'valid', ['rule' => 'decimal'])
            ->requirePresence('net_amount', 'create')
            ->notEmpty('net_amount')
            ->add('net_price', 'valid', ['rule' => 'decimal'])
            ->requirePresence('net_price', 'create')
            ->notEmpty('net_price');

        return $validator;
    }


    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['purchase_order_id'], 'PurchaseOrders'));
        return $rules;
    }
}
