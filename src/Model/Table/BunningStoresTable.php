<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Network\Session;
use ArrayObject;
use Cake\Core\Configure;

/**
 * BunningStores Model
 */
class BunningStoresTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        $this->table('bunning_stores');
        $this->displayField('name');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('ScanBooks', [
            'foreignKey' => 'bunning_store_id'
        ]);
        $this->addBehavior('Common');
        $this->session = new Session();
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->add('id', 'valid', ['rule' => 'numeric', 'message' => 'This value should be Numeric'])
                ->allowEmpty('id', 'create')
                ->requirePresence('type', 'create')
                ->notEmpty('type')
                ->requirePresence('name', 'create')
                ->notEmpty('name')
                ->add('email', 'valid', ['rule' => 'email', 'message' => 'This value need to be in correct format'])
                ->requirePresence('email', 'create')
                ->notEmpty('email')
                ->requirePresence('gln', 'create')
                ->notEmpty('gln')
                ->requirePresence('address', 'create')
                ->notEmpty('address')
                ->requirePresence('city', 'create')
                ->notEmpty('city')
                ->requirePresence('country_iso_code', 'create')
                ->notEmpty('country_iso_code')
                ->requirePresence('language_iso_code', 'create')
                ->notEmpty('language_iso_code')
                ->add('postal_code', 'valid', ['rule' => 'numeric', 'message' => 'Postal Code should be Numeric'])
                ->requirePresence('postal_code', 'create')
                ->notEmpty('postal_code')
                ->requirePresence('state', 'create')
                ->notEmpty('state')
                ->allowEmpty('company_registration_number');

        return $validator;
    }
    
    /**
     * Return query for auth user
     * @param \Cake\Event\Event $event
     * @param \Cake\ORM\Query $query
     * @param ArrayObject $options
     * @return boolean|\Cake\ORM\Query
     */
    public function beforeFind(Event $event, Query $query, ArrayObject $options) {
        /**
         * This condition is checked when we don't need to check current user 
         */
        if (isset($options['remove_user_condition']) && $options['remove_user_condition'] > 0) {
            return TRUE;
        }
        $query->where([$this->alias() . '.user_id' => $this->session->read('Auth.User.id')]);
        return $query;
    }

    /*
     * Return Bunning Stores List
     * findBunningStoresListByIds() method
     * 
     */
    public function findBunningStoresListByIds($ids) {
        $bunningStoresList = $this->ScanBooks->BunningStores->find('all', [
                    'fields' => array('id', 'email', 'name'),
                    'conditions' => array(
                        'BunningStores.id IN' => $ids
                    )]
                )->toArray();
        return $bunningStoresList;
    }
    
    /**
     * modifyMasterItemsArr - Method
     * @param type $dataArr
     * @param type $userId
     * @return type
     */
    public function modifyBunningStoresArr($dataArr, $userId) {
        
        $bunningStoresData = array();
        $bunningStoresColumns = isset($dataArr[Configure::read('bunning-stores.column_name_row_number')]) ? $dataArr[Configure::read('bunning-stores.column_name_row_number')] : array();
        
        $r = 0;
        foreach ($dataArr as $data) {
            if ($r > (Configure::read('bunning-stores.row_start_cnt_for_import') - 1)) {
                $bunningStoresData[$r]['user_id'] = $userId;
                $bunningStoresData[$r]['type'] = Configure::read('bunning_user.Payer');
                foreach ($data as $key => $value) {
                    $bunningStoresData[$r][Configure::read('bunning-stores.columns.'.$bunningStoresColumns[$key])] = $value;
                }
            }
            $r++;
        }
        
        return $bunningStoresData;
        
    }
    
    /**
     * updateFieldsByField - Method
     * @param type $conditionArr
     * @param type $updateFieldsArr
     * @return type
     */
    public function updateFieldsByFields($conditionArr, $updateFieldsArr) {
        $query = $this->query();

        $result = $query->update()
                ->set($updateFieldsArr)
                ->where($conditionArr)
                ->execute();

        return $result;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['gln']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        return $rules;
    }

}
