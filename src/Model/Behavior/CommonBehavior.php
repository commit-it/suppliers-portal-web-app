<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Common behavior
 */
class CommonBehavior extends Behavior {

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /*
     * check dir is exist
     * if not then create
     */

    public function makeDirectoryIfNotExist($dir) {
        $dirResult = '';
        if (!is_dir($dir)) {
            
            try {
                
                $dirResult = mkdir($dir, 0777, true);
                if ($dirResult) {
                    chmod($dir, 0777);
                }
            } catch (Exception $exc) {
                
                throw new Exception($exc->getTraceAsString());
            }

        }
        return $dirResult;
    }

    /**
     * check isset
     * @param type $param
     * @return null
     */
    public function checkIsset($param) {
        return isset($param) ? $param : null;
    }

    /**
     * check field is exist or not in object
     * @param type $param
     * @return null
     */
    public function checkIfFieldIsExist($objResult, $field) {
        return (isset($objResult->$field)) ? $objResult->$field : null;
    }

    /*
     * check file is exist or not
     * @param $$fileName
     * @return true or false
     */

    public function checkFileIsExist($fileName = null) {

        if (!empty($fileName)) {

            if (file_exists($fileName)) {
                return true;
            }
        }
        return false;
    }

    /*
     * move file in another folder
     * @param $fileName, $movePath
     * @return true or false
     */

    public function moveFileInAnotherFolder($fileName = null, $originalPath = null, $movePath = null) {

        $existingFilePath = $originalPath . DS . $fileName;
        $moveFilePath = $movePath . DS . $fileName;

        if ($this->checkFileIsExist($existingFilePath)) {

            $this->makeDirectoryIfNotExist($movePath);

            if (rename($existingFilePath, $moveFilePath)) {

                return true;
            } else {

                return false;
            }
        } else {

            return false;
        }
    }

    /*
     * create separate user address for generate Invoice like - city, state, postalCode etc
     * @param $fulladdress
     * @return result
     */

    public function separateUserAddressForGenerateInvoice($fullAddress, $model, $findByField, $gln) {

        $addressArr = array();

        /* Get data from specific model */
        $usersData = TableRegistry::get($model)->$findByField($gln)->applyOptions(['remove_user_condition' => true])->first();

        /* If data found then define address array */
        if (isset($usersData) && !empty($usersData)) {
            $addressArr['streetAddressOne'] = htmlspecialchars($usersData->address);
            $addressArr['city'] = $usersData->city;
            $addressArr['state'] = $usersData->state;
            $addressArr['postalCode'] = $usersData->postal_code;
            $addressArr['countryISOCode'] = $usersData->country_iso_code;
            $addressArr['languageISOCode'] = $usersData->language_iso_code;
            $addressArr['companyRegistrationNumber'] = $usersData->company_registration_number;
        } else {
            /* If address data not found from database then return as assigned address data */
            $delimiter = Configure::read('invoice.address-delimiter');
            $streetAddressOneOrders = Configure::read('invoice.address-orders');
            $stateOrderFromEnd = Configure::read('invoice.state-order-from-end');
            $postalCodeOrderFromEnd = Configure::read('invoice.postalCode-order-from-end');

            $addressArr['streetAddressOne'] = '';
            $addressArr['city'] = '';
            $addressArr['state'] = '';
            $addressArr['postalCode'] = '';
            $addressArr['countryISOCode'] = 'AU';
            $addressArr['languageISOCode'] = 'EN';
            $addressArr['companyRegistrationNumber'] = '';
            if (is_string($fullAddress)) {

                $fullAddressArr = explode($delimiter, $fullAddress);
                $fullAddressArr = array_filter($fullAddressArr);
                $addressCnt = count($fullAddressArr);

                $streetAddressOne = '';
                $city = '';
                for ($i = 0; $i < $addressCnt; $i++) {

                    if ($i < $streetAddressOneOrders) {
                        if ($streetAddressOne != '') {
                            $streetAddressOne = $streetAddressOne . ' ' . $fullAddressArr[$i];
                        } else {
                            $streetAddressOne = $streetAddressOne . $fullAddressArr[$i];
                        }
                    } elseif (($i >= $streetAddressOneOrders) && ($i < ($addressCnt - $stateOrderFromEnd))) {
                        if ($city != '') {
                            $city = $city . ' ' . $fullAddressArr[$i];
                        } else {
                            $city = $city . $fullAddressArr[$i];
                        }
                    }
                }

                $addressArr['streetAddressOne'] = $streetAddressOne;
                $addressArr['city'] = $city;
                $addressArr['state'] = $fullAddressArr[$addressCnt - $stateOrderFromEnd];
                $addressArr['postalCode'] = $fullAddressArr[$addressCnt - $postalCodeOrderFromEnd];
            }
        }
        return $addressArr;
    }

    /*
     * create object to array
     * get formated date from array
     * getFormatedDateFromObject() method used
     */

    public function getFormatedDateFromObject($dateObj) {
        $dateArr = (array) $dateObj;
        $formattedDate = isset($dateArr['date']) ? $dateArr['date'] : '';
        return $formattedDate;
    }

    /**
     * multisortArray - Method
     * @param type $merge_arr
     * @param type $modelArr
     * @param type $field
     * @param type $sort
     * @return type
     */
    public function multisortArray($merge_arr, $modelArr, $field, $sort) {

        $sortArr = array();
        if (!empty($modelArr)) {
            foreach ($merge_arr as $k => $v) {
                foreach ($modelArr as $modelKey => $modelVal) {
                    if (isset($v[$modelArr[$modelKey]][$field]) && !empty($v[$modelArr[$modelKey]][$field])) {
                        $sortArr[$field][$k] = $v[$modelVal][$field];
                    }
                }
            }
        } else {
            foreach ($merge_arr as $k => $v) {
                $sortArr[$field][$k] = $v[$field];
            }
        }
        
        if ($sort == 'asc') {
            array_multisort($sortArr[$field], SORT_ASC, $merge_arr);
        } else {
            array_multisort($sortArr[$field], SORT_DESC, $merge_arr);
        }
        return $merge_arr;
    }

    /**
     * addPrefixToString - Method
     * @param type $string
     * @param type $prefix
     * @param type $prefixCnt
     * @return string
     */
    public function addPrefixToString($string, $prefix, $prefixCnt = 0) {

        $newString = '';
        if ($prefixCnt > 0) {

            $multiplePrefix = '';
            for ($i = 0; $i < $prefixCnt; $i++) {

                $multiplePrefix = $multiplePrefix . $prefix;
            }
            $newString = $multiplePrefix . $string;
        } else {

            $newString = $prefix . $string;
        }
        return $newString;
    }

    /**
     * removePrefix - Method
     * @param string $text
     * @param type $prefix
     * @return string
     */
    public function removePrefix($text, $prefix) {

        if (0 === strpos($text, $prefix)) {

            $text = substr($text, strlen($prefix)) . '';
        }
        return $text;
    }
    
    /**
     * generateRandomCode - Method
     * @param type $length
     * @return string
     */
    public function generateRandomCode($length) {

        $string = '';
        // You can define your own characters here.
        $characters = "23456789ABCDEFHJKLMNPRTVWXYZabcdefghijklmnopqrstuvwxyz";

        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }
    
    /**
     * generateRandomNumber - Method
     * @param type $length
     * @return number
     */
    public function generateRandomNumber($length) {

        $string = '';
        // You can define your own characters here.
        $characters = "1234567890";

        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }
    
    /**
     * getErrorMsgForDisplay - get validation or error msg
     * @param type $errorMsgArr
     * @return string
     */
    public function getErrorMsgForDisplay($errorMsgArr) {
        
        $errorMsg = '';
        foreach ($errorMsgArr as $errorMsgKey => $errorMsgVal) {
            $mskKey = key($errorMsgVal);
            $errorMessage = isset($errorMsgVal[$mskKey]) ? $errorMsgVal[$mskKey] : '';
            if ($errorMessage != '') {
                if ($errorMsg != '') {

                    $errorMsg = $errorMsg . ', ' . strtoupper($errorMsgKey) . ' - ' . $errorMessage;
                } else {

                    $errorMsg = $errorMsg . strtoupper($errorMsgKey) . ' - ' . $errorMessage;
                }
            }
        }
        
        return $errorMsg;
    }

}
