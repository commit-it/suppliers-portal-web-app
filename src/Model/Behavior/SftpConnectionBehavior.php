<?php

namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\Core\Exception\Exception;
use Cake\Core\Configure;

/**
 * SftpConnection behavior
 */
class SftpConnectionBehavior extends Behavior {

    /**
     * Constructor
     *
     * Checks if ssh2 extension is loaded.
     */
    public function __construct() {

    }

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * Sftp resource created by ssh2_sftp for use with all sftp methods
     *
     * @var resource
     */
    protected $sftpResource = null;

    /**
     * Ssh connection resource created by ssh2_connect for use with all ssh methods
     * @var resource
     */
    protected $sshResource = null;

    /**
     * Sets the resource created by ssh2_sftp used for all Sftp methods
     *
     * @param resource $sftpResource
     * @return self
     */
    protected function setSftpResource($sftpResource) {
        $this->sftpResource = $sftpResource;

        return $this;
    }

    /**
     * Gets the assigned resource used by all Sftp methods
     *
     * @return sftpResource|null
     */
    protected function getSftpResource() {
        return $this->sftpResource;
    }

    /**
     * Sets the resource created by ssh2_connect used for all ssh methods
     *
     * @param resource $sshResource
     * @return self
     */
    protected function setSshResource($sshResource) {
        $this->sshResource = $sshResource;

        return $this;
    }

    /**
     * Gets the assigned resource used by all ssh methods
     *
     * @return self
     */
    protected function getSshResource() {
        return $this->sshResource;
    }

    /**
     * sftpConnection() method.
     *
     * @return success or error connection.
     */
    public function sftpConnection() {

        // check ssh2 extention loaded or not
        if ( !extension_loaded('ssh2') ) {
            
            throw new Exception('Ssh2 extension is not loaded!');
        }


        $strServer = Configure::read('sftp.DNS');
        $strServerPort = Configure::read('sftp.port');
        $strServerUsername = Configure::read('sftp.username');
        $strServerPassword = Configure::read('sftp.password');

        //connect to server
        try {
            $resConnection = ssh2_connect($strServer, $strServerPort);
        } catch (Exception $exc) {
            
            throw new Exception($exc->getMessage());
        }

        if (ssh2_auth_password($resConnection, $strServerUsername, $strServerPassword)) {

            //Initialize SFTP subsystem
            $this->setSshResource($resConnection);
            $this->setSftpResource(ssh2_sftp($resConnection));

            return $this;
        } else {

            throw new Exception("Unable to authenticate SFTP connection");
        }
    }

    /**
     * Checks if connection resources are assigned for further use
     *
     * @return self
     * @throws Exception Invalid connection resource!
     */
    protected function validateSshResource() {
        
        if ($this->getSshResource() === false || $this->getSshResource() === null) {
            
            throw new Exception("Invalid SFTP connection resource!");
        }

        return $this;
    }

    /**
     * downloadFiles - Method
     * @param type $remoteFilePath
     * @param type $localFileName
     * @param type $downloadFileList
     * @return type
     * @throws Exception
     */
    public function downloadFiles($remoteFilePath, $localFileName = null, $downloadFileList = array()) {

        $fileNameArr = array();

        $strType = Configure::read('connection-type');

        $this->validateSshResource();

        if (ssh2_sftp_stat($this->getSftpResource(), $remoteFilePath) === false) {
            throw new Exception($remoteFilePath . " file does not exist or no permissions to read!");
        }

        $savePath = '';

        if (is_string($localFileName) === true) {
            $savePath .= $localFileName;
        } else {
            $path = pathinfo($remoteFilePath);
            $savePath .= $path['basename'];
        }

        /*
         * check dir is exist
         * if not then create
         */
        //$dirResult = $this->Common->makeDirectoryIfNotExist($localFileName);
        if (!is_dir($localFileName)) {
            $dirResult = mkdir($localFileName, 0777, true);
        }

        $sftp = $this->getSftpResource();

        $handle = opendir("ssh2.$strType://$sftp/$remoteFilePath");
        if ($handle === false) {
            
            throw new Exception("Unable to open remote directory!");
        }

        while (false != ($entry = readdir($handle))) {

            if (in_array($entry, $downloadFileList)) {
                
                // Remote stream
                if (!$remotehandle = @fopen("ssh2.$strType://$sftp/$remoteFilePath/$entry", 'r')) {
                    
                    throw new Exception("Unable to open remote file: $remoteFilePath");
                }

                // Local stream
                if (!$localhandle = @fopen("$savePath/$entry", 'w')) {
                    
                    throw new Exception("Unable to open local file for writing: $savePath");
                }

                while ($chunk = fread($remotehandle, Configure::read('download-file-size'))) {
                    
                    fwrite($localhandle, $chunk);
                }
                $fileNameArr[] = $entry;
                fclose($remotehandle);
                fclose($localhandle);
            }
        }

        return $fileNameArr;
    }
    
    /**
     * Attempts to upload a file using SFTP protocol
     *
     * @param string $localFilePath
     * @param string $remoteFileName
     * @return self
     * @throws Exception Invalid connection resource! due to use of validateSshResource.
     * @throws Exception File does not exist or no permissions to read!
     * @throws Exception Could not upload the file!
     */
    public function upload($localFilePath, $remoteFileName = null)
    {
        $strType = Configure::read('connection-type');
        
        $this->validateSshResource();

        if(stat($localFilePath) === false) {
            
            throw new Exception($localFilePath . " file does not exist or no permissions to read!");
        }

        $savePath = '';

        if(is_string($remoteFileName) === true) {
            $savePath .= $remoteFileName;
        } else {
            $path = pathinfo($localFilePath);
            $savePath .= $path['basename'];
        }

        $sftp = $this->getSftpResource();

        // Local stream
        if (!$localStream = @fopen($localFilePath, 'r')) {
            
            throw new Exception("Unable to open local file for reading: $localFilePath");
        }

        // Remote stream
        if (!$remoteStream = @fopen("ssh2.$strType://$sftp/$savePath", 'w')) {
            
            throw new Exception("Unable to open remote file for writing: $savePath");
        }

        // Write from our remote stream to our local stream
        $read = 0;
        $fileSize = filesize($localFilePath);
        while ($read < $fileSize && ($buffer = fread($localStream, $fileSize - $read))) {
            // Increase our bytes read
            $read += strlen($buffer);

            // Write to our local file
            if (fwrite($remoteStream, $buffer) === FALSE) {
                
                throw new Exception("Unable to write to local file: $savePath");
            }
        }

        // Close our streams
        fclose($localStream);
        fclose($remoteStream);

        return $this;
    }
    
    /**
     * Gets the files list
     *
     * @param string $remotePath Remote directory path
     * @return array List of files
     * @throws Exception Invalid connection resource! due to use of validateSshResource.
     * @throws Exception Folder does not exist or no permissions to read!
     * @throws Exception Unable to open remote directory!
     */
    public function getFileList($remotePath) {
        
        $strType = Configure::read('connection-type');

        $this->validateSshResource();
        if (ssh2_sftp_stat($this->getSftpResource(), $remotePath) === false) {
            
            throw new Exception($remotePath . " folder does not exist or no permissions to read!");
        }

        $sftp = $this->getSftpResource();

        $handle = opendir("ssh2.$strType://$sftp/$remotePath");
        if ($handle === false) {
            
            throw new Exception($remotePath . " unable to open remote directory!");
        }

        $files = array();

        while (false != ($entry = readdir($handle))) {
            $files[] = $entry;
        }

        return $files;
    }
    
    /**
     * Delete files
     *
     * @param string $remotePath Remote directory path
     * @return files
     * @throws Exception Invalid connection resource! due to use of validateSshResource.
     * @throws Exception Folder does not exist or no permissions to read!
     * @throws Exception Unable to open remote directory!
     */
    public function deleteFile($remotePath, $file) {

        $this->validateSshResource();
        if (ssh2_sftp_stat($this->getSftpResource(), $remotePath) === false) {
            throw new Exception($remotePath . " folder does not exist or no permissions to read!");
        }

        $sftp = $this->getSftpResource();

        return ssh2_sftp_unlink($sftp, $remotePath . DS . $file);
    }

}
