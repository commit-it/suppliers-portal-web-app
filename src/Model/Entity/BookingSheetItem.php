<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BookingSheetItem Entity.
 */
class BookingSheetItem extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'booking_sheet_id' => true,
        'invoice_id' => true,
        'purchase_order_id' => true,
        'trolleys' => true,
        'office_use_only' => true,
        'booking_sheet' => true,
        'invoice' => true,
        'purchase_order' => true,
    ];
}
