<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProcessLog Entity.
 */
class ProcessLog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'nursery_GLN' => true,
        'user_id' => true,
        'purchase_order_number' => true,
        'action_description' => true,
        'user' => true,
        'is_myob' => true,
        'created' => true,
    ];
}
