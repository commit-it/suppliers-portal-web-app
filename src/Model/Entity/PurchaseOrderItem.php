<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PurchaseOrderItem Entity.
 */
class PurchaseOrderItem extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'purchase_order_id' => true,
        'order_line_number' => true,
        'gtin' => true,
        'supplier_product_id' => true,
        'bunnings_product_id' => true,
        'requested_quantity' => true,
        'unit_of_measure' => true,
        'pack_quantity' => true,
        'quotation_number' => true,
        'description' => true,
        'net_price' => true,
        'net_amount' => true,
        'reference_purchase_order_line_number' => true,
        'gst_amount' => true,
        'gst_percentage' => true,
        'comments' => true,
        'purchase_order' => true,
    ];
}