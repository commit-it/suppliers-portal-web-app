<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FileOverviewe Entity.
 */
class FileOverviewe extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'file_type_id' => true,
        'file_name' => true,
        'file_status_id' => true,
        'invoice_id' => true,
        'file_type' => true,
        'file_status' => true,
        'invoices' => true,
    ];
}
