<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BunningStore Entity.
 */
class BunningStore extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'type' => true,
        'name' => true,
        'email' => true,
        'gln' => true,
        'address' => true,
        'city' => true,
        'country_iso_code' => true,
        'language_iso_code' => true,
        'postal_code' => true,
        'state' => true,
        'company_registration_number' => true,
        'user_id' => true,
        'quick_book_cutomer_id' => true,
        'user' => true,
        'scan_books' => true,
    ];
}
