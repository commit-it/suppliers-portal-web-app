<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FileStatus Entity.
 */
class FileStatus extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'status' => true,
        'file_overviewes' => true,
    ];
}
