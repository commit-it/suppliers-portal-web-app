<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MasterItem Entity.
 */
class MasterItem extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'scan_barcode' => true,
        'bar_code' => true,
        'pot_size' => true,
        'name' => true,
        'description' => true,
        'genus' => true,
        'cultival' => true,
        'species' => true,
        'retail_price' => true,
        'quick_book_item_id' => true,
        'user' => true,
    ];
}
