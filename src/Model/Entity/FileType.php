<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * FileType Entity.
 */
class FileType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'type' => true,
        'file_overviewes' => true,
    ];
}
