<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BookingSheet Entity.
 */
class BookingSheet extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'courier_id' => true,
        'trays' => true,
        'trolleys' => true,
        'pickup_date' => true,
        'drop_date' => true,
        'arrived_time_Vic' => true,
        'arrived_time_NSW' => true,
        'user' => true,
        'booking_sheet_items' => true,
    ];
}
