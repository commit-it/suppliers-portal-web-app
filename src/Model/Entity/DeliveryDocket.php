<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DeliveryDocket Entity.
 */
class DeliveryDocket extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'invoice_id' => true,
        'purchase_order_id' => true,
        'delivery_date' => true,
        'user' => true,
        'invoice' => true,
        'purchase_order' => true,
    ];
}
