<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PaymentHistory Entity.
 */
class PaymentHistory extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'payment_id' => true,
        'plan_id' => true,
        'email' => true,
        'payment_interval' => true,
        'interval_start_date' => true,
        'interval_end_date' => true,
        'amount' => true,
        'currency' => true,
        'payment_date' => true,
        'discount' => true,
        'tax_percent' => true,
        'status' => true,
        'payment' => true
    ];
}
