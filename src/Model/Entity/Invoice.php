<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Invoice Entity.
 */
class Invoice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'purchase_order_id' => true,
        'invoice_number' => true,
        'quick_book_invoice_id' => true,
        'invoice_type' => true,
        'purchase_order_number' => true,
        'reference_delivery_note_number' => true,
        'sub_total' => true,
        'tax' => true,
        'discount_percentage' => true,
        'discount_amount' => true,
        'sales_person' => true,
        'ship_via' => true,
        'invoice_generated' => true,
        'invoice_status' => true,
        'delivery_docket_generated' => true,
        'delivery_docket_no' => true,
        'invoice_date' => true,
        'resend_invoice' => true,
        'invoice_update_to_quick_book' => true,
        'time_of_invoice_sent_to_bunnings' => true,
        'user' => true,
        'purchase_order' => true,
        'invoice_items' => true,
        'file_overviewes' => true,
    ];
}
