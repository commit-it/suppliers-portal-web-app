<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PurchaseOrder Entity.
 */
class PurchaseOrder extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'purchase_order_number' => true,
        'receiver_gln' => true,
        'invoice_number' => true,
        'instance_id' => true,
        'creation_date_time' => true,
        'language_iso_code' => true,
        'seller_gln' => true,
        'seller_additional_id' => true,
        'seller_name' => true,
        'ship_from_gln' => true,
        'ship_from_name' => true,
        'ship_from_additional_id' => true,
        'ship_from_address' => true,
        'ship_from_telephone_number' => true,
        'ship_from_fax_number' => true,
        'bill_to_gln' => true,
        'bill_to_additional_id' => true,
        'bill_to_name' => true,
        'bill_to_address' => true,
        'bill_to_telephone_number' => true,
        'bill_to_fax_number' => true,
        'buyer_gln' => true,
        'buyer_additional_id' => true,
        'buyer_name' => true,
        'order_date' => true,
        'deliver_date' => true,
        'actual_delivery_date' => true,
        'actual_delivery_date_updated' => true,
        'total_order_amount' => true,
        'currency_id' => true,
        'comments' => true,
        'purchase_order_status_id' => true,
        'fulfilment_despatch_advice_identification_number' => true,
        'send_fulfilment_advice_to_bunnings' => true,
        'fulfilment_advice_status' => true,
        'sent_to_myob' => true,
        'sent_to_quick_book' => true,
        'fulfilment_advice_received_date' => true,
        'archived' => true,
        'user' => true,
        'currency' => true,
        'purchase_order_status' => true,
        'invoice_items' => true,
        'invoices' => true,
        'purchase_order_items' => true,
    ];
}