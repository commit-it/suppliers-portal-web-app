<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ScanBook Entity.
 */
class ScanBook extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'scan_book_file' => true,
        'bunning_store_id' => true,
        'user' => true,
        'bunning_store' => true,
    ];
}
