<?php
namespace App\Model\Entity;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\Entity;

/**
 * User Entity.
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'full_name' => true,
        'email' => true,
        'phone_number' => true,
        'address' => true,
        'city' => true,
        'country' => true,
        'country_iso_code' => true,
        'language_iso_code' => true,
        'postal_code' => true,
        'state' => true,
        'company_registration_number' => true,
        'password' => true,
        'seller_gln' => true,
        'seller_business_name' => true,
        'seller_business_logo' => true,
        'seller_business_address' => true,
        'status' => true,
        'user_role_id' => true,
        'parent_id' => true,
        'invoices' => true,
        'process_logs' => true,
        'purchase_orders' => true,
        'scan_books' => true,
        'user_roles' => true,
        'master_items' => true,
        'trial_end_date' => true,
        'access_status' => true,
        'payment_status' => true,
        'current_subscription_end_date' => true,
        'subscription_cancelled' => true,
        'quick_book_access_token' => true,
        'quick_book_access_token_secret' => true,
        'quick_book_consumer_key' => true,
        'quick_book_consumer_secret' => true,
        'quick_book_realm_id' => true,
        'quick_book_payment_term' => true,
        'quick_book_rebate' => true,
        'invoice_generate_by' => true,
        'statistic' => true,
        'reminder_shedule' => true,
        'reminder_sent_time' => true,
        'delivery_docket_no' => true
    ];
    
    
     protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }
}
