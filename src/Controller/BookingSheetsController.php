<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * BookingSheets Controller
 *
 * @property \App\Model\Table\BookingSheetsTable $BookingSheets
 */
class BookingSheetsController extends AppController {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

        // load Component
        $this->loadComponent('ManagePdf');

        // load Model
        $this->loadModel('Invoices');
        $this->loadModel('Couriers');
        $this->loadModel('BookingSheets');
        $this->loadModel('BookingSheetItems');
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Users','BookingSheetItems'],
            'order' => ['BookingSheets.modified' => 'DESC']
        ];
        $this->set('bookingSheets', $this->paginate($this->BookingSheets));
        $this->set('_serialize', ['bookingSheets']);
    }

    /**
     * View method
     *
     * @param string|null $id Booking Sheet id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $bookingSheet = $this->BookingSheets->get($id, [
            'contain' => ['BookingSheetItems', 'Couriers', 'BookingSheetItems.Invoices', 'BookingSheetItems.Invoices.InvoiceItems', 'BookingSheetItems.PurchaseOrders']
        ]);

        $this->set('bookingSheet', $bookingSheet);
        $this->set('_serialize', ['bookingSheet']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {

        if ($this->request->is(['patch', 'post', 'put'])) {

            $this->request->data['user_id'] = $this->Auth->user('id');
            $bookingSheet = $this->BookingSheets->newEntity($this->request->data);
            if ($this->BookingSheets->save($bookingSheet)) {

                $invoiceIds = array();
                foreach ($this->request->data['booking_sheet_items'] as $bookingSheetItem) {
                    $invoiceIds[] = $bookingSheetItem['invoice_id'];
                }
                // update invoice record for delivery_docket_generated column
                $this->Invoices->updateAll(
                        array('booking_sheet_generated' => Configure::read('booking_sheet_generated.yes')), array('id IN' => $invoiceIds)
                );

                $this->Flash->success('The booking sheet has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {

                if (!empty($bookingSheet->errors())) {

                    $errorMsgs = $this->Common->getErrorMsgForDisplay($bookingSheet->errors());
                    $this->Flash->error($errorMsgs);
                } else {

                    $this->Flash->error('The booking sheet could not be saved. Please, try again.');
                }
            }
        } else {

            $this->Flash->error('You can\'t generate Booking Sheet without select Invoice, Please select at least at list one Invoice');
            return $this->redirect(['controller' => 'DeliveryDockets', 'action' => 'generate_booking_sheet']);
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Booking Sheet id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        $bookingSheet = $this->BookingSheets->get($id, [
            'contain' => ['BookingSheetItems', 'BookingSheetItems.Invoices', 'BookingSheetItems.Invoices.InvoiceItems', 'BookingSheetItems.PurchaseOrders']
        ]);

        if (isset($bookingSheet->booking_sheet_items) && !empty($bookingSheet->booking_sheet_items)) {
            $bookingSheet->booking_sheet_items = $this->BookingSheets->multisortArray($bookingSheet->booking_sheet_items, array('purchase_order'), 'bill_to_name', 'asc');
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            $bookingSheet = $this->BookingSheets->patchEntity($bookingSheet, $this->request->data);

            if ($this->BookingSheets->save($bookingSheet)) {

                $this->Flash->success('The booking sheet has been updated.');
                return $this->redirect(['action' => 'index']);
            } else {

                if (!empty($bookingSheet->errors())) {

                    $errorMsgs = $this->Common->getErrorMsgForDisplay($bookingSheet->errors());
                    $this->Flash->error($errorMsgs);
                } else {

                    $this->Flash->error('The booking sheet could not be updated. Please, try again.');
                }
            }
        }

        $courierList = $this->Couriers->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toArray();
        $this->set(compact('bookingSheet', 'courierList'));
        $this->set('_serialize', ['bookingSheet']);
    }

    /**
     * add_booking_sheet method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add_booking_sheet() {

        if ($this->request->is(['patch', 'post', 'put'])) {

            $invoiceIdsList = isset($this->request->data['booking_sheet_id']) ? $this->request->data['booking_sheet_id'] : array();
            if (is_array($invoiceIdsList)) {
                $invoiceIds = $invoiceIdsList;
            } else {
                if (strpos($invoiceIdsList, ',') !== false) {
                    $invoiceIds = explode(',', $invoiceIdsList);
                } else {
                    $invoiceIds = $invoiceIdsList;
                }
            }
            if (!empty($invoiceIds)) {

                $bookingSheetDoneInvoices = $this->Invoices->find('list', [
                            'keyField' => 'id',
                            'valueField' => 'invoice_number',
                            'conditions' => [
                                'Invoices.id IN' => $invoiceIds,
                                'Invoices.booking_sheet_generated' => Configure::read('booking_sheet_generated.yes')
                            ],
                            'contain' => ['InvoiceItems', 'PurchaseOrders']
                        ])->toArray();
                
                if (!empty($bookingSheetDoneInvoices)) {
                    $this->Flash->error('Booking Sheet already generated for ' . implode(',', $bookingSheetDoneInvoices));
                }
                
                $invoices = $this->Invoices->find('all', [
                            'conditions' => [
                                'Invoices.id IN' => $invoiceIds,
                                'Invoices.booking_sheet_generated' => Configure::read('booking_sheet_generated.no')
                            ],
                            'contain' => ['InvoiceItems', 'PurchaseOrders']
                        ])->toArray();

                $courierList = $this->Couriers->find('list', ['keyField' => 'id', 'valueField' => 'name'])->toArray();
                $this->set(compact('bookingSheet', 'invoices', 'courierList'));
                $this->set('_serialize', ['bookingSheet']);
            } else {

                $this->Flash->error('You can\'t generate Booking Sheet without select Invoice, Please select at least at list one Invoice');
                return $this->redirect(['controller' => 'DeliveryDockets', 'action' => 'generate_booking_sheet']);
            }
        } else {

            $this->Flash->error('You can\'t generate Booking Sheet without select Invoice, Please select at least at list one Invoice');
            return $this->redirect(['controller' => 'DeliveryDockets', 'action' => 'generate_booking_sheet']);
        }
    }

    /**
     * print_booking_sheets method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function print_booking_sheets() {

        if ($this->request->is(['patch', 'post', 'put'])) {

            $bookingSheetIds = isset($this->request->data['booking_sheets']) ? $this->request->data['booking_sheets'] : array();
            if (!empty($bookingSheetIds)) {

                $bookingSheets = $this->BookingSheets->find('all', [
                            'conditions' => ['BookingSheets.id IN' => $bookingSheetIds],
                            'contain' => ['Users', 'BookingSheetItems', 'Couriers', 'BookingSheetItems.Invoices', 'BookingSheetItems.Invoices.InvoiceItems',
                                'BookingSheetItems.PurchaseOrders' => [
                                    'strategy' => 'select',
                                    'queryBuilder' => function ($q) {
                                return $q->order(['PurchaseOrders.bill_to_name' => 'ASC']);
                            }
                                ]
                            ]
                        ])->toArray();

                if (!empty($bookingSheets)) {

                    // make delivery docket folder if not exist
                    $mkdir = $this->BookingSheets->makeDirectoryIfNotExist(Configure::read('booking-sheet.localPath'));

                    // generate delivery docket pdf & open that
                    $fileName = Configure::read('booking-sheet.localPath') . DS . 'booking_sheet';
                    $html = $this->BookingSheets->manageBookingSheetPdf($bookingSheets);
                    $papersize = "A4";
                    $orientation = 'portrait';
                    $openPdf = TRUE;
                    $streamFileName = 'Supplier Manifest Booking Sheet';
                    $pdfResult = $this->ManagePdf->generatePdf($fileName, $html, $papersize, $orientation, $openPdf, $streamFileName);
                    if ($pdfResult) {

                        $Sheet = 'Sheet';
                        if (count($bookingSheetIds) > 1) {
                            $Sheet = 'Sheets';
                        }

                        $this->Flash->success('Booking ' . $Sheet . ' created successfully');
                        $this->redirect(['action' => 'index']);
                    } else {

                        $this->Flash->error('Booking Sheet Print was not created, please try again');
                        $this->redirect(['action' => 'index']);
                    }
                } else {

                    $this->Flash->error('Booking Sheet not available for print, please try again');
                    $this->redirect(['action' => 'index']);
                }
            } else {

                $this->Flash->error('Booking Sheet not available for print, please try again');
                $this->redirect(['action' => 'index']);
            }
        } else {

            $this->Flash->error('You can\'t print Booking Sheet without select Booking Sheet, Please select at least at list one Booking Sheet');
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * email_booking_sheets method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function email_booking_sheets() {

        if ($this->request->is(['patch', 'post', 'put'])) {

            $bookingSheetIds = isset($this->request->data['BookingSheets']['id']) ? $this->request->data['BookingSheets']['id'] : array();
            $bookingSheetIds = array_filter(explode(',', $bookingSheetIds));
            if (!empty($bookingSheetIds)) {

                $bookingSheets = $this->BookingSheets->find('all', [
                            'conditions' => ['BookingSheets.id IN' => $bookingSheetIds],
                            'contain' => ['Users', 'BookingSheetItems', 'Couriers', 'BookingSheetItems.Invoices', 'BookingSheetItems.Invoices.InvoiceItems',
                                'BookingSheetItems.PurchaseOrders' => [
                                    'strategy' => 'select',
                                    'queryBuilder' => function ($q) {
                                return $q->order(['PurchaseOrders.bill_to_name' => 'ASC']);
                            }
                                ]
                            ]
                        ])->toArray();

                if (!empty($bookingSheets)) {

                    // make delivery docket folder if not exist
                    $mkdir = $this->BookingSheets->makeDirectoryIfNotExist(Configure::read('booking-sheet.email'));

                    $b = 0;
                    foreach ($bookingSheets as $bookingSheet) {

                        $singleBookingSheet = array();
                        $singleBookingSheet[] = $bookingSheet;

                        // generate delivery docket pdf & open that
                        $fileName = Configure::read('booking-sheet.email') . DS . $bookingSheet['id'] . '_booking_sheet';
                        $html = $this->BookingSheets->manageBookingSheetPdf($singleBookingSheet);
                        $papersize = "A3";
                        $orientation = 'portrait';
                        $openPdf = FALSE;
                        $pdfResult = $this->ManagePdf->generatePdf($fileName, $html, $papersize, $orientation, $openPdf);
                        if ($pdfResult) {
                            $b++;
                        }
                    }

                    if ($b > 0) {

                        $bookingSheetSendResult = 0;
                        foreach ($this->request->data['BookingSheetsEmail'] as $bookingSheetsEmail) {

                            $to = [$bookingSheetsEmail['email'] => $bookingSheetsEmail['name']];

                            foreach ($bookingSheetIds as $bookingSheetId) {

                                $attachments[$bookingSheetId . '_booking_sheet.pdf'] = Configure::read('booking-sheet.email') . DS . $bookingSheetId . '_booking_sheet.pdf';
                            }

                            // send Booking Sheet
                            $scanBookSendResult = $this->Email->generateEmailForBookingSheetAndSend($to, $attachments);
                            if ($scanBookSendResult > 0) {

                                $bookingSheetSendResult = $bookingSheetSendResult + 1;
                            }
                        }

                        if ($bookingSheetSendResult > 0) {

                            $Sheet = 'Sheet';
                            if (count($bookingSheetIds) > 1) {
                                $Sheet = 'Sheets';
                            }

                            $this->Flash->success('Booking ' . $Sheet . ' send successfully');
                            $this->redirect(['action' => 'index']);
                        } else {

                            $this->Flash->success('Booking ' . $Sheet . ' was generated but not send successfully, please try again');
                            $this->redirect(['action' => 'index']);
                        }
                    } else {

                        $this->Flash->error('Booking Sheet not generated for send, please try again');
                        $this->redirect(['action' => 'index']);
                    }
                } else {

                    $this->Flash->error('Booking Sheet not available for send, please try again');
                    $this->redirect(['action' => 'index']);
                }
            } else {

                $this->Flash->error('Booking Sheet not available for send, please try again');
                $this->redirect(['action' => 'index']);
            }
        } else {

            $this->Flash->error('You can\'t send Booking Sheet without select Booking Sheet, Please select at least at list one Booking Sheet');
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * Cancel booking sheet
     * @return type
     */
    public function cancel_booking_sheet() {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $updateInvoice = '';
            $bookingSheetIds = isset($this->request->data['booking_sheet_hidden_ids']) && !empty($this->request->data['booking_sheet_hidden_ids']) ? array_filter(explode(',', $this->request->data['booking_sheet_hidden_ids'])) : [];
            if (!empty($bookingSheetIds)) {
                $sheet = 'sheet';
                if (count($bookingSheetIds) > 1) {
                    $sheet = 'sheets';
                }
                if ($this->BookingSheets->cancelBookingSheets($bookingSheetIds)) {
                    $invoiceIds = $this->BookingSheetItems->getInvoiceIds($bookingSheetIds);
                    $this->BookingSheetItems->cancelBookingSheetItems($bookingSheetIds);
                    if ($invoiceIds) {
                        // update invoice record for delivery_docket_generated column
                        $updateInvoice = $this->Invoices->updateMultipleFields(array('booking_sheet_generated' => Configure::read('booking_sheet_generated.no')), array('id IN' => $invoiceIds));
                    }
                    if ($updateInvoice) {
                        $this->Flash->success('Booking ' . $sheet . ' cancel successfully');
                        $this->redirect(['action' => 'index']);
                    } else {
                        $this->Flash->success('Booking ' . $sheet . ' was not cancel successfully, please try again');
                        $this->redirect(['action' => 'index']);
                    }
                } else {
                    $this->Flash->success('Booking ' . $sheet . ' was not cancel successfully, please try again');
                    $this->redirect(['action' => 'index']);
                }
            } else {
                $this->Flash->error('Booking Sheet not available for send, please try again');
                $this->redirect(['action' => 'index']);
            }
        } else {

            $this->Flash->error('You can\'t Cancel Booking Sheet without select Booking Sheet, Please select at least at list one Booking Sheet');
            return $this->redirect(['action' => 'index']);
        }
    }

}
