<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * DeliveryDockets Controller
 *
 * @property \App\Model\Table\DeliveryDocketsTable $DeliveryDockets
 */
class DeliveryDocketsController extends AppController {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

        // load Component
        $this->loadComponent('ManagePdf');
        $this->loadComponent('Prg');

        // load Model
        $this->loadModel('Invoices');
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        
        $this->Prg->commonProcess();
        
        $this->paginate = [
            'sortWhitelist' => [
                'PurchaseOrders.bill_to_name', 'Invoices.invoice_number', 'PurchaseOrders.purchase_order_number',
                'DeliveryDockets.delivery_date', 'DeliveryDockets.created', 'DeliveryDockets.modified'
            ],
            'contain' => ['Users', 'Invoices', 'PurchaseOrders'],
            'order' => ['DeliveryDockets.created' => 'Desc']
        ];
        
        $this->set('deliveryDockets', $this->paginate($this->DeliveryDockets->find('searchable', $this->Prg->parsedParams())));
        $this->set('_serialize', ['deliveryDockets']);
    }

    /**
     * View method
     *
     * @param string|null $id Delivery Docket id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        
        $deliveryDocket = $this->DeliveryDockets->get($id, [
            'contain' => ['Users', 'Invoices', 'PurchaseOrders']
        ]);
        $this->set('deliveryDocket', $deliveryDocket);
        $this->set('_serialize', ['deliveryDocket']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Delivery Docket id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {
        
        if ($this->request->is(['patch', 'post', 'put'])) {

            $id = isset($this->request->data['DeliveryDockets']['id']) ? $this->request->data['DeliveryDockets']['id'] : '';
            $deliveryDocketArr = $this->DeliveryDockets->get($id, [
                'contain' => []
            ]);

            $deliveryDocket = $this->DeliveryDockets->patchEntity($deliveryDocketArr, $this->request->data);
            if ($this->DeliveryDockets->save($deliveryDocket)) {

                $this->Flash->success('The delivery docket date has been updated.');
                return $this->redirect(['action' => 'index']);
            } else {

                if (!empty($deliveryDocket->errors())) {
                    
                    $errorMsgs = $this->Common->getErrorMsgForDisplay($deliveryDocket->errors());
                    $this->Flash->error($errorMsgs);
                } else {
                    
                    $this->Flash->error('The delivery docket date could not be updated. Please, try again.');
                }
            }
        } else {

            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * print_delivery_dockets() method
     * This method prints delivery dockets
     */
    public function print_delivery_dockets() {

        if ($this->request->is(['patch', 'post', 'put'])) {
            $deliveryDocketIds = isset($this->request->data['delivery_dockets']) ? $this->request->data['delivery_dockets'] : array();
            if (!empty($deliveryDocketIds)) {

                $deliveryDocket = $this->DeliveryDockets->find('all', [
                            'conditions' => ['DeliveryDockets.id IN' => $deliveryDocketIds],
                            'contain' => ['Users', 'Invoices', 'Invoices.InvoiceItems', 'PurchaseOrders'],
                            'order' => ['PurchaseOrders.bill_to_name' => 'ASC']
                        ])->toArray();


                if (!empty($deliveryDocket)) {

                    // make delivery docket folder if not exist
                    $mkdir = $this->DeliveryDockets->makeDirectoryIfNotExist(Configure::read('dd.localPath'));

                    // generate delivery docket pdf & open that
                    $fileName = Configure::read('dd.localPath') . DS . 'delivery_dockets';
//                    echo '$deliveryDocket <pre>';print_r($deliveryDocket);exit;
                    $html = $this->DeliveryDockets->manageDeliveryDocketPdf($deliveryDocket);
                    $papersize = "A4";
                    $orientation = 'portrait';
                    $openPdf = TRUE;
//                    echo $html;exit;
                    $pdfResult = $this->ManagePdf->generatePdf($fileName, $html, $papersize, $orientation, $openPdf);
                    if ($pdfResult) {

                        $docket = 'Docket';
                        if (count($deliveryDocketIds) > 1) {
                            $docket = 'Dockets';
                        }
                        
                        // mark delivery docket as printed 1
                        $updateResult = $this->DeliveryDockets->updateFieldsByFields(array('is_printed_delivery_docket' => Configure::read('is_printed_delivery_docket.yes')), array('id IN' => $deliveryDocketIds));
                    
                        $this->Flash->success('Delivery ' . $docket . ' generated successfully');
                        $this->redirect(['action' => 'index']);
                    } else {

                        $this->Flash->error('Delivery Docket was not generated, please try again');
                        $this->redirect(['action' => 'index']);
                    }
                } else {

                    $this->Flash->error('Delivery Docket not available for print, please try again');
                    $this->redirect(['action' => 'index']);
                }
            } else {

                $this->Flash->error('Delivery Docket not available for print, please try again');
                $this->redirect(['action' => 'index']);
            }
        } else {

            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * booking_sheet() method
     * @result list of RECEIVED & delivery docket generated invoices
     */
    public function generate_booking_sheet() {

        $query = $this->Invoices->find('all', [
            'contain' => ['PurchaseOrders'],
            'conditions' => [
                'delivery_docket_generated' => Configure::read('delivery_docket_generated.yes'),
                'booking_sheet_generated' => Configure::read('booking_sheet_generated.no'),
//                'invoice_status' => Configure::read('invoice_ack_invoice_status.RECEIVED'),
            ],
            'order' => ['Invoices.modified' => 'DESC']
        ]);
        $this->set('invoices', $this->paginate($query));
        $this->set('_serialize', ['invoices']);
    }

}
