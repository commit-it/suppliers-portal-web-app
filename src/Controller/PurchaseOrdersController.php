<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * PurchaseOrders Controller
 *
 * @property \App\Model\Table\PurchaseOrdersTable $PurchaseOrders
 */
class PurchaseOrdersController extends AppController {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();
        
        // load Component
        $this->loadComponent('ManagePdf');
    }
    
    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        
        $passParams  = $this->request->query('pass'); 
        $selectedIds = isset($passParams) && !empty($passParams) ? base64_decode($passParams) : '';
        $selectedIdsArr = !empty($selectedIds) ? explode(',', $selectedIds) : [];
        
        $this->paginate = [
            'contain' => ['Currencies', 'PurchaseOrderStatuses', 'Invoices'],
            'conditions' => [
                'sent_to_myob' => Configure::read('sent_to_myob.no'),
                'sent_to_quick_book' => Configure::read('sent_to_quick_book.no'),
                'archived' => Configure::read('po_archived.no')
            ],
            'order' => ['PurchaseOrders.modified' => 'DESC']
        ];
        $this->set('selectedIdsArr', $selectedIdsArr);
        $this->set('purchaseOrders', $this->paginate($this->PurchaseOrders));
        $this->set('_serialize', ['purchaseOrders']);
    }
    
    /**
     * This method display Archived PO's
     */
    public function archived_purchase_orders() {
        
        $this->paginate = [
            'contain' => ['Currencies', 'PurchaseOrderStatuses', 'Invoices'],
            'conditions' => [
                'archived' => Configure::read('po_archived.yes')
            ],
            'order' => ['PurchaseOrders.modified' => 'DESC']
        ];
        $this->set('purchaseOrders', $this->paginate($this->PurchaseOrders));
        $this->set('_serialize', ['purchaseOrders']);
    }

    /**
     *  Sent Purchase Orders method
     * 
     *  @return void
     */
    public function sent_purchase_orders() {
        $this->paginate = [
            'contain' => ['Currencies', 'PurchaseOrderStatuses', 'Invoices'],
            'conditions' => [
                'OR' => [
                    'sent_to_myob' => Configure::read('sent_to_myob.yes'),
                    'sent_to_quick_book' => Configure::read('sent_to_quick_book.yes')
                ],
                'archived' => Configure::read('po_archived.no')
            ]
        ];
        $this->set('purchaseOrders', $this->paginate($this->PurchaseOrders));
        $this->set('_serialize', ['purchaseOrders']);
    }
    
    /**
     * View method
     *
     * @param string|null $id Purchase Order id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        
        $purchaseOrder = $this->PurchaseOrders->get($id, [
            'contain' => ['Currencies', 'PurchaseOrderStatuses', 'Invoices', 'PurchaseOrderItems']
        ]);
        
        $this->set('purchaseOrder', $purchaseOrder);
        $this->set('_serialize', ['purchaseOrder']);

        /* Paginate data for PurchaseOrderItems */
        $this->paginate = array(
            'PurchaseOrderItems' => array(
                'conditions' => array('PurchaseOrderItems.purchase_order_id' => $id),
                'order' => array('PurchaseOrderItems.description' => 'ASC')
            ),
        );
        $this->set('purchaseOrderItems', $this->paginate('PurchaseOrderItems'));
        $this->set('_serialize', ['purchaseOrderItems']);
    }

    /**
     * send_to_myob method
     */
    public function send_to_myob() {
        
        $responseArr = array();
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $poIdArr = isset($this->request->data['po_ids']) ? explode(',', $this->request->data['po_ids']) : array();
            if (!empty($poIdArr)) {
                
                $purchaseOrderMsg = array();
                $resultArr = $this->PurchaseOrders->purchaseOrderSendToMyob($poIdArr);
                foreach ($resultArr['response'] as $result) {
                    array_push($responseArr, $result);
                }
                
                $errorResponse = array();
                foreach ($resultArr['error'] as $errorMsg) {
                    array_push($errorResponse, $errorMsg);
                }
                
                $purchaseOrderMsg['success'] = $responseArr;
                $purchaseOrderMsg['error'] = $errorResponse;
                $this->Flash->purachaseordermsg('Purchase order message',['key' => 'purchase-order-flash','params' => $purchaseOrderMsg]);
                
            } else {
                $responseArr[] = 'Purchase order file is not selected for send to myob.';
                $this->Flash->purachaseordersuccess('Purchase order success message',['key' => 'purchase-order-flash','params' => $responseArr]);
            }
        } else {
            $responseArr[] = 'Getting some error, please try again.';
            $this->Flash->purachaseordererror('Purchase order error message',['key' => 'purchase-order-flash','params' => $responseArr]);
        }
        echo json_encode($responseArr);
        exit;
    }
    
    /**
     * print_purchase_orders - Method
     * @return type
     */
    public function print_purchase_orders() {
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $poIdArr = isset($this->request->data['purchase_order_id']) ? $this->request->data['purchase_order_id'] : array();
            if (!empty($poIdArr)) {
                
                $purchaseOrders = $this->PurchaseOrders->find('all', [
                    'conditions' => ['PurchaseOrders.id IN' => $poIdArr],
                    'contain' => ['Users', 
                        'PurchaseOrderItems' => [
                            'strategy' => 'select',
                            'queryBuilder' => function ($q) {
                                return $q->order(['description' =>'ASC']);
                            }
                        ]
                    ]
                ])->toArray();
//                echo '<PrE>';print_r($purchaseOrders);exit;
                if (!empty($purchaseOrders)) {
                    
                    // make purchase order folder if not exist
                    $mkdir = $this->PurchaseOrders->makeDirectoryIfNotExist(Configure::read('po.pdf-localPath'));
                    
                    // generate purchase order pdf & open that
                    $fileName = Configure::read('po.pdf-localPath') . DS . 'purchase_order';
                    $html = $this->PurchaseOrders->managePurchaseOrdersPdf($purchaseOrders);
                    $papersize = "A4";
                    $orientation = 'landscape';
                    $openPdf = TRUE;
                    $pdfResult = $this->ManagePdf->generatePdf($fileName,$html,$papersize,$orientation,$openPdf);
                    if ($pdfResult) {
                        
                        $orders = 'Order';
                        if (count($poIdArr) > 1) {
                            $orders = 'Orders';
                        }
                        
                        $this->Flash->success('Purchase ' . $orders . ' created successfully');
                        $this->redirect(['action' => 'index']);
                    } else {
                        
                        $this->Flash->error('Purchase Order Print was not created, please try again');
                        $this->redirect(['action' => 'index']);
                    }
                } else {
                    
                    $this->Flash->error('Purchase Orders not available for print, please try again');
                    $this->redirect(['action' => 'index']);
                }
            } else {
                
                $this->Flash->error('Selected Purchase Orders not available for print, please try again');
                $this->redirect(['action' => 'index']);
            }
        } else {
            
            $this->Flash->error('You can\'t print Purchase Orders without select Purchase Orders, Please select at least at list one Purchase Order');
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * set_actual_delivery_date_of_purchase_order - This method set actual delivery date
     * @return type
     */
    public function set_actual_delivery_date_of_purchase_order() {
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $postData = $this->request->data;
            $id = isset($postData['PurchaseOrder']['id']) && !empty($postData['PurchaseOrder']['id']) ? explode(',',  $postData['PurchaseOrder']['id']) : '';
            if (!empty($postData) && !empty($id)) {
                
                $updateArr['actual_delivery_date_updated'] = 1; 
                $updateArr['actual_delivery_date'] = $postData['PurchaseOrder']['actual_delivery_date']; 
                $this->PurchaseOrders->updateAllFieldsByFields($updateArr, array('id IN' => $id));
                $this->Flash->success('Actual Delivery Date updated successfully');
                return $this->redirect(['action' => 'index','?'=> array('page' => $postData['page'],'pass' => base64_encode(@$postData['PurchaseOrder']['id']))]);
            } else {
                
                $this->Flash->error('You can\'t set Actual Delivery Date without select Purchase Order');
                return $this->redirect(['action' => 'index']);
            }
        } else {
            
            $this->Flash->error('You can\'t set Actual Delivery Date without select Purchase Order');
            return $this->redirect(['action' => 'index']);
        }
    }
    
    /**
     * Check delivery Date
     */
    public function check_delivery_date() {        
        $response = false;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $poIdArr = isset($this->request->data['po_ids']) ? explode(',', $this->request->data['po_ids']) : array();
            $selectedActualDeliveryDate = isset($this->request->data['purchaseorderActualDeliveryDate']) ? $this->request->data['purchaseorderActualDeliveryDate'] : '';
            $fulfilmentAdviceReceivedDates =  $this->PurchaseOrders->getPurchaseOrders($poIdArr);
            
            $fulfilmentAdviceReceivedDate = isset($fulfilmentAdviceReceivedDates[0]) ? $fulfilmentAdviceReceivedDates[0] : ''; 
            if(!empty($fulfilmentAdviceReceivedDate)){
                if(strtotime($selectedActualDeliveryDate) > strtotime($fulfilmentAdviceReceivedDate)){
                    $response = true;
                }
            }
            
        } 
        echo $response;exit;
    }
    
    /**
     * ths method archive selected PO's
     */
    public function archive() {
        
        $responseArr = array();
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            $poIdArr = isset($this->request->data['po_ids']) ? explode(',', $this->request->data['po_ids']) : array();
            if (!empty($poIdArr)) {
                
                $updateArr['archived'] = Configure::read('po_archived.yes'); 
                $this->PurchaseOrders->updateAllFieldsByFields($updateArr, array('id IN' => $poIdArr));
                $orderStr = 'Order';
                if (count($poIdArr) > 1) {
                    $orderStr = 'Order\'s';
                }
                $this->Flash->success('Selected Purchase ' . $orderStr . ' Canceled successfully');
                return $this->redirect(['action' => 'index']);
            } else {
                
                $this->Flash->error('You can\'t Cancel Purchase Order without select Purchase Order');
                return $this->redirect(['action' => 'index']);
            }
            
        } else {
            
            $this->Flash->error('You can\'t Cancel Purchase Order without select Purchase Order');
            return $this->redirect(['action' => 'index']);
        }
    }
}
