<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Couriers Controller
 *
 * @property \App\Model\Table\CouriersTable $Couriers
 */
class CouriersController extends AppController
{
    
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $this->set('couriers', $this->paginate($this->Couriers));
        $this->set('_serialize', ['couriers']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $courier = $this->Couriers->newEntity();
        if ($this->request->is('post')) {
            
            /* Updaload courier_logo */
            if (isset($this->request->data['logo']['tmp_name']) && is_uploaded_file($this->request->data['logo']['tmp_name'])) {
                
                /* Upload new courier logo */
                $newUploadedCourierLogo = $this->Common->uploadImage($this->request->data['logo'], Configure::read('courier_logo_image_path'), 'courier_logo_');
                $resizeImage = $this->Common->smartResizeImage(Configure::read('courier_logo_image_path') . DS . $newUploadedCourierLogo, Configure::read('courier.logo_width'), Configure::read('courier.logo_height'));

                /* Save $newUploadedCourierLogo in database */
                $this->request->data['logo'] = $newUploadedCourierLogo;
            }
            
            $this->request->data['user_id'] = $this->Auth->user('id');
            $courier = $this->Couriers->patchEntity($courier, $this->request->data);
            if ($this->Couriers->save($courier)) {
                $this->Flash->success('The Courier has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                if (!empty($courier->errors())) {
                    
                    $errorMsgs = $this->Common->getErrorMsgForDisplay($courier->errors());
                    $this->Flash->error($errorMsgs);
                } else {

                    $this->Flash->error('The Courier could not be saved. Please, try again.');
                }
            }
        }
        
    }

    /**
     * Edit method
     *
     * @param string|null $id Courier id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $courier = $this->Couriers->get($id, [
            'contain' => []
        ]);
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            
            /* Updaload courier_logo */
            if (isset($this->request->data['courier_logo_new']['tmp_name']) && is_uploaded_file($this->request->data['courier_logo_new']['tmp_name'])) {
                
                /* Upload new courier logo */
                $newUploadedCourierLogo = $this->Common->uploadImage($this->request->data['courier_logo_new'], Configure::read('courier_logo_image_path'), 'courier_logo_');

                /* If logo upload and existing logo exist then remove existing uploaded logo */
                if ($newUploadedCourierLogo != '' && $this->request->data['logo'] != '') {
                    if (file_exists(Configure::read('courier_logo_image_path') . DS . $this->request->data['logo'])) {
                        unlink(Configure::read('courier_logo_image_path') . DS . $this->request->data['logo']);
                    }
                }

                $resizeImage = $this->Common->smartResizeImage(Configure::read('courier_logo_image_path') . DS . $newUploadedCourierLogo, Configure::read('courier.logo_width'), Configure::read('courier.logo_height'));

                /* Save $newUploadedCourierLogo in database */
                $this->request->data['logo'] = $newUploadedCourierLogo;
            }
            
            $courier = $this->Couriers->patchEntity($courier, $this->request->data);
            if ($this->Couriers->save($courier)) {
                $this->Flash->success('The Courier has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                if (!empty($courier->errors())) {
                    
                    $errorMsgs = $this->Common->getErrorMsgForDisplay($courier->errors());
                    $this->Flash->error($errorMsgs);
                } else {

                    $this->Flash->error('The Courier could not be updated. Please, try again.');
                }
            }
        }
        
        $courierLogoImgPath = '/img/no_image.png';
        if (file_exists(Configure::read('courier_logo_image_path') . DS . $courier->logo) && (!empty($courier->logo))) {
            $courierLogoImgPath = Configure::read('uploaded_courier_logo_image_path') . $courier->logo;
        }
        
        $this->set(compact('courier', 'courierLogoImgPath'));
        $this->set('_serialize', ['courier']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Courier id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        
        $courier = $this->Couriers->get($id);
        
        if ($courier->user_id != $this->Auth->user('id')) {
            $this->Flash->error('You don\'t have access to delete that Courier');
            return $this->redirect(['action' => 'index']);
        }
        
        if ($this->Couriers->delete($courier)) {
            $this->Flash->success('The Courier has been deleted.');
        } else {
            $this->Flash->error('The Courier could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
}
