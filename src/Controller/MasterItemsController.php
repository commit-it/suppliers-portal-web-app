<?php

namespace App\Controller;

use App\Controller\AppController;
use \Cake\Core\Configure;

/**
 * MasterItems Controller
 *
 * @property \App\Model\Table\MasterItemsTable $MasterItems
 */
class MasterItemsController extends AppController {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

        $this->loadComponent('PHPExcel');
        $this->loadComponent('Prg');
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {

        $this->Prg->commonProcess();
        $this->set('masterItems', $this->paginate($this->MasterItems->find('searchable', $this->Prg->parsedParams())));
        $this->set('_serialize', ['masterItems']);
    }

    /**
     * View method
     *
     * @param string|null $id Master Item id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $masterItem = $this->MasterItems->get($id);
        $this->set('masterItem', $masterItem);
        $this->set('_serialize', ['masterItem']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {

        $masterItem = $this->MasterItems->newEntity();
        if ($this->request->is('post')) {

            /* Check entered bar_code is unique or not */
            $isUniqueBarCode = $this->MasterItems->findByBarCode($this->request->data['bar_code'])->count();

            /* Save data only if bar_code is unique otherwise show error message */
            if ($isUniqueBarCode < 1) {

                $this->request->data['user_id'] = $this->Auth->user('id');
                $masterItem = $this->MasterItems->patchEntity($masterItem, $this->request->data);
                if ($this->MasterItems->save($masterItem)) {

                    $this->Flash->success('The master item has been saved.');
                    return $this->redirect(['action' => 'index']);
                } else {

                    if (!empty($masterItem->errors())) {

                        $errorMsgs = $this->Common->getErrorMsgForDisplay($masterItem->errors());
                        $this->Flash->error($errorMsgs);
                    } else {

                        $this->Flash->error('The master item could not be saved. Please, try again.');
                    }
                }
            } else {
                $this->Flash->error('Bar code is already exist, please use another bar code.');
            }
        }
        $this->set(compact('masterItem'));
        $this->set('_serialize', ['masterItem']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Master Item id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        $masterItem = $this->MasterItems->get($id, [

            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $masterItem = $this->MasterItems->patchEntity($masterItem, $this->request->data);
            if ($this->MasterItems->save($masterItem)) {

                $this->Flash->success('The master item has been saved.');
                return $this->redirect([ 'action' => 'index']);
            } else {

                if (!empty($masterItem->errors())) {

                    $errorMsgs = $this->Common->getErrorMsgForDisplay($masterItem->errors());
                    $this->Flash->error($errorMsgs);
                } else {

                    $this->Flash->error('The master item could not be updated. Please, try again.');
                }
            }
        }
        $this->set(compact('masterItem'));
        $this->set('_serialize', ['masterItem']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Master Item id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {

        $this->render('/Error/not_access_to_delete', 'custom_error');
    }

    /**
     * import_items method
     *
     * @param null.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function import_items() {

        if ($this->request->is(['patch', 'post', 'put'])) {

            $responseArr = array();

            // upload file
            $fileName = $this->Common->uploadFile(Configure::read('master-items.localPath') . DS . $this->Auth->user('id'), $this->request->data['master_items'], Configure::read('master-items.file-prefix'));

            if ($fileName != '') {

                $responseArr[] = $fileName . ' Master Item file uploaded successfully';

                /*
                 * Import Master Items file in table
                 */
                $excelFileName = Configure::read('master-items.localPath') . DS . $this->Auth->user('id') . DS . $fileName;

                // get Excel Data
                $dataArr = $this->PHPExcel->getExcelFileData($excelFileName);

                if ((!empty($dataArr) ) && (count($dataArr) > Configure::read('master-items.row_start_cnt_for_import'))) {

                    // check Excel file format is correct for import
                    $isAllowToImport = $this->Common->checkFileIsCorrectToImport($dataArr, 'master-items');

                    if ($isAllowToImport) {

                        $responseArr[] = (count($dataArr) - Configure::read('master-items.row_start_cnt_for_import')) . ' data is available for import in ' . $fileName;

                        // format array to store in master_items table
                        $dataArr = array_map('array_filter', $dataArr);
                        $dataArr = array_filter($dataArr);

                        $masterItemsData = $this->MasterItems->modifyMasterItemsArr($dataArr, $this->Auth->user('id'));

                        // save or update data
                        $saveCnt = 0;
                        foreach ($masterItemsData as $masterItem) {

                            $masterItem['bar_code'] = isset($masterItem['bar_code']) ? $masterItem['bar_code'] : '';
                            $article = $this->MasterItems->findByBarCode($masterItem['bar_code'])->first();

                            if (isset($article->id)) {

                                // update if master item is already exist for logged in user
                                $entity = $this->MasterItems->patchEntity($article, $masterItem);
                            } else {

                                // save master item
                                $entity = $this->MasterItems->newEntity($masterItem);
                            }
                            if (!$entity->errors()) {

                                $result = $this->MasterItems->save($entity);
                                if ($result->id) {
                                    $saveCnt++;
                                    $responseArr[] = $entity->bar_code . ' saved or updated successfully';
                                } else {
                                    $responseArr[] = $entity->bar_code . ' record is not saved';
                                }
                            } else {

                                $error = $entity->errors();
                                foreach ($error as $errorKey => $errorVal) {
                                    foreach ($errorVal as $val) {
                                        $responseArr[] = $entity->bar_code . ' record is not saved due to ' . $errorKey . ' = ' . $val;
                                    }
                                }
                            }
                        }

                        if ($saveCnt > 0) {

                            if ((count($dataArr) - Configure::read('master-items.row_start_cnt_for_import')) == $saveCnt) {

                                $this->Flash->success('All Master Items imported or updated successfully...');
                                $responseArr[] = 'All Master Items imported or updated successfully...';
                            } else {

                                $this->Flash->error('Only ' . $saveCnt . ' records inserted or updated from ' . (count($dataArr) - Configure::read('master-items.row_start_cnt_for_import')) . ', may be values are empty for some fields');
                                $responseArr[] = 'Only ' . $saveCnt . ' records inserted or updated from ' . (count($dataArr) - Configure::read('master-items.row_start_cnt_for_import')) . ', may be values are empty for some fields';
                            }

                            // move imported file in out folder
                            if ($this->MasterItems->moveFileInAnotherFolder($fileName, Configure::read('master-items.localPath') . DS . $this->Auth->user('id'), Configure::read('master-items.localPath-out') . DS . $this->Auth->user('id'))) {
                                $responseArr [] = $fileName . " file moved successfully in MasterItems In folder";
                            } else {

                                $responseArr[] = $fileName . " file not moved successfully in MasterItems In folder";
                            }
                        } else {

                            $this->Flash->error('Getting some error during import master items may be some fields are empty, please check you excel file & try agian...');
                            $responseArr[] = 'Data is not save for ' . $entity->bar_code;
                        }
                    } else {

                        $this->Flash->error("The uploaded format of excel file is not valid. Please click Acceptable Format button to see the acceptable format");
                        $responseArr[] = 'The uploaded format of excel file is not valid';
                    }
                } else {

                    $responseArr[] = 'data is not available for import in ' . $fileName;
                    $this->Flash->error('data is not available for import in ' . $fileName);
                }
            } else {

                $this->Flash->error('Getting some error during upload file, please try agian...');
            }

            return $this->redirect(['action' => 'index']);
        }
    }
    
    /**
     * exportToExcel - This method is used for export data in Excel
     */
    public function exportToExcel() {

        $masterItems = $this->MasterItems->find('all')->toArray();
        
        $filePath = Configure::read('master-items.localPath-export-to-excel');
        $filePathForOpen = Configure::read('master-items.localPath-export-to-excel-for-open');
        $headerCellArr = Configure::read('master-items.columns');
        $headerCellArr = array_flip($headerCellArr);
         
        $this->MasterItems->makeDirectoryIfNotExist($filePath);
        
        $fileName = 'Master_Items_' . $this->Auth->user('id') . '_' . $this->Auth->user('full_name') . '.xlsx';
        $file = $filePath . DS . $fileName;
        
        $exportFileResult = $this->PHPExcel->exportMasterItemDataToExcel($masterItems, $file, $headerCellArr);
        if ($exportFileResult) {

            // open generated excel file for download
            $this->redirect( $filePathForOpen . DS . $fileName );
        } else {
            
            $this->Flash->error('Some error occured during export data to Excel, please try again');
            return $this->redirect(['action' => 'index']);
        }
        
    }

}
