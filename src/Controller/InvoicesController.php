<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Invoices Controller
 *
 * @property \App\Model\Table\InvoicesTable $Invoices
 */
class InvoicesController extends AppController {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

        // load Model
        $this->loadModel('MasterItems');
        $this->loadModel('DeliveryDockets');
        $this->loadModel('InvoiceItems');
        $this->loadModel('Invoices');
        $this->loadModel('ProcessLogs');
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        $this->paginate = [
            'contain' => ['PurchaseOrders'],
            'conditions' => [
                'invoice_generated' => Configure::read('invoice_generated.no')
            ]
        ];
        $this->set('invoices', $this->paginate($this->Invoices));
        $this->set('_serialize', ['invoices']);
    }

    /**
     * Generated IInvoices method
     *
     * @return void
     */
    public function generated_invoices() {
        $this->paginate = [
            'contain' => ['PurchaseOrders', 'FileOverviewes', 'FileOverviewes.FileTypes'],
            'conditions' => ['invoice_generated' => Configure::read('invoice_generated.yes')],
            'order' => ['Invoices.modified' => 'DESC']
        ];
        $this->set('invoices', $this->paginate($this->Invoices));
        $this->set('_serialize', ['invoices']);
    }

    /**
     * View method
     *
     * @param string|null $id Invoice id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $invoice = $this->Invoices->get($id, [
            'contain' => ['InvoiceItems']
        ]);
        $this->set('invoice', $invoice);
        $this->set('_serialize', ['invoice']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Invoice id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null) {

        /* Get invoice records */
        $invoice = $this->Invoices->get($id, [
            'contain' => ['InvoiceItems']
        ]);

        $oldInvoice = $this->Invoices->get($id, ['contain' => ['InvoiceItems']]);

        /* Get master items */
        $masterItems = $this->MasterItems->getCurrentUserMasterItems($this->Auth->user('id'));
        $this->set('masterItems', htmlentities(json_encode($masterItems)));

        /* Create only mater items gtin number */
        $masterItemsGtinArr = implode(',', array_filter(array_map(function ($entry) {
                            return $entry->bar_code;
                        }, $masterItems), 'strlen'));
        $this->set('masterItemsGtinArr', $masterItemsGtinArr);

        if ($invoice->invoice_generated == Configure::read('invoice_generated.no')) {

            if ($this->request->is(['patch', 'post', 'put'])) {

                $purchaseOrderId = isset($this->request->data['purchase_order_id']) && $this->request->data['purchase_order_id'] > 0 ? $this->request->data['purchase_order_id'] : 0;

                /* Check if order_line_number and reference_purchase_order_line_number of invoice_items 
                 * then assign max order_line_number value to order_line_number and 
                 * assign max reference_purchase_order_line_number value to reference_purchase_order_line_number
                 */
                if (isset($this->request->data['invoice_items']) && !empty($this->request->data['invoice_items'])) {
                    $this->request->data['invoice_items'] = $this->InvoiceItems->getFormatedValuesForInvoiceItems($this->request->data['invoice_items'], $purchaseOrderId);
                }

                /* Update invoice and invoice items records */
                $invoice = $this->Invoices->patchEntity($invoice, $this->request->data);
                if ($this->Invoices->save($invoice)) {

                    /* Add newly added invoice items */
                    if (isset($this->request->data['invoice_items_new']) && !empty($this->request->data['invoice_items_new'])) {
                        $addInvoiceItemsResult = $this->InvoiceItems->addInvoiceItems($this->request->data['invoice_items_new']);
                    }

                    /* Delete incoice items */
                    if (isset($this->request->data['deleted_inovice_items']) && !empty($this->request->data['deleted_inovice_items'])) {
                        $exploadedDeletedInvoiceItemsIds = explode(',', $this->request->data['deleted_inovice_items']);
                        $invoiceItems = $this->InvoiceItems->deleteInvoiceItems($exploadedDeletedInvoiceItemsIds);
                    }

                    // add or update modified invoice & invoice_items in myob database
                    if ($this->Auth->user('invoice_generate_by') == Configure::read('invoice_generate_by.MYOB')) {

                        $invoiceId = isset($this->request->data['id']) ? $this->request->data['id'] : 0;
                        $myobInvoiceResult = $this->Invoices->AddOrUpdateModifiedInvoiceInMyob($invoiceId);
                    }

                    // update invoice to quick book also
                    $updateInvoiceResponse = $this->updateInvoiceToQuickBooks($oldInvoice, $id);
                    if ($updateInvoiceResponse['status'] == 'success') {

                        $conditionsArr = array('id' => $id);
                        $fieldsArr = array('invoice_update_to_quick_book' => Configure::read('invoice.update_to_quick_book.Yes'));
                        $this->Invoices->updateFieldsByFields($conditionsArr, $fieldsArr);

                        $this->Flash->success('The Invoice has been updated successfully.');
                    } else {

                        // update invoice for faild quick book
                        $conditionsArr = array('id' => $id);
                        $fieldsArr = array('invoice_update_to_quick_book' => Configure::read('invoice.update_to_quick_book.No'));
                        $this->Invoices->updateFieldsByFields($conditionsArr, $fieldsArr);

                        $this->Flash->error('Some error occurred during update Invoice to Quick-Books');
                    }

                    return $this->redirect(['action' => 'index']);
                } else {

                    if (!empty($invoice->errors())) {

                        $errorMsgs = $this->Common->getErrorMsgForDisplay($invoice->errors());
                        $this->Flash->error($errorMsgs);
                    } else {

                        $this->Flash->error('The invoice could not be updated. Please, try again.');
                    }
                }
            }
            $purchaseOrders = $this->Invoices->PurchaseOrders->find('list', ['limit' => 200]);
            $this->set(compact('invoice', 'purchaseOrders'));
            $this->set('_serialize', ['invoice']);
        } else {
            $this->Flash->error('You can\'t edit this invoice.');
            return $this->redirect(['action' => 'index']);
        }
    }

    /*
     * create invoice
     */

    public function create_invoice() {

        if ($this->request->is(['patch', 'post', 'put'])) {

            if (isset($this->request->data['invoice_id']) && !empty($this->request->data['invoice_id'])) {
                $result = $this->Invoices->generateInvoiceXmlFileAndSendToSftp($this->request->data['invoice_id']);
                $this->set('message', $result);
                $this->render('/Tests/index');
            } else {
                return $this->redirect(['action' => 'index']);
            }
        } else {
            return $this->redirect(['action' => 'index']);
        }
    }

    /**
     * generate_delivery_docket method
     * Save delivery dockets for selected invoices
     * @return error msg for error & redirect to Delivery Dockets list on success
     */
    public function generate_delivery_docket() {

        if ($this->request->is(['patch', 'post', 'put'])) {

            // generate delivery docket array from post data for save
            $deliveryDocketArr = $this->Invoices->DeliveryDockets->generateDeliveryDocketArr($this->request->data, $this->Auth->user('id'));
            if (!empty($deliveryDocketArr)) {

                $result = '';
                foreach ($deliveryDocketArr['DeliveryDocket'] as $deliveryDocket) {
                    if (isset($deliveryDocket['id'])) {
                        $deliveryDocketData = $this->Invoices->DeliveryDockets->get($deliveryDocket['id'], ['contain' => []]);
                        $deliveryDocketArticle = $this->Invoices->patchEntity($deliveryDocketData, $deliveryDocket);
                    } else {
                        $deliveryDocketArticle = $this->Invoices->DeliveryDockets->newEntity($deliveryDocket);
                    }
                    if (!$deliveryDocketArticle->errors()) {
                        $result = $this->Invoices->DeliveryDockets->save($deliveryDocketArticle);
                    }
                }

                if ($result) {

                    // update invoice record for delivery_docket_generated column
                    $this->Invoices->updateAll(
                            array('delivery_docket_generated' => Configure::read('delivery_docket_generated.yes')), array('id IN' => $deliveryDocketArr['InvoiceIds'])
                    );

                    $this->Flash->success('Delivery Dockets generated successfully for selected invoices');
                    return $this->redirect(['controller' => 'DeliveryDockets', 'action' => 'index']);
                } else {

                    $this->Flash->error('Delivery Dockets does not generated successfully, please try again');
                    return $this->redirect(['action' => 'generated_invoices']);
                }
            } else {

                $this->Flash->error('Getting some error, please try again');
                return $this->redirect(['action' => 'generated_invoices']);
            }
        } else {

            return $this->redirect(['action' => 'generated_invoices']);
        }
    }

    /**
     * update_regenerate_invoice - method
     * @param type $id
     * @return type
     */
    public function update_regenerate_invoice($id = null) {

        /* Get invoice records */
        $invoice = $this->Invoices->get($id, [
            'contain' => ['InvoiceItems']
        ]);

        $oldInvoice = $this->Invoices->get($id, ['contain' => ['InvoiceItems']]);

        /* Get master items */
        $masterItems = $this->MasterItems->getCurrentUserMasterItems($this->Auth->user('id'));
        $this->set('masterItems', htmlentities(json_encode($masterItems)));

        /* Create only mater items gtin number */
        $masterItemsGtinArr = implode(',', array_filter(array_map(function ($entry) {
                            return $entry->bar_code;
                        }, $masterItems), 'strlen'));
        $this->set('masterItemsGtinArr', $masterItemsGtinArr);


        if ($this->request->is(['patch', 'post', 'put'])) {
            $purchaseOrderId = isset($this->request->data['purchase_order_id']) && $this->request->data['purchase_order_id'] > 0 ? $this->request->data['purchase_order_id'] : 0;

            /* Check if order_line_number and reference_purchase_order_line_number of invoice_items 
             * then assign max order_line_number value to order_line_number and 
             * assign max reference_purchase_order_line_number value to reference_purchase_order_line_number
             */
            if (isset($this->request->data['invoice_items']) && !empty($this->request->data['invoice_items'])) {
                $this->request->data['invoice_items'] = $this->InvoiceItems->getFormatedValuesForInvoiceItems($this->request->data['invoice_items'], $purchaseOrderId);
            }

            /* Update invoice and invoice items records */
            $invoice = $this->Invoices->patchEntity($invoice, $this->request->data);
            if ($this->Invoices->save($invoice)) {

                /* Add newly added invoice items */
                if (isset($this->request->data['invoice_items_new']) && !empty($this->request->data['invoice_items_new'])) {
                    $addInvoiceItemsResult = $this->InvoiceItems->addInvoiceItems($this->request->data['invoice_items_new']);
                }

                /* Delete incoice items */
                if (isset($this->request->data['deleted_inovice_items']) && !empty($this->request->data['deleted_inovice_items'])) {
                    $exploadedDeletedInvoiceItemsIds = explode(',', $this->request->data['deleted_inovice_items']);
                    $invoiceItems = $this->InvoiceItems->deleteInvoiceItems($exploadedDeletedInvoiceItemsIds);
                }

                // update invoice to quick book also
                $updateInvoiceResponse = $this->updateInvoiceToQuickBooks($oldInvoice, $id);
                if ($updateInvoiceResponse['status'] == 'success') {

                    $invoiceIdArr = array($id);
                    $result = $this->Invoices->generateInvoiceXmlFileAndSendToSftp($invoiceIdArr, Configure::read('invoice_generated.yes'));

                    $conditionsArr = array('id' => $id);
                    $fieldsArr = array('invoice_update_to_quick_book' => Configure::read('invoice.update_to_quick_book.Yes'));
                    $this->Invoices->updateFieldsByFields($conditionsArr, $fieldsArr);

                    $this->Flash->success('The invoice has been updated saved.');
                    $this->set('message', $result);
                } else {

                    // update invoice for faild quick book
                    $conditionsArr = array('id' => $id);
                    $fieldsArr = array('invoice_update_to_quick_book' => Configure::read('invoice.update_to_quick_book.No'));
                    $this->Invoices->updateFieldsByFields($conditionsArr, $fieldsArr);

                    $this->Flash->error('Some error occurred during update Invoice to Quick-Books');
                }
                $this->render('/Tests/index');
            } else {

                if (!empty($invoice->errors())) {

                    $errorMsgs = $this->Common->getErrorMsgForDisplay($invoice->errors());
                    $this->Flash->error($errorMsgs);
                } else {

                    $this->Flash->error('The invoice could not be updated. Please, try again.');
                }
            }
        }

        $purchaseOrders = $this->Invoices->PurchaseOrders->find('list', ['limit' => 200]);
        $this->set(compact('invoice', 'purchaseOrders'));
        $this->set('_serialize', ['invoice']);
    }

    /**
     * This method update failed invoice to quick book
     * @param type $invoiceId
     * @param type $action
     * @return type
     */
    public function resend_failed_invoice_to_quick_book($invoiceId = NULL, $action = 'index') {

        if ($invoiceId) {

            $updateInvoiceResponse = $this->updateFailedInvoiceToQuickBooks($invoiceId);
            if ($updateInvoiceResponse['status'] == 'success') {

                $conditionsArr = array('id' => $invoiceId);
                $fieldsArr = array('invoice_update_to_quick_book' => Configure::read('invoice.update_to_quick_book.Yes'));
                $this->Invoices->updateFieldsByFields($conditionsArr, $fieldsArr);
                $this->Flash->success('The invoice has been updated successfully to Quick-Books.');
            } else {

                // update invoice for faild quick book
                $conditionsArr = array('id' => $invoiceId);
                $fieldsArr = array('invoice_update_to_quick_book' => Configure::read('invoice.update_to_quick_book.No'));
                $this->Invoices->updateFieldsByFields($conditionsArr, $fieldsArr);
                $this->Flash->error('Some error occurred during update Invoice to Quick-Books');
            }
        } else {

            $this->Flash->error('Invoice is not available in database please select correct invoice to update');
        }
        return $this->redirect(['action' => $action]);
    }

    /**
     * updateInvoiceToQuickBooks - This moethod update invoice to Quick-Books
     * @param type $invoice
     * @param type $id
     * @return type
     */
    public function updateInvoiceToQuickBooks($invoice, $id) {

        if ($this->Auth->user('invoice_generate_by') == Configure::read('invoice_generate_by.Quick-Books')) {

            // load QuickBook Component
            $this->loadComponent('QuickBook');

            $modifiedInvoice = $this->Invoices->get($id, ['contain' => ['InvoiceItems', 'PurchaseOrders']]);

            $processLogResultArr = array();
            for ($i = 0; $i < count($modifiedInvoice['invoice_items']); $i++) {

                // check Newly added item available to Quick-Books, otherwise add
                $masterItemData = $this->MasterItems->findDataByGtin($modifiedInvoice['invoice_items'][$i]['gtin']);
                if (!empty($masterItemData)) {

                    $listOfExistMasterItems = array();
                    $listOfExistMasterItems[] = $masterItemData;
                    $invoiceItemsResponse = $this->QuickBook->checkItemAvailableToQuickBookOtherWiseCreate($listOfExistMasterItems);

                    if (count($invoiceItemsResponse['error']) < 1) {

                        foreach ($invoiceItemsResponse['success'] as $itemResponseSuccess) {

                            $processLogResultArr[] = $itemResponseSuccess;
                        }
                    } else {

                        foreach ($invoiceItemsResponse['error'] as $invoiceItemsResponseError) {

                            $processLogResultArr[] = $invoiceItemsResponseError;
                        }
                    }
                }

                $masterItemData = $this->MasterItems->findDataByGtin($modifiedInvoice['invoice_items'][$i]['gtin']);
                if (!empty($masterItemData)) {

                    $modifiedInvoice['invoice_items'][$i]['quick_book_item_id'] = $masterItemData->quick_book_item_id;
                    $modifiedInvoice['invoice_items'][$i]['description'] = $masterItemData->description;
                }
            }

            for ($j = 0; $j < count($invoice['invoice_items']); $j++) {

                $masterItemData = $this->MasterItems->findDataByGtin($invoice['invoice_items'][$j]['gtin']);
                if (!empty($masterItemData)) {

                    $invoice['invoice_items'][$j]['quick_book_item_id'] = $masterItemData->quick_book_item_id;
                    $invoice['invoice_items'][$j]['description'] = $masterItemData->description;
                }
            }

            $updateInvoiceResponse = $this->QuickBook->updateInvoice($modifiedInvoice, $invoice);
            if ($updateInvoiceResponse['status'] == 'success') {

                // update invoice items
                $invoiceUpdateResponse = $this->Invoices->updateInvoiceItemsDataModifedByQuickBook($updateInvoiceResponse['response'], $modifiedInvoice);
                $processLogResultArr[] = isset($updateInvoiceResponse['msg']) ? $updateInvoiceResponse['msg'] : 'Invoice updated successfully to Quick-Books';
            } else {

                $processLogResultArr[] = isset($updateInvoiceResponse['msg']) ? $updateInvoiceResponse['msg'] : 'Some error occurred during update Invoice to Quick-Books, please try again';
            }

            if (!empty($processLogResultArr)) {

                // store process log details
                $userId = isset($modifiedInvoice['user_id']) ? $modifiedInvoice['user_id'] : null;
                $sellerGln = isset($modifiedInvoice['purchase_order']['seller_gln']) ? $modifiedInvoice['purchase_order']['seller_gln'] : null;
                $purchaseOrderNumber = isset($modifiedInvoice['purchase_order_number']) ? $modifiedInvoice['purchase_order_number'] : null;
                $processLogResult = $this->ProcessLogs->generateProcessLogArrAndSave($processLogResultArr, $userId, $sellerGln, $purchaseOrderNumber);
            }

            return $updateInvoiceResponse;
        } else {

            return TRUE;
        }
    }

    /**
     * updateFailedInvoiceToQuickBooks - This moethod update failed invoice to Quick-Books
     * @param type $invoice
     * @param type $id
     * @return type
     */
    public function updateFailedInvoiceToQuickBooks($id) {

        if ($this->Auth->user('invoice_generate_by') == Configure::read('invoice_generate_by.Quick-Books')) {

            // load QuickBook Component
            $this->loadComponent('QuickBook');

            $modifiedInvoice = $this->Invoices->get($id, ['contain' => ['InvoiceItems', 'PurchaseOrders']]);

            $processLogResultArr = array();
            for ($i = 0; $i < count($modifiedInvoice['invoice_items']); $i++) {

                // check Newly added item available to Quick-Books, otherwise add
                $masterItemData = $this->MasterItems->findDataByGtin($modifiedInvoice['invoice_items'][$i]['gtin']);
                if (!empty($masterItemData)) {

                    $listOfExistMasterItems = array();
                    $listOfExistMasterItems[] = $masterItemData;
                    $invoiceItemsResponse = $this->QuickBook->checkItemAvailableToQuickBookOtherWiseCreate($listOfExistMasterItems);

                    if (count($invoiceItemsResponse['error']) < 1) {

                        foreach ($invoiceItemsResponse['success'] as $itemResponseSuccess) {

                            $processLogResultArr[] = $itemResponseSuccess;
                        }
                    } else {

                        foreach ($invoiceItemsResponse['error'] as $invoiceItemsResponseError) {

                            $processLogResultArr[] = $invoiceItemsResponseError;
                        }
                    }
                }

                $masterItemData = $this->MasterItems->findDataByGtin($modifiedInvoice['invoice_items'][$i]['gtin']);
                if (!empty($masterItemData)) {

                    $modifiedInvoice['invoice_items'][$i]['quick_book_item_id'] = $masterItemData->quick_book_item_id;
                    $modifiedInvoice['invoice_items'][$i]['description'] = $masterItemData->description;
                }
            }

            $updateInvoiceResponse = $this->QuickBook->updateFailedInvoice($modifiedInvoice);
            if ($updateInvoiceResponse['status'] == 'success') {

                // update invoice items
                $invoiceUpdateResponse = $this->Invoices->updateInvoiceItemsDataModifedByQuickBook($updateInvoiceResponse['response'], $modifiedInvoice);
                $processLogResultArr[] = isset($updateInvoiceResponse['msg']) ? $updateInvoiceResponse['msg'] : 'Invoice updated successfully to Quick-Books';
            } else {

                $processLogResultArr[] = isset($updateInvoiceResponse['msg']) ? $updateInvoiceResponse['msg'] : 'Some error occurred during update Invoice to Quick-Books, please try again';
            }

            if (!empty($processLogResultArr)) {

                // store process log details
                $userId = isset($modifiedInvoice['user_id']) ? $modifiedInvoice['user_id'] : null;
                $sellerGln = isset($modifiedInvoice['purchase_order']['seller_gln']) ? $modifiedInvoice['purchase_order']['seller_gln'] : null;
                $purchaseOrderNumber = isset($modifiedInvoice['purchase_order_number']) ? $modifiedInvoice['purchase_order_number'] : null;
                $processLogResult = $this->ProcessLogs->generateProcessLogArrAndSave($processLogResultArr, $userId, $sellerGln, $purchaseOrderNumber);
            }

            return $updateInvoiceResponse;
        } else {

            return TRUE;
        }
    }

    /**
     * 
     * @param type $invoice_id
     * 
     */
    public function resend_invoice($invoice_id) {
        if (isset($invoice_id) && !empty($invoice_id)) {
            $result = $this->Invoices->resendGenerateInvoiceXmlFileAndSendToSftp($invoice_id);
            $this->set('message', $result);
            $this->set('return_invoice', 1);
            $this->render('/Tests/index');
        } else {
            $this->set('message', 'Invoice ID Not set');
            $this->set('return_invoice', 1);
            $this->render('/Tests/index');
        }
    }

}
