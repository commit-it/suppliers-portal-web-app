<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * FileOverviewes Controller
 *
 * @property \App\Model\Table\FileOverviewesTable $FileOverviewes
 */
class FileOverviewesController extends AppController {

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        $this->paginate = [
            'sortWhitelist' => [
                'FileTypes.type', 
                'FileStatuses.status', 
                'FileOverviewes.file_name', 'FileOverviewes.created'
            ],
            'contain' => ['FileTypes', 'FileStatuses']
        ];
        $this->set('fileOverviewes', $this->paginate($this->FileOverviewes));
        $this->set('_serialize', ['fileOverviewes']);
    }

    /**
     * View method
     *
     * @param string|null $id File Overviewe id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $fileOverviewe = $this->FileOverviewes->get($id, [
            'contain' => ['FileTypes', 'FileStatuses']
        ]);
        $this->set('fileOverviewe', $fileOverviewe);
        $this->set('_serialize', ['fileOverviewe']);
    }

    /**
     * read_file method
     *
     * @param string|null $id File Overviewe id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function read_file($id = null) {

        $fileOverviewe = $this->FileOverviewes->get($id, [
            'contain' => []
        ]);

        $fileName = isset($fileOverviewe->file_name) ? $fileOverviewe->file_name : '';
        $responseArr = array();
        $mainResponseArr = array();
        
        if ($this->FileOverviewes->checkFileIsExist(Configure::read('po.localPath-ack') . DS . $fileName)) {
            $filePath = 'localPath-ack';
        } elseif ($this->FileOverviewes->checkFileIsExist(Configure::read('po.localPath-ack-out') . DS . $fileName)) {
            $filePath = 'localPath-ack-out';
        }
        
        if ($this->FileOverviewes->checkFileIsExist(Configure::read('po.localPath-ack') . DS . $fileName) || $this->FileOverviewes->checkFileIsExist(Configure::read('po.localPath-ack-out') . DS . $fileName)) {
            
            $xmlArray = \Cake\Utility\Xml::toArray(\Cake\Utility\Xml::build(Configure::read('po.' . $filePath) . DS . $fileName));
            if (!empty($xmlArray)) {
                
                $responseArr = $xmlArray;
                if (isset($xmlArray['StandardBusinessDocument']['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['sBDHApplicationReceiptAcknowledgement'])) {
                    
                    $mainResponseArr = $xmlArray['StandardBusinessDocument']['eanucc:message']['eanucc:transaction']['command']['eanucc:documentCommand']['documentCommandOperand']['eanucc:applicationReceiptAcknowledgement']['sBDHApplicationReceiptAcknowledgement'];
                }
            }
        }
        
        $this->set(compact('fileOverviewe', 'responseArr', 'mainResponseArr'));
        
    }

}
