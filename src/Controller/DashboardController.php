<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Dashbord Controller
 */
class DashboardController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadModel('ScanBooks');
        $this->loadModel('PurchaseOrders');
        $this->loadModel('Invoices');
        $this->loadModel('Payments');
        $this->loadModel('Users');
        $this->loadModel('PaymentHistories');
        $this->loadModel('ProcessLogs');
        $this->loadComponent('Stripe');
    }

    private $payment_customer_id = null;

    /**
     * Index method
     *
     * @return void
     */
    public function index() {

        /* Get total send scan book */
        $totalSendScanBook = $this->ScanBooks->getTotalScanBookCount();
        $this->set('totalSendScanBook', $totalSendScanBook);

        /* Get Availabe Purchase Orders Count */
        $totalAvailabelPurchaseOrders = $this->PurchaseOrders->getAvailabelPurchaseOrdersCount();
        $this->set('totalAvailabelPurchaseOrders', $totalAvailabelPurchaseOrders);

        /* Get Availabe Invoices Count */
        $totalAvailabelInvoices = $this->Invoices->getAvailabelInvoicesCount();
        $this->set('totalAvailabelInvoices', $totalAvailabelInvoices);

        /* Get Received Invoices Count */
        $totalReceivedInvoices = $this->Invoices->getReceivedInvoicesCount();
        $this->set('totalReceivedInvoices', $totalReceivedInvoices);
    }

    /**
     * package - Method
     */
    public function package() {

        // check allow to access package url or not
        if (($this->Auth->user('role_name') != \App\Model\Table\UserRolesTable::Admin)) {
            $checkAllowToAccess = $this->Users->checkAllowtoAccessPackageUrl($this->Auth->user('id'));
            if ($checkAllowToAccess < 1) {
                $this->Flash->error('Your don\'t have access to open that page');
                return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
            }
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            //TODO: Get payment table records base on posted email address
            // if email & customer id is exist then post to retireve customer method
            $payment['customer_id'] = $this->Payments->checkIfFieldIsExist($this->Payments->findByUserId($this->Auth->user('id'))->first(), 'customer_id');
            try {

                if (isset($payment['customer_id']) && !empty($payment['customer_id'])) {

                    $postArr = $this->request->data;
                    $postArr['customer_id'] = $payment['customer_id'];
                    
                    // update customer details
                    $updateCustomerResponse = $this->Stripe->updateCustomerData($postArr);
                    
                    // create subscription if the payment_status is cancel
                    $paymentStatus = $this->Users->checkIfFieldIsExist($this->Users->findById($this->Auth->user('id'))->first(), 'payment_status');
                    if ($paymentStatus == Configure::read('payment_status.main.cancel')) {

                        $response = $this->Stripe->createSubscription($postArr);
                        if (!is_string($response)) {

                            // modified & save array & save data in table
                            $paymentId = $this->Payments->checkIfFieldIsExist($this->Payments->findByCustomerId($payment['customer_id'])->first(), 'id');

                            $paymentHistory = $this->PaymentHistories->find('all', [
                                'conditions' => ['payment_id' => $paymentId],
                                'order' => ['interval_end_date' => 'DESC'],
                            ])->first();

                            $emailId = isset($paymentHistory->email) ? $paymentHistory->email : '';

                            $paymentData = $this->Payments->modifiedSubscriptionCreateResponseForPaymentHistories($response, $paymentId, $emailId);
                            $paymentHistoriesEntity = $this->PaymentHistories->newEntity($paymentData);
                            if ($this->PaymentHistories->save($paymentHistoriesEntity)) {

                                if ($paymentData['status'] == Configure::read('payment.status.active')) {

                                    // active user & status is 
                                    $updateArr = array(
                                        'access_status' => Configure::read('access_status.main.active'),
                                        'payment_status' => Configure::read('payment_status.main.paid'),
                                        'current_subscription_end_date' => $paymentData['interval_end_date']
                                    );
                                    $conditionArr = array(
                                        'id' => $this->Auth->user('id')
                                    );

                                    $userActiveResult = $this->Users->updateFieldsByCondition($updateArr, $conditionArr);
                                    if ($userActiveResult) {

                                        $this->Flash->success('Your payment has been done successfully & you can access site now');
                                        return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
                                    } else {

                                        $this->Flash->error('Your status is not activate for access site');
                                    }
                                } else {

                                    $this->Flash->error('Your Selected Package is failed');
                                }
                            } else {

                                $this->Flash->error('Payment details does not saved successfully');
                            }

                        } else {
                            $this->Flash->error('Your Payment is failed due to some error');
                        }
                    } else {
                        $this->Flash->error('Your Payment Status is not Cancel');
                    }
                } else {

                    $response = $this->Stripe->customerCreate($this->request->data);
                    if (!is_string($response)) {

                        // save data in table
                        $paymentData = $this->Payments->modifiedResponseForSave($response, $this->Auth->user('id'));
                        $entity = $this->Payments->newEntity($paymentData, [
                            'associated' => ['PaymentHistories']
                        ]);
                        $entity = $this->Payments->newEntity($paymentData);
                        if ($this->Payments->save($entity)) {

                            if ($paymentData['payment_histories'][0]['status'] == Configure::read('payment.status.active')) {

                                // active user & status is 
                                $updateArr = array(
                                    'access_status' => Configure::read('access_status.main.active'),
                                    'payment_status' => Configure::read('payment_status.main.paid'),
                                    'current_subscription_end_date' => $paymentData['payment_histories'][0]['interval_end_date']
                                );
                                $conditionArr = array(
                                    'id' => $this->Auth->user('id')
                                );

                                $userActiveResult = $this->Users->updateFieldsByCondition($updateArr, $conditionArr);
                                if ($userActiveResult) {

                                    $this->Flash->success('Your payment has been done successfully & you can access site now');
                                    return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
                                } else {

                                    $this->Flash->error('Your status is not activate for access site');
                                }
                            } else {

                                $this->Flash->error('Your Payment is failed');
                            }
                        } else {

                            $this->Flash->error('Payment details does not saved successfully');
                        }
                    } else {

                        $this->Flash->error('Your Payment is failed due to some error');
                    }
                }
            } catch (Exception $exc) {

                $this->Flash->error($exc->getMessage());
            }
        }
    }

    /*
     * Payment Response method
     */

    public function payment_response() {

        $this->key = Configure::read('Stripe.' . Configure::read('Stripe.mode') . '_secret_key');
        \Stripe\Stripe::setApiKey($this->key);
        $body = file_get_contents('php://input');
        $response = json_decode($body);
        $responseType = isset($response->type) ? $response->type : '';
        $currentDate = date('Y-m-d');
        $processLogArr = array();
        $customerId = isset($response->data->object->customer) ? $response->data->object->customer : '';
        $subscriptionId = isset($response->data->object->lines->data[0]->id) ? $response->data->object->lines->data[0]->id : '';

        /* Get user information */
        $userInfo = $this->Payments->findRecordByCustomerId($customerId);

        if (isset($userInfo) && !empty($userInfo)) {
            /* Check Stripe response and process as per that response */
            switch ($responseType) {
                case Configure::read('stripe_response.payment_succeeded'):
                    $result = $this->PaymentHistories->savePaymentHistory($response, $userInfo);
                    if ($result) {

                        $userId = isset($userInfo->user->id) ? $userInfo->user->id : '';
                        $currentSubscriptionEndDate = isset($response->data->object->period_end) ? date('Y-m-d', $response->data->object->period_end) : '';
                        $updateArr = array(
                            'current_subscription_end_date' => $currentSubscriptionEndDate
                        );
                        $conditionArr = array(
                            'id' => $userId
                        );

                        $userActiveResult = $this->Users->updateFieldsByCondition($updateArr, $conditionArr);
                        if ($userActiveResult) {

                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($currentDate . " - " . 'Your payment has been done successfully');
                        } else {

                            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($currentDate . " - " . 'Your status is not activate for access site');
                        }

                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($currentDate . " - " . Configure::read('stripe_response.payment_succeeded') . ": Payment History Saved Successfully.");
                    } else {
                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($currentDate . " - " . Configure::read('stripe_response.payment_succeeded') . ": Payment History Could not be Saved.");
                    }
                    break;
                case Configure::read('stripe_response.payment_failed'):
                    $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($currentDate . " - " . Configure::read('stripe_response.payment_failed') . ": Your payment has been failed.");
                    $result = $this->Stripe->subscription_cancelled($customerId, $subscriptionId);
                    if ($result) {
                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($currentDate . " - " . Configure::read('stripe_response.payment_failed') . ": Your subscription has been cancelled Successfully.");
                    } else {
                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($currentDate . " - " . Configure::read('stripe_response.payment_failed') . ": Your subscription could not be cancelled.");
                    }
                    break;
                case Configure::read('stripe_response.subscription_deleted'):
                    $result = $this->Users->updateUsersInfo($userInfo);
                    if ($result) {
                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($currentDate . " - " . Configure::read('stripe_response.subscription_deleted') . ": Users Updated Successfully.");
                    } else {
                        $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($currentDate . " - " . Configure::read('stripe_response.subscription_deleted') . ": Users Could not be Updated.");
                    }
                    break;
                default:
            }
        } else {
            $processLogArr[] = $this->ProcessLogs->generateProcessLogArr($currentDate . "-" . $responseType . " : customer_id not be found.");
        }
        // save process logs
        if (!empty($processLogArr)) {
            $processLogResult = $this->ProcessLogs->saveProcessLogs($processLogArr);
        }
        echo '<pre>';
        print_r($processLogArr);
        exit;
    }

}
