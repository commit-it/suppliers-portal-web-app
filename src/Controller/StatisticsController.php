<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Statistics Controller
 *
 * @property \App\Model\Table\StatisticsTable $Statistics
 */
class StatisticsController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        
        // check logged in user is admin
        if(!$this->Common->checkUserRole()) {
            $this->Flash->error('You don\'t have access for this action');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
        
        $year = date('Y');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $year = isset($this->request->data['year']) ? $this->request->data['year'] : date('Y');
        }
        
        $statisticsData = $this->Statistics->find('all', [
            'fields' => ['month_list' => 'GROUP_CONCAT(Statistics.month)', 'count_list' => 'GROUP_CONCAT(Statistics.count)', 'Statistics.user_id', 'Users.full_name'],
            'conditions' => ['year' => $year],
            'contain' => ['Users']
        ])->group('user_id')->toArray();
        
        if (!empty($statisticsData)) {
            // modify array for display
            $statisticsData = $this->Statistics->modifyArrForDispaly($statisticsData);
        }
           
        $yearList = $this->Statistics->find('list', ['keyField' => 'year', 'valueField' => 'year'])->group('year')->toArray();
        $this->set('statistics', $statisticsData);
        $this->set('yearList', array_filter($yearList));
        $this->set('year', $year);
    }

}
