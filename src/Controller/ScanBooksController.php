<?php

namespace App\Controller;

use App\Controller\AppController;
use \Cake\Core\Configure;

/**
 * ScanBooks Controller
 *
 * @property \App\Model\Table\ScanBooksTable $ScanBooks
 */
class ScanBooksController extends AppController {

    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        $this->paginate = [
            'contain' => ['Users', 'BunningStores']
        ];
        $this->set('scanBooks', $this->paginate($this->ScanBooks));
        $this->set('_serialize', ['scanBooks']);
    }

    /**
     * View method
     *
     * @param string|null $id Scan Book id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $scanBook = $this->ScanBooks->get($id, [
            'contain' => ['Users', 'BunningStores']
        ]);
        $this->set('scanBook', $scanBook);
        $this->set('_serialize', ['scanBook']);
    }

    /**
     * scan_book method
     *
     * @return upload scan_book file and return result.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function scan_book() {

        if ($this->request->is(['patch', 'post', 'put'])) {

            $responseArr = array();

            // upload file
            $fileName = $this->Common->uploadFile(Configure::read('scan-book.file-path'), $this->request->data['scanbook'], Configure::read('scan-book.file-prefix'));

            if ($fileName != '') {

                if (isset($this->request->data['bunning_store_id']) && !empty($this->request->data['bunning_store_id'])) {

                    /*
                     * Find bunning stores data
                     */
                    $bunningStoresList = $this->ScanBooks->BunningStores->findBunningStoresListByIds($this->request->data['bunning_store_id']);
                    $scanBookData = array();
                    foreach ($bunningStoresList as $bunningStore) {

                        $to = [$bunningStore['email'] => $bunningStore['name']];
                        $attachments = [$fileName => Configure::read('scan-book.file-path') . DS . $fileName];
                        /*
                         * Send Scan Book email to bunnig store
                         */
                        $scanBookSendResult = $this->Email->generateEmailForScanBookAndSend($to, $attachments);
                        if ($scanBookSendResult > 0) {
                            $scanBook = array();
                            $scanBook['user_id'] = $this->Auth->user('id');
                            $scanBook['scan_book_file'] = $fileName;
                            $scanBook['bunning_store_id'] = $bunningStore['id'];
                            $scanBookData[] = $scanBook;
                        }
                    }

                    /*
                     * Save scan book details
                     */
                    $entities = $this->ScanBooks->newEntities($scanBookData);
                    foreach ($entities as $entity) {
                        if (!$entity->errors()) {
                            $result = $this->ScanBooks->save($entity);
                        }
                    }

                    $this->Flash->success('Scan Book send successfully to Bunning Stores...');
                    return $this->redirect(['action' => 'index']);
                } else {

                    $this->Flash->error('Bunnig Stores are not selected to send Scan Book, please select & try agian....');
                }
            } else {

                $this->Flash->error('Getting some error during upload file, please try agian....');
            }

        }

        $bunningStores = $this->ScanBooks->BunningStores->find('all', [
            'fields' => ['state_wise_id' => 'GROUP_CONCAT(BunningStores.id)', 'state_wise_name' => 'GROUP_CONCAT(BunningStores.name)', 'BunningStores.state']
        ])->group('BunningStores.state')->toArray();
        
        $this->set(compact('bunningStores'));
        $this->set('_serialize', ['bunningStores']);
    }

}
