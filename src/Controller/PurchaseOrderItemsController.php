<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PurchaseOrderItems Controller
 *
 * @property \App\Model\Table\PurchaseOrderItemsTable $PurchaseOrderItems
 */
class PurchaseOrderItemsController extends AppController
{

    /**
     * View method
     *
     * @param string|null $id Purchase Order Item id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $purchaseOrderItem = $this->PurchaseOrderItems->get($id, [
            'contain' => ['PurchaseOrders']
        ]);
        $this->set('purchaseOrderItem', $purchaseOrderItem);
        $this->set('_serialize', ['purchaseOrderItem']);
    }

}
