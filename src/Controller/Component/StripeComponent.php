<?php

/**
 * StripeComponent
 *
 * A component that handles payment processing using Stripe.
 *
 * PHP version 5
 *
 * @package		StripeComponent
 * @author		Rohit Shinde
 */

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Exception\Exception;
use Cake\Core\Configure;

/**
 * StripeComponent
 *
 * @package		StripeComponent
 */
class StripeComponent extends Component {

    /**
     * Default Stripe mode to use: Test or Live
     *
     * @var string
     * @access public
     */
    public $mode = 'Test';

    /**
     * Default currency to use for the transaction
     *
     * @var string
     * @access public
     */
    public $currency = 'usd';

    /**
     * Default mapping of fields to be returned: local_field => stripe_field
     *
     * @var array
     * @access public
     */
    public $fields = array('stripe_id' => 'id');

    /**
     * The required Stripe secret API key
     *
     * @var string
     * @access public
     */
    public $key = null;

    /**
     * Valid parameters that can be included in the call to Stripe_Charge::create
     *
     * @var array
     * @access protected
     */
    protected $_chargeParams = array(
        'amount',
        'currency',
        'customer',
        'card',
        'description',
        'metadata',
        'capture',
        'statement_descriptor',
        'receipt_email',
        'application_fee',
        'shipping'
    );

    /**
     * Controller startup. Loads the Stripe API library and sets options from
     * APP/Config/bootstrap.php.
     *
     * @param Controller $controller Instantiating controller
     * @return void
     * @throws Exception
     * @throws Exception
     */
    public function startup() {

        // load the stripe vendor class IF it hasn't been autoloaded (composer)
        require_once ROOT . DS . 'vendor' . DS . 'init.php';

        // if mode is set in bootstrap.php, use it. otherwise, Test.
        $mode = Configure::read('Stripe.mode');
        if ($mode) {
            $this->mode = $mode;
        }

        // set the Stripe API key
        $this->key = Configure::read('Stripe.' . Configure::read('Stripe.mode') . '_secret_key');
        if (!$this->key) {
            throw new Exception('Stripe API key is not set.');
        } else {
            \Stripe\Stripe::setApiKey($this->key);
        }

        // if currency is set in bootstrap.php, use it. otherwise, usd.
        $currency = Configure::read('Stripe.currency');
        if ($currency) {
            $this->currency = $currency;
        }

        // field map for charge response, or use default (set above)
        $fields = Configure::read('Stripe.fields');
        if ($fields) {
            $this->fields = $fields;
        }
    }

    /**
     * The charge method prepares data for Stripe_Charge::create and attempts a
     * transaction.
     *
     * @param array	$data Must contain 'amount' and 'stripeToken'.
     * @return array $charge if success, string $error if failure.
     * @throws Exception
     * @throws Exception
     */
    public function charge($data) {

        if (!isset($data['customer']) && !isset($data['plan'])) {
            throw new Exception('The required stripeToken or stripeCustomer fields are missing.');
        }

        $chargeData = $data;
        if (!isset($chargeData['currency'])) {
            $chargeData['currency'] = $this->currency;
            $chargeData['amount'] = 80;
        }

        try {
            $charge = \Stripe\Charge::create($chargeData);
        } catch (Exception $e) {
            throw new Exception('Payment Error :: ' . $e->getMessage());
        }

        return $this->_formatResult($charge);
    }

    /**
     * The customerCreate method prepares data for Stripe_Customer::create and attempts to
     * create a customer.
     *
     * @param array	$data The data passed directly to Stripe's API.
     * @return array $customer if success, string $error if failure.
     */
    public function customerCreate($data) {

        // for API compatibility with version 1.x of this component
        if (isset($data['stripeToken'])) {
            $data['card'] = $data['stripeToken'];
            unset($data['stripeToken']);
        }

        $postData = array(
            'card' => $data['card'],
            'email' => $data['stripeEmail'],
            'plan' => $data['plan'],
            'quantity' => 1
        );

        $error = null;

        try {
            $customer = \Stripe\Customer::create($postData);
        } catch (Exception $e) {
            CakeLog::error('Customer::Exception: Unknown error.', 'stripe');
            $error = 'There was an error, try again later.';
        }

        if ($error !== null) {
            // an error is always a string
            return (string) $error;
        }

        return $this->_formatResult($customer);
    }

    /**
     * The customerRetrieve method gets a customer object for viewing, updating, or
     * deleting.
     *
     * @param string $id Must be an existing customer id.
     * @return object $customer if success, boolean false if failure or not found.
     */
    public function customerRetrieve($id) {

        $customer = false;

        try {
            $customer = \Stripe\Customer::retrieve($id);
        } catch (Exception $e) {
            return false;
        }
        return $customer;
    }

    /**
     * createSubscription - update customer method
     * @param type $param
     * @return boolean
     */
    public function createSubscription($param) {

        if (empty($param)) {
            return false;
        }
        
        $customer = $this->customerRetrieve($param['customer_id']);
        $response = $customer->subscriptions->create(array("plan" => $param['plan']));
        
        return $this->_formatResult($response);
    }
    
    /**
     * updateCustomerData - Method
     * @param type $data
     * @return type
     */
    public function updateCustomerData($data) {
        
        $customer = $this->customerRetrieve($data['customer_id']);
        $customer->description = "Customer Details updated on " . date('Y-m-d H:i:s') . " during create subsription";
        $customer->source = isset($data['stripeToken']) ? $data['stripeToken'] : '';
        
        $response = $customer->save();
        return $this->_formatResult($response);
    }

    /**
     * 
     * @param type $data
     * @return type
     */
    public function getRetriveCustomerAndCreateORUpdate($data) {
        if (!empty($data)) {
            $existingCustomer = \Stripe\Customer::all();
            $existingCustomerList = array();
            if (isset($existingCustomer->data)) {
                foreach ($existingCustomer->data as $existingCustomerData) {
                    if (in_array($data['stripeEmail'], $existingCustomerList)) {
                        $data['customer_id'] = $existingCustomerData->id;
                        return $this->updateCustomer($data);
                    }
                }
            }

            // create customer 
            return $this->customerCreate($data);
        }
    }

    /**
     * Returns an array of fields we want from Stripe's response objects
     *
     *
     * @param object $response A successful response object from this component.
     * @return array The desired fields from the response object as an array.
     */
    protected function _formatResult($response) {

        return $response;
    }

    /**
     * The getAllEvents method gets all events
     *
     * @return object $events if success, boolean false if failure or not found.
     */
    public function getAllEvents() {

        $events = false;

        try {
            $events = \Stripe\Event::all();
        } catch (Exception $e) {
            return false;
        }
        return $events;
    }

    /**
     * Subscription Cancelled Method
     * @param type $customerId
     * @param type $subscriptionId
     * @return type
     */
    public function subscription_cancelled($customerId, $subscriptionId) {
        try {
            $cu = $this->customerRetrieve($customerId);
            $response = $cu->subscriptions->retrieve($subscriptionId)->cancel();
        } catch (Exception $exc) {
            $response = '';
        }
        return $this->_formatResult($response);
    }

}
