<?php

/**
 * QuickBookComponent
 *
 * A component that handles payment processing using Stripe.
 *
 * PHP version 5
 *
 * @package		QuickBookComponent
 */

namespace App\Controller\Component;

use Cake\Controller\Component;

require_once(ROOT . DS . 'vendor' . DS . 'quick-book-sdk' . DS . 'PHPSample' . DS . 'Customer.php');
require_once(ROOT . DS . 'vendor' . DS . 'quick-book-sdk' . DS . 'PHPSample' . DS . 'Invoice.php');
require_once(ROOT . DS . 'vendor' . DS . 'quick-book-sdk' . DS . 'PHPSample' . DS . 'Items.php');
require_once(ROOT . DS . 'vendor' . DS . 'quick-book-sdk' . DS . 'PHPSample' . DS . 'Account.php');
require_once(ROOT . DS . 'vendor' . DS . 'quick-book-sdk' . DS . 'PHPSample' . DS . 'Term.php');

use Cake\ORM\TableRegistry;
use Cake\Network\Exception\NotFoundException;
use Cake\Core\Configure;
/**
 * StripeComponent
 *
 * @package		StripeComponent
 */
class QuickBookComponent extends Component {

    // The other component your component uses
    public $components = ['Common', 'Auth'];

    // Execute any other additional setup for your component.
    public function initialize(array $config) {

        $this->Users = TableRegistry::get('Users');
        $this->MasterItems = TableRegistry::get('MasterItems');
        $this->BunningStores = TableRegistry::get('BunningStores');

        // get Quick-Books settings arr
        $quickBookSettings = $this->getQuickBooksSettingData();

        if (!empty($quickBookSettings)) {

            $this->Customer = new \PHPSample_Customer($quickBookSettings);
            $this->Invoice = new \PHPSample_Invoice($quickBookSettings);
            $this->Items = new \PHPSample_Items($quickBookSettings);
            $this->Account = new \PHPSample_Account($quickBookSettings);
            $this->Term = new \PHPSample_Term($quickBookSettings);
        } else {

            throw new NotFoundException(__('You can\'t connect to Quick-Books without details, Please provide Quick-Books Details.'));
        }
    }

    /**
     * getQuickBooksSettingData - this method find Quick-Books settings data
     * @return type
     */
    public function getQuickBooksSettingData() {

        $quickBookSettings = $this->Users->get($this->Auth->user('id'), [
            'contain' => []
        ]);

        // check app Settings available or not
        $appSettings = $this->checkQuickBookAppSettings($quickBookSettings);

        return $appSettings;
    }

    /**
     * checkQuickBookAppSettings - Method (Checking the Quick Book Setting Data is available or not)
     * @param type $quickBookSettings
     * @return type
     */
    public function checkQuickBookAppSettings($quickBookSettings) {

        $appSettings = array();

        if ((isset($quickBookSettings->quick_book_access_token) && $quickBookSettings->quick_book_access_token != null) && (isset($quickBookSettings->quick_book_access_token_secret) && $quickBookSettings->quick_book_access_token_secret != null) && (isset($quickBookSettings->quick_book_consumer_key) && $quickBookSettings->quick_book_consumer_key != null) && (isset($quickBookSettings->quick_book_consumer_secret) && $quickBookSettings->quick_book_consumer_secret != null) && (isset($quickBookSettings->quick_book_realm_id) && $quickBookSettings->quick_book_realm_id != null)) {

            $appSettings['AccessToken'] = $quickBookSettings->quick_book_access_token;
            $appSettings['AccessTokenSecret'] = $quickBookSettings->quick_book_access_token_secret;
            $appSettings['ConsumerKey'] = $quickBookSettings->quick_book_consumer_key;
            $appSettings['ConsumerSecret'] = $quickBookSettings->quick_book_consumer_secret;
            $appSettings['RealmID'] = $quickBookSettings->quick_book_realm_id;
        }

        return $appSettings;
    }

    /**
     * createCustomerToQuickBook - Method use for creat customer to quick book
     * @param type $customerData
     * @param type $purchaseOrder
     */
    public function createCustomerToQuickBook($customerData, $purchaseOrder) {
        
        $result = array();

        if (!empty($customerData)) {

            // Manage data for create customer to QuickBook
            $modifyCustomerData = array();
            $modifyCustomerData['BillAddr']['Line1'] = isset($customerData[0]['address']) ? htmlspecialchars($customerData[0]['address'], ENT_QUOTES) : null;
            $modifyCustomerData['BillAddr']['City'] = isset($customerData[0]['city']) ? $customerData[0]['city'] : null;
            $modifyCustomerData['BillAddr']['CountryCode'] = isset($customerData[0]['country_iso_code']) ? $customerData[0]['country_iso_code'] : null;
            $modifyCustomerData['BillAddr']['PostalCode'] = isset($customerData[0]['postal_code']) ? $customerData[0]['postal_code'] : null;
            $modifyCustomerData['Customer']['GivenName'] = isset($customerData[0]['name']) ? $customerData[0]['name'] : null;
            $modifyCustomerData['Customer']['FullyQualifiedName'] = isset($customerData[0]['name']) ? $customerData[0]['name'] : null;
            $modifyCustomerData['Customer']['CompanyName'] = isset($customerData[0]['name']) ? htmlspecialchars($customerData[0]['name'], ENT_QUOTES) : null;
            $customerData[0]['company_registration_number'] = (!empty($customerData[0]['company_registration_number'])) ? $customerData[0]['company_registration_number'] : $customerData[0]['id'];
            $displayUniquName = isset($customerData[0]['name']) ? $customerData[0]['name'] . ' (' . $customerData[0]['company_registration_number'] . ')' : null;
            $modifyCustomerData['Customer']['DisplayName'] = $displayUniquName;
            
            $modifyCustomerData['NameBase']['PrimaryEmailAddr'] = isset($customerData[0]['email']) ? $customerData[0]['email'] : null;
            $modifyCustomerData['NameBase']['PrimaryPhone'] = isset($purchaseOrder['bill_to_telephone_number']) ? $purchaseOrder['bill_to_telephone_number'] : null;
            $modifyCustomerData['NameBase']['Fax'] = isset($purchaseOrder['bill_to_fax_number']) ? $purchaseOrder['bill_to_fax_number'] : null;

            $result = $this->Customer->createCustomer($modifyCustomerData);
        } else {

            $result['status'] = 'error';
            if (empty($customerData)) {

                $result['msg'] = 'You must need Customer Data to create Invoice, please provide & try again';
            } else {

                $result['msg'] = 'Problem while initializing DataService, Please try again.';
            }
        }

        return $result;
    }

    /**
     * findCustomerDetailsByField - This method find the customer details by specific field
     * @param type $fieldName
     * @param type $fieldValue
     * @return string
     */
    public function findCustomerDetailsByField($fieldName, $fieldValue) {

        // find customer details by field
        $result = $this->Customer->findCustomerDetailsByFieldName($fieldName, $fieldValue);
        return $result;
    }

    /**
     * findItemDetailsByField - This method find the Item details by specific field
     * @param type $fieldName
     * @param type $fieldValue
     * @return string
     */
    public function findItemDetailsByField($fieldName, $fieldValue) {

        // find customer details by field
        $result = $this->Items->findItemDetailsByFieldName($fieldName, $fieldValue);
        return $result;
    }
    
    /**
     * findPaymentTermLists - This menthod find Payment Term List
     * @return $result
     */
    public function findPaymentTermList() {

        $result = $this->Term->findPaymentTermLists();
        return $result;
    }
    
    /**
     * findTermDetailByFieldName - This menthod find Payment Term Details
     * @param type $fieldName
     * @param type $fieldValue
     * @return type
     */
    public function findTermDetailByFieldName($fieldName, $fieldValue) {

        $result = $this->Term->findTermDetailsByFieldName($fieldName, $fieldValue);
        return $result;
    }

    /**
     * createInvoiceToQuickBook - This method creating Invoices
     * @param type $customerId
     * @param type $invoiceData
     * @param type $buyerData
     * @return string
     */
    public function createInvoiceToQuickBook($customerId, $invoiceData, $buyerData) {
        
        $result = array();

        if ($customerId) {
            
            $data = array();
            
            $quickBookSettings = $this->Users->get($this->Auth->user('id'), [
                'contain' => []
            ]);

            // Manage data for create Invoice to QuickBook
            $modifyInvoiceData = array();
            $modifyInvoiceData['Invoice']['CustomerRef'] = $customerId;
            if (isset($invoiceData->actual_delivery_date)) {
                
                if ((strtotime($invoiceData->actual_delivery_date) < time())) {
                    
                    $InvoiceDate = date('Y-m-d');
                } else {
                    $InvoiceDate = date('Y-m-d', strtotime($invoiceData->actual_delivery_date));
                }
            } else {
                
                $InvoiceDate = date('Y-m-d');
            }
            
            $modifyInvoiceData['Invoice']['TxnDate'] = $InvoiceDate;
            
            $i = 0;
            foreach ($invoiceData['purchase_order_items'] as $invoiceItem) {
                $modifyInvoiceData['linesData'][$i]['ItemRef'] = $invoiceItem['quick_book_item_id'];
                $modifyInvoiceData['linesData'][$i]['Qty'] = $invoiceItem['requested_quantity'];
                $modifyInvoiceData['linesData'][$i]['UnitPrice'] = $invoiceItem['net_price'];
                $modifyInvoiceData['linesData'][$i]['Description'] = $invoiceItem['description'];
                $i++;
            }

            $modifyInvoiceData['ShipAddr']['Line1'] = isset($buyerData[0]['address']) ? htmlspecialchars($buyerData[0]['address'], ENT_QUOTES) : null;
            $modifyInvoiceData['ShipAddr']['City'] = isset($buyerData[0]['city']) ? $buyerData[0]['city'] : null;
            $modifyInvoiceData['ShipAddr']['CountrySubDivisionCode'] = isset($buyerData[0]['country_iso_code']) ? $buyerData[0]['country_iso_code'] : null;
            $modifyInvoiceData['ShipAddr']['PostalCode'] = isset($buyerData[0]['postal_code']) ? $buyerData[0]['postal_code'] : null;
            
            // check QB payment term is available otherwise set default term
            $quickBookPaymentTerm = '';
            if ($quickBookSettings->quick_book_payment_term) {
                $quickBookPaymentTerm = @$quickBookSettings->quick_book_payment_term;
            } else {
                $quickBookPaymentTermData = $this->findTermDetailByFieldName('Name', Configure::read('quick_book_default_payment_term'));
                if ($quickBookPaymentTermData['status'] == 'success') {
                    $quickBookPaymentTerm = isset($quickBookPaymentTermData['response'][0]->Id) ? $quickBookPaymentTermData['response'][0]->Id : '';
                    
                    // update default quickBookPaymentTerm in database
                    if ($quickBookPaymentTerm) {
                        $updateArr = ['quick_book_payment_term' => $quickBookPaymentTerm];
                        $conditionArr = ['id' => $this->Auth->user('id')];
                        $this->Users->updateFieldsByCondition($updateArr, $conditionArr);
                    }
                }
            }
            
            $data['PurchaseOrderId'] = @$invoiceData->purchase_order_number;
            $data['SalesTermRef'] = $quickBookPaymentTerm;
            $data['Rebate'] = (@$quickBookSettings->quick_book_rebate) ? $quickBookSettings->quick_book_rebate : 0;
            $result = $this->Invoice->createInvoice($modifyInvoiceData, $data);
        } else {

            $result['status'] = 'error';
            if (empty($invoiceData)) {

                $result['msg'] = 'You must need Invoice Data to create Invoice, please provide & try again';
            } else {

                $result['msg'] = 'Problem while initializing DataService, Please try again.';
            }
        }

        return $result;
    }

    /**
     * createItems - This method create the Items for create Invoice
     * @param type $invoiceItemsData
     * @param type $IncomeAccountRef
     * @return string
     */
    public function createItems($invoiceItemsData, $IncomeAccountRef) {

        $result = array();
        if (!empty($invoiceItemsData)) {

            // Manage data for create Invoice to QuickBook
            $modifyInvoiceItemsData = array();
            $modifyInvoiceItemsData['Name'] = isset($invoiceItemsData->name) ? $invoiceItemsData->name : null;
            $modifyInvoiceItemsData['Description'] = isset($invoiceItemsData->description) ? $invoiceItemsData->description : null;
            $modifyInvoiceItemsData['UnitPrice'] = isset($invoiceItemsData->retail_price) ? $invoiceItemsData->retail_price : 0;
            $modifyInvoiceItemsData['Sku'] = isset($invoiceItemsData->bar_code) ? $invoiceItemsData->bar_code : null;
            $modifyInvoiceItemsData['Type'] = 'NonInventory';
            $modifyInvoiceItemsData['ItemCategoryType'] = isset($invoiceItemsData->pot_size) ? $invoiceItemsData->pot_size : null;
            $modifyInvoiceItemsData['IncomeAccountRef'] = $IncomeAccountRef;
            $result = $this->Items->createItem($modifyInvoiceItemsData);
        } else {

            $result['status'] = 'error';
            $result['msg'] = 'You must need to Invoice Items Data to create Items, please provide & try again';
        }
            

        return $result;
    }

    /**
     * updateInvoice - This method update the Invoice to Quick-Books
     * @param type $invoiceData
     * @param type $oldInvoiceData
     * @return string
     */
    public function updateInvoice($invoiceData, $oldInvoiceData) {
        
        $result = array();

        if (!empty($invoiceData)) {

            $oldeInvoiceCount = count($oldInvoiceData['invoice_items']);
            $modifiedInvoiceCount = count($invoiceData['invoice_items']);

            $modifyInvoiceData = array();
            $modifyInvoiceData['Id'] = $invoiceData['quick_book_invoice_id'];
            $modifyInvoiceData['DocNumber'] = $invoiceData['invoice_number'];
            $i = 0;
            $a = 0;
            $newItemIdArr = array();
            $itemIdArr = array();
            $newItemsData = array();
            $deleteItemIds = array();

            foreach ($invoiceData['invoice_items'] as $invoiceItem) {
                $itemIdArr[] = $invoiceItem['quick_book_invoice_item_id'];
            }

            foreach ($oldInvoiceData['invoice_items'] as $invoiceItem) {

                $newItemIdArr[] = $invoiceItem['quick_book_invoice_item_id'];

                if (($modifiedInvoiceCount < $oldeInvoiceCount) && (!in_array($invoiceItem['quick_book_invoice_item_id'], $itemIdArr))) {

                    $deleteItemIds[] = $invoiceItem['quick_book_invoice_item_id'];
                }
            }

            foreach ($invoiceData['invoice_items'] as $invoiceItem) {

                if (($modifiedInvoiceCount > $oldeInvoiceCount) && (!in_array($invoiceItem['quick_book_invoice_item_id'], $newItemIdArr))) {

                    $newItemsData['linesData'][$a]['ItemRef'] = $invoiceItem['quick_book_item_id'];
                    $newItemsData['linesData'][$a]['Qty'] = $invoiceItem['requested_quantity'];
                    $newItemsData['linesData'][$a]['UnitPrice'] = $invoiceItem['net_price'];
                    $newItemsData['linesData'][$a]['Description'] = $invoiceItem['description'];
                    $a++;
                } else {

                    $modifyInvoiceData['linesData'][$i]['Id'] = $invoiceItem['quick_book_invoice_item_id'];
                    $modifyInvoiceData['linesData'][$i]['ItemRef'] = $invoiceItem['quick_book_item_id'];
                    $modifyInvoiceData['linesData'][$i]['Qty'] = $invoiceItem['requested_quantity'];
                    $modifyInvoiceData['linesData'][$i]['UnitPrice'] = $invoiceItem['net_price'];
                    $modifyInvoiceData['linesData'][$i]['Description'] = $invoiceItem['description'];
                    $i++;
                }
            }

            $quickBookSettings = $this->Users->get($this->Auth->user('id'), [
                'contain' => []
            ]);
            
            // update invoice
            $data = array();
            $data['Rebate'] = ($invoiceData['discount_percentage']) ? $invoiceData['discount_percentage'] : 0;
            $result = $this->Invoice->updateQuickBookInvoice($modifyInvoiceData, $newItemsData, $deleteItemIds, $data);
        } else {

            $result['status'] = 'error';
            $result['msg'] = 'Invoice data is not available to update';
        }

        return $result;
    }
    
    /**
     * updateFailedInvoice - This method update the failed Invoice to Quick-Books
     * @param type $invoiceData
     * @return string
     */
    public function updateFailedInvoice($invoiceData) {
        
        $result = array();

        if (!empty($invoiceData)) {

            $modifyInvoiceData = array();$data['Rebate'] = (@$quickBookSettings->quick_book_rebate) ? $quickBookSettings->quick_book_rebate : 0;
            $modifyInvoiceData['Id'] = $invoiceData['quick_book_invoice_id'];
            $modifyInvoiceData['DocNumber'] = $invoiceData['invoice_number'];
            $i = 0;
            $a = 0;
            foreach ($invoiceData['invoice_items'] as $invoiceItem) {

                $modifyInvoiceData['linesData'][$a]['ItemRef'] = $invoiceItem['quick_book_item_id'];
                $modifyInvoiceData['linesData'][$a]['Qty'] = $invoiceItem['requested_quantity'];
                $modifyInvoiceData['linesData'][$a]['UnitPrice'] = $invoiceItem['net_price'];
                $modifyInvoiceData['linesData'][$a]['Description'] = $invoiceItem['description'];
                $a++;
            }

            // update invoice
            $data = array();
            $data['Rebate'] = ($invoiceData['discount_percentage']) ? $invoiceData['discount_percentage'] : 0;
            $result = $this->Invoice->updateFailedQuickBookInvoice($modifyInvoiceData, $data);
        } else {

            $result['status'] = 'error';
            $result['msg'] = 'Invoice data is not available to update';
        }

        return $result;
    }

    /**
     * checkItemAvailableToQuickBookOtherWiseCreate - This method check item available to quick book otherwise create
     * @param type $itemsData
     * @return type
     */
    public function checkItemAvailableToQuickBookOtherWiseCreate($itemsData) {

        $responseArr['success'] = array();
        $responseArr['error'] = array();

        // Check Sales Account type available to quick-books otherwise create
        $IncomeAccountRef = $this->checkSalesAccountAvailableToQuickBookOtherWiseCreate();
        
        $itemId = array();
        foreach ($itemsData as $item) {

            $quickBookItemId = NULL;
            $storedQuickBookItemId = $item->quick_book_item_id;
            
            // find Quick Book Item Details by Id
            $quickBookItemResponse = $this->findItemDetailsByField('Id', $storedQuickBookItemId);
            if ($quickBookItemResponse['status'] == 'success') {

                $quickBookItemId = isset($quickBookItemResponse['response'][0]->Id) ? $quickBookItemResponse['response'][0]->Id : NULL;
                $responseArr['success'][] = $item->name . ' (' . $item->bar_code . ')' . ' item available to Quick-Books';
            } else {

                $barCode = isset($item->bar_code) ? $item->bar_code : null;
                $quickBookItemResponse = $this->findItemDetailsByField('Sku', $barCode);
                if ($quickBookItemResponse['status'] == 'success') {

                    $quickBookItemId = isset($quickBookItemResponse['response'][0]->Id) ? $quickBookItemResponse['response'][0]->Id : NULL;

                    // update Quick Book Item Id in master_items table
                    $updateArr = array('quick_book_item_id' => $quickBookItemId);
                    $conditionArr = array('id' => $item->id);
                    $updateResult = $this->MasterItems->updateFieldsByFields($conditionArr, $updateArr);

                    $responseArr['success'][] = $item->name . ' (' . $item->bar_code . ')' . ' item available to Quick-Books';
                } else {
                    
                    $name = isset($item->name) ? $item->name : null;
                    $quickBookItemResponse = $this->findItemDetailsByField('Name', $name);
                    if ($quickBookItemResponse['status'] == 'success') {

                        $quickBookItemId = isset($quickBookItemResponse['response'][0]->Id) ? $quickBookItemResponse['response'][0]->Id : NULL;

                        // update Quick Book Item Id in master_items table
                        $updateArr = array('quick_book_item_id' => $quickBookItemId);
                        $conditionArr = array('id' => $item->id);
                        $updateResult = $this->MasterItems->updateFieldsByFields($conditionArr, $updateArr);

                        $responseArr['success'][] = $item->name . ' (' . $item->bar_code . ')' . ' item available to Quick-Books';
                    }
                }
            }

            if (!$quickBookItemId) {

                // create item
                $itemResponse = $this->createItems($item, $IncomeAccountRef);
                $itemResponse['status'] = isset($itemResponse['status']) ? $itemResponse['status'] : '';
                $itemResponse['msg'] = isset($itemResponse['msg']) ? $itemResponse['msg'] : 'Some thing wrong, please try again';
                if ($itemResponse['status'] == 'success') {

                    $quickBookItemId = isset($itemResponse['response']->Id) ? $itemResponse['response']->Id : NULL;
                    $itemId[] = $quickBookItemId;

                    $responseArr['success'][] = $item->name . ' (' . $item->bar_code . ')' . ' item created successfully to Quick-Books';

                    // update Quick Book Item Id in master_items table
                    $updateArr = array('quick_book_item_id' => $quickBookItemId);
                    $conditionArr = array('id' => $item->id);
                    $updateResult = $this->MasterItems->updateFieldsByFields($conditionArr, $updateArr);
                } else {

                    $responseArr['error'][] = $item->name . ' - ' . $itemResponse['msg'];
                }
            } else {

                $itemId[] = $quickBookItemId;
            }
        }

        $responseArr['quickBookItemId'] = $itemId;
        return $responseArr;
    }

    /**
     * checkCustomerAvailableToQuickBookOtherWiseCreate - This method check customer is available to site otherwise create
     * @param type $payerData
     * @param type $purchaseOrder
     * @return array
     */
    public function checkCustomerAvailableToQuickBookOtherWiseCreate($payerData, $purchaseOrder) {

        $resultArr['error'] = array();
        $resultArr['success'] = array();

        $bunningstoreId = isset($payerData[0]['id']) ? $payerData[0]['id'] : NULL;
        $storedQuickBookCutomerId = isset($payerData[0]['quick_book_cutomer_id']) ? $payerData[0]['quick_book_cutomer_id'] : NULL;

        $quickBookCustomerId = NULL;

        // check customer is exsit or not to quick book
        $customerResponse = $this->findCustomerDetailsByField('Id', $storedQuickBookCutomerId);
        if ($customerResponse['status'] == 'success') {

            $quickBookCustomerId = isset($customerResponse['response'][0]->Id) ? $customerResponse['response'][0]->Id : NULL;
            $resultArr['success'][] = $payerData[0]['name'] . ' customer available to Quick-Books';
        } else {

            // find customer by DisplayName if quick-books customer Id is not exist in database & store that is exist
            $payerData[0]['company_registration_number'] = (!empty($payerData[0]['company_registration_number'])) ? $payerData[0]['company_registration_number'] : $payerData[0]['id'];
            $displayUniquName = isset($payerData[0]['name']) ? $payerData[0]['name'] . ' (' . $payerData[0]['company_registration_number'] . ')' : null;
            $customerResponse = $this->findCustomerDetailsByField('DisplayName', $displayUniquName);
            if ($customerResponse['status'] == 'success') {

                $quickBookCustomerId = isset($customerResponse['response'][0]->Id) ? $customerResponse['response'][0]->Id : NULL;
                
                // update quick_book_cutomer_id value in bunnings stores
                $updateArr = array('quick_book_cutomer_id' => $quickBookCustomerId);
                $conditionArr = array('id' => $bunningstoreId);
                $updateResult = $this->BunningStores->updateFieldsByFields($conditionArr, $updateArr);
                $resultArr['success'][] = $payerData[0]['name'] . ' customer available to Quick-Books';
            } else {

                $resultArr['error'][] = $customerResponse['msg'];
            }
        }

        if (!$quickBookCustomerId) {

            // create customer to quick book if not exist
            $createCustomerResponse = $this->createCustomerToQuickBook($payerData, $purchaseOrder);
            $createCustomerResponse['status'] = isset($createCustomerResponse['status']) ? $createCustomerResponse['status'] : '';
            $createCustomerResponse['msg'] = isset($createCustomerResponse['msg']) ? $createCustomerResponse['msg'] : 'Some thing wrong, please try again';
            if ($createCustomerResponse['status'] == 'success') {

                $quickBookCustomerId = isset($createCustomerResponse['response']->Id) ? $createCustomerResponse['response']->Id : NULL;
                $resultArr['success'][] = $payerData[0]['name'] . ' customer created successfully to Quick-Books';
                if ($quickBookCustomerId) {

                    // update quick_book_cutomer_id value in bunnings stores
                    $updateArr = array('quick_book_cutomer_id' => $quickBookCustomerId);
                    $conditionArr = array('id' => $bunningstoreId);
                    $updateResult = $this->BunningStores->updateFieldsByFields($conditionArr, $updateArr);
                } else {

                    $resultArr['error'][] = 'Customer have created successfully to Quick Book but we can\'t receive Customer Id to store in database';
                }
            } else {

                $resultArr['error'][] = $createCustomerResponse['msg'];
            }
        }

        $resultArr['quickBookCustomerId'] = $quickBookCustomerId;

        return $resultArr;
    }
    
    
    /**
     * checkSalesAccountAvailableToQuickBookOtherWiseCreate - This method check Sales Account type is available otherwise create
     * @return type
     */
    public function checkSalesAccountAvailableToQuickBookOtherWiseCreate() {
        
        // find the Sales Account Data
        $accountSpecificData = $this->Account->findDataByFieldAndValue('Name', 'Sales');
        $IncomeAccountRef = isset($accountSpecificData[0]->Id) ? $accountSpecificData[0]->Id : NULL;
        if ($IncomeAccountRef == NULL) {
            
            $accountArr['Name'] = 'Sales';
            $accountArr['FullyQualifiedName'] = 'Sales';
            $accountArr['Classification'] = 'Revenue';
            $accountArr['AccountType'] = 'Income';
            $accountArr['AccountSubType'] = 'SalesOfProductIncome';
            
            $response = $this->Account->createAccount($accountArr);
            $IncomeAccountRef = isset($response['response']->Id) ? $response['response']->Id : NULL;
        }
        
        return $IncomeAccountRef;
    }

}
