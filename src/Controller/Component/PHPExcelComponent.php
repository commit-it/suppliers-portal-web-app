<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

// load PHP_Excel of vendor
use PHPExcel_IOFactory;
use PHPExcel_Cell;

require_once(ROOT . DS . 'vendor' . DS . 'PHP_Excel' . DS . 'PHPExcel' . DS . 'IOFactory.php');

class PHPExcelComponent extends Component {

    public $controller = null;

    /*
     * read cells from excel file
     */

    public function getExcelFileData($excelFileName = null) {

        $dataArr = array();

        if ($excelFileName != null) {

            // load PHPExcel_IOFactory class
            $objPHPExcel = PHPExcel_IOFactory::load($excelFileName);

            // get getWorksheetIterator
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {

                $highestRow = $worksheet->getHighestRow(); // e.g. 10
                $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

                for ($row = 1; $row <= $highestRow; ++$row) {

                    for ($col = 0; $col < $highestColumnIndex; ++$col) {

                        // getCellByColumnAndRow
                        $cell = $worksheet->getCellByColumnAndRow($col, $row);
                        $dataArr[$row][$col] = $cell->getValue();
                    }
                }
            }
        }

        return $dataArr;
    }
    
    /**
     * exportMasterItemDataToExcel - This method export master_items data into Excel
     * @param type $data
     * @param type $file
     * @param type $headerCellArr
     * @return type
     * @throws Exception
     */
    public function exportMasterItemDataToExcel($data, $file, $headerCellArr) {
        
        // Instantiate a new PHPExcel object
        $objPHPExcel = new \PHPExcel();
        
        // Set the active Excel worksheet to sheet 0
        $objPHPExcel->setActiveSheetIndex(0);
        
        // Initialise the Excel row number
        $rowCount = 2;
        
        foreach(range('A','I') as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }
        
        foreach ($data as $masterItem) {

            
            // generate Excel header
            if ($rowCount < 3) {
                
                $firstColumn = 1;
                $objPHPExcel->getActiveSheet()->SetCellValue('A' . $firstColumn, $headerCellArr['scan_barcode']);
                $objPHPExcel->getActiveSheet()->SetCellValue('B' . $firstColumn, $headerCellArr['bar_code']);
                $objPHPExcel->getActiveSheet()->SetCellValue('C' . $firstColumn, $headerCellArr['pot_size']);
                $objPHPExcel->getActiveSheet()->SetCellValue('D' . $firstColumn, $headerCellArr['name']);
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $firstColumn, $headerCellArr['description']);
                $objPHPExcel->getActiveSheet()->SetCellValue('F' . $firstColumn, $headerCellArr['genus']);
                $objPHPExcel->getActiveSheet()->SetCellValue('G' . $firstColumn, $headerCellArr['species']);
                $objPHPExcel->getActiveSheet()->SetCellValue('H' . $firstColumn, $headerCellArr['cultival']);
                $objPHPExcel->getActiveSheet()->SetCellValue('I' . $firstColumn, $headerCellArr['retail_price']);
            }
            
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $masterItem['scan_barcode']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, $masterItem['bar_code']);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, $masterItem['pot_size']);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, $masterItem['name']);
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, $masterItem['description']);
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, $masterItem['genus']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, $masterItem['species']);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, $masterItem['cultival']);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, $masterItem['retail_price']);

            // Increment the Excel row counter
            $rowCount++;
        }
        
        // Instantiate a Writer to create an OfficeOpenXML Excel .xlsx file
        $objWriter = new \PHPExcel_Writer_Excel2007($objPHPExcel);
        
        // Write the Excel file to $file
        try {

            $objWriter->save($file);
            return TRUE;
        } catch (Exception $ex) {
            
            throw new Exception($ex->getTraceAsString());
        }
        
    }

}
