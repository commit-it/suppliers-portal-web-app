<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller\Component;

use Cake\Controller\Component;

// load dompdf of vendor
use DOMPDF;
require_once(ROOT . DS . 'vendor' . DS . 'dompdf' . DS . 'dompdf_config.inc.php');

class ManagePdfComponent extends Component {

    public $controller = null;

    /**
     * generatePdf method - this is generating pdf
     * @param type $fileName
     * @param type $html
     * @param type $papersize
     * @param type $orientation
     * @param type $openPdf
     * @return int
     */
    public function generatePdf($fileName,$html,$papersize,$orientation,$openPdf,$streamFileName = NULL) {

        try {
            $dompdf = new DOMPDF();
        } catch (Exception $e) {
            echo $e;
        }
        
        $dompdf->load_html($html);
        $dompdf->set_paper($papersize, $orientation);
        $dompdf->render();
        $dompdf->set_base_path(WWW_ROOT . 'css' . DS . 'dompdf_style.css');
        $output = $dompdf->output();

        $result = file_put_contents($fileName . '.pdf', $output);
        
        if (!empty($result)) {
            
            if ($openPdf) {
                
                // open generated pdf
                if ($streamFileName) {
                    $dompdf->stream($streamFileName . '.pdf', array('Attachment' => 0));
                } else {
                    $dompdf->stream($fileName . '.pdf', array('Attachment' => 0));
                }
            }
            
            return TRUE;
        } else {
            
            return FALSE;
        }
    }

}
