<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Core\Configure;

class CommonComponent extends Component {

    public $controller = null;
    public $components = array('Auth');

    /**
     * uploadFile - Method
     * @param type $path
     * @param array $data
     * @param type $prefix
     * @return string
     */
    public function uploadFile($path, $data, $prefix = null) {

        $fileName = $prefix . time() . "_" . $data['name'];
        $data['name'] = $fileName;
        $filePath = $path . DS . $data['name'];
        $uploads_root = $path;

        if (!is_dir($uploads_root)) {
            
            try {

                $dirResult = mkdir($uploads_root, 0777, true);
                if ($dirResult) {
                    chmod($uploads_root, 0777);
                }
            } catch (Exception $exc) {

                throw new Exception($exc->getTraceAsString());
            }
        }

        $tempPath = $data['tmp_name'];
        if (move_uploaded_file($tempPath, $filePath)) {

            return $data['name'];
        } else {

            return '';
        }
    }

    /**
     * uploadImage - Method
     * @param array $data
     * @param type $uploads_root
     * @param type $prefix
     * @return string
     */
    public function uploadImage($data, $uploads_root, $prefix) {

        /* Create filename */
        $name = isset($data['name']) ? $data['name'] : '';

        $extention = explode('.', $name);
        $fileName = $prefix . time() . "." . $extention[1];
        $data['name'] = $fileName;

        /* Set file path */
        $filePath = $uploads_root . DS . $data['name'];

        if (!is_dir($uploads_root)) {
            
            try {

                $dirResult = mkdir($uploads_root, 0777, true);
                if ($dirResult) {
                    chmod($uploads_root, 0777);
                }
            } catch (Exception $exc) {

                throw new Exception($exc->getTraceAsString());
            }
        }
        
        /* Set temp path */
        $tempPath = isset($data['tmp_name']) ? $data['tmp_name'] : '';

        /* Move uploaded file */
        if (move_uploaded_file($tempPath, $filePath)) {
            return $data['name'];
        } else {
            return '';
        }
    }

    /**
     * generateRandomCode - Method
     * @param type $length
     * @return string
     */
    public function generateRandomCode($length) {

        $string = '';
        // You can define your own characters here.
        $characters = "23456789ABCDEFHJKLMNPRTVWXYZabcdefghijklmnopqrstuvwxyz";

        for ($p = 0; $p < $length; $p++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    /**
     * checkFileIsCorrectToImport - this menthod check that the uploaded excel file is correct to import
     * @param type $dataArr
     * @param type $fileType
     * @return boolean
     */
    public function checkFileIsCorrectToImport($dataArr, $fileType) {

        $masterItemsColumns = isset($dataArr[Configure::read($fileType . '.column_name_row_number')]) ? $dataArr[Configure::read($fileType . '.column_name_row_number')] : array();
        $excelFileColumns = Configure::read($fileType . '.columns');
        $matchKeyCnt = 0;

        foreach ($excelFileColumns as $excelKey => $excelVal) {
            echo $excelKey;
            if (in_array($excelKey, $masterItemsColumns)) {
                $matchKeyCnt = $matchKeyCnt + 1;
            }
        }

        if (count($excelFileColumns) == $matchKeyCnt) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * smartResizeImage - Method
     * @param type $file
     * @param type $width
     * @param type $height
     * @param type $proportional
     * @param type $output
     * @param type $delete_original
     * @param type $use_linux_commands
     * @return boolean
     */
    public function smartResizeImage($file, $width = 0, $height = 0, $proportional = false, $output = 'file', $delete_original = true, $use_linux_commands = false) {

        if ($height <= 0 && $width <= 0)
            return false;

        # Setting defaults and meta
        $info = getimagesize($file);
        $image = '';
        $final_width = 0;
        $final_height = 0;
        list($width_old, $height_old) = $info;

        # Calculating proportionality
        if ($proportional) {
            if ($width == 0)
                $factor = $height / $height_old;
            elseif ($height == 0)
                $factor = $width / $width_old;
            else
                $factor = min($width / $width_old, $height / $height_old);

            $final_width = round($width_old * $factor);
            $final_height = round($height_old * $factor);
        }
        else {
            $final_width = ( $width <= 0 ) ? $width_old : $width;
            $final_height = ( $height <= 0 ) ? $height_old : $height;
        }

        # Loading image to memory according to type
        switch ($info[2]) {
            case IMAGETYPE_GIF: $image = imagecreatefromgif($file);
                break;
            case IMAGETYPE_JPEG: $image = imagecreatefromjpeg($file);
                break;
            case IMAGETYPE_PNG: $image = imagecreatefrompng($file);
                break;
            default: return false;
        }


        # This is the resizing/resampling/transparency-preserving magic
        $image_resized = imagecreatetruecolor($final_width, $final_height);
        if (($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG)) {
            $transparency = imagecolortransparent($image);

            if ($transparency >= 0) {
                $transparent_color = imagecolorsforindex($image, $trnprt_indx);
                $transparency = imagecolorallocate($image_resized, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
                imagefill($image_resized, 0, 0, $transparency);
                imagecolortransparent($image_resized, $transparency);
            } elseif ($info[2] == IMAGETYPE_PNG) {
                imagealphablending($image_resized, false);
                $color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
                imagefill($image_resized, 0, 0, $color);
                imagesavealpha($image_resized, true);
            }
        }
        imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);

        # Taking care of original, if needed
        if ($delete_original) {
            if ($use_linux_commands)
                exec('rm ' . $file);
            else
                @unlink($file);
        }

        # Preparing a method of providing result
        switch (strtolower($output)) {
            case 'browser':
                $mime = image_type_to_mime_type($info[2]);
                header("Content-type: $mime");
                $output = NULL;
                break;
            case 'file':
                $output = $file;
                break;
            case 'return':
                return $image_resized;
                break;
            default:
                break;
        }

        # Writing image according to type to the output destination
        switch ($info[2]) {
            case IMAGETYPE_GIF: imagegif($image_resized, $output);
                break;
            case IMAGETYPE_JPEG: imagejpeg($image_resized, $output);
                break;
            case IMAGETYPE_PNG: imagepng($image_resized, $output);
                break;
            default: return false;
        }

        return true;
    }

    /**
     * checkUserRole - Method
     * @param type $id
     * @return boolean
     */
    public function checkUserRole($id = NULL) {
        
        if (($this->Auth->user('role_name') == \App\Model\Table\UserRolesTable::Admin) || ($this->Auth->user('id') == $id)) {
            
            return TRUE;
        } else {
            
            return FALSE;
        }
    }

    /**
     * checkAllowOrNot - Action
     * @return boolean
     */
    public function checkAllowOrNot($id) {

        if ($this->Auth->user('id') != $id) {
            
            return TRUE;
        } else {
            
            return FALSE;
        }
    }

    /**
     * checkParentIdAllow() method
     * @param type $userData
     * @return true or redirect to list page
     */
    public function checkParentIdAllow($userData) {

        $parentId = isset($userData->parent_id) ? $userData->parent_id : '';
        if ($parentId == $this->Auth->user('id')) {
            
            return TRUE;
        } else {
            
            return FALSE;
        }
    }
    
    /**
     * getErrorMsgForDisplay - get validation or error msg
     * @param type $errorMsgArr
     * @return string
     */
    public function getErrorMsgForDisplay($errorMsgArr) {
        
        $errorMsg = '';
        foreach ($errorMsgArr as $errorMsgKey => $errorMsgVal) {
            $mskKey = key($errorMsgVal);
            $errorMessage = isset($errorMsgVal[$mskKey]) ? $errorMsgVal[$mskKey] : '';
            if ($errorMessage != '') {
                if ($errorMsg != '') {

                    $errorMsg = $errorMsg . ', ' . strtoupper($errorMsgKey) . ' - ' . $errorMessage;
                } else {

                    $errorMsg = $errorMsg . strtoupper($errorMsgKey) . ' - ' . $errorMessage;
                }
            }
        }
        
        return $errorMsg;
    }

}
