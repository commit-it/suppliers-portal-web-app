<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Network\Email\Email;
use \Cake\Core\Configure;


class EmailComponent extends Component {

    public $controller = null;

    /**
     * generateEmailForScanBookAndSend - Method
     * @param type $to
     * @param type $attachments
     * @return int
     */
    public function generateEmailForScanBookAndSend($to = null, $attachments = null) {

        $nurseryName = 'Supplier Portal';
        $response = 0;
        // send scanbook to bunning
        $emailDetails = array();
        $emailDetails['subject'] = Configure::read('scan-book.email-subject') . ' (' . $nurseryName . ')';
        $emailDetails['to'] = $to;
        $emailDetails['email-profile'] = Configure::read('scan-book.email-profile');
        $emailDetails['template'] = Configure::read('scan-book.email-template');
        $emailDetails['layout'] = Configure::read('scan-book.email-layout');

        $nameOfTo = array_values($to);
        $nameOfBunning = isset($nameOfTo[0]) ? $nameOfTo[0] : '';

        $emailDetails['viewVars'] = [
            'bunningName' => $nameOfBunning,
            'nurseryName' => Configure::read('email.thanks')
        ];

        $emailDetails['attachments'] = $attachments;
        $emailResult = $this->sendEmail($emailDetails);

        if ($emailResult) {
            $response = 1;
        }

        return $response;
    }

    /**
     * generateEmailForBookingSheetAndSend - Method
     * @param type $to
     * @param type $attachments
     * @return int
     */
    public function generateEmailForBookingSheetAndSend($to = null, $attachments = null) {

        $nurseryName = 'Supplier Portal';
        $response = 0;
        // send scanbook to bunning
        $emailDetails = array();
        $emailDetails['subject'] = Configure::read('booking-sheet.email-subject') . ' (' . $nurseryName . ')' . ' - ' . date(Configure::read('date-format.d-M-Y'));
        $emailDetails['to'] = $to;
        $emailDetails['email-profile'] = Configure::read('booking-sheet.email-profile');
        $emailDetails['template'] = Configure::read('booking-sheet.email-template');
        $emailDetails['layout'] = Configure::read('booking-sheet.email-layout');

        $nameOfTo = array_values($to);
        $nameOfBunning = isset($nameOfTo[0]) ? $nameOfTo[0] : '';

        $emailDetails['viewVars'] = [
            'clientName' => $nameOfBunning,
            'nurseryName' => Configure::read('email.thanks')
        ];

        $emailDetails['attachments'] = $attachments;
        $emailResult = $this->sendEmail($emailDetails);

        if ($emailResult) {
            $response = 1;
        }

        return $response;
    }

    /**
     * generateEmailForForgetPasswordAndSend - Method
     * @param type $to
     * @param type $password
     * @return int
     */
    public function generateEmailForForgetPasswordAndSend($to = null, $password = null) {

        $nurseryName = 'Supplier Portal';
        $response = 0;

        if ($password != null) {

            // send scanbook to bunning
            $emailDetails = array();
            $emailDetails['subject'] = Configure::read('forget-password.email-subject') . ' (' . $nurseryName . ')';
            $emailDetails['to'] = $to;
            $emailDetails['email-profile'] = Configure::read('forget-password.email-profile');
            $emailDetails['template'] = Configure::read('forget-password.email-template');
            $emailDetails['layout'] = Configure::read('forget-password.email-layout');

            $nameOfTo = array_values($to);
            $nameOfUser = isset($nameOfTo[0]) ? $nameOfTo[0] : '';

            $emailDetails['viewVars'] = [
                'bunningName' => $nameOfUser,
                'password' => $password,
                'nurseryName' => Configure::read('email.thanks')
            ];

            $emailResult = $this->sendEmail($emailDetails);

            if ($emailResult) {
                $response = 1;
            }
        }

        return $response;
    }

    /**
     * purchaseOrderRequestNotificationEmail - This method send email for notify new purchase orders of users
     * @param type $to
     * @return int
     */
    public function purchaseOrderRequestNotificationEmail($to) {
        
        $nurseryName = 'Supplier Portal';
        $response = 0;
        // send scanbook to bunning
        $emailDetails = array();
        $emailDetails['subject'] = Configure::read('purchase-order.email-subject') . ' (' . $nurseryName . ')';
        $emailDetails['to'] = $to;
        $emailDetails['email-profile'] = Configure::read('purchase-order.email-profile');
        $emailDetails['template'] = Configure::read('purchase-order.email-template');
        $emailDetails['layout'] = Configure::read('purchase-order.email-layout');
        $emailDetails['emailFormat'] = 'html';

        $nameOfTo = array_values($to);
        $nameOfUser = isset($nameOfTo[0]) ? $nameOfTo[0] : '';
            
        $url = Configure::read('login-url');
        $emailDetails['viewVars'] = [
            'userName' => $nameOfUser,
            'url' => $url,
            'nurseryName' => Configure::read('email.thanks')
        ];

        $emailResult = $this->sendEmail($emailDetails);

        if ($emailResult) {
            $response = 1;
        }

        return $response;
    }

    /*
     * @sendEmail method
     * send email
     * return success or error
     */

    public function sendEmail($emailDetails) {

        $email = new Email();
        
        if (isset($emailDetails['emailFormat'])) {
            $email->emailFormat($emailDetails['emailFormat']);
        }
        
        $email->profile($emailDetails['email-profile']);
        $email->template($emailDetails['template'], $emailDetails['layout']);
        $email->viewVars($emailDetails['viewVars']);
        $email->to($emailDetails['to']);
        $email->subject($emailDetails['subject']);
        if (isset($emailDetails['attachments'])) {
            $email->attachments($emailDetails['attachments']);
        }
        

        try {

            $res = $email->send();
            return $res;
        } catch (Exception $e) {

            echo 'Exception : ', $e->getMessage(), "\n";
            exit;
        }
    }

}
