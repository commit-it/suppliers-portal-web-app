<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

        $this->loadComponent('Flash');
        $this->loadComponent('Common');
        $this->loadComponent('Email');

        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginRedirect' => [
                'controller' => 'Dashboard',
                'action' => 'index'
            ],
        ]);

        $this->loadModel('PurchaseOrders');
        $this->loadModel('Users');
    }

    public function beforeFilter(\Cake\Event\Event $event) {

        $this->Auth->allow(['login', 'display', 'forget_password', 'payment_response','testemail']);

        $authUser = $this->Auth->user();
        $this->set(compact('authUser'));

        /**
         * check new purchase orders is present
         * And update in session
         */
        $newPOCnt = $this->PurchaseOrders->find()->where([
                    'sent_to_myob' => \Cake\Core\Configure::read('sent_to_myob.no'),
                    'sent_to_quick_book' => \Cake\Core\Configure::read('sent_to_quick_book.no'),
                    'archived' => \Cake\Core\Configure::read('po_archived.no')
                ])->count();
        $this->set('newPurchaseOrders', $newPOCnt);

        /**
         * check logged In user is expired or not
         * if yes, then redirect that user to package page
         */
        $isExpired = $this->Users->find('all', [
                    'conditions' => [
                        'Users.id' => $this->Auth->user('id'),
                        'Users.access_status' => \Cake\Core\Configure::read('access_status.main.inactive'),
                        'UserRoles.name' => \App\Model\Table\UserRolesTable::User,
                    ],
                    'contain' => ['UserRoles']
                ])->count();
        if ($isExpired > 0) {
            if ($this->request->action != 'package' && $this->request->action != 'payment_response' && $this->request->action != 'logout' && $this->request->action != 'user_payment_history') {
                return $this->redirect(['controller' => 'Dashboard', 'action' => 'package']);
            }
        }
    }

}
