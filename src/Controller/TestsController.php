<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;
/**
 * Users TestsController
 */
class TestsController extends AppController {

    public function initialize() {
        parent::initialize();

        $this->loadModel('FileOverviewes');
        $this->loadModel('PurchaseOrders');
        $this->loadModel('Invoices');
        $this->loadModel('ProcessLogs');
        $this->loadModel('Users');
    }

    /*
     * Check Files if exist or not for download
     */

    public function checkpofile() {
        $result = $this->FileOverviewes->getPOFileList();
        $this->set('message', $result);
        $this->render('/Tests/index');
    }

    /*
     * Download Purchase Order files
     */

    public function downloadpofile() {
        $result = $this->FileOverviewes->downloadPOFileList();
        $this->set('message', $result);
        $this->render('/Tests/index');
    }

    /*
     * impoer purchase order xml file in db
     */

    public function import_po_xml_file() {
        $result = $this->FileOverviewes->importXmlPurchaseOrders();
        // generate fulfilment Advice & send to myob
        $FAResult = $this->FileOverviewes->generateFulfilmentAdviceAndSendToSftp();
        $this->set('message', $result);
        $this->render('/Tests/index');
    }

    /**
     * generate_fa_xml_file - This method generate Fulfilment Advice & send that to sftp
     */
    public function generate_fa_xml_file() {

        $result = $this->FileOverviewes->generateFulfilmentAdviceAndSendToSftp();
        $this->set('message', $result);
        $this->render('/Tests/index');
    }

    /*
     * get Generated Invoices From Myob
     */

    public function get_generated_invoices_from_myob() {
        $result = $this->Invoices->getGeneratedInvoicesFromMyob();
        $this->set('message', $result);
        $this->render('/Tests/index');
    }

    /**
     * readProcessLogFromMyob  - method
     * Read process log from myob db
     * Then insert that process log in supplier portal db
     * After insert delete inserted logs from Myob db
     */
    public function read_process_log_from_myob() {
        $result = $this->ProcessLogs->readProcessLogFromMyob();
        $this->set('message', $result);
        $this->render('/Tests/index');
    }

    /**
     * inactive_expired_nursery - method
     */
    public function inactive_expired_nursery() {
        $result = $this->Users->inactiveExpiredNursery();
        $this->set('message', $result);
        $this->render('/Tests/index');
    }

    /**
     * send_invoice_reminder - this method send the pending Invoices to Bunnings
     */
    public function send_invoice_reminder() {
        $result = $this->Invoices->sendInvoiceReminder();
        $this->set('message', $result);
        $this->render('/Tests/index');
    }

    /*
     * Display process messages
     */

    public function index() {
//        echo 'time zone => ' . date_default_timezone_get() . "<br /> \n";
//        echo 'date => ' . date('Y-m-d H:i:s');exit;
//        $userObj = TableRegistry::get('Users');
//        //Get delivery_docket_no of that user
//        $userData = $userObj->findById(4)->first();
//        $deliveryDocketNo = $userData->delivery_docket_no;
//        echo $deliveryDocketNo.' / $deliveryDocketNo <pre>';print_r($userData);exit;
        
    }
//    public function testemail() {
//        echo 'welcome';
//        $result = $this->FileOverviewes->testemailsend();
//             $this->set('message', $result);
//        $this->render('/Tests/index');
//    }

    /**
     * execute_command() - Method
     */
    public function execute_command() {

        exec("HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`");
        exec("setfacl -R -m u:${HTTPDUSER}:rwx tmp");
        exec("setfacl -R -d -m u:${HTTPDUSER}:rwx tmp");
        exec("setfacl -R -m u:${HTTPDUSER}:rwx logs");
        exec("setfacl -R -d -m u:${HTTPDUSER}:rwx logs");
        exit;
    }

}
