<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Session;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController {

    public $components = ['Cookie'];

    /**
     * load the model
     */
    public function initialize() {
        parent::initialize();
        $this->loadModel('UserRoles');
        $this->loadModel('Payments');
        $this->Session = new Session();
    }

    /**
     * restoreAuthData - restored modified data in auth data
     * @param type $id
     */
    public function restoreAuthData($id) {

        $user = $this->Users->get($id)->toArray();
        $user['role_name'] = $this->UserRoles->checkIfFieldIsExist($this->UserRoles->findById($user['user_role_id'])->first(), 'name');

        /* Delete current user data */
        $this->Session->delete('Auth.User');

        /* Stored userdata as the logged in user. */
        $this->Auth->setUser($user);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {

        // check logged in user is admin
        if (!$this->Common->checkUserRole()) {
            $this->Flash->error('You don\'t have access for this action');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }
        $this->paginate = [
            'conditions' => ['UserRoles.name' => \App\Model\Table\UserRolesTable::User],
            'contain' => [
                'UserRoles',
                'ProcessLogs' => [
                    'conditions' => ['ProcessLogs.is_myob' => Configure::read('process_logs.is_myob_active')],
                ]
            ],
        ];

        $this->set('users', $this->paginate($this->Users));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {

        // check logged in user is admin
        if (!$this->Common->checkUserRole()) {
            $this->Flash->error('You don\'t have access for this action');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }

        // check allow to edit or not
        if (!$this->Common->checkAllowOrNot($id)) {
            $this->Flash->error('You don\'t have access for this action');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }

        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add() {

        // check logged in user is admin
        if (!$this->Common->checkUserRole()) {
            $this->Flash->error('You don\'t have access for this action');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }

        $user = $this->Users->newEntity();

        if ($this->request->is('post')) {

            $this->request->data['parent_id'] = $this->Auth->user('id');
            $user = $this->Users->patchEntity($user, $this->request->data);
            $checkEmailIsExistOrNot = $this->Users->findByEmail($this->request->data['email'])->first();

            if (empty($checkEmailIsExistOrNot)) {
                if ($this->Users->save($user)) {
                    $this->Flash->success('The user has been saved.');
                    return $this->redirect(['action' => 'index']);
                } else {
                    if (!empty($user->errors())) {

                        $errorMsgs = $this->Common->getErrorMsgForDisplay($user->errors());
                        $this->Flash->error($errorMsgs);
                    } else {

                        $this->Flash->error('The user could not be saved. Please, try again.');
                    }
                }
            } else {
                $this->Flash->error('The email address is already exist.');
            }
        }
        $defaultRole = $this->UserRoles->checkIfFieldIsExist($this->UserRoles->findByName(\App\Model\Table\UserRolesTable::User)->first(), 'id');
        $userRoleList = $this->UserRoles->find('list', ['keyField' => 'id', 'valueField' => 'name']);
        $this->set(compact('defaultRole'));
        $this->set(compact('userRoleList'));
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * edit - Action
     * @param type $id
     * @param type $action
     * @return type
     */
    public function edit($id = null, $action = null) {

        // check logged in user is admin
        if (!$this->Common->checkUserRole($id)) {
            $this->Flash->error('You don\'t have access for this action');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }

        if ($action == null) {

            // check allow to edit or not
            if (!$this->Common->checkAllowOrNot($id)) {
                $this->Flash->error('You don\'t have access for this action');
                return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
            }
        }

        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

//                if (strtotime($this->request->data['old_trial_end_date']) != strtotime($this->request->data['trial_end_date'])) {
//                    $this->request->data['payment_status'] = Configure::read('payment_status.main.trial');
//                }
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {

                if ($id == $this->Auth->user('id')) {

                    // restored modified data in auth data
                    $this->restoreAuthData($this->Auth->user('id'));

                    $this->Flash->success('Your profile updated successfully.');
                    return $this->redirect(['action' => 'profile']);
                } else {

                    $this->Flash->success('The user has been updated.');
                    return $this->redirect(['action' => 'index']);
                }
            } else {

                if (!empty($user->errors())) {

                    $errorMsgs = $this->Common->getErrorMsgForDisplay($user->errors());
                    $this->Flash->error($errorMsgs);
                } else {

                    $this->Flash->error('The user could not be updated. Please, try again.');
                }
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null) {

        $this->render('/Error/not_access_to_delete', 'custom_error');
    }

    /**
     * login the user
     * @return type
     */
    public function login() {

        $this->layout = 'login';

        if ($this->Auth->user()) {
            $this->redirect($this->Auth->redirectUrl());
        }

        $userForm = $this->Users->newEntity();
        if ($this->request->is('post')) {

            $user = $this->Auth->identify();
            if ($user) {

                if ($this->request->data['remember_me']) {
                    $this->Cookie->write('User.email', $this->request->data['email']);
                    $this->Cookie->write('User.password', $this->request->data['password']);
                    $this->Cookie->write('User.remember_me', $this->request->data['remember_me']);
                } else if (!$this->request->data['remember_me']) {
                    $this->Cookie->delete('User.email');
                    $this->Cookie->delete('User.password');
                    $this->Cookie->delete('User.remember_me');
                }

                $user['role_name'] = $this->UserRoles->checkIfFieldIsExist($this->UserRoles->findById($user['user_role_id'])->first(), 'name');
                $this->Auth->setUser($user);

                if ($this->Auth->user('role_name') == \App\Model\Table\UserRolesTable::Admin) {
                    return $this->redirect($this->Auth->redirectUrl(['controller' => 'Users', 'action' => 'index']));
                } else {
                    return $this->redirect($this->Auth->redirectUrl());
                }
            }

            $this->Flash->error('Your username or password is incorrect.');
        }
        $this->set(compact('userForm'));
        $this->set('cookieHelper', $this->Cookie->read('User'));
    }

    /**
     * logout
     * @return type
     */
    public function logout() {

        if ($this->Session->read('Auth.Admin')) {

            /* Stored userdata as the logged in user. */
            $this->Auth->setUser($this->Session->read('Auth.Admin'));

            $this->Session->delete('Auth.Admin');

            /* Redirect to user dashboard */
            if ($this->Auth->user('role_name') == \App\Model\Table\UserRolesTable::Admin) {
                return $this->redirect($this->Auth->redirectUrl(['controller' => 'Users', 'action' => 'index']));
            } else {
                return $this->redirect($this->Auth->redirectUrl());
            }
        }

        return $this->redirect($this->Auth->logout());
    }

    /**
     * profile - Method
     * @return type
     */
    public function profile() {

        if ($this->request->is(['patch', 'post', 'put'])) {

            if ($this->request->data['new_password'] == $this->request->data['confirm_password']) {

                $checkOldPasswordMatchWithDB = $this->Users->compareOldPasswordWithDB($this->request->data['old_password'], $this->Auth->user('id'));
                if ($checkOldPasswordMatchWithDB) {

                    $this->request->data['password'] = $this->request->data['confirm_password'];
                    $userData = $this->Users->get($this->Auth->user('id'));
                    $user = $this->Users->patchEntity($userData, $this->request->data);

                    if ($this->Users->save($user)) {

                        $this->Flash->success('The Password has been updated successfully.');
                    } else {

                        if (!empty($user->errors())) {

                            $errorMsgs = $this->Common->getErrorMsgForDisplay($user->errors());
                            $this->Flash->error($errorMsgs);
                        } else {

                            $this->Flash->error('The Password has not been updated, please try again');
                        }
                    }
                } else {

                    $this->Flash->error('The Old Password not match with DB password, please try again');
                }
            } else {

                $this->Flash->error('New Password not match with Confirm Password');
            }

            return $this->redirect(['action' => 'profile']);
        }

        $user = $this->Users->get($this->Auth->user('id'));
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * business_profile() - Method
     */
    public function business_profile() {

        $user = $this->Users->get($this->Auth->user('id'));

        $sellerBusinessLogoImg = '/img/no_image.png';
        if (isset($user->seller_business_logo)) {
            if (file_exists(Configure::read('seller_business_logo_image_path') . DS . $user->seller_business_logo) && (!empty($user->seller_business_logo))) {
                $sellerBusinessLogoImg = Configure::read('uploaded_seller_business_logo_image_path') . $user->seller_business_logo;
            }
        }
        $this->set(compact('user', 'sellerBusinessLogoImg'));
        $this->set('_serialize', ['user']);
    }

    /**
     * edit_business_profile - Method
     * @return type
     */
    public function edit_business_profile() {

        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            /* Updaload seller_business_logo */
            if (isset($this->request->data['seller_business_logo_new']['tmp_name']) && is_uploaded_file($this->request->data['seller_business_logo_new']['tmp_name'])) {

                /* Upload new seller business logo */
                $newUploadedSellerBusinessLogo = $this->Common->uploadImage($this->request->data['seller_business_logo_new'], Configure::read('seller_business_logo_image_path'), 'seller_business_logo_');

                /* If logo upload and existing logo exist then remove existing uploaded logo */
                if ($newUploadedSellerBusinessLogo != '' && $this->request->data['seller_business_logo'] != '') {
                    if (file_exists(Configure::read('seller_business_logo_image_path') . DS . $this->request->data['seller_business_logo'])) {
                        unlink(Configure::read('seller_business_logo_image_path') . DS . $this->request->data['seller_business_logo']);
                    }
                }

                $resizeImage = $this->Common->smartResizeImage(Configure::read('seller_business_logo_image_path') . DS . $newUploadedSellerBusinessLogo, Configure::read('seller_business.logo_width'), Configure::read('seller_business.logo_height'));

                /* Save $newUploadedCourierLogo in database */
                $this->request->data['seller_business_logo'] = $newUploadedSellerBusinessLogo;
            }

            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {

                // restored modified data in auth data
                $this->restoreAuthData($this->Auth->user('id'));

                $this->Flash->success('Your Business Profile updated successfully.');
                return $this->redirect(['action' => 'business_profile']);
            } else {

                if (!empty($user->errors())) {

                    $errorMsgs = $this->Common->getErrorMsgForDisplay($user->errors());
                    $this->Flash->error($errorMsgs);
                } else {

                    $this->Flash->error('The User Business details could not be updated. Please, try again.');
                }
            }
        }

        $sellerBusinessLogoImg = '/img/no_image.png';
        if (file_exists(Configure::read('seller_business_logo_image_path') . DS . $user->seller_business_logo) && (!empty($user->seller_business_logo))) {
            $sellerBusinessLogoImg = Configure::read('uploaded_seller_business_logo_image_path') . $user->seller_business_logo;
        }
        $this->set(compact('user', 'sellerBusinessLogoImg'));
        $this->set('_serialize', ['user']);
    }

    /**
     * configuration() Action
     * get user, customer (bunnings stores), nursery setup, accout details
     * @return data
     */
    public function configuration() {

        $bunningStoresCnt = $this->Users->BunningStores->find('all')->count('*');
        $masterItemsCnt = $this->Users->MasterItems->find('all')->count('*');
        $userId = $this->Auth->user('id');
        $this->set(compact('bunningStoresCnt', 'masterItemsCnt', 'userId'));
    }

    /**
     * forget_password - Method
     * @return type
     */
    public function forget_password() {

        if ($this->request->is(['patch', 'post', 'put'])) {

            $email = isset($this->request->data['email']) ? $this->request->data['email'] : '';
            $userInfo = $this->Users->findByEmail($email)->first();
            $userDetails = (!empty($userInfo)) ? $userInfo->toArray() : array();
            if (!empty($userDetails)) {

                // generate random password with 8 character to send
                $password = $this->Common->generateRandomCode(8);
                $to = [$userDetails['email'] => $userDetails['full_name']];
                // Send Forget Password Email
                $forgetPasswordSendResult = $this->Email->generateEmailForForgetPasswordAndSend($to, $password);
                if ($forgetPasswordSendResult > 0) {

                    // update password
                    $changeData = array();
                    $changeData['password'] = $password;
                    $userData = $this->Users->get($userDetails['id']);
                    $user = $this->Users->patchEntity($userData, $changeData);
                    if ($this->Users->save($user)) {

                        $this->Flash->success('Please check your e-mail for password. We have sent password on your e-mail.');
                    } else {

                        if (!empty($user->errors())) {

                            $errorMsgs = $this->Common->getErrorMsgForDisplay($user->errors());
                            $this->Flash->error($errorMsgs);
                        } else {

                            $this->Flash->error('Email sent successfully with password but didn\'t modify sent password in database. So can\'t login with sent password. Please try again');
                        }
                    }
                } else {

                    $this->Flash->error('Getting some error during send password, please try again');
                }
            } else {

                $this->Flash->error('E-mail is not exist for send new password, please try with correct e-mail');
            }
        } else {

            $this->Flash->error('Getting some error, please try again');
        }

        return $this->redirect(['action' => 'login']);
    }

    /**
     * login_as_nursery - Method
     * @param type $id
     */
    public function login_as_nursery($id) {

        /* If auth role_name is Admin then only store data in session in 'Auth.Admin' variable  */
        if ($this->Auth->user('role_name') == \App\Model\Table\UserRolesTable::Admin) {
            $this->Session->write('Auth.Admin', $this->Session->read('Auth.User'));
        }


        $this->restoreAuthData($id);

        /* Redirect to user dashboard */
        if ($this->Auth->user('role_name') == \App\Model\Table\UserRolesTable::Admin) {
            return $this->redirect($this->Auth->redirectUrl(['controller' => 'Users', 'action' => 'index']));
        } else {
            return $this->redirect($this->Auth->redirectUrl());
        }
    }

    /**
     * Current Logged In User Payment History
     */
    public function user_payment_history() {

        /* Find Record By User Id */
        $paymentDetails = $this->Users->findRecordByUserId($this->Auth->user('id'));
        if (isset($paymentDetails['payments'][0]['payment_histories'])) {
            $paymentDetails['payments'][0]['payment_histories'] = $this->Users->multisortArray($paymentDetails['payments'][0]['payment_histories'], array(), 'created', 'desc');
        }

        $this->set('paymentDetails', $paymentDetails);
    }

    /**
     * All Users Payment History For Admin
     */
    public function payment_history() {

        // check logged in user is admin
        if (!$this->Common->checkUserRole()) {
            $this->Flash->error('You don\'t have access for this action');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }

        /* Get Users And Payment information */
        $this->paginate = $this->Users->getUsersAndPaymentInfo();
        $this->set('users', $this->paginate());
        $this->set('_serialize', ['users']);
    }

    /**
     * View Payment History Of User For Only Admin
     */
    public function view_payment_history($userId) {

        // check logged in user is admin
        if (!$this->Common->checkUserRole()) {
            $this->Flash->error('You don\'t have access for this action');
            return $this->redirect(['controller' => 'Dashboard', 'action' => 'index']);
        }

        /* Find Record By User Id */
        $paymentDetails = $this->Users->findRecordByUserId($userId);
        if (isset($paymentDetails['payments'][0]['payment_histories'])) {
            $paymentDetails['payments'][0]['payment_histories'] = $this->Users->multisortArray($paymentDetails['payments'][0]['payment_histories'], array(), 'created', 'desc');
        }

        $this->set('paymentDetails', $paymentDetails);
    }

    /**
     * quick_book_profile - Method
     * @param type $update
     * @return type
     */
    public function quick_book_profile($update = null) {

        $updateOrNot = 0;
        $quickBookProfileData = $this->Users->get($this->Auth->user('id'), [
            'contain' => []
        ]);

        $quickBookPaymentTermList = array();
        $paymentTerm = '';
        // load QuickBook Component
        $this->loadComponent('QuickBook');
        if ($update == 'update') {

            $updateOrNot = 1;
            $quickBookPaymentTerms = $this->QuickBook->findPaymentTermList();
            if ($quickBookPaymentTerms['status'] == 'success') {

                foreach ($quickBookPaymentTerms['response'] as $quickBookPaymentTerm) {

                    $quickBookPaymentTermList[$quickBookPaymentTerm->Id] = $quickBookPaymentTerm->Name;
                }
            }
        } else {

            if ((isset($quickBookProfileData['quick_book_payment_term']) && $quickBookProfileData['quick_book_payment_term'])) {

                $quickBookPaymentTerm = $this->QuickBook->findTermDetailByFieldName('Id', $quickBookProfileData['quick_book_payment_term']);
                if ($quickBookPaymentTerm['status'] == 'success') {
                    $paymentTerm = isset($quickBookPaymentTerm['response'][0]->Name) ? $quickBookPaymentTerm['response'][0]->Name : '';
                }
            }
        }


        if ($this->request->is(['patch', 'post', 'put'])) {

            $user = $this->Users->patchEntity($quickBookProfileData, $this->request->data);
            if ($this->Users->save($user)) {

                // restored modified data in auth data
                $this->restoreAuthData($this->Auth->user('id'));

                $this->Flash->success('Your Quick Book Profile updated successfully.');
                return $this->redirect(['action' => 'quick_book_profile']);
            } else {

                if (!empty($user->errors())) {

                    $errorMsgs = $this->Common->getErrorMsgForDisplay($user->errors());
                    $this->Flash->error($errorMsgs);
                } else {

                    $this->Flash->error('The Quick Book Profile details could not be updated. Please, try again.');
                }
            }
        }

        $this->set('quickBookData', $quickBookProfileData);
        $this->set('quickBookPaymentTermList', $quickBookPaymentTermList);
        $this->set('paymentTerm', $paymentTerm);
        $this->set('updateOrNot', $updateOrNot);
    }

}
