<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * ScanBooks Controller
 *
 * @property \App\Model\Table\ScanBooksTable $ScanBooks
 */
class QuickBooksController extends AppController {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

        // load Component
        $this->loadComponent('QuickBook');

        // load Model
        $this->loadModel('PurchaseOrders');
        $this->loadModel('BunningStores');
        $this->loadModel('Users');
        $this->loadModel('MasterItems');
        $this->loadModel('Invoices');
        $this->loadModel('ProcessLogs');
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index() {

        $resultArr = array();
        if ($this->request->is(['patch', 'post', 'put'])) {

            $poIdArr = isset($this->request->data['po_ids']) ? explode(',', $this->request->data['po_ids']) : array();
            if (!empty($poIdArr)) {

                $processLogResultArr = array();

                // find bunnings store data by gln number
                $purchaseOrdersIds = $poIdArr;
                $purchaseOrderData = $this->PurchaseOrders->find('all', [
                            'conditions' => ['PurchaseOrders.id IN' => $purchaseOrdersIds],
                            'contain' => ['PurchaseOrderItems']
                        ])->toArray();
                if (!empty($purchaseOrderData)) {
                    $invoiceCreateDataObjOfSP = '';
                    foreach ($purchaseOrderData as $purchaseOrder) {

                        if ($purchaseOrder['actual_delivery_date_updated'] > 0) {
                            
                            $payerData = $this->BunningStores->findByGln($purchaseOrder['bill_to_gln'])->toArray();
                            $buyerData = $this->BunningStores->findByGln($purchaseOrder['buyer_gln'])->toArray();

                            if (!empty($payerData)) {

                                $customerResponse = $this->QuickBook->checkCustomerAvailableToQuickBookOtherWiseCreate($payerData, $purchaseOrder);
                                $quickBookCustomerId = isset($customerResponse['quickBookCustomerId']) ? $customerResponse['quickBookCustomerId'] : NULL;
                                if ($quickBookCustomerId) {

                                    foreach ($customerResponse['success'] as $customerResponseSuccess) {

                                        $resultArr['success'][$purchaseOrder['purchase_order_number']][] = $customerResponseSuccess;
                                        $processLogResultArr[] = $customerResponseSuccess;
                                    }
                                } else {

                                    foreach ($customerResponse['error'] as $customerResponseError) {

                                        $resultArr['error'][$purchaseOrder['purchase_order_number']][] = $customerResponseError;
                                        $processLogResultArr[] = $customerResponseError;
                                    }
                                }

                                if ($quickBookCustomerId) {

                                    // create invoice items to Quick Book if not exist otherwise find details
                                    if (isset($purchaseOrder['purchase_order_items']) && !empty($purchaseOrder['purchase_order_items'])) {

                                        $listOfExistMasterItems = array();
                                        $listOfNotExistMasterItems = array();
                                        foreach ($purchaseOrder['purchase_order_items'] as $purchaseOrderItem) {
                                            $masterItemData = $this->MasterItems->findDataByGtin($purchaseOrderItem['gtin']);
                                            if (!empty($masterItemData)) {

                                                $listOfExistMasterItems[] = $masterItemData;
                                            } else {

                                                $listOfNotExistMasterItems[] = $purchaseOrderItem['gtin'];
                                            }
                                        }

                                        if (count($listOfNotExistMasterItems) < 1) {

                                            // create invoice items to Quick Book if not exist otherwise find details
                                            $invoiceItemsResponse = $this->QuickBook->checkItemAvailableToQuickBookOtherWiseCreate($listOfExistMasterItems);

                                            if (count($invoiceItemsResponse['error']) < 1) {

                                                foreach ($invoiceItemsResponse['success'] as $itemResponseSuccess) {

                                                    $resultArr['success'][$purchaseOrder['purchase_order_number']][] = $itemResponseSuccess;
                                                    $processLogResultArr[] = $itemResponseSuccess;
                                                }

                                                if (count($invoiceItemsResponse['quickBookItemId']) > 0) {

                                                    for ($i = 0; $i < count($purchaseOrder['purchase_order_items']); $i++) {

                                                        $masterItemData = $this->MasterItems->findDataByGtin($purchaseOrder['purchase_order_items'][$i]['gtin']);
                                                        if (!empty($masterItemData)) {

                                                            $purchaseOrder['purchase_order_items'][$i]['quick_book_item_id'] = $masterItemData->quick_book_item_id;
                                                            $purchaseOrder['purchase_order_items'][$i]['description'] = $masterItemData->description;
                                                        }
                                                    }

                                                    // create invoice of customer
                                                    $invoiceResponse = $this->QuickBook->createInvoiceToQuickBook($quickBookCustomerId, $purchaseOrder, $buyerData);
                                                    $invoiceResponse['status'] = isset($invoiceResponse['status']) ? $invoiceResponse['status'] : '';
                                                    $invoiceResponse['msg'] = isset($invoiceResponse['msg']) ? $invoiceResponse['msg'] : 'Some thing wrong, please try again';
                                                    $invoiceCreateDataObjOfSP = isset($invoiceResponse['invoiceCreateDataObjOfSP']) ? $invoiceResponse['invoiceCreateDataObjOfSP'] : '';
                                                    
                                                    if ($invoiceResponse['status'] == 'success') {

                                                        $resultArr['success'][$purchaseOrder['purchase_order_number']][] = $invoiceResponse['msg'];

                                                        // create invoice DB
                                                        $invoiceCreateResponse = $this->Invoices->createQuickBookInvoiceAndUpdateRelatedDataInDB($invoiceResponse['response'], $purchaseOrder);
                                                        if ($invoiceCreateResponse) {

                                                            if (isset($invoiceCreateResponse['success'])) {
                                                                foreach ($invoiceCreateResponse['success'] as $success) {
                                                                    $resultArr['success'][$purchaseOrder['purchase_order_number']][] = $success;
                                                                    $processLogResultArr[] = $success;
                                                                }
                                                            }

                                                            if (isset($invoiceCreateResponse['error'])) {
                                                                foreach ($invoiceCreateResponse['error'] as $error) {
                                                                    $resultArr['error'][$purchaseOrder['purchase_order_number']][] = $error;
                                                                    $processLogResultArr[] = $error;
                                                                }
                                                            }

                                                            $processLogResultArr[] = 'Invoice Created Successfully to Quick-Books';
                                                            $processLogResultArr[] = 'Invoice Inserted Successfully in database';
                                                        } else {

                                                            $resultArr['error'][$purchaseOrder['purchase_order_number']][] = 'Invoice does not Inserted Successfully in database';
                                                            $processLogResultArr[] = 'Invoice does not Inserted Successfully in database';
                                                        }
                                                    } else {

                                                        $resultArr['error'][$purchaseOrder['purchase_order_number']][] = $invoiceResponse['msg'];
                                                        $processLogResultArr[] = $invoiceResponse['msg'];
                                                    }
                                                } else {

                                                    $resultArr['error'][$purchaseOrder['purchase_order_number']][] = 'Items is not present to create Invoice';
                                                    $processLogResultArr[] = 'Items is not present to create Invoice';
                                                }
                                            } else {

                                                foreach ($invoiceItemsResponse['error'] as $invoiceItemsResponseError) {

                                                    $resultArr['error'][$purchaseOrder['purchase_order_number']][] = $invoiceItemsResponseError;
                                                    $processLogResultArr[] = $invoiceItemsResponseError;
                                                }
                                            }
                                        } else {

                                            $resultArr['error'][$purchaseOrder['purchase_order_number']][] = 'You can\'t generate Invoice because the ' . implode(', ', $listOfNotExistMasterItems) . ' items are not present in Master Items list.';
                                            $processLogResultArr[] = 'You can\'t generate Invoice because the ' . implode(', ', $listOfNotExistMasterItems) . ' items are not present in Master Items list.';
                                        }
                                    } else {

                                        $resultArr['error'][$purchaseOrder['purchase_order_number']][] = 'You can\'t create Invoice without Items';
                                        $processLogResultArr[] = 'You can\'t create Invoice without Items';
                                    }
                                } else {

                                    $resultArr['error'][$purchaseOrder['purchase_order_number']][] = 'You can\'t create Invoice without Customer and we are getting error during create customer. Please try again';
                                    $processLogResultArr[] = 'You can\'t create Invoice without Customer and we are getting error during create customer. Please try again';
                                }
                            } else {

                                $resultArr['error'][$purchaseOrder['purchase_order_number']][] = 'You can\'t create Invoice without Customer (Buyer), Please add entry for ' . $purchaseOrder['bill_to_gln'] . ' in Bunning Stores before create Invoice';
                                $processLogResultArr[] = 'You can\'t create Invoice without Customer (Buyer), Please add entry for ' . $purchaseOrder['bill_to_gln'] . ' in Bunning Stores before create Invoice';
                            }

                        } else {
                            
                            $resultArr['error'][$purchaseOrder['purchase_order_number']][] = 'You can\'t send that PO to Quick-Books, you need to set Actual Delivery Date';
                            $processLogResultArr[] = 'You can\'t send ' . $purchaseOrder['purchase_order_number'] . ' that PO to Quick-Books, you need to set Actual Delivery Date';
                        }
                        
                        if (!empty($processLogResultArr)) {

                            // store process log details
                            $processLogResult = $this->ProcessLogs->generateProcessLogArrAndSave($processLogResultArr, $purchaseOrder['user_id'], $purchaseOrder['seller_gln'], $purchaseOrder['purchase_order_number']);
                            
                            // Below log only for admin
                            $processLogResult = $this->ProcessLogs->generateProcessLogArrAndSave('Data Object of Supplier Portal to Send Quick-Book for create Invoice : ' . $invoiceCreateDataObjOfSP);
                        }
                    }
                } else {

                    $orderText = 'Order is';
                    if (count($purchaseOrdersIds) > 1) {

                        $orderText = 'Orders are';
                    }
                    $resultArr['error'] = 'Selected Purchase ' . $orderText . ' not available to create Invoice';

                    // store process log details
                    $processLogResult = $this->ProcessLogs->generateProcessLogArrAndSave('Selected Purchase ' . $orderText . ' not available to create Invoice');
                }
            } else {

                $resultArr['error'] = 'You can\'t send empty data to Quick Book for generate Invoice';
            }
        } else {

            $resultArr['error'] = 'You can\'t send empty data to Quick Book for generate Invoice';
        }

        $this->Flash->quickbookmsg('Quick Book Message', ['key' => 'quick-book-flash', 'params' => $resultArr]);
        return $this->redirect(['controller' => 'PurchaseOrders', 'action' => 'index']);
    }

}
