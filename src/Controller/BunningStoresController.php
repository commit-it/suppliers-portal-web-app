<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * BunningStores Controller
 *
 * @property \App\Model\Table\BunningStoresTable $BunningStores
 */
class BunningStoresController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

        $this->loadComponent('PHPExcel');
    }
    
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $this->set('bunningStores', $this->paginate($this->BunningStores));
        $this->set('_serialize', ['bunningStores']);
    }

    /**
     * View method
     *
     * @param string|null $id Bunning Store id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bunningStore = $this->BunningStores->get($id, [
            'contain' => ['Users']
        ]);
        $this->set('bunningStore', $bunningStore);
        $this->set('_serialize', ['bunningStore']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bunningStore = $this->BunningStores->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['user_id'] = $this->Auth->user('id');
            $bunningStore = $this->BunningStores->patchEntity($bunningStore, $this->request->data);
            if ($this->BunningStores->save($bunningStore)) {
                $this->Flash->success('The bunning store has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                if (!empty($bunningStore->errors())) {
                    
                    $errorMsgs = $this->Common->getErrorMsgForDisplay($bunningStore->errors());
                    $this->Flash->error($errorMsgs);
                } else {
                    
                    $this->Flash->error('The bunning store could not be saved. Please, try again.');
                }
            }
        }
        $this->set(compact('bunningStore'));
        $this->set('_serialize', ['bunningStore']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Bunning Store id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bunningStore = $this->BunningStores->get($id, [
            'contain' => []
        ]);
       
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bunningStore = $this->BunningStores->patchEntity($bunningStore, $this->request->data);
            if ($this->BunningStores->save($bunningStore)) {
                $this->Flash->success('The bunning store has been saved.');
                return $this->redirect(['action' => 'index']);
            } else {
                if (!empty($bunningStore->errors())) {
                    
                    $errorMsgs = $this->Common->getErrorMsgForDisplay($bunningStore->errors());
                    $this->Flash->error($errorMsgs);
                } else {
                    
                    $this->Flash->error('The bunning store could not be updated. Please, try again.');
                }
            }
        }
        $this->set(compact('bunningStore'));
        $this->set('_serialize', ['bunningStore']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Bunning Store id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $bunningStore = $this->BunningStores->get($id);
        
        if ($bunningStore->user_id != $this->Auth->user('id')) {
            $this->Flash->error('You don\'t have access to delete that bunning store');
            return $this->redirect(['action' => 'index']);
        }
        
        if ($this->BunningStores->delete($bunningStore)) {
            $this->Flash->success('The bunning store has been deleted.');
        } else {
            $this->Flash->error('The bunning store could not be deleted. Please, try again.');
        }
        return $this->redirect(['action' => 'index']);
    }
    
    /**
     * import_bunning_stores - Method
     * @return type
     */
    public function import_bunning_stores() {
    
        if ($this->request->is(['patch', 'post', 'put'])) {

            $responseArr = array();
            
            // upload file
            $fileName = $this->Common->uploadFile(Configure::read('bunning-stores.localPath') . DS . $this->Auth->user('id'), $this->request->data['bunningstores'], Configure::read('bunning-stores.file-prefix'));

            if ($fileName != '') {

                $responseArr[] = $fileName . ' Bunning Stores file uploaded successfully';

                /*
                 * Bunning Stores file file in table
                 */
                $excelFileName = Configure::read('bunning-stores.localPath') . DS . $this->Auth->user('id') . DS . $fileName;

                    // get Excel Data
                    $dataArr = $this->PHPExcel->getExcelFileData($excelFileName);

                    if ((!empty($dataArr)) && (count($dataArr) > Configure::read('bunning-stores.row_start_cnt_for_import'))) {

                        // check Excel file format is correct for import
                        $isAllowToImport = $this->Common->checkFileIsCorrectToImport($dataArr, 'bunning-stores');

                        if ($isAllowToImport) {
                            
                            $responseArr[] = (count($dataArr) - Configure::read('bunning-stores.row_start_cnt_for_import')) . ' data is available for import in ' . $fileName;

                            // format array to store in bunning_stores table
                            $bunningStoresData = $this->BunningStores->modifyBunningStoresArr($dataArr, $this->Auth->user('id'));
                            
                            // save or update data
                            $saveCnt = 0;
                            foreach ($bunningStoresData as $bunningStore) {
                                
                                // check for duplicate email id
                                $bunningDuplicateEmailCnt = $this->BunningStores->find('all',
                                        ['conditions' => [
                                            'email' => $bunningStore['email'],
                                            'gln !=' => $bunningStore['gln'],
                                        ]])->count();
                                
                                if ($bunningDuplicateEmailCnt < 1) {
                                    
                                    $article = $this->BunningStores->findByGln($bunningStore['gln'])->first();

                                    if (isset($article->id)) {

                                        // update if master item is already exist for logged in user
                                        $entity = $this->BunningStores->patchEntity($article, $bunningStore);
                                    } else {

                                        // save master item
                                        $entity = $this->BunningStores->newEntity($bunningStore);
                                    }
                                    if (!$entity->errors()) {

                                        $result = $this->BunningStores->save($entity);
                                        if ($result->id) {
                                            $saveCnt++;
                                        } else {
                                            $responseArr[] = $entity->name . ' record is not saved';
                                        }
                                    } else {

                                        $error = $entity->errors();
                                        foreach ($error as $errorKey => $errorVal) {
                                            foreach ($errorVal as $val) {
                                                $responseArr[] = $entity->name . ' record is not saved due to ' . $errorKey . ' = ' . $val;
                                            }
                                        }
                                    }
                                } else {
                                    
                                    $responseArr[] = $entity->name . ' record is not saved due to duplicate email';
                                }
                            }

                            if ($saveCnt > 0) {

                                if ((count($dataArr) - Configure::read('bunning-stores.row_start_cnt_for_import')) == $saveCnt) {
                                    
                                    $this->Flash->success('All Bunning Stores imported or updated successfully...');
                                    $responseArr[] = 'All Bunning Stores imported or updated successfully...';
                                } else {
                                    
                                    $this->Flash->error('Only ' . $saveCnt .' records inserted or updated if exist from ' . (count($dataArr) - Configure::read('bunning-stores.row_start_cnt_for_import')) . ', may be values are empty for some fields');
                                    $responseArr[] = 'Only ' . $saveCnt .' records inserted or updated if exist from ' . (count($dataArr) - Configure::read('bunning-stores.row_start_cnt_for_import')) . ', may be values are empty for some fields';
                                }

                                // move imported file in out folder
                                if ($this->BunningStores->moveFileInAnotherFolder($fileName, Configure::read('bunning-stores.localPath') . DS . $this->Auth->user('id'), Configure::read('bunning-stores.localPath-out') . DS . $this->Auth->user('id'))) {
                                    
                                    $responseArr[] = $fileName . " file moved successfully in BunningStores In folder";
                                } else {
                                    
                                    $responseArr[] = $fileName . " file not moved successfully in BunningStores In folder";
                                }

                            } else {
                                
                                $this->Flash->error('Getting some error during import bunning stores may be some fields are empty, please check you excel file & try agian...');
                                $responseArr[] = 'Data is not save for ' . $entity->name;
                            }
                        } else {
                            
                            $this->Flash->error("The uploaded format of excel file is not valid. Please click Acceptable Format button to see the acceptable format");
                            $responseArr[] = 'The uploaded format of excel file is not valid';
                        }
                } else {
                    
                    $responseArr[] = 'data is not available for import in ' . $fileName;
                    $this->Flash->error('data is not available for import in ' . $fileName);
                }
            } else {

                $this->Flash->error('Getting some error during upload file, please try agian...');
            }
            
            return $this->redirect(['action' => 'index']);
        }
        
    }
    
}
