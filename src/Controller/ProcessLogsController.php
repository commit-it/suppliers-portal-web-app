<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * ProcessLogs Controller
 *
 * @property \App\Model\Table\ProcessLogsTable $ProcessLogs
 */
class ProcessLogsController extends AppController {

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * @return void
     */
    public function initialize() {
        parent::initialize();

        $this->loadComponent('Prg');
    }
    
    /**
     * Index method
     *
     * @return void
     */
    public function index() {
        
        $this->paginate = [
            'order' => ['ProcessLogs.created' => 'DESC'],
            'contain' => ['Users']
        ];
        
        $this->Prg->commonProcess();
        $this->set('processLogs', $this->paginate($this->ProcessLogs->find('searchable', $this->Prg->parsedParams())));
        $this->set('_serialize', ['processLogs']);
    }

    /**
     * View method
     *
     * @param string|null $id Process Log id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null) {
        $processLog = $this->ProcessLogs->get($id, [
            'contain' => ['Users']
        ]);
        $this->set('processLog', $processLog);
        $this->set('_serialize', ['processLog']);
    }

}
