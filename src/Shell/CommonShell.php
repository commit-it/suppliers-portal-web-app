<?php

namespace App\Shell;

use Cake\Console\Shell;
use Cake\Core\Configure;

/**
 * Common shell command.
 */
class CommonShell extends Shell {

    public function initialize() {
        parent::initialize();
    }

}
