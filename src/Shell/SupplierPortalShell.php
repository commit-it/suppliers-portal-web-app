<?php

namespace App\Shell;

use Cake\Console\Shell;

/**
 * SupplierPortal shell command.
 */
class SupplierPortalShell extends Shell {

    public function initialize() {
        parent::initialize();

        $this->loadModel('FileOverviewes');
        $this->loadModel('Invoices');
        $this->loadModel('ProcessLogs');
        $this->loadModel('Users');
    }

    /**
     * getFileList() method.
     *
     * @return Purchase Order files
     */
    public function getFileList() {
        $result = $this->FileOverviewes->getPOFileList();
        $this->out(print_r($result), TRUE);
    }

    /**
     * downloadPurchaseOrderFiles() method.
     *
     * @return Purchase Order files
     */
    public function downloadPurchaseOrderFiles() {
        $result = $this->FileOverviewes->downloadPOFileList();
        $this->out(print_r($result), TRUE);
    }

    /**
     * importPurchaseOrderFiles() method.
     *
     * @return result
     */
    public function importPurchaseOrderFiles() {
        $result = $this->FileOverviewes->importXmlPurchaseOrders();
        $this->out(print_r($result), TRUE);

        // generate fulfilment Advice & send to myob
        $FAResult = $this->FileOverviewes->generateFulfilmentAdviceAndSendToSftp();
        $this->out(print_r($FAResult), TRUE);
    }

    /**
     * generateFulfilmentAdvice() method.
     *
     * @return result
     */
    public function generateFulfilmentAdvice() {
        // generate fulfilment Advice & send to myob
        $FAResult = $this->FileOverviewes->generateFulfilmentAdviceAndSendToSftp();
        $this->out(print_r($FAResult), TRUE);
    }

    /**
     * getGeneratedInvoicesFromMyob() method.
     *
     * @return invoices result
     */
    public function getGeneratedInvoicesFromMyob() {
        $result = $this->Invoices->getGeneratedInvoicesFromMyob();
        $this->out(print_r($result), TRUE);
    }

    /**
     * generateInvoice() method.
     *
     * @return invoices xml file result
     */
    public function generateInvoice() {
        $result = $this->Invoices->generateInvoiceXmlFileAndSendToSftp();
        $this->out(print_r($result), TRUE);
    }

    /**
     * readProcessLogFromMyob  - method
     * Read process log from myob db
     * Then insert that process log in supplier portal db
     * After insert delete inserted logs from Myob db
     */
    public function readProcessLogFromMyob() {
        $result = $this->ProcessLogs->readProcessLogFromMyob();
        $this->out(print_r($result), TRUE);
    }

    /**
     * inactiveExpiredNursery - method
     */
    public function inactiveExpiredNursery() {
        $result = $this->Users->inactiveExpiredNursery();
        $this->out(print_r($result), TRUE);
    }

    /**
     * sendInvoiceReminder - This method send pending invoices to bunnings
     */
    public function sendInvoiceReminder() {
        $result = $this->Invoices->sendInvoiceReminder();
        $this->out(print_r($result), TRUE);
    }

}
