
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Overview') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body row">
            <div class="form-group form-md-line-input">
                <div class="col-md-12">
                    <?php
                    if (!empty($message)) {
                        foreach ($message as $msg) {
                            if (is_array($msg)) {
                                echo '<pre>';
                                print_r($msg);
                            } else if (is_string($msg)) {
                                echo $msg;
                            }
                            echo "<br />";
                        }
                    } else {
                        echo "Nothing to display";
                    }
                    ?>
                </div>
            </div> 
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-11 col-md-1">
                    <?php
                    if (isset($return_invoice) && $return_invoice > 0) {
                        echo $this->Html->link(__('Back'), ['controller' => 'Invoices', 'action' => 'index'], ['class' => 'btn default']);
                    } else {
                        echo $this->Html->link(__('Back'), '/', ['class' => 'btn default']);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>



