<?php use Cake\Core\Configure; ?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Process Logs') ?></span>
        </div>
    </div>
    <div class="portlet-body form">     
        <div class="form-body row">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Purshase Order Number') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($processLog->purchase_order_number)  ?></label>
                </div>
            </div>   
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Nursery GLN') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $processLog->nursery_GLN ?></label>
                </div>
            </div> 
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Created') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= date(Configure::read('date-format.default'), strtotime($processLog->created)) ?></label>
                </div>
            </div>   
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Action Description') ?>:</label>
                <div class="col-md-9">
                    <label class="large"> <?= $this->Text->autoParagraph(h($processLog->action_description)); ?></label>
                </div>
            </div> 
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-11 col-md-1">
                    <?= $this->Html->link(__('Back'), ['controller' => 'ProcessLogs', 'action' => 'index'], ['class' => 'btn default']) ?>
                </div>
            </div>
        </div>
    </div>
</div>



