<?php

use Cake\Core\Configure;

//   echo '<pre>';print_r($processLogs);exit;
?>

<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('Actions') ?>
        </div>
    </div>
    <div class="portlet-body">

        <div class="portlet-body form">
            <?= $this->Form->create('process_logs', ['class' => 'form-horizontal', 'onsubmit' => 'showLoading()']); ?>
            <div class="form-body">
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-7">
                        <?= $this->Form->input('action_description', array('placeholder' => 'Action Description', 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                    </div>
                    <div class="col-md-1">
                        <?= $this->Form->submit('Search', array('label' => false, 'div' => false, 'class' => 'btn btn-primary')) ?>
                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>
        </div>

        <?php
        $sortDir = $this->Paginator->sortDir() === 'asc' ? 'fa-sort-asc sort-active-color' : 'fa-sort-desc sort-active-color';
        $sortKey = $this->Paginator->sortKey();
        ?>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th class="width-60-per">
                            <?= $this->Paginator->sort('action_description') ?>
                            <i class="fa <?php
                            if ($sortKey == 'ProcessLogs.action_description') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="width-20-per">
                            <?= $this->Paginator->sort('created') ?>
                            <i class="fa <?php
                            if ($sortKey == 'ProcessLogs.created') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            Nursery
                        </th>
                        <th class="actions width-5-per"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($processLogs as $processLog): ?>
                        <tr>
                            <td style="word-break: break-all"><?= $processLog->action_description ?></td>
                            <td><?= date(Configure::read('date-format.default'), strtotime($processLog->created)) ?></td>
                            <td><?= $processLog->user->full_name; ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__(''), ['action' => 'view', $processLog->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?> &nbsp;
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php if (iterator_count($processLogs) < 1) { ?>
                        <tr>
                            <td colspan="5" class="align-center">No records found</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <?php if (iterator_count($processLogs) > 0) { ?>
            <div class="row">
                <div class="col-md-2 col-sm-12">
                    Showing <?= $this->Paginator->counter() ?>
                </div>
                <div class="col-md-10 col-sm-12">
                    <ul class="pagination float-right-important">
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
