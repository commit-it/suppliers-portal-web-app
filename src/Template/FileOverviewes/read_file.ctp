<?php

use Cake\Core\Configure; ?>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __(Configure::read('file_type.Application Receipt Acknowledgement')) ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body row">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('File Name') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($fileOverviewe->file_name) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('File Type') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= isset($responseArr['StandardBusinessDocument']['sh:StandardBusinessDocumentHeader']['sh:DocumentIdentification']['sh:Type']) ? $responseArr['StandardBusinessDocument']['sh:StandardBusinessDocumentHeader']['sh:DocumentIdentification']['sh:Type'] : '' ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Status Type') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= isset($mainResponseArr['statusType']) ? $mainResponseArr['statusType'] : '' ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Error Count') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= isset($mainResponseArr['errorCount']) ? $mainResponseArr['errorCount'] : 0 ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Created') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= isset($responseArr['StandardBusinessDocument']['sh:StandardBusinessDocumentHeader']['sh:DocumentIdentification']['sh:CreationDateAndTime']) ? date(Configure::read('date-format.default'), strtotime($responseArr['StandardBusinessDocument']['sh:StandardBusinessDocumentHeader']['sh:DocumentIdentification']['sh:CreationDateAndTime'])) : '' ?></label>
                </div>
            </div>
        </div>

        <!-- Start Related Purchase Order Items --> 
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-book"></i><?= __('Errors or Warnings') ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <tr class="action-anch  thead-col">
                            <th style="width: 5%"><?= __('No.') ?></th>
                            <th style="width: 25%"><?= __('Code') ?></th>
                            <th style="width: 10%"><?= __('Language') ?></th>
                            <th style="width: 60%"><?= __('Description') ?></th>
                        </tr>
                        <tbody> 
                            <?php
                            if (isset($mainResponseArr['errorOrWarning'])) {
                                foreach ($mainResponseArr['errorOrWarning'] as $key => $errorOrWarning):
                                    ?>
                                    <tr>
                                        <td><?= $key + 1; ?></td>
                                        <td><?= $errorOrWarning['code']; ?></td>
                                        <td><?= $errorOrWarning['codeDescription']['description']['language']['languageISOCode']; ?></td>
                                        <td><?= $errorOrWarning['codeDescription']['description']['longText']; ?></td>
                                    </tr>
                                    <?php
                                endforeach;
                            }
                            else {
                                ?>
                                <tr><td colspan="4"> <center><?= __('No error available') ?></center></td></tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End Related Purchase Order Items --> 

        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-11 col-md-1">
                    <?= $this->Html->link(__('Back'), ['controller' => 'Invoices', 'action' => 'index'], ['class' => 'btn default']) ?>
                </div>
            </div>
        </div>
    </div>
</div>
