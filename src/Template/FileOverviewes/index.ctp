<?php

use Cake\Core\Configure ?>
<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('File List') ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <?php
            $sortDir = $this->Paginator->sortDir() === 'asc' ? 'fa-sort-asc sort-active-color' : 'fa-sort-desc sort-active-color';
            $sortKey = $this->Paginator->sortKey();
            ?>
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th>
                            <?= $this->Paginator->sort('FileTypes.type', 'File Type') ?>
                            <i class="fa <?php
                            if ($sortKey == 'FileTypes.type') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>                 
                        </th>
                        <th>
                            <?= $this->Paginator->sort('FileOverviewes.file_name', 'File Name') ?>
                            <i class="fa <?php
                            if ($sortKey == 'FileOverviewes.file_name') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>                 
                        </th>
                        <th>
                            <?= $this->Paginator->sort('FileStatuses.status', 'Status') ?>
                            <i class="fa <?php
                            if ($sortKey == 'FileStatuses.status') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>                 
                        </th>
                        <th>
                            <?= $this->Paginator->sort('FileOverviewes.created', 'Created') ?>
                            <i class="fa <?php
                            if ($sortKey == 'FileOverviewes.created') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>                 
                        </th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($fileOverviewes as $fileOverviewe): ?>
                        <tr>
                            <td>
                                <?= h(Configure::read('file_type.' . $fileOverviewe->file_type->type)) ?>
                            </td>
                            <td><?= h($fileOverviewe->file_name) ?></td>
                            <td>
                                <?= h($fileOverviewe->file_status->status) ?>
                            </td>
                            <td><?= date(Configure::read('date-format.default'), strtotime($fileOverviewe->created)) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__(''), ['action' => 'view', $fileOverviewe->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?> &nbsp;
                                <?php if ($fileOverviewe->file_type->type == \App\Model\Table\FileTypesTable::Ack) { ?>
                                    <?= $this->Html->link(__(''), ['action' => 'read_file', $fileOverviewe->id], ['class' => 'glyphicon glyphicon-file link-deco', 'title' => 'Read File', 'target' => '_blank']) ?>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php if (iterator_count($fileOverviewes) < 1) { ?>
                        <tr>
                            <td colspan="5" class="align-center">No records found</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <?php if (iterator_count($fileOverviewes) > 0) { ?>
            <div class="row">
                <div class="col-md-2 col-sm-12">
                    Showing <?= $this->Paginator->counter() ?>
                </div>
                <div class="col-md-10 col-sm-12">
                    <ul class="pagination float-right-important">
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

