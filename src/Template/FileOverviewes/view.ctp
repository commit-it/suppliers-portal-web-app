<?php use Cake\Core\Configure; ?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('File Overviewes') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body row">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('File Type') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($fileOverviewe->file_type->type) ?></label>
                </div>
            </div> 
        </div>
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('File Name') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($fileOverviewe->file_name) ?></label>
            </div>
        </div>
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('File Status') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($fileOverviewe->file_status->status) ?></label>
            </div>
        </div>
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Created') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= date(Configure::read('date-format.default'), strtotime($fileOverviewe->created)) ?></label>
            </div>
        </div>   
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Modified') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= date(Configure::read('date-format.default'), strtotime($fileOverviewe->modified)) ?></label>
            </div>
        </div>   
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-11 col-md-1">
                    <?= $this->Html->link(__('Back'), ['controller' => 'Invoices', 'action' => 'index'], ['class' => 'btn default']) ?>
                </div>
            </div>
        </div>
    </div>
</div>


