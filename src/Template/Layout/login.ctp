<?php

/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>
            <?= $this->fetch('title') ?>
        </title>
        <?php // echo $this->Html->meta('icon')  ?>

        <?php // echo $this->Html->css('base.css')  ?>
        <?php // echo $this->Html->css('cake.css') ?>
        <?php // echo $this->Html->css('style.css') ?>

        <?php // echo $this->fetch('meta')  ?>
        <?php // echo $this->fetch('css') ?>
        <?php // echo $this->fetch('script') ?>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?php echo $this->Html->css('/assets/global/plugins/font-awesome/css/font-awesome.min.css') ?>
        <?php echo $this->Html->css('/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') ?>
        <?php echo $this->Html->css('/assets/global/plugins/bootstrap/css/bootstrap.min.css') ?>
        <?php echo $this->Html->css('/assets/global/plugins/uniform/css/uniform.default.css') ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <?php echo $this->Html->css('/assets/admin/pages/css/login.css') ?>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME STYLES -->
        <?php echo $this->Html->css('/assets/global/css/components.css') ?>
        <?php echo $this->Html->css('/assets/global/css/plugins.css') ?>
        <?php echo $this->Html->css('/assets/admin/layout/css/layout.css') ?>
        <?php echo $this->Html->css('/assets/admin/layout/css/themes/darkblue.css') ?>
        <?php echo $this->Html->css('/assets/admin/layout/css/custom.css') ?>
        <!-- END THEME STYLES -->
        
        <!-- BEGIN MIN JS -->
        <?php echo $this->Html->script('/assets/global/plugins/jquery.min.js'); ?>
        <!-- END MIN JS -->
        
        <!-- stort favicon -->
        <?php echo $this->Html->meta('supplier_portal.ico','img/supplier_portal.ico',['type' => 'icon']); ?>
        <!-- end favicon -->

    </head>
    <body class="login">

        <!-- loader -->
        <?= $this->element('loader') ?>
        <!-- loader -->
        
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="menu-toggler sidebar-toggler"></div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        <div class="logo">
            <a href="/">
                <?php echo $this->Html->image('/img/logo-big.png', array('alt' => 'CakePHP')); ?>
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <?= $this->Flash->render() ?>
            <?php echo $this->fetch('content') ?>
        </div>
        <div class="copyright bold">            
            <?php echo date("Y"); ?> &COPY; Suppliers Portal.           
        </div>
        <!-- END LOGIN -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="../../assets/global/plugins/respond.min.js"></script>
        <script src="../../assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->
        <?php echo $this->Html->script('/assets/global/plugins/jquery-migrate.min.js'); ?>
        <?php echo $this->Html->script('/assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>
        <?php echo $this->Html->script('/assets/global/plugins/jquery.blockui.min.js'); ?>
        <?php echo $this->Html->script('/assets/global/plugins/jquery.cokie.min.js'); ?>
        <?php echo $this->Html->script('/assets/global/plugins/uniform/jquery.uniform.min.js'); ?>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <?php echo $this->Html->script('/assets/global/plugins/jquery-validation/js/jquery.validate.min.js'); ?>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <?php echo $this->Html->script('/assets/global/scripts/metronic.js'); ?>
        <?php echo $this->Html->script('/assets/admin/layout/scripts/layout.js'); ?>
        <?php echo $this->Html->script('/assets/admin/layout/scripts/demo.js'); ?>
        <?php echo $this->Html->script('/assets/admin/pages/scripts/login.js'); ?>
        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                Login.init();
                Demo.init();
            });
        </script>

    </body>
</html>
