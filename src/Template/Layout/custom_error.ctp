<?php use Cake\Network\Session;
// check user is logged in or not
$this->session = new Session();
if ($this->session->read('Auth.User.id') == NULL) {
    header('Location: users/logout');exit;
} 
?>

<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <title>
            <?= $this->fetch('title') ?>
        </title>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <meta content="" name="description"/>
        <meta content="" name="author"/>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?php echo $this->Html->css('/assets/global/plugins/font-awesome/css/font-awesome.min.css') ?>
        <?php echo $this->Html->css('/assets/global/plugins/simple-line-icons/simple-line-icons.min.css') ?>
        <?php echo $this->Html->css('/assets/global/plugins/bootstrap/css/bootstrap.min.css') ?>
        
        <!--Image upload css-->
        <?php echo $this->Html->css('/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') ?>
        <!--Image upload css-->

        <?php echo $this->Html->css('/assets/global/plugins/uniform/css/uniform.default.css') ?>
        <?php echo $this->Html->css('/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- START CORE PLUGINS -->
        <?php echo $this->Html->css('/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') ?>
        <?php echo $this->Html->css('/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') ?>
        <!-- END CORE PLUGINS -->

        <!-- BEGIN THEME STYLES -->
        <?php echo $this->Html->css('/assets/global/css/components.css') ?>
        <?php echo $this->Html->css('/assets/global/css/plugins.css') ?>
        <?php echo $this->Html->css('/assets/admin/layout/css/layout.css') ?>
        <?php echo $this->Html->css('/assets/admin/layout/css/themes/darkblue.css', ['id' => 'style_color']) ?>
        <?php echo $this->Html->css('/assets/admin/layout/css/custom.css') ?>
        <!-- END THEME STYLES -->

        <!-- custom css start -->
        <?php echo $this->Html->css('style.css?23072015?') ?>
        <!-- custom css end -->

        <!-- stort favicon -->
        <?php echo $this->Html->meta('supplier_portal.ico', 'img/supplier_portal.ico', ['type' => 'icon']); ?>
        <!-- end favicon -->

        <?php echo $this->Html->script('jquery-2.1.4.min.js') ?>
        <?php echo $this->Html->script('common.js?20072015') ?>
    </head>

    <body class="page-header-fixed page-quick-sidebar-over-content page-full-width">

        <!-- BEGIN HEADER -->
        <?php if ($this->session->read('Auth.User.role_name') == \App\Model\Table\UserRolesTable::Admin) {
            echo $this->element('admin_header');
        } else {
            echo $this->element('header');
        } ?>
        <!-- END HEADER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container" id="container">
            <div class="page-content-wrapper">
                <div class="page-content" id="content">
                    <?= $this->Flash->render() ?>
                    <?php echo $this->fetch('content'); ?>
                </div>
            </div>
        </div>
        <!-- END CONTAINER -->

        <!-- BEGIN FOOTER -->
        <?php echo $this->element('footer'); ?>
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="../../assets/global/plugins/respond.min.js"></script>
        <script src="../../assets/global/plugins/excanvas.min.js"></script> 
        <![endif]-->

        <?php echo $this->Html->script('/assets/global/plugins/jquery.min.js'); ?>
        <?php echo $this->Html->script('/assets/global/plugins/jquery-migrate.min.js'); ?>

        <!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <?php echo $this->Html->script('/assets/global/plugins/jquery-ui/jquery-ui.min.js'); ?>
        <?php echo $this->Html->script('/assets/global/plugins/bootstrap/js/bootstrap.min.js'); ?>
        <!--Image upload js-->
        <?php echo $this->Html->script('/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js'); ?>
        <!--Image upload js-->
        <?php echo $this->Html->script('/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js'); ?>
        <?php echo $this->Html->script('/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js'); ?>
        <?php echo $this->Html->script('/assets/global/plugins/jquery.blockui.min.js'); ?>
        <?php echo $this->Html->script('/assets/global/plugins/jquery.cokie.min.js'); ?>
        <?php echo $this->Html->script('/assets/global/plugins/uniform/jquery.uniform.min.js'); ?>
        <?php echo $this->Html->script('/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js'); ?>
        <?php echo $this->Html->script('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js'); ?>
        <?php echo $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>
        <!-- END CORE PLUGINS -->

        <?php echo $this->Html->script('/assets/global/scripts/metronic.js'); ?>
        <?php echo $this->Html->script('/assets/admin/layout/scripts/layout.js'); ?>
        <?php echo $this->Html->script('/assets/admin/layout/scripts/quick-sidebar.js'); ?>
        <?php echo $this->Html->script('/assets/admin/layout/scripts/demo.js'); ?>
        <?php echo $this->Html->script('/assets/admin/pages/scripts/table-advanced.js'); ?>

        <script>
            jQuery(document).ready(function() {
                Metronic.init(); // init metronic core components
                Layout.init(); // init current layout
                QuickSidebar.init(); // init quick sidebar
                Demo.init(); // init demo features
                TableAdvanced.init();
            });
        </script>
        <!-- END JAVASCRIPTS -->

    </body>
</html>
