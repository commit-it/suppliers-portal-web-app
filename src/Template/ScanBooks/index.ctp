<?php

use Cake\Core\Configure; ?>
<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('Scan Book List') ?>
        </div>
        <div class="actions">
            <?php echo $this->Html->link(__('Upload Scan Book'), ['action' => 'scan_book'], array('class' => 'btn green', 'escape' => false)); ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <?php
            $sortDir = $this->Paginator->sortDir() === 'asc' ? 'fa-sort-asc sort-active-color' : 'fa-sort-desc sort-active-color';
            $sortKey = $this->Paginator->sortKey();
            ?>
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th>
                            <?= $this->Paginator->sort('scan_book_file') ?>
                            <i class="fa <?php
                            if ($sortKey == 'ScanBooks.scan_book_file') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('bunning_store_id') ?>
                            <i class="fa <?php
                            if ($sortKey == 'ScanBooks.bunning_store_id') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('created') ?>
                            <i class="fa <?php
                            if ($sortKey == 'ScanBooks.created') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (iterator_count($scanBooks) > 0) { ?>
                        <?php foreach ($scanBooks as $scanBook): ?>
                            <tr>
                                <td><?= $this->Html->link($scanBook->scan_book_file, Configure::read('scan-book.view-path') . $scanBook->scan_book_file, ['class' => 'link-deco', 'target' => '_blanck']) ?></td>
                                <td>
                                    <?= $scanBook->has('bunning_store') ? $this->Html->link($scanBook->bunning_store->name, ['controller' => 'BunningStores', 'action' => 'view', $scanBook->bunning_store->id], ['class' => 'link-deco']) : '' ?>
                                </td>
                                <td><?= date(Configure::read('date-format.default'), strtotime($scanBook->created)); ?></td>
                                <td class="actions action-anch">
                                    <?= $this->Html->link(__(''), ['action' => 'view', $scanBook->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php }else { ?>
                    <td colspan="4"> <center><?= __('Records not found!!!') ?></center></td>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <?php if (iterator_count($scanBooks) > 0) { ?>
            <div class="paginator">
                <ul class="pagination large-10 medium-10 columns">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p class="large-2 medium-2 columns"><?= $this->Paginator->counter() ?></p>
            </div>
        <?php } ?>
    </div>
</div>    