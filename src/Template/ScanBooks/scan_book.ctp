<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Scan Book') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="alert alert-danger display-hide" id="error-message">
            <span></span>
        </div>
        <?= $this->Form->create('scanbook', array('type' => 'file', 'id' => 'scan-book-form', 'class' => 'form-horizontal', 'onsubmit' => 'showLoading()')); ?>
        <div class="form-body row">
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label" for="form_control_1"><?= __('Select Scan Book') ?><span class="required"> * </span></label>
                <div class="col-md-10">
                    <?php echo $this->Form->file('scanbook', array("onchange" => "checkfileIsExcel('scan_file');", 'id' => 'scan_file')); ?>
                    <div class="form-control-focus">
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <div class="col-md-offset-2 col-md-9">

                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr>
                                <th colspan="2"><?= __('Bunning Store') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($bunningStores) > 0) { 
                                $b = 1; ?>
                                <?php foreach ($bunningStores as $bunningStoresrKey => $bunningStoresValue): ?>
                                    <tr>
                                        <td class="align-center th-5-wd">
                                            <span class="md-checkbox">
                                                <?= $this->Form->checkbox('bunning_store_state[]', ['id' => 'bunning-store-state-' . $bunningStoresrKey, 'onclick' => 'selectStateWiseBunningStores(' . $bunningStoresrKey . ')', 'value' => $bunningStoresValue['state'], 'div' => false, 'lable' => false, 'hiddenField' => false, 'class' => 'md-check']) ?>
                                                <label for="bunning-store-state-<?= $bunningStoresrKey ?>">
                                                    <span></span>
                                                    <span class="check"></span>
                                                    <span class="box"></span>
                                                </label>
                                            </span>
                                        </td>
                                        <td><b><?= h($bunningStoresValue['state']) ?></b></td>
                                    </tr>
                                    <?php 
                                    
                                    if(stripos($bunningStoresValue['state_wise_id'], ',') !== false)
                                    {
                                        $bunningIdArr = explode(',', $bunningStoresValue['state_wise_id']);
                                        $bunningNameArr = explode(',', $bunningStoresValue['state_wise_name']);
                                        $bunningIdArr = array_filter($bunningIdArr);
                                        $bunningNameArr = array_filter($bunningNameArr);
                                        for ($i = 0; $i < count($bunningIdArr); $i++) { ?>
                                            <tr>
                                                <td class="align-center">
                                                    <span class="md-checkbox">
                                                        <?= $this->Form->checkbox('bunning_store_id[]', ['id' => 'bunning-store-' . $bunningIdArr[$i], 'value' => $bunningIdArr[$i], 'div' => false, 'lable' => false, 'hiddenField' => false, 'class' => 'md-check state_wise_bunning_stores_' . $bunningStoresrKey]) ?>
                                                        <label for="bunning-store-<?= $bunningIdArr[$i] ?>">
                                                            <span></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span>
                                                        </label>
                                                    </span>
                                                </td>
                                                <td>
                                                    <?= h($bunningNameArr[$i]) ?>
                                                </td>
                                            </tr>
                                        <?php }
                                    } else { ?>
                                            <tr>
                                                <td class="align-center">
                                                    <span class="md-checkbox">
                                                        <?= $this->Form->checkbox('bunning_store_id[]', ['id' => 'bunning-store-' . $bunningStoresValue['state_wise_id'], 'value' => $bunningStoresValue['state_wise_id'], 'div' => false, 'lable' => false, 'hiddenField' => false, 'class' => 'md-check state_wise_bunning_stores_' . $bunningStoresrKey]) ?>
                                                        <label for="bunning-store-<?= $bunningStoresValue['state_wise_id'] ?>">
                                                            <span></span>
                                                            <span class="check"></span>
                                                            <span class="box"></span>
                                                        </label>
                                                    </span>
                                                </td>
                                                <td>
                                                    <?= h($bunningStoresValue['state_wise_name']) ?>
                                                </td>
                                            </tr>
                                    <?php } ?>
                                    
                                    <?php if ($b < count($bunningStores)) { ?>    
                                        <tr>
                                            <td colspan="2"><br /></td>
                                        </tr>
                                    <?php } ?>
                                    <?php $b++; ?>
                                <?php endforeach; ?>
                            <?php } else { ?>
                                <tr><td colspan="2"><center><?= __('Records not found!!!') ?></center></td></tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>           
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-10 col-md-2">
                    <?= $this->Html->link(__('Cancel'), ['controller' => 'ScanBooks', 'action' => 'index'], ['class' => 'btn default']) ?>
                    <?= $this->Form->button(__('Submit'), array('type' => 'button', 'id' => 'send_scan_book', 'class' => 'btn blue')) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<?php echo $this->Html->script('scan_book.js') ?>