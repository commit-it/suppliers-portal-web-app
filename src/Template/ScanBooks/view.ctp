<?php use Cake\Core\Configure; ?>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Scan Book') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body row">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Scan Book File') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $this->Html->link($scanBook->scan_book_file, Configure::read('scan-book.view-path') . $scanBook->scan_book_file, ['class' => 'link-deco', 'target' => '_blanck'])?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Bunning Store') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $scanBook->has('bunning_store') ? $this->Html->link($scanBook->bunning_store->name, ['controller' => 'BunningStores', 'action' => 'view', $scanBook->bunning_store->id],['class' => 'link-deco']) : '' ?></label>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-11 col-md-1">
                    <?= $this->Html->link(__('Back'), ['controller' => 'ScanBooks', 'action' => 'index'], ['class' => 'btn default']) ?>
                </div>
            </div>
        </div>
    </div>
</div>