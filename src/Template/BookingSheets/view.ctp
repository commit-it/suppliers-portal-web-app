<?php

use Cake\Core\Configure ?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('View Booking Sheet') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body row">            
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Pickup Date') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= date(Configure::read('date-format.default'), strtotime($bookingSheet->pickup_date)) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Courier') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= isset($bookingSheet->courier->name) ? $bookingSheet->courier->name : '' ?></label>
                </div>
            </div>
        </div>
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-book"></i><?= __('Booking Sheets Items') ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="booking-sheets-items-table">
                        <tr class="action-anch  thead-col">
                            <th><?= __('Store Name') ?></th>
                            <th><?= __('Suppliers') ?></th>
                            <th><?= __('Suppliers Invoice No') ?></th>
                            <th><?= __('#Quantity') ?></th>                                              
                            <th><?= __('Stillages') ?></th>                                              
                        </tr>
                        <tbody> 
                            <?php if (count($bookingSheet->booking_sheet_items) > 0) { ?>
                                <?php foreach ($bookingSheet->booking_sheet_items as $bookingSheetItems): ?>
                                    <tr>
                                        <td>
                                            <span><?= $bookingSheetItems->purchase_order->bill_to_name ?></span>
                                        </td>
                                        <td>
                                            <span><?= $bookingSheetItems->purchase_order->seller_name ?></span>
                                        </td>
                                        <td>
                                            <span><?= $bookingSheetItems->invoice->invoice_number ?></span>
                                        </td>
                                        <td>
                                            <?php
                                            $quantity = 0;
                                            foreach ($bookingSheetItems->invoice->invoice_items as $invoiceItems):
                                                $quantity = $quantity + $invoiceItems->requested_quantity;
                                            endforeach;
                                            echo $quantity;
                                            ?>
                                        </td>
                                        <td>
                                            <span><?= $bookingSheetItems->trolleys ?></span>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php }else { ?>
                                <tr><td colspan="4"> <center><?= __('Records not found!!!') ?></center></td></tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-11 col-md-1">
                    <?= $this->Html->link(__('Back'), ['controller' => 'BookingSheets', 'action' => 'index'], ['class' => 'btn default']) ?>
                </div>
            </div>
        </div>
    </div>
</div>