
<?php $t = 1; ?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Add Booking Sheet') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <?php if (!empty($invoices)) { ?>
            <?= $this->Form->create('bookingSheet', ['class' => 'form-horizontal', 'action' => 'add', 'onsubmit' => 'showLoading()']); ?>
            <!-- Start invoice information -->
            <div class="form-body">
                <div class="form-group form-md-line-input">
                    <label class="col-md-2 control-label"><?= __('Pickup Date') ?></label>
                    <div class="col-md-6">
                        <div class="input-icon">
                            <?php echo $this->Form->text('pickup_date', array('type' => 'text', 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control date-time-picker', 'onkeydown' => 'return false')); ?>
                            <div class="form-control-focus">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-2 control-label"><?= __('Courier') ?><span class="required"> * </span></label>
                    <div class="col-md-6">
                        <div class="input-icon">
                            <?php if (empty($courierList)) { ?>
                                <b style="color: red;">Please add at list one Courier Company, instead of that you can't create Booking Sheet. Click <?= $this->Html->link(__('here'), ['controller' => 'Couriers', 'action' => 'index'], ['class' => 'link-deco']) ?> to add Courier Companies list</b><br /><br />
                            <?php } ?>
                            <?php echo $this->Form->input('courier_id', array('options' => $courierList, 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)); ?>
                            <div class="form-control-focus">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="invoice-items-table">
                        <thead>
                            <tr class="action-anch  thead-col">
                                <th><?= __('Store Name') ?></th>
                                <th><?= __('Suppliers') ?></th>
                                <th><?= __('Suppliers Invoice No') ?></th>
                                <th><?= __('#Quantity') ?></th>
                                <th style="width: 10%;"><?= __('Stillages') ?></th>
                            </tr>
                        </thead>
                        <tbody> 
                            <?php if (!empty($invoices)) { ?>
                                <?php foreach ($invoices as $key => $invoice): ?>
                                    <tr>
                                        <td>
                                            <span><?= $invoice['purchase_order']['bill_to_name'] ?></span>
                                        </td>
                                        <td>
                                            <span><?= $invoice['purchase_order']['seller_name'] ?></span>
                                        </td>
                                        <td>
                                            <span><?= $invoice['invoice_number'] ?></span>
                                        </td>
                                        <td>
                                            <?php
                                            $quantity = 0;
                                            foreach ($invoice['invoice_items'] as $invoiceItems):
                                                $quantity = $quantity + $invoiceItems['requested_quantity'];
                                            endforeach;
                                            echo $quantity;
                                            ?>
                                            <?php echo $this->Form->hidden('invoice_id', array('name' => 'booking_sheet_items[' . $key . '][invoice_id]', 'value' => $invoice['id'])); ?>
                                            <?php echo $this->Form->hidden('purchase_order_id', array('name' => 'booking_sheet_items[' . $key . '][purchase_order_id]', 'value' => $invoice['purchase_order']['id'])); ?>
                                        </td>
                                        <td style="width: 10%;">
                                            <?php echo $this->Form->number('trolleys', array( 'name' => 'booking_sheet_items[' . $key . '][trolleys]', 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php } else { ?>
                                <tr id="not_items_tr"><td colspan="4"> <center><?= __('Records not found!!!') ?></center></td></tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-10 col-md-2">
                        <?= $this->Html->link(__('Cancel'), ['controller' => 'BookingSheets', 'action' => 'index'], ['tabindex' => $t++, 'class' => 'btn default']) ?>
                        <?= $this->Form->button(__('Submit'), array('tabindex' => $t++, 'class' => 'btn blue')) ?>
                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>
        <?php } ?>
    </div>
</div>
