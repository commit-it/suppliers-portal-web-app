
<?php

use Cake\Core\Configure;

$t = 1;
?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Edit Booking Sheet') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <?= $this->Form->create($bookingSheet, ['class' => 'form-horizontal', 'action' => 'edit', 'onsubmit' => 'showLoading()']); ?>
        <?php echo $this->Form->hidden('id'); ?>
        <!-- Start invoice information -->
        <div class="form-body">
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Pickup Date') ?></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?php echo $this->Form->text('pickup_date', array('value' => ($bookingSheet->pickup_date != null) ? date(Configure::read('date-format.Y-m-d-H-i-s'), strtotime($bookingSheet->pickup_date)) : '', 'type' => 'text', 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control date-time-picker', 'onkeydown' => 'return false')); ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Courier') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?php if (empty($courierList)) { ?>
                            <b style="color: red;">Please add at list one Courier Company, instead of that you can't create Booking Sheet. Click <?= $this->Html->link(__('here'), ['controller' => 'Couriers', 'action' => 'index'], ['class' => 'link-deco']) ?> to add Courier Companies list</b><br /><br />
                        <?php } ?>
                        <?php echo $this->Form->input('courier_id', array('options' => $courierList, 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)); ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="portlet-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" id="invoice-items-table">
                    <thead>
                        <tr class="action-anch  thead-col">
                            <th><?= __('Store Name') ?></th>
                            <th><?= __('Suppliers') ?></th>
                            <th><?= __('Suppliers Invoice No') ?></th>
                            <th><?= __('#Quantity') ?></th>
                            <th style="width: 10%;"><?= __('Stillages') ?></th>
                        </tr>
                    </thead>
                    <tbody> 
                        <?php $bookingSheetItemsObj = new ArrayIterator($bookingSheet->booking_sheet_items); ?>
                        <?php if (iterator_count($bookingSheetItemsObj) > 0) { ?>
                            <?php foreach ($bookingSheet->booking_sheet_items as $key => $bookingSheetItems): ?>
                                <tr>
                                    <td>
                                        <span><?= $bookingSheetItems->purchase_order->bill_to_name ?></span>
                                    </td>
                                    <td>
                                        <span><?= $bookingSheetItems->purchase_order->seller_name ?></span>
                                    </td>
                                    <td>
                                        <span><?= $bookingSheetItems->invoice->invoice_number ?></span>
                                    </td>
                                    <td>
                                        <?php
                                        $quantity = 0;
                                        foreach ($bookingSheetItems->invoice->invoice_items as $invoiceItems):
                                            $quantity = $quantity + $invoiceItems->requested_quantity;
                                        endforeach;
                                        echo $quantity;
                                        ?>
                                        <?php echo $this->Form->hidden('id', array('name' => 'booking_sheet_items[' . $key . '][id]', 'value' => $bookingSheetItems->id)); ?>
                                    </td>
                                    <td style="width: 10%;">
                                        <?php echo $this->Form->number('trolleys', array('name' => 'booking_sheet_items[' . $key . '][trolleys]', 'value' => $bookingSheetItems->trolleys, 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php } else { ?>
                            <tr id="not_items_tr"><td colspan="7"> <center><?= __('Records not found!!!') ?></center></td></tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-10 col-md-2">
                    <?= $this->Html->link(__('Cancel'), ['controller' => 'BookingSheets', 'action' => 'index'], ['tabindex' => $t++, 'class' => 'btn default']) ?>
                    <?= $this->Form->button(__('Submit'), array('tabindex' => $t++, 'class' => 'btn blue')) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
