<?php

use Cake\Core\Configure;

$t = 1;
?>

<!-- generate delivery docket info box start -->
<?= $this->Form->create('BookingSheet', array('action' => 'email_booking_sheets', 'onsubmit' => 'showLoading()')); ?>
<div class="modal fade" id="email_booking_sheet_info_box" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog width-750">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-label="Close" style="margin-top:3px"><span aria-hidden="true">&times;</span></button>
                <div class="bootbox-body">
                    <?= $this->Form->input('BookingSheets.id', array('type' => 'hidden', 'id' => 'booking_sheet_ids_for_email', 'label' => false, 'div' => false)) ?>

                    <div class="row">
                        <label class="col-md-3 control-label"><?= __('Email') ?><span class="required">*</span></label>
                        <div class="col-md-3">
                            <?= $this->Form->input('BookingSheetsEmail[0].name', array('type' => 'text', 'tabindex' => $t++, 'autofocus' => 'On', 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true, 'placeholder' => 'Name')) ?>
                        </div>
                        <div class="col-md-4">
                            <?= $this->Form->input('BookingSheetsEmail[0].email', array('type' => 'email', 'tabindex' => $t++, 'autofocus' => 'On', 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true, 'placeholder' => 'Email')) ?>
                        </div>
                        <div class="col-md-1  col-md-pop-1">
                            <a class="btn btn-icon-only green" href="javascript:;" onclick="addMoreEmails()" title="Add more Email">
                                <i class="glyphicon glyphicon-plus"></i>
                            </a>
                        </div>
                    </div>

                    <div id="booking-sheets-emails-div"></div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-cancel" class="btn default" data-dismiss="modal" tabindex="<?= $t++ ?>">Cancel</button>
                <button type="submit" class="btn btn-primary" tabindex="<?= $t++ ?>">Submit</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?= $this->Form->end() ?>
<!-- generate delivery docket info box end -->
<?= $this->Form->create('', array('id' => 'cancel_booking_sheet_form', 'name' => 'cancel_booking_sheet_form', 'action' => 'cancel_booking_sheet')); ?>
<?= $this->Form->input('booking_sheet_hidden_ids', array('type' => 'hidden', 'id' => 'booking_sheet_hidden_ids', 'label' => false, 'div' => false)) ?>
<?= $this->Form->end() ?>

<?= $this->Form->create('PrintBookingSheet', array('target' => '_blank', 'id' => 'print_booking_sheet_form', 'name' => 'print_booking_sheet_form', 'action' => 'print_booking_sheets')); ?>
<?= $this->Form->hidden('email', array('id' => 'open_email_booking_sheet_info_box', 'data-toggle' => 'modal', 'data-dismiss' => 'modal', 'data-target' => '#email_booking_sheet_info_box')) ?>
<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('Booking Sheets') ?>
        </div>
        <div class="actions">
            <?php if (iterator_count($bookingSheets) > 0) { ?>
                <?= $this->Form->button(__('Cancel Booking Sheet'), ['type' => 'button', 'id' => 'cancel_booking_sheet', 'class' => 'btn green']) ?>
                <?= $this->Form->button(__('Print'), ['type' => 'button', 'id' => 'print_booking_sheets', 'class' => 'btn green']) ?>
                <?= $this->Form->button(__('Email'), ['type' => 'button', 'id' => 'email_booking_sheets', 'class' => 'btn green']) ?>
            <?php } else { ?>
                <?= $this->Form->button(__('Cancel Booking Sheet'), ['type' => 'button', 'id' => 'cancel_booking_sheet', 'class' => 'btn green', 'disabled' => true]) ?>
                <?= $this->Form->button(__('Print'), ['type' => 'button', 'id' => 'print_booking_sheets', 'class' => 'btn green', 'disabled' => true]) ?>
                <?= $this->Form->button(__('Email'), ['type' => 'button', 'id' => 'email_booking_sheets', 'class' => 'btn green', 'disabled' => true]) ?>
            <?php } ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <?php
            $sortDir = $this->Paginator->sortDir() === 'asc' ? 'fa-sort-asc sort-active-color' : 'fa-sort-desc sort-active-color';
            $sortKey = $this->Paginator->sortKey();
            ?>
            <div class="alert alert-danger display-hide" id="error-message">
                <span></span>
            </div>
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th class="align-center">
                            <?php if (iterator_count($bookingSheets) > 0) { ?>
                                <span class="md-checkbox">
                                    <?= $this->Form->checkbox('booking_sheet', ['id' => 'select_deselect_booking_sheets', 'div' => false, 'lable' => false, 'class' => 'md-check', 'hiddenField' => false]); ?>
                                    <label for="select_deselect_booking_sheets">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span>
                                    </label>
                                </span>
                            <?php } ?>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('Users.seller_gln', 'Seller GLN') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Users.seller_gln') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('Users.seller_business_name', 'Seller Business Name') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Users.seller_business_name') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('BookingSheets.trolleys', 'Stillages') ?>
                            <i class="fa <?php
                            if ($sortKey == 'BookingSheets.trolleys') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('BookingSheets.pickup_date', 'Pickup Date') ?>
                            <i class="fa <?php
                            if ($sortKey == 'BookingSheets.pickup_date') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th> 
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (iterator_count($bookingSheets) > 0) { ?>
                        <?php foreach ($bookingSheets as $bookingSheet):
                            ?>
                            <tr>
                                <td class="align-center">
                                    <span class="md-checkbox">
                                        <?= $this->Form->checkbox('booking_sheets[]', ['id' => 'booking_sheet_' . $bookingSheet->id, 'value' => $bookingSheet->id, 'div' => false, 'lable' => false, 'class' => 'md-check', 'hiddenField' => false]) ?>
                                        <label for="booking_sheet_<?= $bookingSheet->id ?>">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                        </label>
                                    </span>
                                </td>
                                <td><?= $bookingSheet->user->seller_gln ?></td>
                                <td><?= $bookingSheet->user->seller_business_name ?></td>
                                <td>
                                    <?php
                                    $trolleys = 0;
                                    foreach ($bookingSheet['booking_sheet_items'] as $bookingSheetItem):
                                        $trolleys+=$bookingSheetItem['trolleys'];
                                    endforeach;
                                    echo $trolleys;
                                    ?>
                                </td>
                                <td><?= date(Configure::read('date-format.default'), strtotime($bookingSheet->pickup_date)) ?></td>
                                <td class="actions action-anch">
                                    <?= $this->Html->link(__(''), ['action' => 'edit', $bookingSheet->id], ['class' => 'glyphicon glyphicon-edit link-deco', 'title' => 'Edit']) ?> &nbsp;
                                    <?= $this->Html->link(__(''), ['action' => 'view', $bookingSheet->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } else { ?>
                    <td colspan="6"> <center><?= __('Records not found!!!') ?></center></td>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <?php if (iterator_count($bookingSheets) > 0) { ?>
            <div class="paginator">
                <ul class="pagination large-10 medium-10 columns">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p class="large-2 medium-2 columns"><?= $this->Paginator->counter() ?></p>
            </div>
        <?php } ?>
    </div>
</div>
<?= $this->Form->end() ?>

<?php echo $this->Html->script('delivery_docket.js?08082015') ?>