<?php use Cake\Core\Configure; ?>

<h3 class="page-title">Dashboard</h3>

<!-- BEGIN DASHBOARD STATS -->
<div class="row dashboard">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue-madison">
            <div class="visual">
                <i class="fa fa-comments"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?= $totalSendScanBook ?>
                </div>
                <div class="desc">
                    <?= __('Scan Book total sent') ?>
                </div>
            </div>
            <?= $this->Html->link(__('Upload Scan Book') . '<i class="glyphicon glyphicon-circle-arrow-right"></i>', ['controller' => 'ScanBooks', 'action' => 'scan_book'], ['class' => 'more bold font-size-13', 'escape' => FALSE]) ?>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red-intense">
            <div class="visual">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?= $totalAvailabelPurchaseOrders ?>
                </div>
                <div class="desc">
                    <?= __('Available POs') ?>
                </div>
            </div>
            <?php if ($authUser['invoice_generate_by'] == Configure::read('invoice_generate_by.Quick-Books')) {
                $invoiceGenerateBy = Configure::read('invoice_generate_by.Quick-Books');
            } else {
                $invoiceGenerateBy = Configure::read('invoice_generate_by.MYOB');
            } ?>
            <?= $this->Html->link(__('Send Bunning PO\'s to ' . $invoiceGenerateBy) . '<i class="glyphicon glyphicon-circle-arrow-right"></i>', ['controller' => 'PurchaseOrders', 'action' => 'index'], ['class' => 'more bold font-size-13', 'escape' => FALSE]) ?>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green-haze">
            <div class="visual">
                <i class="fa fa-shopping-cart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?= $totalAvailabelInvoices ?>
                </div>
                <div class="desc">
                    <?= __('Available Invoices') ?>
                </div>
            </div>
            <?= $this->Html->link(__('Send Invoice to Bunning') . '<i class="glyphicon glyphicon-circle-arrow-right"></i>', ['controller' => 'Invoices', 'action' => 'index'], ['class' => 'more bold font-size-13', 'escape' => FALSE]) ?>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat purple-plum">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number">
                    <?= $totalReceivedInvoices ?>
                </div>
                <div class="desc">
                    <?= __('Received Invoices') ?>
                </div>
            </div>
            <?= $this->Html->link(__('Generate Delivery Docket') . '<i class="glyphicon glyphicon-circle-arrow-right"></i>', ['controller' => 'Invoices', 'action' => 'generated_invoices'], ['class' => 'more bold font-size-13', 'escape' => FALSE]) ?>
        </div>
    </div>
</div>
<!-- END DASHBOARD STATS -->

<div class="clearfix"></div>