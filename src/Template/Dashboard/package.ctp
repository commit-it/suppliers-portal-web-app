<?php use Cake\Core\Configure; ?>

<h3 class="page-title">Packages</h3>
<!-- BEGIN DASHBOARD STATS -->
<div class="row packages">
    <div class="col-md-10 col-md-offset-1 col-md-pop-1">
        <div class="row">
            <div class="col-md-4">
                <div class="top-news width-85-per margin-0-auto">
                    <a class="btn green package-header" href="javascript:;">
                        <span><center>LARGE</center></span>
                    </a>
                    <div class="packages-details">
                        <p class="Price">$ <span>8</span></p>
                        <p class="Duration">Duration: 1 Month</p>
                        <ul>
                            <li>750 max volume</li>
                            <li>Package</li>
                            <li>Home Page</li>
                        </ul>
                        <center>
                            <form method="POST">
                                <input type="hidden" name="plan" value="<?= Configure::read('Stripe.packages.large.plan_id') ;?>" />
                                <script
                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                    data-key="<?= Configure::read('Stripe.' . Configure::read('Stripe.mode') . '_publishable_key'); ?>"
                                    data-amount="<?= Configure::read('Stripe.packages.large.amount') ;?>"
                                    data-name="<?= Configure::read('Stripe.packages.large.name') ;?>"
                                    data-description="<?= Configure::read('Stripe.packages.large.description') ;?>"
                                    data-image="/img/supplier_portal.png">
                                </script>
                            </form>
                        </center>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="top-news width-85-per margin-0-auto">
                    <a class="btn green package-header" href="javascript:;">
                        <span><center>MEDIUM</center></span>
                    </a>
                    <div class="packages-details">
                        <p class="Price">$ <span>2</span></p>
                        <p class="Duration">Duration: 1 Month</p>
                        <ul>
                            <li>500 max volume</li>
                            <li>Package</li>
                            <li>Home Page</li>
                        </ul>
                        <center>
                            <form method="POST">
                                <input type="hidden" name="plan" value="<?= Configure::read('Stripe.packages.medium.plan_id') ;?>" />
                                <script
                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                    data-key="<?= Configure::read('Stripe.' . Configure::read('Stripe.mode') . '_publishable_key'); ?>"
                                    data-amount="<?= Configure::read('Stripe.packages.medium.amount') ;?>"
                                    data-name="<?= Configure::read('Stripe.packages.medium.name') ;?>"
                                    data-description="<?= Configure::read('Stripe.packages.medium.description') ;?>"
                                    data-image="/img/supplier_portal.png">
                                </script>
                            </form>
                        </center>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="top-news width-85-per margin-0-auto">
                    <a class="btn green package-header" href="javascript:;">
                        <span><center>SMALL</center></span>
                    </a>
                    <div class="packages-details">
                        <p class="Price">$ <span>1</span></p>
                        <p class="Duration">Duration: 1 Month</p>
                        <ul>
                            <li>200 max volume</li>
                            <li>Package</li>
                            <li>Home Page</li>
                        </ul>
                        <center>
                            <form method="POST">
                                <input type="hidden" name="plan" value="<?= Configure::read('Stripe.packages.small.plan_id') ;?>" />
                                <script
                                    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                    data-key="<?= Configure::read('Stripe.' . Configure::read('Stripe.mode') . '_publishable_key'); ?>"
                                    data-amount="<?= Configure::read('Stripe.packages.small.amount') ;?>"
                                    data-name="<?= Configure::read('Stripe.packages.small.name') ;?>"
                                    data-description="<?= Configure::read('Stripe.packages.small.description') ;?>"
                                    data-image="/img/supplier_portal.png">
                                </script>
                            </form>
                        </center>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 padding-top-30 padding-rt-40">
                <a href="/Users/user_payment_history" class="float-right txt-bold text-deco-none sub-link">View Subscription Details</a>
            </div>
        </div>
    </div>
</div>
<!-- END DASHBOARD STATS -->

<div class="clearfix"></div>