<?php $t = 1; ?>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Add Bunning Store') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <?= $this->Form->create($bunningStore,['class' => 'form-horizontal', 'onsubmit' => 'showLoading()']); ?>
        <!-- Start invoice information -->
        <div class="form-body">
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Type') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->input('type', array('options' => Cake\Core\Configure::read('bunning_user'), 'default' => Cake\Core\Configure::read('bunning_user.Payer'), 'tabindex' => $t++, 'autofocus' => 'On', 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Name') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('name', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Email') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('email', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('GLN') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('gln', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Address') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('address', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('City') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('city', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('State') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->input('state', array('options' => \Cake\Core\Configure::read('state'), 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'empty' => 'Select State')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Postal Code') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('postal_code', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Country') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->input('country_iso_code', array('options' => Cake\Core\Configure::read('country_iso_code'), 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'empty' => 'Select Country')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Language Iso Code') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <label class="large"><?= \Cake\Core\Configure::read('language_iso_code') ?></label>
                    <?= $this->Form->hidden('language_iso_code', array('value' => \Cake\Core\Configure::read('language_iso_code'), 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('ABN') ?></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('company_registration_number', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-10 col-md-2">
                    <?= $this->Html->link(__('Cancel'), ['controller' => 'BunningStores', 'action' => 'index'], ['tabindex' => $t++, 'class' => 'btn default']) ?>
                    <?= $this->Form->button(__('Submit'), array('tabindex' => $t++, 'class' => 'btn blue')) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>