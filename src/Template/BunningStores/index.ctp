
<!-- delete dialog box start -->
<?= $this->element('deletedialogbox') ?>
<!-- delete dialog box end -->

<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('Bunning Stores List') ?>
        </div>
        <div class="actions">
            <?= $this->Html->link(__('Import Bunning Store'), ['controller' => 'BunningStores', 'action' => 'import_bunning_stores'], ['class' => 'btn green']) ?>
            <?= $this->Html->link(__('Add New Bunning Store'), ['controller' => 'BunningStores', 'action' => 'add'], ['class' => 'btn green']) ?>
            <?= $this->Html->link(__('Acceptable Format'), '/bunning-store/Bunning_Stores.xls', ['class' => 'btn green'], ['escape' => false]) ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <?php
            $sortDir = $this->Paginator->sortDir() === 'asc' ? 'fa-sort-asc sort-active-color' : 'fa-sort-desc sort-active-color';
            $sortKey = $this->Paginator->sortKey();
            ?>
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th>
                            <?= $this->Paginator->sort('name') ?>
                            <i class="fa <?php
                            if ($sortKey == 'BunningStores.name') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('type') ?>
                            <i class="fa <?php
                            if ($sortKey == 'BunningStores.type') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('email') ?>
                            <i class="fa <?php
                            if ($sortKey == 'BunningStores.email') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>

                        </th>
                        <th>
                            <?= $this->Paginator->sort('gln', 'GLN') ?>
                            <i class="fa <?php
                            if ($sortKey == 'BunningStores.gln') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>         
                        </th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($bunningStores as $bunningStore): ?>
                        <tr>
                            <td><?= h($bunningStore->name) ?></td>
                            <td><?= h($bunningStore->type) ?></td>
                            <td><?= h($bunningStore->email) ?></td>
                            <td><?= h($bunningStore->gln) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__(''), ['action' => 'view', $bunningStore->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?> &nbsp;
                                <?= $this->Html->link(__(''), ['action' => 'edit', $bunningStore->id], ['class' => 'glyphicon glyphicon-edit link-deco', 'title' => 'Edit']) ?> &nbsp;
                                <?php $name = "'" . $bunningStore->name . "'"; ?>
                                <?= $this->Html->link(__(''), 'javascript:void(0)', ['class' => 'glyphicon glyphicon-trash link-deco', 'data-toggle' => 'modal', 'data-dismiss' => 'modal', 'data-target' => '#basic', 'title' => 'Delete', 'onclick' => 'deleteItem(' . $bunningStore->id . ',' . $name . ')']) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php if (iterator_count($bunningStores) < 1) { ?>
                        <tr>
                            <td colspan="5" class="align-center">No records found</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <?php if (iterator_count($bunningStores) > 0) { ?>
            <div class="row">
                <div class="col-md-2 col-sm-12">
                    Showing <?= $this->Paginator->counter() ?>
                </div>
                <div class="col-md-10 col-sm-12">
                    <ul class="pagination float-right-important">
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                </div>
            </div>    
        <?php } ?>
    </div>
</div>

<script>

    $('#btn-ok').click(function() {
        window.location.href = '/bunning_stores/delete/' + $('#delete_item_id').val();
    });

</script>