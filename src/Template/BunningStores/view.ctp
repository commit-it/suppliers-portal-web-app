<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Bunning Stores') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body row">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Type') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $this->Text->autoParagraph(h($bunningStore->type)); ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Name') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($bunningStore->name) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Email') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($bunningStore->email) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('GLN') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($bunningStore->gln) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Address') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($bunningStore->address) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('City') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($bunningStore->city) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('State') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= \Cake\Core\Configure::read('state.' . $bunningStore->state) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Postal Code') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($bunningStore->postal_code) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Country') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= \Cake\Core\Configure::read('country_iso_code.' . $bunningStore->country_iso_code) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Language Iso Code') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($bunningStore->language_iso_code) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('ABN') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($bunningStore->company_registration_number) ?></label>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-11 col-md-1">
                    <?= $this->Html->link(__('Back'), ['controller' => 'BunningStores', 'action' => 'index'], ['class' => 'btn default']) ?>
                </div>
            </div>
        </div>
    </div>
</div>