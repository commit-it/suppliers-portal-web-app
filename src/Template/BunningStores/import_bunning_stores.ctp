<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Import Bunning Stores') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="alert alert-danger display-hide" id="error-message">
            <span></span>
        </div>
        <?= $this->Form->create('bunningstores', array('type' => 'file', 'id' => 'import_bunning_stores_form', 'class' => 'form-horizontal')); ?>
        <div class="form-body row">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label" for="form_control_1"><?= __('Select Bunning Stores file') ?><span class="required"> * </span></label>
                <div class="col-md-9">
                    <?php echo $this->Form->file('bunningstores', array("onchange" => "checkfileIsExcel('bunning_stores');", 'id' => 'bunning_stores')); ?>
                    <div class="form-control-focus">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-10 col-md-2">
                    <?= $this->Html->link(__('Cancel'), ['controller' => 'BunningStores', 'action' => 'index'], ['class' => 'btn default']) ?>
                    <?= $this->Form->button(__('Submit'), array('type' => 'submit', 'id' => 'import_bunning_stores', 'class' => 'btn blue')) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<script>
    
$(document).ready(function() {
    $("form#import_bunning_stores_form").submit(function(e) {
        e.preventDefault();
        var result = checkfileIsExcel('bunning_stores');
        if (result) {
            $('#error-message > span').html('');
            $('#error-message').removeClass('error');
            $('#error-message').hide();
            $(this).attr('onsubmit','showLoading()');
            $(this).unbind('submit').submit();
        } 
    });
});

</script>