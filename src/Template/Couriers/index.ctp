<?php

use Cake\Core\Configure; ?>

<!-- delete dialog box start -->
<?= $this->element('deletedialogbox') ?>
<!-- delete dialog box end -->

<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('Couriers List') ?>
        </div>
        <div class="actions">
            <?php echo $this->Html->link(__('Add Courier'), ['action' => 'add'], array('class' => 'btn green', 'escape' => false)); ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <?php
            $sortDir = $this->Paginator->sortDir() === 'asc' ? 'fa-sort-asc sort-active-color' : 'fa-sort-desc sort-active-color';
            $sortKey = $this->Paginator->sortKey();
            ?>
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th>
                            <?= $this->Paginator->sort('name') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Couriers.name') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('telephone') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Couriers.telephone') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('email') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Couriers.email') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (iterator_count($couriers) > 0) { ?>
                        <?php foreach ($couriers as $courier): ?>
                            <tr>
                                <td><?= h($courier->name) ?></td>
                                <td><?= h($courier->telephone) ?></td>
                                <td><?= h($courier->email) ?></td>
                                <td class="actions action-anch">
                                    <?= $this->Html->link(__(''), ['action' => 'edit', $courier->id], ['class' => 'glyphicon glyphicon-edit link-deco', 'title' => 'Edit']) ?>
                                    <?php $name = "'" . $courier->name . "'"; ?>
                                    <?= $this->Html->link(__(''), 'javascript:void(0)', ['class' => 'glyphicon glyphicon-trash link-deco', 'data-toggle' => 'modal', 'data-dismiss' => 'modal', 'data-target' => '#basic', 'title' => 'Delete', 'onclick' => 'deleteItem(' . $courier->id . ',' . $name . ')']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php }else { ?>
                    <td colspan="4"> <center><?= __('Records not found!!!') ?></center></td>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <?php if (iterator_count($couriers) > 0) { ?>
            <div class="paginator">
                <ul class="pagination large-10 medium-10 columns">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p class="large-2 medium-2 columns"><?= $this->Paginator->counter() ?></p>
            </div>
        <?php } ?>
    </div>
</div>    

<script>

    $('#btn-ok').click(function() {
        window.location.href = '/couriers/delete/' + $('#delete_item_id').val();
    });

</script>