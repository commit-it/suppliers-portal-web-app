
<?php $t = 1; ?>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Edit Courier') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <?= $this->Form->create($courier, ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'onsubmit' => 'showLoading()']); ?>
        <?php echo $this->Form->hidden('id'); ?>
        <!-- Start invoice information -->
        <div class="form-body">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Courier Name') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('name', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Courier Logo') ?></label>
                <div class="col-md-6">
                    <?php
                    /* If file upload alredy then use fileinput-exists class otherwise use fileinput-new class */
                    $fileinputClass = 'fileinput-new';
                    if ($courier->logo) {
                        $fileinputClass = 'fileinput-exists';
                    } 
                    ?>
                    <?= $this->Form->hidden('logo') ?> 
                    <div class="courier-logo-thumb fileinput <?= $fileinputClass ?>" data-provides="fileinput">
                        <div class="fileinput-new thumbnail">
                            <?php echo $this->Html->image('/img/no_image.png', array('alt' => '')); ?>
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail">
                            <?php if ($courier->logo) { echo $this->Html->image($courierLogoImgPath, array('alt' => '')); }?>
                        </div>
                        <div>
                            <span class="btn default btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" accept="image/*" name="courier_logo_new" tabindex="<?= $t++ ?>" />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Courier Address') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('address', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Courier Telephone') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('telephone', array( 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'pattern' => '^([0|\+[0-9]{1,5})?([0-9][0-9]{9})$', 'required' => true)) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Courier Email') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('email', array('type' => 'email', 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-10 col-md-2">
                    <?= $this->Html->link(__('Cancel'), ['action' => 'index'], ['class' => 'btn default']) ?>
                    <?= $this->Form->button(__('Submit'), array('class' => 'btn blue')) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
