<?php

use Cake\Core\Configure; ?>

<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('Master Items') ?>
        </div>
        <div class="actions">
            <?= $this->Html->link(__('Add Item'), ['controller' => 'MasterItems', 'action' => 'add'], ['class' => 'btn green'], ['escape' => false]) ?>
            <?= $this->Html->link(__('Import Items'), ['controller' => 'MasterItems', 'action' => 'import_items'], ['class' => 'btn green'], ['escape' => false]) ?>
            <?= $this->Html->link(__('Acceptable Format'), '/master-items/Master_Items.xls', ['class' => 'btn green'], ['escape' => false]) ?>
            <?= $this->Html->link(__('Export to Excel'), ['controller' => 'MasterItems', 'action' => 'exportToExcel'], ['class' => 'btn green'], ['escape' => false]) ?>
        </div>
    </div>
    <div class="portlet-body">

        <div class="portlet-body form">
            <?php echo $this->Form->create('master_items', ['class' => 'form-horizontal', 'onsubmit' => 'showLoading()']); ?>
            <div class="form-body">
                <div class="form-group">
                    <div class="col-md-2 col-md-offset-1">
                        <?php echo $this->Form->input('scan_barcode', array('placeholder' => 'Fine Line', 'div' => false, 'label' => FALSE, 'class' => 'form-control')); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $this->Form->input('bar_code', array('placeholder' => 'Bar Code', 'div' => false, 'label' => FALSE, 'class' => 'form-control')); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $this->Form->input('pot_size', array('placeholder' => 'Pot Size', 'div' => false, 'label' => FALSE, 'class' => 'form-control')); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $this->Form->input('name', array('placeholder' => 'Name', 'div' => false, 'label' => FALSE, 'class' => 'form-control')); ?>
                    </div>
                    <div class="col-md-2">
                        <?php echo $this->Form->input('retail_price', array('placeholder' => 'Cost Price', 'div' => false, 'label' => FALSE, 'class' => 'form-control')); ?>
                    </div>
                    <div class="col-md-1">
                        <?php echo $this->Form->submit(__('Search'), array('label' => false, 'div' => false, 'class' => 'btn btn-primary')); ?>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>

        <?php
        $sortDir = $this->Paginator->sortDir() === 'asc' ? 'fa-sort-asc sort-active-color' : 'fa-sort-desc sort-active-color';
        $sortKey = $this->Paginator->sortKey();
        ?>

        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th class="width-15-per">
                            <?= $this->Paginator->sort('scan_barcode','Fine Line') ?>
                            <i class="fa <?php
                            if ($sortKey == 'MasterItems.scan_barcode') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="width-20-per">
                            <?= $this->Paginator->sort('bar_code') ?>
                            <i class="fa <?php
                            if ($sortKey == 'MasterItems.bar_code') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="width-10-per">
                            <?= $this->Paginator->sort('pot_size') ?>
                            <i class="fa <?php
                            if ($sortKey == 'MasterItems.pot_size') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="width-25-per">
                            <?= $this->Paginator->sort('name') ?>
                            <i class="fa <?php
                            if ($sortKey == 'MasterItems.name') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="width-15-per">
                            <?= $this->Paginator->sort('retail_price', 'Cost Price ($)') ?>
                            <i class="fa <?php
                            if ($sortKey == 'MasterItems.retail_price') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="width-5-per actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($masterItems as $masterItem): ?>
                        <tr>
                            <td><?= h($masterItem->scan_barcode) ?></td>
                            <td><?= h($masterItem->bar_code) ?></td>
                            <td><?= h($masterItem->pot_size) ?></td>
                            <td><?= h($masterItem->name) ?></td>
                            <td><?= $this->Number->format($masterItem->retail_price, Configure::read('number-format-settings')) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__(''), ['action' => 'view', $masterItem->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?> &nbsp;
                                <?= $this->Html->link(__(''), ['action' => 'edit', $masterItem->id], ['class' => 'glyphicon glyphicon-edit link-deco', 'title' => 'Edit']) ?> &nbsp;
                                <?php $name = "'" . $masterItem->name . "'"; ?>                            
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php if (iterator_count($masterItems) < 1) { ?>
                        <tr>
                            <td colspan="6" class="align-center">No records found</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <?php if (iterator_count($masterItems) > 0) { ?>
            <div class="row">
                <div class="col-md-2 col-sm-12">
                    Showing <?= $this->Paginator->counter() ?>
                </div>
                <div class="col-md-10 col-sm-12">
                    <ul class="pagination float-right-important">
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
