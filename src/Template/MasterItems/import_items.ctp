<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Import Master Items') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="alert alert-danger display-hide" id="error-message">
            <span></span>
        </div>
        <?= $this->Form->create('import_items', array('type' => 'file', 'id' => 'master_items', 'class' => 'form-horizontal', 'onsubmit' => 'showLoading()')); ?>
        <div class="form-body row">
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label" for="form_control_1"><?= __('Select Master Items File') ?><span class="required"> * </span></label>
                <div class="col-md-10">
                    <?php echo $this->Form->file('master_items', array("onchange" => "checkfileIsExcel('master_item');" , 'id' => 'master_item')); ?>
                    <div class="form-control-focus"></div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-10 col-md-2">
                    <?= $this->Html->link(__('Cancel'), ['controller' => 'MasterItems', 'action' => 'index'], ['class' => 'btn default']) ?>
                    <?= $this->Form->button(__('Submit'), array('type' => 'submit', 'class' => 'btn blue')) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>

<script>
    
$(document).ready(function() {
    $("form#master_items").submit(function(e) {
        e.preventDefault();
        var result = checkfileIsExcel('master_item');
        if (result) {
            $('#error-message > span').html('');
            $('#error-message').removeClass('error');
            $('#error-message').hide();
            $(this).unbind('submit').submit();
        } 
    });
});

</script>