<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Add Master Item') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <?= $this->Form->create($masterItem, ['class' => 'form-horizontal', 'onsubmit' => 'showLoading()']); ?>
        <!-- Start invoice information -->
        <div class="form-body">
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Name') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('name', array('tabindex' => '1', 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Pot Size') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('pot_size', array('tabindex' => '2', 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Description') ?></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('description', array('type' => 'textarea', 'tabindex' => '3', 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Genus') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('genus', array('tabindex' => '4', 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Cultival') ?></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('cultival', array('tabindex' => '5', 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Species') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('species', array('tabindex' => '6', 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Cost Price ($)') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('retail_price', array('tabindex' => '7', 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Fine Line') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('scan_barcode', array('tabindex' => '8', 'maxlength' => 9, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Barcode') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('bar_code', array('tabindex' => '9', 'minlength' => 10,'maxlength' => 100, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-10 col-md-2">
                    <?= $this->Html->link(__('Cancel'), ['controller' => 'MasterItems', 'action' => 'index'], ['class' => 'btn default']) ?>
                    <?= $this->Form->button(__('Submit'), array('class' => 'btn blue')) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>