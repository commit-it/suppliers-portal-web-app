<?php use Cake\Core\Configure; ?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Master Item') ?></span>
        </div>
    </div>
    <div class="portlet-body form">       
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Fine Line') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($masterItem->scan_barcode) ?></label>
            </div>
        </div>        
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Bar Code') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($masterItem->bar_code)  ?></label>
            </div>
        </div>   
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Pot Size') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($masterItem->pot_size) ?></label>
            </div>
        </div> 
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Name') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($masterItem->name) ?></label>
            </div>
        </div>   
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Genus') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($masterItem->genus) ?></label>
            </div>
        </div>   
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Cultival') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($masterItem->cultival) ?></label>
            </div>
        </div>
         <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Species') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($masterItem->species) ?></label>
            </div>
        </div>
         <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Cost Price ($)') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= $this->Number->format($masterItem->retail_price, Configure::read('number-format-settings'))?></label>
            </div>
        </div>  
         <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Created') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= date(Configure::read('date-format.default'), strtotime($masterItem->created)) ?></label>
            </div>
        </div>  
         <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Modified') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= date(Configure::read('date-format.default'), strtotime($masterItem->modified)) ?></label>
            </div>
        </div> 
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Description') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($masterItem->description) ?></label>
            </div>
        </div> 
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-11 col-md-1">
                    <?= $this->Html->link(__('Back'), ['controller' => 'MasterItems', 'action' => 'index'], ['class' => 'btn default']) ?>
                </div>
            </div>
        </div>
    </div>
</div>




