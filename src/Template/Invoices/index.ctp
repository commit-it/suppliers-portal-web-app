<?php

use Cake\Core\Configure; ?>
<style>
    .waiting_fa_status {
        cursor: default!important;
    }
</style>

<!-- generate delivery docket info box start -->
<?= $this->Form->create('delivery_docket_invoices', array('id' => 'delivery_docket_invoices', 'name' => 'delivery_docket_invoices', 'action' => 'generate_delivery_docket', 'onsubmit' => 'showLoading()')); ?>
<div class="modal fade" id="generate_delivery_docket_info_box" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-label="Close" style="margin-top:3px"><span aria-hidden="true">&times;</span></button>
                <div class="bootbox-body">
                    <div class="row">
                        <label class="col-md-3 control-label"><?= __('Delivery Date') ?><span class="required"> * </span></label>
                        <div class="col-md-6 col-md-pop-3">
                            <div class="input-icon">
                                <i class="fa fa-calendar"></i>
                                <?= $this->Form->input('delivery_date', array('size' => '16', 'type' => 'text', 'data-date-format' => 'yyyy-mm-dd', 'tabindex' => '1', 'autofocus' => 'On', 'label' => false, 'div' => false, 'class' => 'form-control date-picker', 'required' => true, 'onkeydown' => 'return false')) ?>
                                <?= $this->Form->input('generate_delivery_docket_checkbox', array('type' => 'hidden', 'label' => false, 'div' => false, 'id' => 'generate_delivery_docket_checkbox')) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-cancel" class="btn default" data-dismiss="modal">Cancel</button>
                <input type="submit" id="btn-ok" class="btn btn-primary" value="Submit" />
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?= $this->Form->end() ?>
<!-- generate delivery docket info box end -->

<!-- button of open delivery docket details window -->
<button type="button" class="display-none" id="open_delivery_docket_details_window" data-toggle="modal" data-dismiss="modal" data-target="#generate_delivery_docket_info_box"></button>


<!-- booking sheet form start -->
<?= $this->Form->create('invoices', array('id' => 'booking_sheet_form', 'name' => 'booking_sheet_form', 'url' => ['controller' => 'BookingSheets', 'action' => 'add_booking_sheet'], 'onsubmit' => 'showLoading()')); ?>
<?= $this->Form->input('booking_sheet_id', array('type' => 'hidden', 'label' => false, 'div' => false, 'id' => 'booking_sheet_id')) ?>
<?= $this->Form->end() ?>
<!-- booking sheet form end -->

<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('Invoices') ?>
        </div>
        <div class="actions">
            <?php
            if (iterator_count($invoices) > 0) {
                echo $this->Form->button(__('Generate Delivery Docket'), array('type' => 'button', 'id' => 'generate_delivery_docket', 'class' => 'btn green'));
                echo $this->Form->button(__('Generate Booking Sheet'), array('type' => 'button', 'id' => 'generate_booking_sheet', 'class' => 'btn green'));
                echo $this->Form->button(__('Send to Bunnings'), array('type' => 'button', 'id' => 'generate_invoice', 'class' => 'btn green'));
            } else {
                echo $this->Form->button(__('Generate Delivery Docket'), array('type' => 'button', 'id' => 'generate_delivery_docket', 'class' => 'btn green', 'disabled' => true));
                echo $this->Form->button(__('Generate Booking Sheet'), array('type' => 'button', 'id' => 'generate_booking_sheet', 'class' => 'btn green', 'disabled' => true));
                echo $this->Form->button(__('Send to Bunnings'), array('type' => 'button', 'id' => 'generate_invoice', 'class' => 'btn green', 'disabled' => true));
            }
            echo $this->Html->link(__('Invoices Sent to Bunnings'), ['controller' => 'Invoices', 'action' => 'generated_invoices'], ['class' => 'btn green']);
            ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <?php
            $sortDir = $this->Paginator->sortDir() === 'asc' ? 'fa-sort-asc sort-active-color' : 'fa-sort-desc sort-active-color';
            $sortKey = $this->Paginator->sortKey();
            ?>
            <div class="alert alert-danger display-hide" id="error-message">
                <span></span>
            </div>
            <?= $this->Form->create('invoices', array('id' => 'invoices', 'name' => 'invoices', 'action' => 'create_invoice', 'onsubmit' => 'showLoading()')); ?>
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th class="th-5-wd align-center">
                            <span class="md-checkbox">
                                <?= $this->Form->checkbox('select_deselect', ['id' => 'select_deselect_invoices', 'div' => false, 'lable' => false, 'class' => 'md-check', 'hiddenField' => false]); ?>
                                <label for="select_deselect_invoices">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span>
                                </label>
                            </span>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('invoice_number', 'Invoice No.') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.invoice_number') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('purchase_order_number', 'Purchase Order No.') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.purchase_order_number') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('sub_total', 'Sub Total ($)') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.sub_total') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('tax', 'GST (%)') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.tax') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('sales_person') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.sales_person') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('ship_via') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.ship_via') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <?php if ($authUser['invoice_generate_by'] == Configure::read('invoice_generate_by.Quick-Books')) { ?>
                            <th>
                                <?= $this->Paginator->sort('invoice_update_to_quick_book', 'Updated To QB') ?>
                                <i class="fa <?php
                                if ($sortKey == 'Invoices.invoice_update_to_quick_book') {
                                    echo $sortDir;
                                } else {
                                    echo 'fa-sort sort-default-color';
                                }
                                ?> float-right-important"></i>
                            </th>
                        <?php } ?>
                        <th>
                            <?= $this->Paginator->sort('delivery_docket_generated', 'Delivery Docket') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.delivery_docket_generated') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('booking_sheet_generated', 'Booking Sheet') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.booking_sheet_generated') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (iterator_count($invoices) > 0) {
                        ?>
                        <?php foreach ($invoices as $invoice): ?>
                            <tr>
                                <td class="align-center">
                                    <?php if ($invoice->purchase_order->fulfilment_advice_status == Configure::read('fulfilment_advice_ack_status.RECEIVED')) { ?>
                                        <span class="md-checkbox">
                                            <?= $this->Form->checkbox('invoice_id[]', ['id' => 'invoice-id-' . $invoice->id, 'value' => $invoice->id, 'div' => false, 'lable' => false, 'hiddenField' => false, 'class' => 'md-check']) ?>
                                            <label for= "invoice-id-<?= $invoice->id ?>">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                            </label>
                                        </span>
                                    <?php } ?>
                                </td>
                                <td><?= h($invoice->invoice_number) ?></td>
                                <td><?= h($invoice->purchase_order_number) ?></td>
                                <td><?= $this->Number->format($invoice->sub_total, Configure::read('number-format-settings')) ?></td>
                                <td><?= $invoice->tax ?></td>
                                <td><?= h($invoice->sales_person) ?></td>
                                <td><?= h($invoice->ship_via) ?></td>
                                <?php if ($authUser['invoice_generate_by'] == Configure::read('invoice_generate_by.Quick-Books')) { ?>
                                    <td>
                                        <?= ucwords(h($invoice->invoice_update_to_quick_book)) ?>
                                        &nbsp;&nbsp;
                                        <?php
                                        if ($invoice->invoice_update_to_quick_book == Configure::read('invoice.update_to_quick_book.No')) {
                                            echo $this->Html->link(__('Resend to Quick-Book'), ['controller' => 'Invoices', 'action' => 'resend_failed_invoice_to_quick_book', $invoice->id, $this->request->action], ['class' => 'btn blue-hoki btn-xs display-loader-onclick']);
                                        }
                                        ?>
                                    </td>
                                <?php } ?>
                                <td><?= ucfirst(h($invoice->delivery_docket_generated)) ?></td>
                                <td><?= ucfirst(h($invoice->booking_sheet_generated)) ?></td>
                                <td class="actions action-anch">
                                    <?php if ($invoice->purchase_order->fulfilment_advice_status != Configure::read('fulfilment_advice_ack_status.RECEIVED')) { ?>
                                        <div class="btn-group btn-group-xs btn-group-solid">
                                            <span title="Waiting for Fulfilment Advoice Acknowledgment of Bunnings" class="btn btn-warning waiting_fa_status">Waiting to FA Ack</span>
                                        </div><br />
                                        <?php
                                        echo $this->Html->link(__('Resend to Bunnings'), ['controller' => 'Invoices', 'action' => 'resend_invoice', $invoice->id], ['class' => 'btn red-haze btn-xs display-loader-onclick']);
                                    }
                                    ?><br />
                                    <?= $this->Html->link(__(''), ['action' => 'view', $invoice->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?>
                                    <?= $this->Html->link(__(''), ['action' => 'edit', $invoice->id], ['class' => 'glyphicon glyphicon-edit link-deco', 'title' => 'Edit']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } else { ?>
                    <td colspan="<?php if ($authUser['invoice_generate_by'] == Configure::read('invoice_generate_by.Quick-Books')) { ?> 11 <?php } else { ?> 10 <?php } ?>"> <center><?= __('Records not found!!!') ?></center></td>
                <?php } ?>

                </tbody>
            </table>
            <?= $this->Form->end() ?>
        </div>
        <?php if (iterator_count($invoices) > 0) { ?>
            <div class="paginator">
                <ul class="pagination large-10 medium-10 columns">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p class="large-2 medium-2 columns"><?= $this->Paginator->counter() ?></p>
            </div>
        <?php } ?>
    </div>
</div>
<?php echo $this->Html->script('invoice.js?10072015') ?>

<!--<script>
    
// Auto reload page after some time    
setTimeout(function() {
    window.location.reload();
}, <?php //echo Configure::read('auto_refresh_page_time');       ?>);

</script>-->
