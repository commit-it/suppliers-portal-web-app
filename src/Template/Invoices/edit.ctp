<?php use Cake\Core\Configure; ?>

<style>
    .form-horizontal .form-group.form-md-line-input > label {
        padding-top: 0!important;
        font-size: 14px;
        color: #333333;
    }
</style>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Edit Invoice') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <!--Start not save invoice items error -->
        <div class="alert alert-danger hide-cls" id="not_save_editable_fields_error">
            <span><?= __('Please save all invoice items.') ?></span>
        </div> 
        <!--End not save invoice items error -->

        <?= $this->Form->create($invoice, array('id' => 'invoice-edit-form', 'class' => 'form-horizontal', 'onsubmit' => 'showLoading()')); ?>
        <!-- Start invoice information -->
        <div class="form-body">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Invoice Number') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $invoice->invoice_number ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Invoice Type') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $invoice->invoice_type ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right"><?= __('Purchase Order Number') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($invoice->purchase_order_number) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right"><?= __('Sub Total ($)') ?>:</label>
                <div class="col-md-9">
                    <label class="large" id="sub-total"><?= $this->Number->format($invoice->sub_total, Configure::read('number-format-settings')) ?></label>
                </div>
                <input type="hidden" id="sub-total-input" name="sub_total" value="<?= $invoice->sub_total; ?>"/>
                <input type="hidden" id="sub-total-input-hidden" value="<?= $invoice->sub_total; ?>"/>
                <?= $this->Form->hidden('id'); ?>
                <?= $this->Form->hidden('purchase_order_id'); ?>
            </div>  
            <?php if ($invoice->discount_amount > 0) { ?>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Discount (%)') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= $invoice->discount_percentage ?></label>
                        <?= $this->Form->text('discount_percentage', array('value' => ($invoice->discount_percentage > 0) ? $invoice->discount_percentage : 0, 'type' => 'hidden', 'id' => 'discount_percentage', 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Sub Total after Discount ($)') ?>:</label>
                    <div class="col-md-9">
                        <label class="large" id="sub_total_after_discount"><?= $this->Number->format(($invoice->sub_total - $invoice->discount_amount), Configure::read('number-format-settings')) ?></label>
                    </div>
                </div>
            <?php } ?>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('GST (%)') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $invoice->tax ?></label>
                    <?= $this->Form->text('tax', array('value' => ($invoice->tax > 0) ? $invoice->tax : 10, 'type' => 'hidden', 'id' => 'tax', 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Total Include GST ($)') ?>:</label>
                <div class="col-md-9">
                    <label class="large" id="total_include_gst">
                        <?php 
                            if ($invoice->discount_amount > 0) {
                                $subTotalWithDiscount = $invoice->sub_total - $invoice->discount_amount;
                                $gstAmount = ($subTotalWithDiscount * $invoice->tax) / 100;
                                echo $this->Number->format(($subTotalWithDiscount + $gstAmount), Configure::read('number-format-settings'));
                            } else {
                                $gstAmount = ($invoice->sub_total * $invoice->tax) / 100;
                                echo $this->Number->format(($invoice->sub_total + $gstAmount), Configure::read('number-format-settings'));
                            }
                        ?>
                    </label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right"><?= __('Sales Person') ?>:</label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('sales_person', array('autofocus' => 'On', 'tabindex' => '1', 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right"><?= __('Ship Via') ?>:</label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('ship_via', array('tabindex' => '2', 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Starts list of invoice items -->
            <?php
            $invoiceItemsIdArr = array();
            $invoiceItemsGtinArr = array();
            ?>
            <?php
            /**
             *  Note: Whenever change the sequence of table head update this array 
             *  This variable is used for define table column numbers which we want editable 
             *  field: is used for name of the editable input fields
             *  col_num: is used for which column you want to edit
             *  required: if you want to mandidatory field then used required = true otherwise use blank value
             */
            $tableFieldsTdNum = json_encode(array(0 => array('field' => 'requested_quantity', 'col_num' => '1', 'required' => 'true')));
            ?>
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i><?= __('Invoice Items') ?>
                    </div>
                    <div class="actions">
                        <?= $this->Form->button(__('Add Invoice Item'), ['type' => 'button', 'class' => 'button btn green', 'id' => 'add_invoice_items', 'onclick' => 'addInvoiceItemRow(' . $invoice->id . ');']) ?>
                    </div>
                </div>
                <!-- Hidden inputs which are used after added new invoice items -->
                <input type="hidden" id="purchase_order_id_hidden" value="<?= $invoice->purchase_order_id ?>"/>
                <input type="hidden" id="master_items_hidden" value="<?= $masterItems ?>"/>
                <input type="hidden" id="master_items_gtin_array_hidden" value="<?= $masterItemsGtinArr ?>"/>
                <input type="hidden" id="table_fields_td_num_hidden" value="<?= htmlentities($tableFieldsTdNum) ?>"/>
                <input type="hidden" id="new_invoice_items_count" value="0"/>
                <div class="portlet-body">                    
                    <!-- Start invoice items errors -->
                    <div class="alert alert-danger hide-cls" id="duplicate_description_span_error">
                        <span><?= __('This item is already exist in list, please select another one.') ?></span>
                    </div> 
                    <div class="alert alert-danger hide-cls" id="master_items_not_available_span_error">
                        <span><?= __('No items available for add.') ?></span>
                    </div> 
                    <!-- End invoice items errors -->
                    <div class="padding-top-bottom-10">
                        <label><?= __('Note: Please click on submit button for store modified invoice items') ?></label>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="invoice-items-table">
                            <thead>
                                <tr class="action-anch  thead-col">
                                    <th class="width-35-per"><?= __('Description') ?></th>
                                    <th class="width-15-per"><?= __('Requested Quantity') ?></th>
                                    <th class="width-10-per"><?= __('Net Price ($)') ?></th>
                                    <th class="width-10-per"><?= __('Net Amount ($)') ?></th>
                                    <th class="width-10-per"><?= __('GST Amount ($)') ?></th>
                                    <th class="width-15-per"><?= __('GST Percentage (%)') ?></th>
                                    <th class="actions width-10-per"><?= __('Actions') ?></th>
                                </tr>
                            </thead>
                            <tbody> 
                                <?php $invoiceObj = new ArrayIterator($invoice->invoice_items); ?>
                                <?php if (iterator_count($invoiceObj) > 0) { ?>
                                    <?php foreach ($invoice->invoice_items as $invoiceItem): ?>
                                        <?php $invoiceItemsIdArr[] = $invoiceItem->id; ?> 
                                        <?php $invoiceItemsGtinArr[] = $invoiceItem->gtin; ?> 
                                        <tr>
                                            <td>
                                                <span class="edit-span" id="description_dropdown_span_<?= $invoiceItem->id; ?>">
                                                    <?= $this->Common->getSubString($invoiceItem->description, 0, 55) ?>
                                                    <input type="hidden" name="invoice_items[<?= $invoiceItem->id; ?>][description]" value="<?= $invoiceItem->description; ?>" />
                                                </span>
                                                <input type="hidden" id="gtin_hidden_input_<?= $invoiceItem->id; ?>" name="invoice_items[<?= $invoiceItem->id; ?>][gtin]" value="<?= $invoiceItem->gtin; ?>">
                                                <input type="hidden" name="invoice_items[<?= $invoiceItem->id; ?>][order_line_number]" value="<?= $invoiceItem->order_line_number; ?>"/>
                                                <input type="hidden" name="invoice_items[<?= $invoiceItem->id; ?>][reference_purchase_order_line_number]" value="<?= $invoiceItem->reference_purchase_order_line_number; ?>"/>
                                            </td>
                                            <td>
                                                <span class="edit-span" id="requested_quantity_new_span_<?= $invoiceItem->id; ?>"><?= $invoiceItem->requested_quantity ?></span>
                                                <input type="hidden" id="hidden_requested_quantity_<?= $invoiceItem->id; ?>" value="<?= $invoiceItem->requested_quantity; ?>" />
                                                <input type="hidden" id="requested_quantity_<?= $invoiceItem->id; ?>" name="invoice_items[<?= $invoiceItem->id; ?>][requested_quantity]" value="<?= $invoiceItem->requested_quantity; ?>" />
                                                <input type="hidden" id="invoice_item_id_<?= $invoiceItem->id; ?>" name="invoice_items[<?= $invoiceItem->id; ?>][id]" value="<?= $invoiceItem->id; ?>" />
                                            </td>
                                            <td>
                                                <span class="edit-span" id="net_price_span_<?= $invoiceItem->id; ?>"><?= $this->Number->format($invoiceItem->net_price, Configure::read('number-format-settings')) ?></span>
                                                <input type="hidden" id="net_price_<?= $invoiceItem->id; ?>" value="<?= $this->Number->format($invoiceItem->net_price, Configure::read('number-format-settings')) ?>" name="invoice_items[<?= $invoiceItem->id; ?>][net_price]"/>
                                                <input type="hidden" id="hidden_net_price_<?= $invoiceItem->id; ?>" class="net-price-input" value="<?php echo $this->Number->format($invoiceItem->net_price, Configure::read('number-format-settings')); ?>" />
                                            </td>
                                            <td>
                                                <span class="net-amount-span edit-span" id="net_amount_span_<?php echo $invoiceItem->id; ?>">
                                                    <?= $this->Number->format($invoiceItem->net_amount, Configure::read('number-format-settings')) ?>
                                                </span>
                                                <input type="hidden" id="net_amount_<?= $invoiceItem->id; ?>" class="net-amount-input" value="<?php echo $this->Number->format($invoiceItem->net_amount, Configure::read('number-format-settings')); ?>" name="invoice_items[<?= $invoiceItem->id; ?>][net_amount]"/>
                                                <input type="hidden" id="hidden_net_amount_<?= $invoiceItem->id; ?>" class="net-amount-input" value="<?php echo $this->Number->format($invoiceItem->net_amount, Configure::read('number-format-settings')); ?>" />
                                            </td>
                                            <td>
                                                <span class="edit-span" id="gst_amount_span_<?= $invoiceItem->id; ?>"><?= $this->Number->format($invoiceItem->gst_amount, Configure::read('number-format-settings')) ?></span>
                                                <input type="hidden" id="gst_amount_<?= $invoiceItem->id; ?>" name="invoice_items[<?= $invoiceItem->id; ?>][gst_amount]" class="net-amount-input" value="<?= $this->Number->format($invoiceItem->gst_amount, Configure::read('number-format-settings')) ?>" />
                                                <input type="hidden" id="hidden_gst_amount_<?= $invoiceItem->id; ?>"  class="net-amount-input" value="<?= $this->Number->format($invoiceItem->gst_amount, Configure::read('number-format-settings')) ?>" />
                                            </td>
                                            <td>
                                                <span class="edit-span" id="gst_percentage_span_<?= $invoiceItem->id; ?>"><?= $invoiceItem->gst_percentage ?></span>
                                                <input type="hidden" id="gst_percentage_<?= $invoiceItem->id; ?>" name="invoice_items[<?= $invoiceItem->id; ?>][gst_percentage]" value="<?= $invoiceItem->gst_percentage ?>" />
                                                <input type="hidden" id="hidden_gst_percentage_<?= $invoiceItem->id; ?>" value="<?= $invoiceItem->gst_percentage ?>" />
                                            </td>
                                            <td class="actions">
                                                <?= $this->Html->link(__(''), 'javascript:void(0);', array('id' => 'edit_save_cancel_invoice_items_' . $invoiceItem->id, 'onclick' => 'inlineEditOrCancel(this,' . $tableFieldsTdNum . ',' . $invoiceItem->id . ')', 'class' => 'edit glyphicon glyphicon-edit link-deco', 'custattr' => 'edit', 'title' => 'Edit')); ?>
                                                <?= $this->Html->link(__(''), 'javascript:void(0);', array('id' => 'delete_cancel_invoice_items_' . $invoiceItem->id, 'onclick' => 'deleteOrCancelInvoiceItems(' . $invoiceItem->id . ',this,"");', 'custdeletecancelattr' => 'delete', 'class' => 'glyphicon glyphicon-trash link-deco', 'title' => 'Delete')); ?>
                                            </td>
                                        </tr>

                                    <?php endforeach; ?>
                                <?php }else { ?>
                                    <tr id="not_items_tr"><td colspan="7"> <center><?= __('Records not found!!!') ?></center></td></tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="hidden" id="deleted_invoice_items" name="deleted_inovice_items"/>
                    <input type="hidden" id="invoice_items_ids_arr" name="[]" value="<?= implode(',', $invoiceItemsIdArr); ?>"/>
                    <input type="hidden" id="invoice_items_gtin_arr" name="[]" value="<?= implode(',', $invoiceItemsGtinArr); ?>"/>
                </div>
            </div>
            <!-- End list of invoice items -->
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-10 col-md-2">
                    <?= $this->Html->link(__('Cancel'), ['controller' => 'Invoices', 'action' => 'index'], ['class' => 'btn default']) ?>
                    <?= $this->Form->button(__('Submit'), array('id' => 'submit-invoice-form', 'class' => 'btn blue')) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
<?php echo $this->Html->script('invoice.js?19082016') ?>
<script>
    $(document).ready(function() {
        calculateGstAmountForInvoiceItems();
    });
</script>