<?php

use Cake\Core\Configure; ?>

<?= $this->Form->create('invoices', array('id' => 'invoices', 'name' => 'invoices', 'action' => 'generate_delivery_docket', 'onsubmit' => 'showLoading()')); ?>

<!-- generate delivery docket info box start -->
<div class="modal fade" id="generate_delivery_docket_info_box" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-label="Close" style="margin-top:3px"><span aria-hidden="true">&times;</span></button>
                <div class="bootbox-body">
                    <div class="row">
                        <label class="col-md-3 control-label"><?= __('Delivery Date') ?><span class="required"> * </span></label>
                        <div class="col-md-6 col-md-pop-3">
                            <div class="input-icon">
                                <i class="fa fa-calendar"></i>
                                <?= $this->Form->input('delivery_date', array('size' => '16', 'type' => 'text', 'data-date-format' => 'yyyy-mm-dd', 'tabindex' => '1', 'autofocus' => 'On', 'label' => false, 'div' => false, 'class' => 'form-control date-picker', 'required' => true, 'onkeydown' => 'return false')) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-cancel" class="btn default" data-dismiss="modal">Cancel</button>
                <input type="submit" id="btn-ok" class="btn btn-primary" value="Submit" />
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- generate delivery docket info box end -->


<div class="portlet box green-haze">

    <!-- button of open delivery docket details window -->
    <button type="button" class="display-none" id="open_delivery_docket_details_window" data-toggle="modal" data-dismiss="modal" data-target="#generate_delivery_docket_info_box"></button>

    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('Invoices Sent to Bunnings') ?>
        </div>
        <div class="actions">
            <?php
            if (iterator_count($invoices) > 0) {
                echo $this->Form->button(__('Generate Delivery Docket'), array('type' => 'button', 'id' => 'generate_delivery_docket', 'class' => 'btn green'));
            } else {
                echo $this->Form->button(__('Generate Delivery Docket'), array('type' => 'button', 'id' => 'generate_delivery_docket', 'class' => 'btn green', 'disabled' => true));
            }
            ?>
            <?= $this->Html->link(__('Delivery Dockets'), ['controller' => 'DeliveryDockets', 'action' => 'index'], ['class' => 'btn green']); ?>

        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <?php
            $sortDir = $this->Paginator->sortDir() === 'asc' ? 'fa-sort-asc sort-active-color' : 'fa-sort-desc sort-active-color';
            $sortKey = $this->Paginator->sortKey();
            ?>
            <div class="alert alert-danger display-hide" id="error-message">
                <span></span>
            </div>
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th class="th-5-wd align-center">
                            <span class="md-checkbox">
                                <?= $this->Form->checkbox('generate_delivery_docket_checkbox', ['id' => 'generate_delivery_docket_checkbox', 'div' => false, 'lable' => false, 'class' => 'md-check', 'hiddenField' => false]); ?>
                                <label for="generate_delivery_docket_checkbox">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span>
                                </label>
                            </span>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('invoice_number') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.invoice_number') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('purchase_order_number') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.purchase_order_number') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('sub_total', 'Sub Total ($)') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.sub_total') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('tax', 'GST (%)') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.tax') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('sales_person') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.sales_person') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('ship_via') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.ship_via') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('invoice_status') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.invoice_status') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('time_of_invoice_sent_to_bunnings', 'Invoice sent time') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.time_of_invoice_sent_to_bunnings') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <?php if ($authUser['invoice_generate_by'] == Configure::read('invoice_generate_by.Quick-Books')) { ?>
                            <th>
                                <?= $this->Paginator->sort('invoice_update_to_quick_book', 'Invoice Updated To Quick Book') ?>
                                <i class="fa <?php
                                if ($sortKey == 'Invoices.invoice_update_to_quick_book') {
                                    echo $sortDir;
                                } else {
                                    echo 'fa-sort sort-default-color';
                                }
                                ?> float-right-important"></i>
                            </th>
                        <?php } ?>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (iterator_count($invoices) > 0) { ?>
                        <?php foreach ($invoices as $invoice): ?>
                            <tr>
                                <td class="align-center">
                                    <?php if ($invoice->delivery_docket_generated == Configure::read('delivery_docket_generated.no')) { ?>
                                        <span class="md-checkbox">
                                            <?= $this->Form->checkbox('generate_delivery_docket', ['value' => $invoice->id . ',' . $invoice->purchase_order_id, 'name' => 'generate_delivery_docket_checkbox[]', 'id' => 'generate_delivery_docket_checkbox_' . $invoice->id, 'div' => false, 'lable' => false, 'class' => 'md-check', 'hiddenField' => false]); ?>
                                            <label for="generate_delivery_docket_checkbox_<?= $invoice->id ?>">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                            </label>
                                        </span>
                                    <?php } ?>
                                </td>
                                <td><?= h($invoice->invoice_number) ?></td>
                                <td><?= h($invoice->purchase_order_number) ?></td>
                                <td><?= $this->Number->format($invoice->sub_total, Configure::read('number-format-settings')) ?></td>
                                <td><?= $invoice->tax ?></td>
                                <td><?= h($invoice->sales_person) ?></td>
                                <td><?= h($invoice->ship_via) ?></td>
                                <td <?php if ($invoice->invoice_status == Configure::read('invoice_ack_invoice_status.ERROR')) { ?> class="text-red" <?php } ?>><?= h($invoice->invoice_status) ?></td>
                                <td><?= ($invoice->time_of_invoice_sent_to_bunnings) ? date(Configure::read('date-format.default'), strtotime($invoice->time_of_invoice_sent_to_bunnings)) : '' ?></td>
                                <?php if ($authUser['invoice_generate_by'] == Configure::read('invoice_generate_by.Quick-Books')) { ?>
                                    <td>
                                        <?= ucwords(h($invoice->invoice_update_to_quick_book)) ?>
                                        &nbsp;&nbsp;
                                        <?php
                                        if ($invoice->invoice_update_to_quick_book == Configure::read('invoice.update_to_quick_book.No')) {
                                            echo $this->Html->link(__('Resend to Quick-Book'), ['controller' => 'Invoices', 'action' => 'resend_failed_invoice_to_quick_book', $invoice->id, $this->request->action], ['class' => 'btn blue-hoki btn-xs display-loader-onclick']);
                                        }
                                        ?>
                                    </td>
                                <?php } ?>
                                <td class="actions action-anch">
                                    <?= $this->Html->link(__(''), ['action' => 'view', $invoice->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?> 
                                    <?php
                                    foreach ($invoice->file_overviewes as $fileOverviewe) {
                                        if (isset($fileOverviewe->file_type->type) && ($fileOverviewe->file_type->type == \App\Model\Table\FileTypesTable::Ack) && ($fileOverviewe->invoice_id != null)) {
                                            ?>
                                            <?= $this->Html->link(__(''), ['controller' => 'file_overviewes', 'action' => 'read_file', $fileOverviewe->id], ['class' => 'glyphicon glyphicon-file link-deco', 'title' => 'Read File', 'target' => '_blank']) ?> 
                                            <?php
                                        }
                                    }
                                    ?>
                                    <?php if ($invoice->invoice_status == Configure::read('invoice_ack_invoice_status.ERROR') && $invoice->resend_invoice == Configure::read('resend_invoice.no')) { ?>
                                        <?= $this->Html->link(__(''), ['action' => 'update_regenerate_invoice', $invoice->id], ['class' => 'glyphicon glyphicon-upload  link-deco', 'title' => 'Regenerate Invoice']) ?> 
                                    <?php } ?>
                                </td>
                            </tr>

                        <?php endforeach; ?>
                    <?php } else { ?>
                    <td colspan="<?php if ($authUser['invoice_generate_by'] == Configure::read('invoice_generate_by.Quick-Books')) { ?> 11 <?php } else { ?> 10 <?php } ?>"> <center><?= __('Records not found!!!') ?></center></td>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <?php if (iterator_count($invoices) > 0) { ?>
            <div class="paginator">
                <ul class="pagination large-10 medium-10 columns">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p class="large-2 medium-2 columns"><?= $this->Paginator->counter() ?></p>
            </div>
        <?php } ?>
    </div>
</div>
<?= $this->Form->end() ?>

<?php echo $this->Html->script('delivery_docket.js?08082015') ?>

<!--<script>
    
// Auto reload page after some time    
setTimeout(function() {
    window.location.reload();
}, <?php // echo Configure::read('auto_refresh_page_time');         ?>);

</script>-->
