<?php

use Cake\Core\Configure; ?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('View Invoice') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body row">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Invoice Number') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $invoice->invoice_number ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Invoice Type') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $invoice->invoice_type ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Purchase Order Number') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($invoice->purchase_order_number) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Sub Total ($)') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $this->Number->format($invoice->sub_total, Configure::read('number-format-settings')) ?></label>
                </div>
            </div>
            <?php if ($invoice->discount_amount > 0) { ?>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Discount (%)') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= $invoice->discount_percentage ?></label>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Sub Total after Discount ($)') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= $this->Number->format(($invoice->sub_total - $invoice->discount_amount), Configure::read('number-format-settings')) ?></label>
                    </div>
                </div>
            <?php } ?>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('GST (%)') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $invoice->tax ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Total Include GST ($)') ?>:</label>
                <div class="col-md-9">
                    <label class="large">
                        <?php 
                            if ($invoice->discount_amount > 0) {
                                $subTotalWithDiscount = $invoice->sub_total - $invoice->discount_amount;
                                $gstAmount = ($subTotalWithDiscount * $invoice->tax) / 100;
                                echo $this->Number->format(($subTotalWithDiscount + $gstAmount), Configure::read('number-format-settings'));
                            } else {
                                $gstAmount = ($invoice->sub_total * $invoice->tax) / 100;
                                echo $this->Number->format(($invoice->sub_total + $gstAmount), Configure::read('number-format-settings'));
                            }
                        ?>
                    </label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Sales Person') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($invoice->sales_person) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Ship Via') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($invoice->ship_via) ?></label>
                </div>
            </div>
        </div>

        <!-- Start Related Invoice Items --> 
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-book"></i><?= __('Related Invoice Items') ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <tr class="action-anch  thead-col">
                            <th><?= __('Description') ?></th>
                            <th><?= __('Requested Quantity') ?></th>
                            <th><?= __('Net Price ($)') ?></th>
                            <th><?= __('Net Amount ($)') ?></th>
                            <th><?= __('GST Amount ($)') ?></th>
                            <th><?= __('GST Percentage (%)') ?></th>
                        </tr>
                        <tbody> 
                            <?php if (count($invoice->invoice_items) > 0) { ?>
                                <?php foreach ($invoice->invoice_items as $invoiceItem): ?>
                                    <tr>
                                        <td><?= h($invoiceItem->description) ?></td>
                                        <td><?= h($invoiceItem->requested_quantity) ?></td>
                                        <td><?= $this->Number->format($invoiceItem->net_price, Configure::read('number-format-settings')) ?></td>
                                        <td><?= $this->Number->format($invoiceItem->net_amount, Configure::read('number-format-settings')) ?></td>
                                        <td><?= $this->Number->format($invoiceItem->gst_amount, Configure::read('number-format-settings')) ?></td>
                                        <td><?= h($invoiceItem->gst_percentage) ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php }else { ?>
                                <tr><td colspan="6"> <center><?= __('Records not found!!!') ?></center></td></tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End Related Invoice Items -->        
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-11 col-md-1">
                    <?= $this->Html->link(__('Back'), ['controller' => 'Invoices', 'action' => 'index'], ['class' => 'btn default']) ?>
                </div>
            </div>
        </div>
    </div>
</div>