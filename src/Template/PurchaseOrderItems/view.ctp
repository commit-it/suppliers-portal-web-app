<?php use Cake\Core\Configure; ?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Purchase Order Items') ?></span>
        </div>
    </div>
    <div class="portlet-body form">       
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Purchase Order') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($purchaseOrderItem->purchase_order) ? $this->Html->link($purchaseOrderItem->purchase_order->purchase_order_number, ['controller' => 'PurchaseOrders', 'action' => 'view', $purchaseOrderItem->purchase_order->id]) : '' ?></label>
            </div>
        </div>        
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Order Line Number') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= $purchaseOrderItem->order_line_number ?></label>
            </div>
        </div> 
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Gtin') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= $purchaseOrderItem->gtin ?></label>
            </div>
        </div>   
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Requested Quantity') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= $purchaseOrderItem->requested_quantity ?></label>
            </div>
        </div>   
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Price ($)') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= $this->Number->format($purchaseOrderItem->net_price, Configure::read('number-format-settings')) ?></label>
            </div>
        </div>

        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Created') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= date(Configure::read('date-format.default'), strtotime($purchaseOrderItem->created)) ?></label>
            </div>
        </div>  
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Modified') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= date(Configure::read('date-format.default'), strtotime($purchaseOrderItem->modified)) ?></label>
            </div>
        </div> 
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Description') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= $this->Text->autoParagraph(h($purchaseOrderItem->description)); ?></label>
            </div>
        </div> 
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-11 col-md-1">
                    <?php if (h($purchaseOrderItem->purchase_order)) {
                        echo $this->Html->link(__('Back'), ['controller' => 'PurchaseOrders', 'action' => 'view', $purchaseOrderItem->purchase_order->id], ['class' => 'btn default']);
                    } else {
                        echo $this->Html->link(__('Back'), ['controller' => 'PurchaseOrders', 'action' => 'index'], ['class' => 'btn default']);
                    } ?>
                </div>
            </div>
        </div>
    </div>
</div>




