<?php use Cake\Core\Configure ?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('View Delivery Docket') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body row">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Invoice Number') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $deliveryDocket->has('invoice') ? $this->Html->link($deliveryDocket->invoice->invoice_number, ['controller' => 'Invoices', 'action' => 'view', $deliveryDocket->invoice->id]) : '' ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Purchase Order Number') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $deliveryDocket->has('purchase_order') ? $this->Html->link($deliveryDocket->purchase_order->purchase_order_number, ['controller' => 'PurchaseOrders', 'action' => 'view', $deliveryDocket->purchase_order->id],['class' => 'link-deco']) : '' ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Delivery Date') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= date(Configure::read('date-format.default'), strtotime($deliveryDocket->delivery_date)) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Created') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= date(Configure::read('date-format.default'), strtotime($deliveryDocket->created)) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Modified') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= date(Configure::read('date-format.default'), strtotime($deliveryDocket->modified)) ?></label>
                </div>
            </div>
        </div>
        
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-11 col-md-1">
                    <?= $this->Html->link(__('Back'), ['controller' => 'DeliveryDockets', 'action' => 'index'], ['class' => 'btn default']) ?>
                </div>
            </div>
        </div>
    </div>
</div>