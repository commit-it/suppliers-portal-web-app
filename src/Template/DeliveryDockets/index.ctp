<?php

use Cake\Core\Configure; ?>
<?= $this->Form->create('DeliveryDockets', array('action' => 'edit', 'id' => 'update_record_form', 'onsubmit' => 'showLoading()')); ?>

<!-- generate delivery docket info box start -->
<div class="modal fade" id="generate_delivery_docket_info_box" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-label="Close" style="margin-top:3px"><span aria-hidden="true">&times;</span></button>
                <div class="bootbox-body">
                    <?= $this->Form->input('DeliveryDockets.id', array('type' => 'hidden', 'label' => false, 'div' => false)) ?>
                    <div class="row">
                        <label class="col-md-4 control-label"><?= __('Change Delivery Date') ?><span class="required"> * </span></label>
                        <div class="col-md-7 col-md-pop-2">
                            <div class="input-icon">
                                <i class="fa fa-calendar"></i>
                                <?= $this->Form->input('DeliveryDockets.delivery_date', array('size' => '16', 'type' => 'text', 'data-date-format' => 'yyyy-mm-dd', 'tabindex' => '1', 'autofocus' => 'On', 'label' => false, 'div' => false, 'class' => 'form-control date-picker', 'required' => true, 'onkeydown' => 'return false')) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-cancel" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="button" id="update_delivery_docket" class="btn btn-primary">Submit</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- generate delivery docket info box end -->

<?= $this->Form->end() ?>


<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('Delivery Dockets') ?>
        </div>
        <div class="actions">
            <?php if (iterator_count($deliveryDockets) > 0) { ?>
                <?= $this->Form->button(__('Print'), ['type' => 'button', 'id' => 'print_delivery_dockets', 'class' => 'btn green']) ?>
            <?php } else { ?>
                <?= $this->Form->button(__('Print'), ['type' => 'button', 'id' => 'print_delivery_dockets', 'class' => 'btn green', 'disabled' => true]) ?>
            <?php } ?>
        </div>
    </div>
    <div class="portlet-body">
        
        <div class="portlet-body form">
            <?php echo $this->Form->create('delivery_docket', ['class' => 'form-horizontal', 'onsubmit' => 'showLoading()']); ?>
            <div class="form-body">
                <div class="form-group">
                    <div class="col-md-2 col-md-offset-8">
                        <div class="input-icon">
                            <i class="fa fa-calendar"></i>
                            <?php echo $this->Form->input('delivery_date', array('type' => 'text', 'placeholder' => 'Delivery Date', 'data-date-format' => 'yyyy-mm-dd', 'label' => false, 'div' => false, 'class' => 'form-control date-picker simple-date-picker', 'onkeydown' => 'return false')); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="row">
                            <div class="col-md-5">
                                <?php echo $this->Form->submit(__('Search'), array('label' => false, 'div' => false, 'class' => 'btn btn-primary')); ?>
                            </div>
                            <div class="col-md-5">
                                <?php echo $this->Html->link(__('Clear'), ['controller' => 'DeliveryDockets', 'action' => 'index'], ['class' => 'btn default']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        
        <?= $this->Form->create('PrintDeliveryDockets', array('id' => 'print_delivery_dockets_form', 'name' => 'print_delivery_dockets_form', 'action' => 'print_delivery_dockets')); ?>
            <div class="table-responsive">
                <?php
                $sortDir = $this->Paginator->sortDir() === 'asc' ? 'fa-sort-asc sort-active-color' : 'fa-sort-desc sort-active-color';
                $sortKey = $this->Paginator->sortKey();
                ?>
                <div class="alert alert-danger display-hide" id="error-message">
                    <span></span>
                </div>
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                        <tr class="action-anch thead-col">
                            <th class="align-center">
                                <?php if (iterator_count($deliveryDockets) > 0) { ?>
                                    <span class="md-checkbox">
                                        <?= $this->Form->checkbox('delivery_docket', ['id' => 'select_deselect_delivery_docket', 'div' => false, 'lable' => false, 'class' => 'md-check', 'hiddenField' => false]); ?>
                                        <label for="select_deselect_delivery_docket">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                        </label>
                                    </span>
                                <?php } ?>
                            </th>
                            <th>
                                <?= $this->Paginator->sort('PurchaseOrders.bill_to_name', 'Store Name') ?>
                                <i class="fa <?php
                                if ($sortKey == 'PurchaseOrders.bill_to_name') {
                                    echo $sortDir;
                                } else {
                                    echo 'fa-sort sort-default-color';
                                }
                                ?> float-right-important"></i>            
                            </th>
                            <th>
                                <?= $this->Paginator->sort('Invoices.invoice_number', 'Invoice Number') ?>
                                <i class="fa <?php
                                if ($sortKey == 'Invoices.invoice_number') {
                                    echo $sortDir;
                                } else {
                                    echo 'fa-sort sort-default-color';
                                }
                                ?> float-right-important"></i>
                            </th>
                            <th>
                                <?= $this->Paginator->sort('PurchaseOrders.purchase_order_number', 'Purchase Order Number') ?>
                                <i class="fa <?php
                                if ($sortKey == 'PurchaseOrders.purchase_order_number') {
                                    echo $sortDir;
                                } else {
                                    echo 'fa-sort sort-default-color';
                                }
                                ?> float-right-important"></i>            
                            </th>
                            <th>
                                <?= $this->Paginator->sort('DeliveryDockets.delivery_date', 'Delivery Date') ?>
                                <i class="fa <?php
                                if ($sortKey == 'DeliveryDockets.delivery_date') {
                                    echo $sortDir;
                                } else {
                                    echo 'fa-sort sort-default-color';
                                }
                                ?> float-right-important"></i>            
                            </th>
                            <th>
                                <?= $this->Paginator->sort('DeliveryDockets.created', 'Created') ?>
                                <i class="fa <?php
                                if ($sortKey == 'DeliveryDockets.created') {
                                    echo $sortDir;
                                } else {
                                    echo 'fa-sort sort-default-color';
                                }
                                ?> float-right-important"></i>                 
                            </th>
                            <th>
                                <?= $this->Paginator->sort('DeliveryDockets.modified', 'Modified') ?>
                                <i class="fa <?php
                                if ($sortKey == 'DeliveryDockets.modified') {
                                    echo $sortDir;
                                } else {
                                    echo 'fa-sort sort-default-color';
                                }
                                ?> float-right-important"></i>                 
                            </th>
                            <th class="actions"><?= __('Printed') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (iterator_count($deliveryDockets) > 0) { ?>
                            <?php foreach ($deliveryDockets as $deliveryDocket): ?>
                                <tr>
                                    <td class="align-center">
                                        <span class="md-checkbox">
                                            <?= $this->Form->checkbox('delivery_dockets[]', ['id' => 'delivery_dockets_' . $deliveryDocket->id, 'value' => $deliveryDocket->id, 'div' => false, 'lable' => false, 'class' => 'md-check', 'hiddenField' => false]) ?>
                                            <label for="delivery_dockets_<?= $deliveryDocket->id ?>">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span>
                                            </label>
                                        </span>
                                    </td>
                                    <td>
                                        <?= $deliveryDocket->purchase_order->bill_to_name ?>
                                    </td>
                                    <td>
                                        <?= $deliveryDocket->has('invoice') ? $this->Html->link($deliveryDocket->invoice->invoice_number, ['controller' => 'Invoices', 'action' => 'view', $deliveryDocket->invoice->id]) : '' ?>
                                    </td>
                                    <td>
                                        <?= $deliveryDocket->has('purchase_order') ? $this->Html->link($deliveryDocket->purchase_order->purchase_order_number, ['controller' => 'PurchaseOrders', 'action' => 'view', $deliveryDocket->purchase_order->id]) : '' ?>
                                    </td>
                                    <td><?= date(Configure::read('date-format.default'), strtotime($deliveryDocket->delivery_date)) ?></td>
                                    <td><?= date(Configure::read('date-format.default'), strtotime($deliveryDocket->created)) ?></td>
                                    <td><?= date(Configure::read('date-format.default'), strtotime($deliveryDocket->modified)) ?></td>
                                    <td><?= ($deliveryDocket->is_printed_delivery_docket) ? 'Yes' : 'No' ?></td>
                                    <td class="actions action-anch">
                                        <?php $deliveryDocketDate = "'" . date('Y-m-d', strtotime($deliveryDocket->delivery_date)) . "'"; ?>
                                        <?= $this->Html->link(__(''), ['action' => 'view', $deliveryDocket->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?>
                                        <?= $this->Html->link(__(''), 'javascript:void(0)', ['class' => 'glyphicon glyphicon-edit link-deco', 'title' => 'Edit', 'data-toggle' => 'modal', 'data-dismiss' => 'modal', 'data-target' => '#generate_delivery_docket_info_box', 'onclick' => 'modifyDeliveryDocketData(' . $deliveryDocket->id . ',' . $deliveryDocketDate . ')']) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        <?php } else { ?>
                        <td colspan="9"> <center><?= __('Records not found!!!') ?></center></td>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
            <?php if (iterator_count($deliveryDockets) > 0) { ?>
                <div class="paginator">
                    <ul class="pagination large-10 medium-10 columns">
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                    <p class="large-2 medium-2 columns"><?= $this->Paginator->counter() ?></p>
                </div>
            <?php } ?>
        <?= $this->Form->end() ?>
    </div>
</div>

<?php echo $this->Html->script('delivery_docket.js?08082015') ?>

<!--<script>
    
// Auto reload page after some time    
setTimeout(function() {
    window.location.reload();
}, <?php //echo Configure::read('auto_refresh_page_time');       ?>);

</script>-->
