<?php

use Cake\Core\Configure; ?>

<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('Generate Booking Sheet') ?>
        </div>
        <div class="actions">
            <?php
            if (iterator_count($invoices) > 0) {
                echo $this->Form->button(__('Generate Booking Sheet'), array('type' => 'button', 'id' => 'generate_booking_sheet', 'class' => 'btn green'));
            } else {
                echo $this->Form->button(__('Generate Booking Sheet'), array('type' => 'button', 'id' => 'generate_booking_sheet', 'class' => 'btn green', 'disabled' => true));
            }
            ?>
            <?= $this->Html->link(__('Booking Sheet'), ['controller' => 'BookingSheets', 'action' => 'index'], ['class' => 'btn green']) ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <?php
            $sortDir = $this->Paginator->sortDir() === 'asc' ? 'fa-sort-asc sort-active-color' : 'fa-sort-desc sort-active-color';
            $sortKey = $this->Paginator->sortKey();
            ?>
            <div class="alert alert-danger display-hide" id="error-message">
                <span></span>
            </div>
            <?= $this->Form->create('invoices', array('id' => 'booking_sheet_form', 'name' => 'booking_sheet_form', 'url' => ['controller' => 'BookingSheets', 'action' => 'add_booking_sheet'], 'onsubmit' => 'showLoading()')); ?>
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th class="th-5-wd align-center">
                            <?php if (iterator_count($invoices) > 0) { ?>
                                <span class="md-checkbox">
                                    <?= $this->Form->checkbox('booking_sheet', ['id' => 'select_deselect_booking_sheet', 'div' => false, 'lable' => false, 'class' => 'md-check', 'hiddenField' => false]); ?>
                                    <label for="select_deselect_booking_sheet">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span>
                                    </label>
                                </span>
                            <?php } ?>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('invoice_number') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.invoice_number') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>  
                        </th>
                        <th>
                            <?= $this->Paginator->sort('purchase_order_number') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.purchase_order_number') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>  
                        </th>
                        <th>
                            <?= $this->Paginator->sort('sub_total', 'Sub Total ($)') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.sub_total') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>  
                        </th>
                        <th>
                            <?= $this->Paginator->sort('tax', 'GST (%)') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.tax') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>  
                        </th>
                        <th>
                            <?= $this->Paginator->sort('sales_person') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.sales_person') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>  
                        </th>
                        <th>
                            <?= $this->Paginator->sort('ship_via') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Invoices.ship_via') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>  
                        </th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (iterator_count($invoices) > 0) {
                        ?>
                        <?php foreach ($invoices as $invoice): ?>
                            <tr>

                                <td class="align-center">
                                    <span class="md-checkbox">
                                        <?= $this->Form->checkbox('booking_sheet_id[]', ['id' => 'booking-sheet-id-' . $invoice->id, 'value' => $invoice->id, 'div' => false, 'lable' => false, 'hiddenField' => false, 'class' => 'md-check']) ?>
                                        <label for="booking-sheet-id-<?= $invoice->id ?>">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                        </label>
                                    </span>
                                </td>
                                <td><?= h($invoice->invoice_number) ?></td>
                                <td><?= h($invoice->purchase_order_number) ?></td>
                                <td><?= $this->Number->format($invoice->sub_total, Configure::read('number-format-settings')) ?></td>
                                <td><?= $invoice->tax ?></td>
                                <td><?= h($invoice->sales_person) ?></td>
                                <td><?= h($invoice->ship_via) ?></td>
                                <td class="actions action-anch">
                                    <?= $this->Html->link(__(''), ['action' => 'view', $invoice->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } else { ?>
                    <td colspan="8"> <center><?= __('Records not found!!!') ?></center></td>
                <?php } ?>

                </tbody>
            </table>
            <?= $this->Form->end() ?>
        </div>
        <?php if (iterator_count($invoices) > 0) { ?>
            <div class="paginator">
                <ul class="pagination large-10 medium-10 columns">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p class="large-2 medium-2 columns"><?= $this->Paginator->counter() ?></p>
            </div>
        <?php } ?>
    </div>
</div>
<?php echo $this->Html->script('delivery_docket.js?11082015') ?>

<!--<script>
    
// Auto reload page after some time    
setTimeout(function() {
    window.location.reload();
}, <?php // echo Configure::read('auto_refresh_page_time');         ?>);

</script>-->

