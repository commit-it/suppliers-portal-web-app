<?php

use Cake\Core\Configure; ?>
<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('Statistics') ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <?php if (!empty($yearList)) { ?>
                <?= $this->Form->create('statistics', ['class' => 'form-horizontal', 'onsubmit' => 'showLoading()']); ?>
                <div class="form-body">
                    <div class="form-group">
                        <div class="col-md-2">
                            <div class="input-icon">
                                <?= $this->Form->input('year', array('options' => $yearList, 'default' => $year, 'label' => false, 'div' => false, 'class' => 'form-control', 'onchange' => "this.form.submit()")) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            <?php } ?>

            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th><?= __('Nursery Name') ?></th>
                        <?php foreach (Configure::read('month') as $month) { ?>
                            <th><?= $month ?></th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($statistics as $statistic): ?>
                        <tr>
                            <td><?= h($statistic['full_name']) ?></td>
                            <?php
                            foreach ($statistic['data'] as $monthCount) {
                                echo '<td>' . $monthCount . '</td>';
                            }
                            ?>
                        </tr>
                    <?php endforeach; ?>
                    <?php if (count($statistics) < 1) { ?>
                        <tr>
                            <td colspan="13" class="align-center">No records found</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div> 
    </div>
</div>

