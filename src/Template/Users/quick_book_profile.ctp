<?php $t = 1; ?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-user font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?php if ($updateOrNot > 0) { echo __('Update Quick Book Details'); } else { echo __('Quick Book Details'); } ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        
        <?php if ($updateOrNot > 0) { ?>
        
            <?= $this->Form->create($quickBookData, ['class' => 'form-horizontal', 'onsubmit' => 'showLoading()']); ?>
            <!-- Start invoice information -->
            <div class="form-body">
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label"><?= __('Access Token') ?><span class="required"> * </span></label>
                    <div class="col-md-6">
                        <div class="input-icon">
                            <?= $this->Form->text('quick_book_access_token', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)) ?>
                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label"><?= __('Access Token Secret') ?><span class="required"> * </span></label>
                    <div class="col-md-6">
                        <div class="input-icon">
                            <?= $this->Form->text('quick_book_access_token_secret', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)) ?>
                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label"><?= __('Consumer Key') ?><span class="required"> * </span></label>
                    <div class="col-md-6">
                        <div class="input-icon">
                            <?= $this->Form->text('quick_book_consumer_key', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)) ?>
                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label"><?= __('Consumer Secret') ?><span class="required"> * </span></label>
                    <div class="col-md-6">
                        <div class="input-icon">
                            <?= $this->Form->text('quick_book_consumer_secret', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)) ?>
                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label"><?= __('Realm Id (Company Registration No.)') ?><span class="required"> * </span></label>
                    <div class="col-md-6">
                        <div class="input-icon">
                            <?= $this->Form->text('quick_book_realm_id', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)) ?>
                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label"><?= __('Payment Term') ?></label>
                    <div class="col-md-6">
                        <div class="input-icon">
                            <?= $this->Form->input('quick_book_payment_term', array('options' => $quickBookPaymentTermList, 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => false)) ?>
                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label"><?= __('Rebate (%)') ?></label>
                    <div class="col-md-6">
                        <div class="input-icon">
                            <?= $this->Form->text('quick_book_rebate', array('type' => 'number', 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => false)) ?>
                            <div class="form-control-focus"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-10 col-md-2">
                        <?= $this->Html->link(__('Cancel'), ['action' => 'quick_book_profile'], ['tabindex' => $t++, 'class' => 'btn default']) ?>
                        <?= $this->Form->button(__('Submit'), array('tabindex' => $t++, 'class' => 'btn blue')) ?>
                    </div>
                </div>
            </div>
            <?= $this->Form->end() ?>
        
        <?php } else { ?>
            <div class="form-body row">
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Access Token') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= h($quickBookData->quick_book_access_token) ?></label>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Access Token Secret') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= h($quickBookData->quick_book_access_token_secret) ?></label>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Consumer Key') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= h($quickBookData->quick_book_consumer_key) ?></label>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Consumer Secret') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= h($quickBookData->quick_book_consumer_secret) ?></label>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Realm Id (Company Registration No.)') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= h($quickBookData->quick_book_realm_id) ?></label>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Payment Term') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= $paymentTerm ?></label>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Rebate (%)') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= h($quickBookData->quick_book_rebate) ?></label>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-10 col-md-2">
                        <?= $this->Html->link(__('Back'), ['action' => 'quick_book_profile'], ['class' => 'btn default']) ?>
                        <?= $this->Html->link(__('Update'), ['action' => 'quick_book_profile', 'update'], array('class' => 'btn blue', 'escape' => false)); ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>