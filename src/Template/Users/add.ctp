<?php $t = 1; ?>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Add Nursery') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <?= $this->Form->create($user, ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'onsubmit' => 'showLoading()']); ?>
        <!-- Start invoice information -->
        <div class="form-body">
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Full Name') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->input('full_name', array('tabindex' => $t++, 'autofocus' => 'On', 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Email') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('email', array('type' => 'email', 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Phone Number') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('phone_number', array('type' => 'number', 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label control-label-top"><?= __('Role') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <label class="large"><?= \App\Model\Table\UserRolesTable::User ?></label>
                        <?= $this->Form->text('user_role_id', array('type' => 'hidden', 'value' => $defaultRole, 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Address') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('address', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('City') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('city', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Country') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('country', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Password') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('password', array('type' => 'password', 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control', 'pattern' => '.{8,}')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('GLN') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('seller_gln', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
<!--            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label"><?= __('Trial End Date') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('trial_end_date', array('data-start-date' => date('Y-m-d'), 'data-date-format' => 'yyyy-mm-dd', 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control date-picker', 'onkeydown' => 'return false')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>-->
            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label control-label-top"><?= __('Invoice Generate By') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon radio-input-icon">
                        <?= $this->Form->text('invoice_generate_by', array('type' => 'radio', 'options' => \Cake\Core\Configure::read('invoice_generate_by'), 'value' => \Cake\Core\Configure::read('invoice_generate_by.MYOB'), 'tabindex' => $t++, 'label' => true, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-10 col-md-2">                    
                    <?= $this->Html->link(__('Cancel'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'btn default']) ?>
                    <?= $this->Form->button(__('Submit'), array('class' => 'btn blue')) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
