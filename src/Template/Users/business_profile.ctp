
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-user font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Business Profile') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body row">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Nursery Name') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($user->seller_business_name) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right"><?= __('Business Logo') ?>:</label>
                <div class="col-md-9">
                    <div class="courier-logo-thumb fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail">
                            <?php echo $this->Html->image($sellerBusinessLogoImg, array('alt' => 'logo')); ?>
                        </div>
                    </div>
                    <br /><br /><br />
                </div>
            </div>
            <div class="clear"><br /><br /><br /><br /></div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('GLN') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($user->seller_gln) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Nursery Address') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($user->seller_business_address) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Country') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= Cake\Core\Configure::read('country_iso_code.'.$user->country_iso_code) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Language Iso Code') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($user->language_iso_code) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Postal Code') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($user->postal_code) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('State') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= Cake\Core\Configure::read('state.'.$user->state) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('ABN') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($user->company_registration_number) ?></label>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-10 col-md-2">
                    <?= $this->Html->link(__('Back'), ['controller' => 'Dashboard', 'action' => 'index'], ['class' => 'btn default']) ?>
                    <?= $this->Html->link(__('Update'), ['action' => 'edit_business_profile'], array('class' => 'btn blue', 'escape' => false)); ?>
                </div>
            </div>
        </div>
    </div>
</div>