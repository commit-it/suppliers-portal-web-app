<?php

use Cake\Core\Configure;

/* Set variables */
$userTrialEndDate = isset($paymentDetails['trial_end_date']) ? (array) $paymentDetails['trial_end_date'] : '';
$userTrialStartDate = isset($paymentDetails['created']) ? (array) $paymentDetails['created'] : '';
$userSubscriptionCanceled = isset($paymentDetails['subscription_cancelled']) ? (array) $paymentDetails['subscription_cancelled'] : '';
?>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('View Subscription Details') ?></span>
        </div>
    </div>
    <div class="portlet-body form">                   
        <div class="form-body row">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Nursery Name') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= isset($paymentDetails['full_name']) && !empty($paymentDetails['full_name']) ? ucfirst($paymentDetails['full_name']) : '------'; ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Email') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= isset($paymentDetails['email']) && !empty($paymentDetails['email']) ? $paymentDetails['email'] : '------'; ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Access Status') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= isset($paymentDetails['access_status']) && !empty($paymentDetails['access_status']) ? ucfirst($paymentDetails['access_status']) : '------'; ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Payment Status') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= isset($paymentDetails['payment_status']) && !empty($paymentDetails['payment_status']) ? ucfirst($paymentDetails['payment_status']) : '------'; ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Customer Id') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= isset($paymentDetails['payments'][0]['customer_id']) ? ucfirst($paymentDetails['payments'][0]['customer_id']) : '------'; ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Trial Start Date') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= isset($userTrialStartDate['date']) && (strtotime($userTrialStartDate['date']) > 0) ? date(Configure::read('date-format.default'), strtotime($userTrialStartDate['date'])) : '------'; ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Trial End Date') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= isset($userTrialEndDate['date']) && (strtotime($userTrialEndDate['date']) > 0) ? date(Configure::read('date-format.default'), strtotime($userTrialEndDate['date'])) : '------'; ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Subscription Cancelled') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= isset($userSubscriptionCanceled['date']) && (strtotime($userSubscriptionCanceled['date']) > 0) ? date(Configure::read('date-format.default'), strtotime($userSubscriptionCanceled['date'])) : '------'; ?></label>
                </div>
            </div>           
        </div>
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-book"></i><?= __('Payment History') ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <thead>
                            <tr class="action-anch thead-col">
                                <th>
                                    <?= __('Package') ?>
                                </th>
                                <th>
                                    <?= __('Payment Interval') ?>
                                </th>
                                <th>
                                    <?= __('Interval Start Date') ?>
                                </th>
                                <th>
                                    <?= __('Interval End Date') ?>
                                </th>
                                <th>
                                    <?= __('Amount') ?>
                                </th>
                                <th>
                                    <?= __('Created') ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (isset($paymentDetails['payments'][0]['payment_histories']) && !empty($paymentDetails['payments'][0]['payment_histories'])) {
                                foreach ($paymentDetails['payments'][0]['payment_histories'] as $paymentDetail) {
                                    ?>
                                    <?php
                                    $paymentIntervalStartDate = isset($paymentDetail['interval_start_date']) ? (array) $paymentDetail['interval_start_date'] : '';
                                    $paymentIntervalEndDate = isset($paymentDetail['interval_end_date']) ? (array) $paymentDetail['interval_end_date'] : '';
                                    ?>
                                    <tr>
                                        <td><?= isset($paymentDetail['plan_id']) && !empty($paymentDetail['plan_id']) ? Configure::read('Stripe.packages_names.' . $paymentDetail['plan_id']) : '------'; ?></td>
                                        <td><?= isset($paymentDetail['payment_interval']) && !empty($paymentDetail['payment_interval']) ? ucfirst($paymentDetail['payment_interval']) : '------'; ?></td>
                                        <td><?= isset($paymentIntervalStartDate['date']) && (strtotime($paymentIntervalStartDate['date']) > 0) ? date(Configure::read('date-format.default'), strtotime($paymentIntervalStartDate['date'])) : '------'; ?></td>
                                        <td><?= isset($paymentIntervalEndDate['date']) && strtotime($paymentIntervalEndDate['date']) > 0 ? date(Configure::read('date-format.default'), strtotime($paymentIntervalEndDate['date'])) : '------'; ?></td>
                                        <td><?= isset($paymentDetail['amount']) && $paymentDetail['amount'] > 0 ? $this->Common->convertCentToPrise($paymentDetail['amount'], 2, '$') : 0; ?></td>
                                        <td><?= date(Configure::read('date-format.default'), strtotime($paymentDetail['created'])); ?></td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr><td colspan="6"> <center><?= __('Records not found!!!') ?></center></td></tr>
                        <?php }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-11 col-md-1">
                    <?= $this->Html->link(__('Back'), ['controller' => 'Users', 'action' => 'payment_history'], ['class' => 'btn default']) ?>
                </div>
            </div>
        </div>
    </div>
</div>