<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="glyphicon glyphicon-gift"></i><?= __('Configuration') ?>
        </div>
    </div>
    <div class="portlet-body configuration-portlet-body">
        <br />
        <div class="row">                
            <div class="col-md-3"> 
                <div class="icon-btn configuration-icon-btn">
                    <i class="fa fa-group"></i>
                    <div class="actions">                            
                        <?= $this->Html->link(__('Bunnings Stores'), ['controller' => 'BunningStores', 'action' => 'index'], ['class' => 'link-deco']) ?>
                    </div>
                    <span class="badge badge-danger"><?= $bunningStoresCnt ?></span>                   
                </div>
            </div>
            <div class="col-md-3">
                <div class="icon-btn configuration-icon-btn">
                    <i class="glyphicon glyphicon-home"></i>
                    <div class="actions">                            
                        <?= $this->Html->link(__('Master Items'), ['controller' => 'MasterItems', 'action' => 'index'], ['class' => 'link-deco']) ?>
                    </div>                       
                    <span class="badge badge-danger"><?= $masterItemsCnt ?></span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="icon-btn configuration-icon-btn">
                    <i class="fa fa-cog"></i>
                    <div class="actions">                            
                        <?= $this->Html->link(__('Administrator'), ['controller' => 'ProcessLogs', 'action' => 'index'], ['class' => 'link-deco']) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="icon-btn configuration-icon-btn">
                    <i class="glyphicon glyphicon-user"></i>
                    <div class="actions">                            
                        <?= $this->Html->link(__('Couriers Setup'), ['controller' => 'Couriers', 'action' => 'index'], ['class' => 'link-deco']) ?>
                    </div>
                </div>
            </div>
        </div>
        <br />
    </div>
</div>