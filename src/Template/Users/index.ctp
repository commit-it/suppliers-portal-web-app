<?php use Cake\Core\Configure; ?> 
<!-- delete dialog box start -->
<?= $this->element('deletedialogbox') ?>
<!-- delete dialog box end -->

<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('Nursery List') ?>
        </div>
        <div class="actions">
            <?php echo $this->Html->link(__('Add New Nursery'), ['action' => 'add'], array('class' => 'btn green', 'escape' => false)); ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <?php
            $sortDir = $this->Paginator->sortDir() === 'asc' ? 'fa-sort-asc sort-active-color' : 'fa-sort-desc sort-active-color';
            $sortKey = $this->Paginator->sortKey();
            ?>
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th class="width-15-per">
                            <?= $this->Paginator->sort('full_name') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Users.full_name') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="width-15-per">
                            <?= $this->Paginator->sort('email') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Users.email') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="width-15-per">
                            <?= $this->Paginator->sort('seller_gln', 'Seller GLN') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Users.seller_gln') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="width-15-per">
                            <?= $this->Paginator->sort('phone_number') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Users.phone_number') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="width-10-per">
                            <?= $this->Paginator->sort('access_status', 'Status') ?>
                            <i class="fa <?php
                            if ($sortKey == 'Users.access_status') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="width-15-per">Last Myob Connection</th>
                        <th class="actions width-15-per"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user): ?>
                        <tr>
                            <td><?= h($user->full_name) ?></td>
                            <td><?= h($user->email) ?></td>
                            <td><?= h($user->seller_gln) ?></td>
                            <td><?= h($user->phone_number) ?></td>
                            <td><?= \Cake\Core\Configure::read('access_status.display.' . $user->access_status) ?></td>
                            <td>
                                <?php
                                $processLogsDate = '';
                                if (isset($user->process_logs) && !empty($user->process_logs)) {
                                    $user->process_logs = $this->Common->multisortArray($user->process_logs, array(), 'created', 'desc');
                                    $processLogsDate = isset($user->process_logs[0]['created']) ? date(Configure::read('date-format.default'), strtotime($user->process_logs[0]['created'])) : '';
                                }
                                echo !empty($processLogsDate) ? $processLogsDate :'-------';
                                ?>
                            </td>
                            <td class="actions">
                                <?= $this->Html->link(__(''), ['action' => 'view', $user->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?> &nbsp;
                                <?= $this->Html->link(__(''), ['action' => 'edit', $user->id], ['class' => 'glyphicon glyphicon-edit link-deco', 'title' => 'Edit']) ?> <br />
                                <?= $this->Html->link('<b>' . __('Login as this Nursery') . '</b>', ['action' => 'login_as_nursery', $user->id], ['class' => 'link-deco', 'escape' => FALSE, 'target' => '_blank']) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php if (iterator_count($users) < 1) { ?>
                        <tr>
                            <td colspan="7" class="align-center">No records found</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <?php if (iterator_count($users) > 0) { ?>
            <div class="row">
                <div class="col-md-2 col-sm-12">
                    Showing <?= $this->Paginator->counter() ?>
                </div>
                <div class="col-md-10 col-sm-12">
                    <ul class="pagination float-right-important">
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                </div>
            </div>
        <?php } ?>            
    </div>
</div>
<script>

    $('#btn-ok').click(function() {
        window.location.href = '/users/delete/' + $('#delete_item_id').val();
    });

</script>
