<?php $t = 1; ?>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Update Courier Details') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <?= $this->Form->create($user, ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'onsubmit' => 'showLoading()']); ?>
        <!-- Start invoice information -->
        <div class="form-body">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Courier Name') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('courier_name', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Courier Logo') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <?php
                    /* If file upload alredy then use fileinput-exists class otherwise use fileinput-new class */
                    $fileinputClass = 'fileinput-new';
                    if ($user->courier_logo) {
                        $fileinputClass = 'fileinput-exists';
                    } 
                    ?>
                    <?= $this->Form->hidden('courier_logo') ?> 
                    <div class="fileinput <?= $fileinputClass ?>" data-provides="fileinput">
                        <div class="fileinput-new thumbnail courier-logo-thumb">
                            <?php echo $this->Html->image('/img/no_image.png', array('alt' => '')); ?>
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail">
                            <?php if ($user->courier_logo) { echo $this->Html->image($courierLogoImgPath, array('alt' => '')); }?>
                        </div>
                        <div>
                            <span class="btn default btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" accept="image/*" name="courier_logo_new" tabindex="<?= $t++ ?>" <?php if (!$user->courier_logo){ ?> required="required" <?php }?>>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Courier Address') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('courier_address', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Courier Telephone') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('courier_telephone', array('type' => 'number', 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Courier Email') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('courier_email', array('type' => 'email', 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-10 col-md-2">
                    <?= $this->Html->link(__('Cancel'), ['controller' => 'Users', 'action' => 'configuration'], ['class' => 'btn default']) ?>
                    <?= $this->Form->button(__('Submit'), array('class' => 'btn blue')) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
