<?php $t = 1; ?>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Update Business Profile') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <?= $this->Form->create($user, ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'onsubmit' => 'showLoading()']); ?>
        <!-- Start invoice information -->
        <div class="form-body">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Nursery Name') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('seller_business_name', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Business Logo') ?><span class="required"> * </span></label>
                <div class="col-md-9">
                    <?php
                    /* If file upload alredy then use fileinput-exists class otherwise use fileinput-new class */
                    $fileinputClass = 'fileinput-new';
                    if ($user->seller_business_logo) {
                        $fileinputClass = 'fileinput-exists';
                    } 
                    ?>
                    <?= $this->Form->hidden('seller_business_logo') ?> 
                    <div class="courier-logo-thumb fileinput <?= $fileinputClass ?>" data-provides="fileinput">
                        <div class="fileinput-new thumbnail">
                            <?php echo $this->Html->image('/img/no_image.png', array('alt' => '')); ?>
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail">
                            <?php if ($user->seller_business_logo) { echo $this->Html->image($sellerBusinessLogoImg, array('alt' => 'logo')); }?>
                        </div>
                        <div>
                            <span class="btn default btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" accept="image/*" name="seller_business_logo_new" tabindex="<?= $t++ ?>" />
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('GLN') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('seller_gln', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Nursery Address') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('seller_business_address', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Country') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->input('country_iso_code', array('options' => Cake\Core\Configure::read('country_iso_code'), 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control','empty'=>'Select')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Language Iso Code') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <label class="large"><?= \Cake\Core\Configure::read('language_iso_code') ?></label>
                    <?= $this->Form->hidden('language_iso_code', array('value' => \Cake\Core\Configure::read('language_iso_code'), 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Postal Code') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('postal_code', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('State') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->input('state', array('options' => Cake\Core\Configure::read('state'),'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control','empty'=>'Select')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('ABN') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('company_registration_number', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-10 col-md-2">
                    <?= $this->Html->link(__('Cancel'), ['controller' => 'Users', 'action' => 'business_profile'], ['tabindex' => $t++, 'class' => 'btn default']) ?>
                    <?= $this->Form->button(__('Submit'), array('tabindex' => $t++, 'class' => 'btn blue')) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
