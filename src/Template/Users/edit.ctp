<?php
use Cake\Core\Configure;
$t = 1;
//echo '<PrE>';print_r(Configure::read('hours'));exit;?>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Update Nursery') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <?= $this->Form->create($user, ['class' => 'form-horizontal', 'enctype' => 'multipart/form-data', 'onsubmit' => 'showLoading()']); ?>
        <!-- Start invoice information -->
        <div class="form-body">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Name') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('full_name', array('autofocus' => 'On', 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Phone Number') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('phone_number', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Address') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('address', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('City') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('city', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label"><?= __('Country') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon">
                        <?= $this->Form->text('country', array('tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <?php if ($user->payment_status == 'trial') { ?>
<!--                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label"><?= __('Trial End Date') ?><span class="required"> * </span></label>
                    <div class="col-md-6">
                        <div class="input-icon">
                            <?= $this->Form->hidden('old_trial_end_date', array('value' => isset($user->trial_end_date) ? date(\Cake\Core\Configure::read('date-format.db-date'), strtotime($user->trial_end_date)) : '', 'data-date-format' => 'yyyy-mm-dd', 'label' => false, 'div' => false)) ?>
                            <?= $this->Form->text('trial_end_date', array('id' => 'trial_end_date', 'value' => isset($user->trial_end_date) ? date(\Cake\Core\Configure::read('date-format.db-date'), strtotime($user->trial_end_date)) : '', 'data-date-format' => 'yyyy-mm-dd', 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control date-picker', 'onkeydown' => 'return false')) ?>
                            <div class="form-control-focus">
                            </div>
                        </div>
                    </div>
                </div>-->
            <?php } ?>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label control-label-top"><?= __('Status') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon radio-input-icon">
                        <?= $this->Form->text('access_status', array('type' => 'radio', 'options' => \Cake\Core\Configure::read('access_status.display'), 'tabindex' => $t++, 'label' => true, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label control-label-top"><?= __('Invoice Generate By') ?><span class="required"> * </span></label>
                <div class="col-md-6">
                    <div class="input-icon radio-input-icon">
                        <?= $this->Form->text('invoice_generate_by', array('type' => 'radio', 'options' => \Cake\Core\Configure::read('invoice_generate_by'), 'tabindex' => $t++, 'label' => true, 'div' => false, 'class' => 'form-control')) ?>
                        <div class="form-control-focus">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label"><?= __('Invpoice send to Bunnings Reminder Duraction') ?><span class="required"> * </span></label>
            <div class="col-md-6">
                <div class="input-icon">
                    <?php echo $this->Form->select('reminder_shedule', Configure::read('hours'), ['default' => 1, 'tabindex' => $t++, 'label' => false, 'div' => false, 'class' => 'form-control']) ?>
                    <div class="form-control-focus">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-10 col-md-2">
                    <?= $this->Html->link(__('Cancel'), ['controller' => 'Users', 'action' => 'profile'], ['tabindex' => $t++, 'class' => 'btn default']) ?>
                    <?= $this->Form->button(__('Submit'), array('tabindex' => $t++, 'class' => 'btn blue')) ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
