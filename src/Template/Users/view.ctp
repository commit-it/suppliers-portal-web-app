<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Nursery') ?></span>
        </div>
    </div>
    <div class="portlet-body form">       
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Full Name') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($user->full_name) ?></label>
            </div>
        </div>        
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Email') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($user->email) ?></label>
            </div>
        </div>   
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Phone Number') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($user->phone_number) ?></label>
            </div>
        </div> 
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Address') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($user->address) ?></label>
            </div>
        </div>   
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('City') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($user->city) ?></label>
            </div>
        </div>   
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Country') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($user->country) ?></label>
            </div>
        </div>
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Trial End Date') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= isset($user->trial_end_date) ? date(\Cake\Core\Configure::read('date-format.d-M-Y'), strtotime($user->trial_end_date)) : '' ?></label>
            </div>
        </div>
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Status') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= \Cake\Core\Configure::read('access_status.display.' . $user->access_status) ?></label>
            </div>
        </div>
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Invoice Generate By') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= \Cake\Core\Configure::read('invoice_generate_by.' . $user->invoice_generate_by) ?></label>
            </div>
        </div>
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Invpoice send to Bunnings Reminder Duraction') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($user->reminder_shedule) ?> <?php echo __('Hour'); ?></label>
            </div>
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-11 col-md-1">
                    <?= $this->Html->link(__('Back'), ['controller' => 'Users','action' => 'index'], ['class' => 'btn default']) ?>
                </div>
            </div>
        </div>
    </div>
</div>




