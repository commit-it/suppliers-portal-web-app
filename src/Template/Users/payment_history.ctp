<?php

use Cake\Core\Configure; ?>

<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('Payment History') ?>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th class="width-20-per"><?= __('Nursery Name') ?></th>
                        <th class="width-17-per"><?= __('Package') ?></th>
                        <th class="width-18-per"><?= __('Amount') ?></th>
                        <th class="width-20-per"><?= __('Payment Date') ?></th>
                        <th class="width-15-per"><?= __('Status') ?></th>
                        <th class="width-10-per"><?= __('Action') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user): ?>
                        <?php
                        $paymentHistories = isset($user->payments[0]->payment_histories) ? end($user->payments[0]->payment_histories) : '';
                        $paymentDate = isset($paymentHistories->payment_date) ? (array) $paymentHistories->payment_date : '';
                        ?>
                        <tr>
                            <td><?= h($user->full_name) ?></td>
                            <td><?= isset($paymentHistories['plan_id']) && !empty($paymentHistories['plan_id']) ? Configure::read('Stripe.packages_names.' . $paymentHistories['plan_id']) : '------'; ?></td>
                            <td><?= isset($paymentHistories['amount']) && $paymentHistories['amount'] > 0 ? $this->Common->convertCentToPrise($paymentHistories['amount'], 2, '$') : 0; ?></td>
                            <td><?= isset($paymentDate['date']) && strtotime($paymentDate['date']) > 0 ? date(Configure::read('date-format.default'), strtotime($paymentDate['date'])) : '------'; ?></td>
                            <td><?= isset($paymentHistories->status) && !empty($paymentHistories->status) ? ucfirst($paymentHistories->status) : '------'; ?></td>
                            <td class="actions action-anch">
                                <?= $this->Html->link(__(''), ['action' => 'view_payment_history', $user->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View Payment History']) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php if (iterator_count($users) < 1) { ?>
                        <tr>
                            <td colspan="6" class="align-center">No records found</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <?php if (iterator_count($users) > 0) { ?>
            <div class="row">
                <div class="col-md-2 col-sm-12">
                    Showing <?= $this->Paginator->counter() ?>
                </div>
                <div class="col-md-10 col-sm-12">
                    <ul class="pagination float-right-important">
                        <?= $this->Paginator->prev('< ' . __('previous')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('next') . ' >') ?>
                    </ul>
                </div>
            </div>
        <?php } ?>
    </div>
</div>