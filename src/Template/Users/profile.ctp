
<!-- generate delivery docket info box start -->
<div class="modal fade" id="change_password" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <?= $this->Form->create('users', array('id' => 'change_password_form','url' => ['controller' => 'Users', 'action' => 'profile'])); ?>
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-label="Close" style="margin-top:3px"><span aria-hidden="true">&times;</span></button>
                <div class="bootbox-body">
                    <div id="change_password_error_msg" class="alert alert-danger display-hide"></div>
                    <div class="row">
                        <label class="col-md-4 control-label"><?= __('Old Password') ?><span class="required"> * </span></label>
                        <div class="col-md-7 col-md-pop-2">
                            <div class="input-icon">
                                <?= $this->Form->input('old_password', array('type' => 'password', 'tabindex' => '1', 'autofocus' => 'On', 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)) ?>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <label class="col-md-4 control-label"><?= __('New Password') ?><span class="required"> * </span></label>
                        <div class="col-md-7 col-md-pop-2">
                            <div class="input-icon">
                                <?= $this->Form->input('new_password', array('type' => 'password', 'tabindex' => '2', 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)) ?>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <label class="col-md-4 control-label"><?= __('Confirm Password') ?><span class="required"> * </span></label>
                        <div class="col-md-7 col-md-pop-2">
                            <div class="input-icon">
                                <?= $this->Form->input('confirm_password', array('type' => 'password', 'tabindex' => '3', 'label' => false, 'div' => false, 'class' => 'form-control', 'required' => true)) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-cancel" class="btn default" data-dismiss="modal" tabindex="5">Cancel</button>
                <button type="button" id="change_password" class="btn btn-primary" onclick="changePassword()" tabindex="4">Submit</button>
            </div>
        </div>
        <?= $this->Form->end() ?>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- generate delivery docket info box end -->

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-user font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('Profile') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body row">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Full Name') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($user->full_name) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Email') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($user->email) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Phone Number') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($user->phone_number) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Address') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($user->address) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('City') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($user->city) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Country') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($user->country) ?></label>
                </div>
            </div>
             <?php if ($user->payment_status == 'trial') { ?>
<!--            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Trial End Date') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= isset($user->trial_end_date) ? date(\Cake\Core\Configure::read('date-format.d-M-Y'), strtotime($user->trial_end_date)) : '' ?></label>
                </div>
            </div>-->
             <?php } ?>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Status') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= \Cake\Core\Configure::read('access_status.display.' . $user->access_status) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Invoice Generate By') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= \Cake\Core\Configure::read('invoice_generate_by.' . $user->invoice_generate_by) ?></label>
                </div>
            </div>
        <div class="form-group form-md-line-input">
            <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Invoice send to Bunnings Reminder Duration') ?>:</label>
            <div class="col-md-9">
                <label class="large"><?= h($user->reminder_shedule) ?></label>
            </div>
        </div>

        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-8 col-md-4">
                    <?= $this->Html->link(__('Back'), ['controller' => 'Dashboard', 'action' => 'index'], ['class' => 'btn default']) ?>
                    <?= $this->Html->link(__('Change Password'), 'javascript:void(0)', array('class' => 'btn purple', 'escape' => false, 'data-toggle' => 'modal', 'data-dismiss' => 'modal', 'data-target' => '#change_password')); ?>
                    <?= $this->Html->link(__('Update'), ['action' => 'edit', $user->id, 'profile'], array('class' => 'btn blue', 'escape' => false)); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('user.js?17082015') ?>