
<!-- BEGIN LOGIN FORM -->
<?php echo $this->Form->create($userForm, ['class' => 'login-form', 'onsubmit' => 'showLoading()']); ?>
<h3 class="form-title">Sign In</h3>
<div class="alert alert-danger display-hide">
    <button class="close" data-close="alert"></button>
    <span>Enter any username and password. </span>
</div>

<div class="form-group">
    <?php echo $this->form->input('email', ['value' => isset($cookieHelper['email']) ? $cookieHelper['email'] : '', 'label' => false, 'class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Email']); ?>
</div>
<div class="form-group">
    <?php echo $this->form->input('password', ['value' => isset($cookieHelper['password']) ? $cookieHelper['password'] : '', 'label' => false, 'class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Password']); ?>
</div>

<div class="form-actions">
    <label class="rememberme check"><?= $this->Form->checkbox('remember_me', array('type' => 'checkbox', "id" => "id_remember_me", 'name' => 'remember_me', 'label' => false, 'checked' => isset($cookieHelper['remember_me']) ? true : false)); ?> Remember</label>
    <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
</div>
<div class="form-actions">
    <?php echo $this->Form->button('Sign In', ['type' => 'submit', 'class' => 'btn btn-success uppercase']); ?>
</div>

<?php echo $this->Form->end(); ?>
<!-- END LOGIN FORM -->

<!-- BEGIN FORGOT PASSWORD FORM -->
<?php echo $this->Form->create('forget_password', ['action' => 'forget_password', 'class' => 'forget-form']); ?>
    <h3>Forget Password ?</h3>
    <p>
        Enter your e-mail address below to reset your password.
    </p>
    <div class="form-group">
        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" required="required" />
    </div>
    <div class="form-actions">
        <button type="button" id="back-btn" class="btn btn-default">Back</button>
        <button type="submit" id="forget-password-form-submit" class="btn btn-success uppercase pull-right">Submit</button>
    </div>
<?php echo $this->Form->end(); ?>
<!-- END FORGOT PASSWORD FORM -->
<span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=DgO0LNs6Jo5P1RbC4IFZzfQnYrnlpJq8BUcYYcH4TrzmJSihLRNRa4qNCnmI"></script></span>
