<?php

use Cake\Core\Configure;
use Cake\Network\Session;

$this->session = new Session();

/* Set variables */
$accessStatus = isset($paymentDetails['access_status']) ? $paymentDetails['access_status'] : '';
$paymentStatus = isset($paymentDetails['payment_status']) ? $paymentDetails['payment_status'] : '';

$userTrialEndDate = isset($paymentDetails['trial_end_date']) ? (array) $paymentDetails['trial_end_date'] : '';
$expiredDate = isset($userTrialEndDate['date']) && (strtotime($userTrialEndDate['date']) > 0) ? date(Configure::read('date-format.default'), strtotime($userTrialEndDate['date'])) : '------';

$userTrialStartDate = isset($paymentDetails['created']) ? (array) $paymentDetails['created'] : '';
$trialStartDate = isset($userTrialStartDate['date']) && (strtotime($userTrialStartDate['date']) > 0) ? date(Configure::read('date-format.default'), strtotime($userTrialStartDate['date'])) : '------';

$userSubscriptionCanceled = isset($paymentDetails['subscription_cancelled']) ? (array) $paymentDetails['subscription_cancelled'] : '';
$subscriptionCanceled = isset($userSubscriptionCanceled['date']) && (strtotime($userSubscriptionCanceled['date']) > 0) ? date(Configure::read('date-format.default'), strtotime($userSubscriptionCanceled['date'])) : '------';
?>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('View Subscription Details') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <?php if ($accessStatus == Configure::read('access_status.main.inactive') && $paymentStatus == Configure::read('payment_status.main.trial')) { ?>
            <div class="alert alert-danger"> 
                Your trial period is expired on <?php echo $expiredDate; ?>
            </div>
            <div class="form-body row">
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Trial Start Date') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= $trialStartDate ?></label>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Trial End Date') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= $expiredDate ?></label>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Status') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= h(ucfirst($this->session->read('Auth.User.payment_status'))) ?></label>
                    </div>
                </div>           
            </div>            
        <?php } else { ?>            
            <div class="form-body row">
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Trial Start Date') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= $trialStartDate ?></label>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Trial End Date') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= $expiredDate ?></label>
                    </div>
                </div>
                <div class="form-group form-md-line-input">
                    <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Subscription Cancelled') ?>:</label>
                    <div class="col-md-9">
                        <label class="large"><?= $subscriptionCanceled ?></label>
                    </div>
                </div>           
            </div>
            <div class="portlet box green-haze">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-book"></i><?= __('Payment History') ?>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="sample_2">
                            <thead>
                                <tr class="action-anch thead-col">
                                    <th>
                                        <?= __('Plan') ?>
                                    </th>
                                    <th>
                                        <?= __('Email') ?>
                                    </th>
                                    <th>
                                        <?= __('Payment Interval') ?>
                                    </th>
                                    <th>
                                        <?= __('Interval Start Date') ?>
                                    </th>
                                    <th>
                                        <?= __('Interval End Date') ?>
                                    </th>
                                    <th>
                                        <?= __('Amount') ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($paymentDetails['payments'][0]['payment_histories']) && !empty($paymentDetails['payments'][0]['payment_histories'])) {
                                    foreach ($paymentDetails['payments'][0]['payment_histories'] as $paymentDetail) {
                                        ?>
                                        <?php
                                        $paymentIntervalStartDate = isset($paymentDetail['interval_start_date']) ? (array) $paymentDetail['interval_start_date'] : '';
                                        $intervalStartDate = isset($paymentIntervalStartDate['date']) && (strtotime($paymentIntervalStartDate['date']) > 0) ? date(Configure::read('date-format.default'), strtotime($paymentIntervalStartDate['date'])) : '------';
                                        $paymentIntervalEndDate = isset($paymentDetail['interval_end_date']) ? (array) $paymentDetail['interval_end_date'] : '';
                                        $intervalEndDate = isset($paymentIntervalEndDate['date']) && strtotime($paymentIntervalEndDate['date']) > 0 ? date(Configure::read('date-format.default'), strtotime($paymentIntervalEndDate['date'])) : '------';
                                        ?>
                                        <tr>
                                            <td><?= isset($paymentDetail['plan_id']) && !empty($paymentDetail['plan_id']) ? Configure::read('Stripe.packages_names.' . $paymentDetail['plan_id']) : '------'; ?></td>
                                            <td><?= isset($paymentDetail['email']) && !empty($paymentDetail['email']) ? $paymentDetail['email'] : '------'; ?></td>
                                            <td><?= isset($paymentDetail['payment_interval']) && !empty($paymentDetail['payment_interval']) ? ucfirst($paymentDetail['payment_interval']) : '------'; ?></td>
                                            <td><?= $intervalStartDate ?></td>
                                            <td><?= $intervalEndDate ?></td>
                                            <td><?= isset($paymentDetail['amount']) && $paymentDetail['amount'] > 0 ? $this->Common->convertCentToPrise($paymentDetail['amount'], 2, '$') : 0; ?></td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr><td colspan="6"> <center><?= __('Records not found!!!') ?></center></td></tr>
                            <?php }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>