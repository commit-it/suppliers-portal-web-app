<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="/dashboard">
                <?php echo $this->Html->image('/img/logo.png', array('alt' => 'Supplier Portal','class' => "logo-default")); ?>
            </a>
        </div>
        <!-- END LOGO -->
        
        <!-- BEGIN HORIZANTAL MENU -->
        <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
        <!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) sidebar menu below. So the horizontal menu has 2 seperate versions -->
        <?php $selectedTagSpan = '<span class="selected"></selected>'; ?>
        <div class="hor-menu hor-menu-light hidden-sm hidden-xs">
            <ul class="nav navbar-nav">
                <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
                <li class="classic-menu-dropdown  <?php if ($this->common->checkActiveAction(array('Users'), array('index', 'view', 'edit', 'add', 'profile'))) { ?> active <?php } ?>">
                    <?php if ($this->common->checkActiveAction(array('Users'), array('index', 'view', 'edit', 'add', 'profile'))) { $anchorTitle = __('Nursery Management') . $selectedTagSpan; } else { $anchorTitle = __('Nursery Management'); } ?>
                    <?= $this->Html->link($anchorTitle, ['controller' => 'Users', 'action' => 'index'], ['escape' => FALSE]) ?>
                </li>
                <li class="classic-menu-dropdown  <?php if ($this->common->checkActiveAction(array('Statistics'), array('index'))) { ?> active <?php } ?>">
                    <?php if ($this->common->checkActiveAction(array('Statistics'), array('index'))) { $anchorTitle = __('Statistics') . $selectedTagSpan; } else { $anchorTitle = __('Statistics'); } ?>
                    <?= $this->Html->link($anchorTitle, ['controller' => 'Statistics', 'action' => 'index'], ['escape' => FALSE]) ?>
                </li>
                <li class="classic-menu-dropdown <?php if ($this->common->checkActiveAction(array('Users'), array('payment_history','view_payment_history'))) { ?> active <?php } ?>">
                    <?php if ($this->common->checkActiveAction(array('Users'), array('payment_history'))) { $anchorTitle = __('Payment History') . $selectedTagSpan; } else { $anchorTitle = __('Payment History'); } ?>
                    <?= $this->Html->link($anchorTitle, ['controller' => 'Users', 'action' => 'payment_history'], ['escape' => FALSE]) ?>
                </li>
            </ul>
        </div>
        <!-- END HORIZANTAL MENU -->
        
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"></a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="glyphicon glyphicon-user"></i>
                        <span class="username username-hide-on-mobile"><?= isset($authUser['full_name']) ? $authUser['full_name'] : 'User Name'; ?></span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <?= $this->Html->link('<i class="icon-user"></i>' . __('My Profile'), ['controller' => 'Users', 'action' => 'profile'], ['escape' => FALSE]) ?>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <?= $this->Html->link('<i class="icon-key"></i>' . __('Log Out'), ['controller' => 'Users', 'action' => 'logout'], ['escape' => FALSE]) ?>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
                
                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-quick-sidebar-toggler">
                    <?= $this->Html->link('<i class="icon-logout"></i>' . __(''), ['controller' => 'Users', 'action' => 'logout'], ['class' => 'dropdown-toggle', 'escape' => FALSE]) ?>
                </li>
                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix"></div>