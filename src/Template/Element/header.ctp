<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="/dashboard">
                <?php echo $this->Html->image('/img/logo.png', array('alt' => 'Supplier Portal','class' => "logo-default")); ?>
            </a>
        </div>
        <!-- END LOGO -->
        
        <!-- BEGIN HORIZANTAL MENU -->
        <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
        <!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) sidebar menu below. So the horizontal menu has 2 seperate versions -->
        <?php $selectedTagSpan = '<span class="selected"></selected>'; ?>
        <div class="hor-menu hor-menu-light hidden-sm hidden-xs">
            <ul class="nav navbar-nav">
                <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
                <li class="classic-menu-dropdown <?php if ($this->common->checkActiveAction(array('Dashboard'), array('index', 'package'))) { ?> active <?php } ?>">
                    <?php if ($this->common->checkActiveAction(array('Dashboard'), array('index', 'package'))) { $anchorTitle = __('Dashboard') . $selectedTagSpan; } else { $anchorTitle = __('Dashboard'); } ?>
                    <?= $this->Html->link($anchorTitle, ['controller' => 'Dashboard', 'action' => 'index'], ['escape' => FALSE]) ?>
                </li>
                <li class="classic-menu-dropdown <?php if ($this->common->checkActiveAction(array('ScanBooks'), array('index', 'view', 'scan_book'))) { ?> active <?php } ?>">
                    <?php if ($this->common->checkActiveAction(array('ScanBooks'), array('index', 'view', 'scan_book'))) { $anchorTitle = __('Scan Book') . $selectedTagSpan; } else { $anchorTitle = __('Scan Book'); } ?>
                    <?= $this->Html->link($anchorTitle, ['controller' => 'ScanBooks', 'action' => 'index'], ['escape' => FALSE]) ?>
                </li>
                <li class="classic-menu-dropdown <?php if ($this->common->checkActiveAction(array('PurchaseOrders','PurchaseOrderItems'), array('index', 'sent_purchase_orders', 'view'))) { ?> active <?php } ?>">
                    <?php 
                        $poOrderCnt = (isset($newPurchaseOrders) && $newPurchaseOrders > 0) ? ' <span class="badge badge-danger active-po-record-cnt">' . $newPurchaseOrders . '</span>' : ''; 
                    ?>
                    <?php if ($this->common->checkActiveAction(array('PurchaseOrders','PurchaseOrderItems'), array('index', 'sent_purchase_orders', 'view'))) { $anchorTitle = __('Orders' . $poOrderCnt) . $selectedTagSpan; } else { $anchorTitle = __('Orders' . $poOrderCnt); } ?>
                    <?= $this->Html->link($anchorTitle, ['controller' => 'PurchaseOrders', 'action' => 'index'], ['escape' => FALSE]) ?>
                </li>
                <li class="mega-menu-dropdown <?php if ($this->common->checkActiveAction(array('Tests', 'Invoices', 'DeliveryDockets', 'BookingSheets', 'FileOverviewes'), array('add', 'index', 'generated_invoices', 'view', 'edit', 'generate_booking_sheet', 'add_booking_sheet', 'create_invoice', 'update_regenerate_invoice', 'read_file'))) { ?> active <?php } ?>">
                    <?php if ($this->common->checkActiveAction(array('Tests', 'Invoices', 'DeliveryDockets', 'BookingSheets', 'FileOverviewes'), array('add', 'index', 'generated_invoices', 'view', 'edit', 'generate_booking_sheet', 'add_booking_sheet', 'create_invoice', 'update_regenerate_invoice', 'read_file'))) { $anchorTitle = __('Invoices') . $selectedTagSpan; } else { $anchorTitle = __('Invoices Docs'); } ?>
                    <a data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
                        <?= $anchorTitle ?> <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li <?php if ($this->common->checkActiveAction(array('Invoices'), array('index'))) { ?> class="active" <?php } ?>>
                            <?= $this->Html->link('<i class="fa fa-angle-right"></i>' . __('Invoices'), ['controller' => 'Invoices', 'action' => 'index'], ['escape' => FALSE]) ?>
                        </li>
                        <li <?php if ($this->common->checkActiveAction(array('Invoices'), array('generated_invoices'))) { ?> class="active" <?php } ?>>
                            <?= $this->Html->link('<i class="fa fa-angle-right"></i>' . __('Invoices Sent to Bunnings'), ['controller' => 'Invoices', 'action' => 'generated_invoices'], ['escape' => FALSE]) ?>
                        </li>
                        <li <?php if ($this->common->checkActiveAction(array('DeliveryDockets'), array('index'))) { ?> class="active" <?php } ?>>
                            <?= $this->Html->link('<i class="fa fa-angle-right"></i>' . __('Delivery Dockets'), ['controller' => 'DeliveryDockets', 'action' => 'index'], ['escape' => FALSE]) ?>
                        </li>
                        <li <?php if ($this->common->checkActiveAction(array('DeliveryDockets'), array('generate_booking_sheet'))) { ?> class="active" <?php } ?>>
                            <?= $this->Html->link('<i class="fa fa-angle-right"></i>' . __('Booking Sheet'), ['controller' => 'DeliveryDockets', 'action' => 'generate_booking_sheet'], ['escape' => FALSE]) ?>
                        </li>
                    </ul>
                </li>
                <li class="classic-menu-dropdown <?php if ($this->common->checkActiveAction(array('Users', 'BunningStores', 'MasterItems', 'ProcessLogs', 'Couriers'), array('index', 'view', 'edit', 'add', 'import_items', 'configuration', 'manage_courier_detail', 'business_profile', 'edit_business_profile', 'import_bunning_stores', 'profile', 'quick_book_profile'))) { ?> active <?php } ?>">
                    <?php if ($this->common->checkActiveAction(array('Users', 'BunningStores', 'MasterItems', 'ProcessLogs', 'Couriers'), array('index', 'view', 'edit', 'add', 'import_items', 'configuration', 'manage_courier_detail', 'business_profile', 'edit_business_profile', 'import_bunning_stores', 'profile', 'quick_book_profile'))) { $anchorTitle = __('Configuration') . $selectedTagSpan; } else { $anchorTitle = __('Configuration'); } ?>
                    <?= $this->Html->link($anchorTitle, ['controller' => 'Users', 'action' => 'configuration'], ['escape' => FALSE]) ?>
                </li>
            </ul>
        </div>
        <!-- END HORIZANTAL MENU -->
        
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"></a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="glyphicon glyphicon-user"></i>
                        <span class="username username-hide-on-mobile"><?= isset($authUser['full_name']) ? $authUser['full_name'] : 'User Name'; ?></span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <?= $this->Html->link('<i class="icon-user"></i>' . __('My Profile'), ['controller' => 'Users', 'action' => 'profile'], ['escape' => FALSE]) ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<i class="icon-user"></i>' . __('Business Profile'), ['controller' => 'Users', 'action' => 'business_profile'], ['escape' => FALSE]) ?>
                        </li>
                        <li>
                            <?= $this->Html->link('<i class="icon-user"></i>' . __('Quick Book Details'), ['controller' => 'Users', 'action' => 'quick_book_profile'], ['escape' => FALSE]) ?>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <?= $this->Html->link('<i class="icon-key"></i>' . __('Log Out'), ['controller' => 'Users', 'action' => 'logout'], ['escape' => FALSE]) ?>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
                
                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-quick-sidebar-toggler">
                    <?= $this->Html->link('<i class="icon-logout"></i>' . __(''), ['controller' => 'Users', 'action' => 'logout'], ['class' => 'dropdown-toggle', 'escape' => FALSE]) ?>
                </li>
                <!-- END QUICK SIDEBAR TOGGLER -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix"></div>