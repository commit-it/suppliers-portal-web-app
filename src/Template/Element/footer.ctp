<div class="page-footer">
    <div class="page-footer-inner bold">
        <?php echo date("Y"); ?> &COPY; Suppliers Portal
        &nbsp;&nbsp;<a href="mailto:support@suppliersportal.com.au">support@suppliersportal.com.au</a>
        &nbsp;|&nbsp;&nbsp;<a href="https://www.suppliersportal.com.au/support" target="_blanck">www.suppliersportal.com.au/support</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
