<!-- BEGIN HORIZONTAL RESPONSIVE MENU -->
<div class="page-sidebar navbar-collapse collapse">
    <ul class="page-sidebar-menu" data-slide-speed="200" data-auto-scroll="true">
        <!-- BEGIN HORIZANTAL MENU -->
        <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
        <!-- DOC: This is desktop version of the horizontal menu. The mobile version is defined(duplicated) sidebar menu below. So the horizontal menu has 2 seperate versions -->
        <?php $selectedTagSpan = '<span class="selected"></selected>'; ?>

        <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the horizontal opening on mouse hover -->
        <li class="<?php if ($this->common->checkActiveAction(array('Dashboard'), array('index', 'package'))) { ?> active <?php } ?>">
            <?php
            if ($this->common->checkActiveAction(array('Dashboard'), array('index', 'package'))) {
                $anchorTitle = __('Dashboard') . $selectedTagSpan;
            } else {
                $anchorTitle = __('Dashboard');
            }
            ?>
            <?= $this->Html->link($anchorTitle, ['controller' => 'Dashboard', 'action' => 'index'], ['escape' => FALSE]) ?>
        </li>
        <li class="<?php if ($this->common->checkActiveAction(array('ScanBooks'), array('index', 'view', 'scan_book'))) { ?> active <?php } ?>">
            <?php
            if ($this->common->checkActiveAction(array('ScanBooks'), array('index', 'view', 'scan_book'))) {
                $anchorTitle = __('Scan Book') . $selectedTagSpan;
            } else {
                $anchorTitle = __('Scan Book');
            }
            ?>
            <?= $this->Html->link($anchorTitle, ['controller' => 'ScanBooks', 'action' => 'index'], ['escape' => FALSE]) ?>
        </li>
        <li class="<?php if ($this->common->checkActiveAction(array('PurchaseOrders', 'PurchaseOrderItems'), array('index', 'sent_purchase_orders', 'view'))) { ?> active <?php } ?>">
            <?php
            $poOrderCnt = (isset($newPurchaseOrders) && $newPurchaseOrders > 0) ? ' <span class="badge badge-danger active-po-record-cnt">' . $newPurchaseOrders . '</span>' : '';
            ?>
            <?php
            if ($this->common->checkActiveAction(array('PurchaseOrders', 'PurchaseOrderItems'), array('index', 'sent_purchase_orders', 'view'))) {
                $anchorTitle = __('Orders' . $poOrderCnt) . $selectedTagSpan;
            } else {
                $anchorTitle = __('Orders' . $poOrderCnt);
            }
            ?>
            <?= $this->Html->link($anchorTitle, ['controller' => 'PurchaseOrders', 'action' => 'index'], ['escape' => FALSE]) ?>
        </li>
        <li class="<?php if ($this->common->checkActiveAction(array('Tests', 'Invoices', 'DeliveryDockets', 'BookingSheets', 'FileOverviewes'), array('add', 'index', 'generated_invoices', 'view', 'edit', 'generate_booking_sheet', 'add_booking_sheet', 'create_invoice', 'update_regenerate_invoice', 'read_file'))) { ?> active <?php } ?>">
            <?php
            if ($this->common->checkActiveAction(array('Tests', 'Invoices', 'DeliveryDockets', 'BookingSheets', 'FileOverviewes'), array('add', 'index', 'generated_invoices', 'view', 'edit', 'generate_booking_sheet', 'add_booking_sheet', 'create_invoice', 'update_regenerate_invoice', 'read_file'))) {
                $anchorTitle = __('Invoices') . $selectedTagSpan;
            } else {
                $anchorTitle = __('Invoices Docs');
            }
            ?>
            <a data-toggle="dropdown" href="javascript:;" class="dropdown-toggle">
                <?= $anchorTitle ?> <span class="selected"></span>
            </a>
            <ul class="sub-menu">
                <li <?php if ($this->common->checkActiveAction(array('Invoices'), array('index'))) { ?> class="active" <?php } ?>>
                    <?= $this->Html->link('<i class="fa fa-angle-right"></i>' . __('Invoices'), ['controller' => 'Invoices', 'action' => 'index'], ['escape' => FALSE]) ?>
                </li>
                <li <?php if ($this->common->checkActiveAction(array('Invoices'), array('generated_invoices'))) { ?> class="active" <?php } ?>>
                    <?= $this->Html->link('<i class="fa fa-angle-right"></i>' . __('Invoices Sent to Bunnings'), ['controller' => 'Invoices', 'action' => 'generated_invoices'], ['escape' => FALSE]) ?>
                </li>
                <li <?php if ($this->common->checkActiveAction(array('DeliveryDockets'), array('index'))) { ?> class="active" <?php } ?>>
                    <?= $this->Html->link('<i class="fa fa-angle-right"></i>' . __('Delivery Dockets'), ['controller' => 'DeliveryDockets', 'action' => 'index'], ['escape' => FALSE]) ?>
                </li>
                <li <?php if ($this->common->checkActiveAction(array('DeliveryDockets'), array('generate_booking_sheet'))) { ?> class="active" <?php } ?>>
                    <?= $this->Html->link('<i class="fa fa-angle-right"></i>' . __('Booking Sheet'), ['controller' => 'DeliveryDockets', 'action' => 'generate_booking_sheet'], ['escape' => FALSE]) ?>
                </li>
            </ul>
        </li>
        <li class="<?php if ($this->common->checkActiveAction(array('Users', 'BunningStores', 'MasterItems', 'ProcessLogs', 'Couriers'), array('index', 'view', 'edit', 'add', 'import_items', 'configuration', 'manage_courier_detail', 'business_profile', 'edit_business_profile', 'import_bunning_stores', 'profile', 'quick_book_profile'))) { ?> active <?php } ?>">
            <?php
            if ($this->common->checkActiveAction(array('Users', 'BunningStores', 'MasterItems', 'ProcessLogs', 'Couriers'), array('index', 'view', 'edit', 'add', 'import_items', 'configuration', 'manage_courier_detail', 'business_profile', 'edit_business_profile', 'import_bunning_stores', 'profile', 'quick_book_profile'))) {
                $anchorTitle = __('Configuration') . $selectedTagSpan;
            } else {
                $anchorTitle = __('Configuration');
            }
            ?>
            <?= $this->Html->link($anchorTitle, ['controller' => 'Users', 'action' => 'configuration'], ['escape' => FALSE]) ?>
        </li>
    </ul>
    <!-- END HORIZANTAL MENU -->           
</div>
<!-- END HORIZONTAL RESPONSIVE MENU -->