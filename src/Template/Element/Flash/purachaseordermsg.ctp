<?php if (isset($params['success']) && !empty($params['success'])) { ?>
<div class="alert alert-success"> 
    <?php
        foreach ($params['success'] as $param){ ?>
        <p><?= $param ?></p>
        <?php }
    ?>
</div>
<?php } ?>

<?php if (isset($params['error']) && !empty($params['error'])) { ?>
<div class="alert alert-danger">
    <?php
        foreach ($params['error'] as $param){ ?>
        <p><?= $param ?></p>
        <?php }
    ?>
</div>
<?php } ?>
