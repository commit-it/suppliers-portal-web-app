<?php

if (isset($params['success']) && !empty($params['success'])) { ?>
<div class="alert alert-success"> 
    <?php if (is_array($params['success'])) { 
        foreach ($params['success'] as $key => $param) { ?>
            <?php $msgWord = (count($params['success'][$key]) > 1) ? 'Messages' :  'Message'; ?>
            <p class="bold"><?= 'Below ' . $msgWord . ' for ' . $key . ' Purchase Order' ?></p>
            <?php foreach ($param as $msg) { ?>
                <p><?= $msg ?></p>
            <?php } ?>
            <p><hr /></p>
    <?php } ?>
    <?php } else { ?>
        <p><?= $params['success'] ?></p>
    <?php } ?>
</div>
<?php } ?>

<?php if (isset($params['error']) && !empty($params['error'])) { ?>
<div class="alert alert-danger">
    <?php if (is_array($params['error'])) {
        foreach ($params['error'] as $key => $param) { ?>
            <?php $msgWord = (count($params['error'][$key]) > 1) ? 'Messages' :  'Message'; ?>
            <p class="bold"><?= 'Below ' . $msgWord . ' for ' . $key . ' Purchase Order' ?></p>
            <?php foreach ($param as $msg) { ?>
                <p><?= $msg ?></p>
            <?php } ?>
            <p><hr /></p>
    <?php } ?> 
    <?php } else { ?>
        <p><?= $params['error'] ?></p>
    <?php } ?>
</div>
<?php } ?>
