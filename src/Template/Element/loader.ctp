<style>

.settingloder{
    display: block;
    text-align: center;
    visibility:visible;
    width: 100%;
    height: 100%;
    margin: auto;
    position: fixed;
    z-index: 100000 !important;
    padding:17% 35%;
    color: #d2322d;
}
body {
    opacity: 0.7;
}

</style>

<!-- Loader Start -->
<div class="settingloder showLoder">
    <div>
        <?php echo $this->Html->image('/img/loading.gif', array('alt' => 'Loading', 'width' => '70px', 'height' => '70px')); ?>
    </div>
    <div>
        <span>
            <h4>  Processing . . . Please Wait. </h4>
        </span>
    </div>
</div>
<!-- Loader Ends -->

<script>
    
    $(window).load(function () {
        $('.settingloder').hide();
        $('body').css('opacity', '1');
    });
    
    function showLoading() {
        $('.settingloder').show();
        $('body').css('opacity', '0.7');
        return true;
    }
    
    function hideLoading() {
        $('.settingloder').hide();
        $('body').css('opacity', '1');
    }
    
</script>