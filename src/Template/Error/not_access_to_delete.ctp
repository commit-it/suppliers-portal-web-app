<?php $this->layout = 'custom_error'; ?>
<div class="columns large-6">
    <p class="alert alert-danger">
        <strong>Error: </strong>
        You don't have access to delete this record!!!
    </p>
    <p>
        <?= $this->Html->link(__('Go to Home'), '/', ['class' => 'btn default']) ?>
    </p> 
</div>