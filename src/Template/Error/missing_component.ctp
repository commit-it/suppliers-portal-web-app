<?php $this->layout = 'custom_error'; ?>
<div class="row">
    <div class="col-md-12">
        <p class="alert alert-danger">
            <strong>Error: </strong>
            <?php $errorMsg = ($message) ? $message : 'Missing Component'; 
                echo $errorMsg;
            ?>
        </p>
        <p>
            <?= $this->Html->link(__('Go to Home'), '/', ['class' => 'btn default']) ?>
        </p> 
    </div>
</div>