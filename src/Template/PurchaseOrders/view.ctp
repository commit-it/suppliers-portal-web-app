<?php

use Cake\Core\Configure; ?>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-green-haze">
            <i class="icon-book-open font-green-haze"></i>
            <span class="caption-subject bold uppercase"><?= __('View Purchase Order') ?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <div class="form-body row">
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold aligh-right" for="form_control_1"><?= __('Purchase Order Number') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($purchaseOrder->purchase_order_number) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Bill To Name') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($purchaseOrder->bill_to_name) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Bill To Address') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($purchaseOrder->bill_to_address) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Currency') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($purchaseOrder->currency->name) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Purchase Order Status') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= h($purchaseOrder->purchase_order_status->status) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Bill To GLN') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $purchaseOrder->bill_to_gln ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Sub Total ($)') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $this->Number->format($purchaseOrder->total_order_amount, Configure::read('number-format-settings')) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Order Date') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= date(Configure::read('date-format.default'), strtotime($purchaseOrder->order_date)) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Deliver Date') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= date(Configure::read('date-format.default'), strtotime($purchaseOrder->deliver_date)) ?></label>
                </div>
            </div>
            <div class="form-group form-md-line-input">
                <label class="col-md-3 control-label bold  aligh-right" for="form_control_1"><?= __('Comments') ?>:</label>
                <div class="col-md-9">
                    <label class="large"><?= $this->Text->autoParagraph(h($purchaseOrder->comments)); ?></label>
                </div>
            </div>
        </div>

        <!-- Start Related Purchase Order Items --> 
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-book"></i><?= __('Related Purchase Order Items') ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <tr class="action-anch  thead-col">
                            <th><?= __('Order Line Number') ?></th>
                            <th><?= __('Gtin') ?></th>
                            <th><?= __('Requested Quantity') ?></th>
                            <th><?= __('Description') ?></th>
                            <th><?= __('Comment') ?></th>
                            <th><?= __('Net Price ($)') ?></th>
                            <th><?= __('Net Amount ($)') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <tbody> 
                            <?php if (iterator_count($purchaseOrderItems) > 0) { ?>
                                <?php foreach ($purchaseOrderItems as $purchaseOrderItem): ?>
                                    <tr>
                                        <td><?= h($purchaseOrderItem->order_line_number) ?></td>
                                        <td><?= h($purchaseOrderItem->gtin) ?></td>
                                        <td><?= h($purchaseOrderItem->requested_quantity) ?></td>
                                        <td><?= h($purchaseOrderItem->description) ?></td>
                                        <td><?= $this->Text->autoParagraph(h($purchaseOrderItem->comments)); ?></td>
                                        <td><?= $this->Number->format($purchaseOrderItem->net_price, Configure::read('number-format-settings')) ?></td>
                                        <td><?= $this->Number->format($purchaseOrderItem->net_amount, Configure::read('number-format-settings')) ?></td>
                                        <td>
                                            <?= $this->Html->link(__(''), ['controller' => 'PurchaseOrderItems', 'action' => 'view', $purchaseOrderItem->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php }else { ?>
                                <tr><td colspan="7"> <center><?= __('Records not found!!!') ?></center></td></tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End Related Purchase Order Items --> 

        <!-- Start Invoice --> 
        <div class="portlet box green-haze">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-book"></i><?= __('Invoice') ?>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="sample_2">
                        <tr class="action-anch  thead-col">
                            <th><?= __('Invoice Number') ?></th>
                            <th><?= __('Invoice Type') ?></th>
                            <th><?= __('Sub Total ($)') ?></th>
                            <th><?= __('GST (%)') ?></th>
                            <th><?= __('Sales Person') ?></th>
                            <th><?= __('Ship Via') ?></th>
                            <th><?= __('Created') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <tbody> 
                            <?php $purchaseOrderInvoiceObj = new ArrayIterator($purchaseOrder->invoices); ?>
                            <?php if (iterator_count($purchaseOrderInvoiceObj) > 0) { ?>
                                <?php foreach ($purchaseOrder->invoices as $invoice): ?>
                                    <tr>
                                        <td><?= h($invoice->invoice_number) ?></td>
                                        <td><?= h($invoice->invoice_type) ?></td>
                                        <td><?= $this->Number->format($invoice->sub_total, Configure::read('number-format-settings')) ?></td>
                                        <td><?= h($invoice->tax) ?></td>
                                        <td><?= h($invoice->sales_person) ?></td>
                                        <td><?= h($invoice->ship_via) ?></td>
                                        <td><?= date(Configure::read('date-format.default'), strtotime($invoice->created)) ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link(__(''), ['controller' => 'Invoices', 'action' => 'view', $invoice->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php }else { ?>
                                <tr><td colspan="8"> <center><?= __('Records not found!!!') ?></center></td></tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- End Related Purchase Order Items --> 

        <div class="form-actions">
            <div class="row">
                <div class="col-md-offset-11 col-md-1">
                    <?= $this->Html->link(__('Back'), ['controller' => 'PurchaseOrders', 'action' => 'index'], ['class' => 'btn default']) ?>
                </div>
            </div>
        </div>
    </div>
</div>
