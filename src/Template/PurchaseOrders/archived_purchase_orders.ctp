<?php use Cake\Core\Configure; ?>


<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('Canceled Purchase Order List') ?>
        </div>
        <div class="actions">
            <?php echo $this->Html->link(__('Purchase Orders'), ['controller' => 'PurchaseOrders', 'action' => 'index'], ['class' => 'btn green']); ?>
        </div>
    </div>
    <div class="portlet-body">
        <?php
        $sortDir = $this->Paginator->sortDir() === 'asc' ? 'fa-sort-asc sort-active-color' : 'fa-sort-desc sort-active-color';
        $sortKey = $this->Paginator->sortKey();
        ?>
        <div class="alert alert-danger display-hide" id="error-message">
            <span></span>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th>
                            <?= $this->Paginator->sort('purchase_order_number', 'Purchase Order No') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.purchase_order_number') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('bill_to_gln', 'Bill To GLN') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.bill_to_gln') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('bill_to_name') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.bill_to_name') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('bill_to_address') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.bill_to_address') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('order_date') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.order_date') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('deliver_date') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.deliver_date') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (iterator_count($purchaseOrders) > 0) { ?>
                        <?php foreach ($purchaseOrders as $purchaseOrder): ?>
                            <tr>
                                <td><?= $purchaseOrder->purchase_order_number ?></td>
                                <td><?= $purchaseOrder->bill_to_gln ?></td>
                                <td><?= h($purchaseOrder->bill_to_name) ?></td>
                                <td><?= h($purchaseOrder->bill_to_address) ?></td>
                                <td><?= date(Configure::read('date-format.default'), strtotime($purchaseOrder->order_date)) ?></td>
                                <td><?= date(Configure::read('date-format.default'), strtotime($purchaseOrder->deliver_date)) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__(''), ['action' => 'view', $purchaseOrder->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } else { ?>
                    <td colspan="7"> <center><?= __('Records not found!!!') ?></center></td>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <?php if (iterator_count($purchaseOrders) > 0) { ?>
            <div class="paginator">
                <ul class="pagination large-10 medium-10 columns">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p class="large-2 medium-2 columns"><?= $this->Paginator->counter() ?></p>
            </div>
        <?php } ?>
    </div>
</div>
<?= $this->Form->end() ?>

<?php echo $this->Html->script('purchase_orders.js') ?>


<script>

// Auto reload page after some time    
    setTimeout(function() {
        window.location.reload();
    }, <?php echo Configure::read('auto_refresh_page_time'); ?>);

</script>