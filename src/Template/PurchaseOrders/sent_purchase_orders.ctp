<?php

use Cake\Core\Configure; ?>

<?= $this->Form->create('PrintPurchaseOrders', array('target' => '_blank', 'id' => 'print_purchase_orders_form', 'name' => 'print_purchase_orders_form', 'action' => 'print_purchase_orders')); ?>
<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <?php
            if ($authUser['invoice_generate_by'] == Configure::read('invoice_generate_by.Quick-Books')) {
                $invoiceGenerateBy = Configure::read('invoice_generate_by.Quick-Books');
            } else {
                $invoiceGenerateBy = Configure::read('invoice_generate_by.MYOB');
            }
            ?>
            <i class="fa fa-book"></i><?= __('POs ready for ' . $invoiceGenerateBy) ?>
        </div>
        <div class="actions">
            <?php if (iterator_count($purchaseOrders) > 0) { ?>
                <?= $this->Form->button(__('Print Picking List'), ['type' => 'button', 'id' => 'print_purchase_orders', 'class' => 'btn green']) ?>
            <?php } else { ?>
                <?= $this->Form->button(__('Print Picking List'), ['type' => 'button', 'id' => 'print_purchase_orders', 'class' => 'btn green', 'disabled' => true]) ?>
            <?php } ?>
        </div>
    </div>
    <div class="portlet-body">
        <?php
        $sortDir = $this->Paginator->sortDir() === 'asc' ? 'fa-sort-asc sort-active-color' : 'fa-sort-desc sort-active-color';
        $sortKey = $this->Paginator->sortKey();
        ?>
        <div class="alert alert-danger display-hide" id="error-message">
            <span></span>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th class="th-5-wd align-center">
                            <?php if (iterator_count($purchaseOrders) > 0) { ?>
                                <span class="md-checkbox">
                                    <?= $this->Form->checkbox('select_deselect', ['id' => 'select_deselect_pourchase_ordes', 'div' => false, 'lable' => false, 'class' => 'md-check', 'hiddenField' => false]); ?>
                                    <label for="select_deselect_pourchase_ordes">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span>
                                    </label>
                                </span>
                            <?php } ?>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('purchase_order_number', 'Purchase Order No') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.purchase_order_number') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('bill_to_gln', 'Bill To GLN') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.bill_to_gln') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('bill_to_name') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.bill_to_name') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('bill_to_address') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.bill_to_address') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('order_date') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.order_date') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('deliver_date') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.deliver_date') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (iterator_count($purchaseOrders) > 0) { ?>
                        <?php foreach ($purchaseOrders as $purchaseOrder): ?>
                            <tr>
                                <td class="align-center">
                                    <span class="md-checkbox">
                                        <?= $this->Form->checkbox('purchase_order_id[]', ['id' => 'purchase-order-id-' . $purchaseOrder->id, 'value' => $purchaseOrder->id, 'div' => false, 'lable' => false, 'class' => 'md-check', 'hiddenField' => false]) ?>
                                        <label for= "purchase-order-id-<?= $purchaseOrder->id ?>">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                        </label>
                                    </span>
                                </td>
                                <td><?= $purchaseOrder->purchase_order_number ?></td>
                                <td><?= $purchaseOrder->bill_to_gln ?></td>
                                <td><?= h($purchaseOrder->bill_to_name) ?></td>
                                <td><?= h($purchaseOrder->bill_to_address) ?></td>
                                <td><?= date(Configure::read('date-format.default'), strtotime($purchaseOrder->order_date)) ?></td>
                                <td><?= date(Configure::read('date-format.default'), strtotime($purchaseOrder->deliver_date)) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__(''), ['action' => 'view', $purchaseOrder->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?>
                                    <!--
                                    <?= $this->Html->link(__(''), ['action' => 'edit', $purchaseOrder->id], ['class' => 'glyphicon glyphicon-edit link-deco', 'title' => 'Edit']) ?>
                                    <?= $this->Form->postLink(__(''), ['action' => 'delete', $purchaseOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $purchaseOrder->id), 'class' => 'glyphicon glyphicon-trash link-deco', 'title' => 'Delete']) ?>
                                    -->
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } else { ?>
                    <td colspan="8"> <center><?= __('Records not found!!!') ?></center></td>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <?php if (iterator_count($purchaseOrders) > 0) { ?>
            <div class="paginator">
                <ul class="pagination large-10 medium-10 columns">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p class="large-2 medium-2 columns"><?= $this->Paginator->counter() ?></p>
            </div>
        <?php } ?>
    </div>
</div>    
<?= $this->Form->end() ?>

<?php echo $this->Html->script('purchase_orders.js') ?>