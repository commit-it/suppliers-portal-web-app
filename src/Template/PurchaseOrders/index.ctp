<?php

use Cake\Core\Configure;
?>
<?= $this->Flash->render('purchase-order-flash') ?>
<?= $this->Flash->render('quick-book-flash') ?>

<!-- archive_purchase_order dialog box start -->
<div class="modal fade" id="archive_purchase_order_model" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="bootbox-close-button close archive-purchase-order-btn-cancel" data-dismiss="modal" aria-label="Close" style="margin-top:3px"><span aria-hidden="true">&times;</span></button>
                <br />
                <div class="bootbox-body"><?= __('Do you really want to Cancel selected POs? Please note this action cannot be reversed.') ?></div>
            </div>
            <div class="modal-footer">
                <button type="button" id="archive-purchase-order-btn-cancel" class="btn default archive-purchase-order-btn-cancel" data-dismiss="modal">No</button>
                <button type="button" id="archive_purchase_order_final" class="btn btn-primary">Yes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<a id="archive_purchase_order_popup" data-target="#archive_purchase_order_model" data-dismiss="modal" data-toggle="modal" class="display-none" href="javascript:void(0)"></a>
<!-- archive_purchase_order dialog box end -->

<!-- send_to_myob dialog box start -->
<div class="modal fade" id="send_to_myob_popup" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-label="Close" style="margin-top:3px"><span aria-hidden="true">&times;</span></button>
                <br />
                <div class="bootbox-body"><?= __('Please Close MYOB and open Suppliers Portal Sync Application.') ?></div>
            </div>
            <div class="modal-footer">
                <button type="button" id="send_to_myob_final" class="btn btn-primary">Ok</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<a id="open_send_to_myob_popup" data-target="#send_to_myob_popup" data-dismiss="modal" data-toggle="modal" class="display-none" href="javascript:void(0)"></a>
<!-- send_to_myob dialog box end -->

<!-- set Actual Delivery Date box start -->
<div class="modal fade" id="generate_delivery_docket_info_box" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <?= $this->Form->create('PurchaseOrder', array('action' => 'set_actual_delivery_date_of_purchase_order', 'onsubmit' => 'showLoading()')); ?>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-label="Close" style="margin-top:3px"><span aria-hidden="true">&times;</span></button>
                <h4 id="purchase_order_number_text">Set the Actual Delivery Date of that Purchase Order</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-danger hide" id="response-msg"></div>
                <input type="hidden" value="<?php echo @$this->request->params['paging']['PurchaseOrders']['page']; ?>"  name="page"/>
                <div class="bootbox-body">
                    <br />
                    <?= $this->Form->input('PurchaseOrder.id', array('type' => 'hidden', 'label' => false, 'div' => false)) ?>
                    <div class="row">
                        <label class="col-md-4 control-label"><?= __('Actual Delivery Date') ?><span class="required"> * </span></label>
                        <div class="col-md-7 col-md-pop-2">
                            <div class="input-icon">
                                <i class="fa fa-calendar"></i>
                                <?= $this->Form->input('PurchaseOrder.actual_delivery_date', array('type' => 'text', 'data-date-format' => 'yyyy-mm-dd', 'tabindex' => '1', 'autofocus' => 'On', 'label' => false, 'div' => false, 'class' => 'form-control date-picker set-date-picker-for-tomorrow', 'required' => true, 'onkeydown' => 'return false')) ?>
                            </div>
                        </div>
                    </div>
                    <br />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-cancel" class="btn default" data-dismiss="modal">Cancel</button>
                <button type="submit" id="update_actaul_delivery_date" class="btn btn-primary" disabled="disabled">Submit</button>
            </div>
        </div>
        <?= $this->Form->end() ?>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- set Actual Delivery Date box start -->

<?= $this->Form->create('PrintPurchaseOrders', array('target' => '_blank', 'id' => 'print_purchase_orders_form', 'name' => 'print_purchase_orders_form', 'action' => 'print_purchase_orders')); ?>
<div class="portlet box green-haze">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-book"></i><?= __('Purchase Order List') ?>
        </div>
        <div class="actions">
            <!-- button of open set actual delivery date window -->
            <button class="display-none" id="set_actual_delivery_date_window" data-target="#generate_delivery_docket_info_box" data-dismiss="modal" data-toggle="modal" type="button"></button>
            <?php
            if ($authUser['invoice_generate_by'] == Configure::read('invoice_generate_by.Quick-Books')) {
                $invoiceGenerateBy = Configure::read('invoice_generate_by.Quick-Books');
            } else {
                $invoiceGenerateBy = Configure::read('invoice_generate_by.MYOB');
            }
            ?>
            <?php
            $disabled = TRUE;
            if (iterator_count($purchaseOrders) > 0) {
                $disabled = FALSE;
            }
            echo $this->Form->button(__('Cancel'), ['type' => 'button', 'id' => 'archive_purchase_orders', 'class' => 'btn green', 'disabled' => $disabled]);
            echo $this->Form->button(__('Set Actual Delivery Date'), ['type' => 'button', 'id' => 'set_actual_delivery_date', 'class' => 'btn green', 'disabled' => $disabled]);
            echo $this->Form->button(__('Print Picking List'), ['type' => 'button', 'id' => 'print_purchase_orders', 'class' => 'btn green', 'disabled' => $disabled]);
            if ($authUser['invoice_generate_by'] == Configure::read('invoice_generate_by.Quick-Books')) {
                echo $this->Form->button(__('Send to ' . $invoiceGenerateBy), ['type' => 'button', 'class' => 'button', 'id' => 'send_to_quick_book', 'class' => 'btn green', 'disabled' => $disabled]);
            } else {
                echo $this->Form->button(__('Send to ' . $invoiceGenerateBy), ['type' => 'button', 'class' => 'button', 'id' => 'send_to_myob', 'class' => 'btn green', 'disabled' => $disabled]);
            }
            if ($authUser['invoice_generate_by'] == Configure::read('invoice_generate_by.MYOB')) {
                echo $this->Html->link(__('POs ready for ' . $invoiceGenerateBy), ['controller' => 'PurchaseOrders', 'action' => 'sent_purchase_orders'], ['class' => 'btn green']);
            }
            echo $this->Html->link(__('Canceled PO\'s'), ['controller' => 'PurchaseOrders', 'action' => 'archived_purchase_orders'], ['class' => 'btn green']);
            ?>
        </div>
    </div>
    <div class="portlet-body">
        <?php
        $sortDir = $this->Paginator->sortDir() === 'asc' ? 'fa-sort-asc sort-active-color' : 'fa-sort-desc sort-active-color';
        $sortKey = $this->Paginator->sortKey();
        ?>
        <div class="alert alert-danger display-hide" id="error-message">
            <span></span>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" id="sample_2">
                <thead>
                    <tr class="action-anch thead-col">
                        <th class="th-5-wd align-center">
                            <?php if (iterator_count($purchaseOrders) > 0) { ?>
                                <span class="md-checkbox">
                                    <?= $this->Form->checkbox('select_deselect', ['id' => 'select_deselect_pourchase_ordes', 'div' => false, 'lable' => false, 'class' => 'md-check', 'hiddenField' => false]); ?>
                                    <label for="select_deselect_pourchase_ordes">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span>
                                    </label>
                                </span>
                            <?php } ?>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('purchase_order_number', 'Purchase Order No') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.purchase_order_number') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('bill_to_gln', 'Bill To GLN') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.bill_to_gln') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('bill_to_name') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.bill_to_name') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('bill_to_address') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.bill_to_address') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('order_date') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.order_date') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th>
                            <?= $this->Paginator->sort('deliver_date') ?>
                            <i class="fa <?php
                            if ($sortKey == 'PurchaseOrders.deliver_date') {
                                echo $sortDir;
                            } else {
                                echo 'fa-sort sort-default-color';
                            }
                            ?> float-right-important"></i>
                        </th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (iterator_count($purchaseOrders) > 0) { ?>
                        <?php foreach ($purchaseOrders as $purchaseOrder): ?>
                            <?php
                            $checked = false;
                            if (in_array($purchaseOrder->id, $selectedIdsArr)) {
                                $checked = true;
                            }
                            ?>
                            <tr>
                                <td class="align-center">
                                    <span class="md-checkbox">
                                        <input type="hidden" class="fulfilment_advice_received_date" value="<?php echo date('Y-m-d', strtotime($purchaseOrder->fulfilment_advice_received_date)); ?>" />
                                        <?= $this->Form->checkbox('purchase_order_id[]', ['id' => 'purchase-order-id-' . $purchaseOrder->id, 'value' => $purchaseOrder->id, 'div' => false, 'lable' => false, 'checked' => $checked, 'class' => 'md-check', 'hiddenField' => false]) ?>
                                        <label for= "purchase-order-id-<?= $purchaseOrder->id ?>">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span>
                                        </label>
                                    </span>
                                </td>
                                <td><?= $purchaseOrder->purchase_order_number ?></td>
                                <td><?= $purchaseOrder->bill_to_gln ?></td>
                                <td><?= h($purchaseOrder->bill_to_name) ?></td>
                                <td><?= h($purchaseOrder->bill_to_address) ?></td>
                                <td><?= date(Configure::read('date-format.default'), strtotime($purchaseOrder->order_date)) ?></td>
                                <td><?= date(Configure::read('date-format.default'), strtotime($purchaseOrder->deliver_date)) ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__(''), ['action' => 'view', $purchaseOrder->id], ['class' => 'glyphicon glyphicon-eye-open link-deco', 'title' => 'View']) ?>
                                    <?php if ($purchaseOrder->actual_delivery_date_updated > 0) { ?>
                                    <br /><span class="bold"><?= date(Configure::read('date-format.d-M-Y'), strtotime($purchaseOrder->actual_delivery_date)); ?></span>
                                    <?php } else { ?>
                                        <?php $actualDeliveryDate = ($purchaseOrder->actual_delivery_date_updated > 0) ? "'" . date('Y-m-d', strtotime($purchaseOrder->actual_delivery_date)) . "'" : "'" . '' . "'"; ?>
                                        <?= $this->Form->button(__('Set Actual Delivery Date'), ['type' => 'button', 'class' => 'btn btn-xs green', 'data-toggle' => 'modal', 'data-dismiss' => 'modal', 'data-target' => '#generate_delivery_docket_info_box', 'onclick' => 'setActualDeliveryDate(' . $purchaseOrder->id . ',' . $actualDeliveryDate . ',' . "'" . $purchaseOrder->purchase_order_number . "'" . ')']) ?>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php } else { ?>
                    <td colspan="8"> <center><?= __('Records not found!!!') ?></center></td>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <?php if (iterator_count($purchaseOrders) > 0) { ?>
            <div class="paginator">
                <ul class="pagination large-10 medium-10 columns">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>
                <p class="large-2 medium-2 columns"><?= $this->Paginator->counter() ?></p>
            </div>
        <?php } ?>
    </div>
</div>
<?= $this->Form->end() ?>

<?php echo $this->Html->script('purchase_orders.js') ?>


<script>

// Auto reload page after some time    
    setTimeout(function () {
        window.location.reload();
    }, <?php echo Configure::read('auto_refresh_page_time'); ?>);

</script>